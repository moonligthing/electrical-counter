entity CommonCode {
  status String required
  name String required
  code String required unique
  description String
  createdBy String
  createdOn Instant
  lastModifiedBy String
  lastModifiedOn Instant
  isDeleted Boolean
}

entity CommonValue {
  name String required
  code String required unique
  description String
  order Long
  dataType String required // [STRING, INT, On]
  dataString String required
  status String
  dataShort String
  createdBy String
  createdOn Instant
  lastModifiedBy String
  lastModifiedOn Instant
  isDeleted Boolean
}

entity Modem {
  modelName String
  protocolTypeName String
  ipAddress String
  port String
  name String
  address String
  location String
  simInfo String
  cronTab String
  createdBy String
  createdOn Instant
  lastModifiedBy String
  lastModifiedOn Instant
  isDeleted Boolean
  flag Boolean
}

entity Meter {
  modemName String
  modelName String
  meterTypeName String
  currentName String
  voltageName String
  installationOn Instant
  warrantyOn Instant
  number String
  name String
  address String
  ctRatio String
  vtRatio String
  deviceAddress String
  status String
  rate1 String // Import, Export
  rate2 String // Import, Export
  rate3 String // Import, Export
  createdBy String
  createdOn Instant
  lastModifiedBy String
  lastModifiedOn Instant
}

entity MeterReading {
  meterName String
  meterNumber String
  meterAddress String
  cycle String
  ctRatio String
  vtRatio String
  totalImportActiveEnergy String
  totalExportActiveEnergy String
  totalImportReactiveEnergy String
  totalExportReactiveEnergy String
  totalImportApparentEnergy String
  totalExportApparentEnergy String
  totalInstantaneousActivePower String
  totalInstantaneousReactivePower String
  totalInstantaneousApparentPower String
  totalPowerFactor String
  status String
  readOn Instant
  importActiveEnergy1 String
  exportActiveEnergy1 String
  importReactiveEnergy1 String
  exportReactiveEnergy1 String
  importApparentEnergy1 String
  exportApparentEnergy1 String
  importActiveEnergy2 String
  exportActiveEnergy2 String
  importReactiveEnergy2 String
  exportReactiveEnergy2 String
  importApparentEnergy2 String
  exportApparentEnergy2 String
  importActiveEnergy3 String
  exportActiveEnergy3 String
  importReactiveEnergy3 String
  exportReactiveEnergy3 String
  importApparentEnergy3 String
  exportApparentEnergy3 String
  voltageA String
  currentA String
  activePowerA String
  reactivePowerA String
  powerFactorA String
  voltageB String
  currentB String
  activePowerB String
  reactivePowerB String
  powerFactorB String
  voltageC String
  currentC String
  activePowerC String
  reactivePowerC String
  powerFactorC String
  createdBy String
  createdOn Instant
  lastModifiedBy String
  lastModifiedOn Instant
}


relationship ManyToOne {
  CommonValue{parent(code)} to CommonCode
  Meter{modem} to Modem
  Meter{meterType} to CommonValue
  Meter{current} to CommonValue
  Meter{voltage} to CommonValue
  Meter{model} to CommonValue
  MeterReading{meter} to Meter
  Modem{model} to CommonValue
  Modem{protocolType} to CommonValue
}

skipClient *
paginate * with infinite-scroll
filter *
dto all with mapstruct