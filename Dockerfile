# Use an official Java runtime as a parent image
FROM eclipse-temurin:11-jre-focal

# Set the working directory in the container
WORKDIR /app

# Copy the application's JAR file to the container
COPY target/e-counter-0.0.1-SNAPSHOT.jar app.jar

# Expose port 8081 to the outside world
EXPOSE 8081

# Run the JAR file
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app/app.jar"]
