package mn.electrical.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import mn.electrical.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MeterReadingDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MeterReadingDTO.class);
        MeterReadingDTO meterReadingDTO1 = new MeterReadingDTO();
        meterReadingDTO1.setId(1L);
        MeterReadingDTO meterReadingDTO2 = new MeterReadingDTO();
        assertThat(meterReadingDTO1).isNotEqualTo(meterReadingDTO2);
        meterReadingDTO2.setId(meterReadingDTO1.getId());
        assertThat(meterReadingDTO1).isEqualTo(meterReadingDTO2);
        meterReadingDTO2.setId(2L);
        assertThat(meterReadingDTO1).isNotEqualTo(meterReadingDTO2);
        meterReadingDTO1.setId(null);
        assertThat(meterReadingDTO1).isNotEqualTo(meterReadingDTO2);
    }
}
