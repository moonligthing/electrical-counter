package mn.electrical.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import mn.electrical.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CommonValueDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CommonValueDTO.class);
        CommonValueDTO commonValueDTO1 = new CommonValueDTO();
        commonValueDTO1.setId(1L);
        CommonValueDTO commonValueDTO2 = new CommonValueDTO();
        assertThat(commonValueDTO1).isNotEqualTo(commonValueDTO2);
        commonValueDTO2.setId(commonValueDTO1.getId());
        assertThat(commonValueDTO1).isEqualTo(commonValueDTO2);
        commonValueDTO2.setId(2L);
        assertThat(commonValueDTO1).isNotEqualTo(commonValueDTO2);
        commonValueDTO1.setId(null);
        assertThat(commonValueDTO1).isNotEqualTo(commonValueDTO2);
    }
}
