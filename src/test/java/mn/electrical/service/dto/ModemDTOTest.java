package mn.electrical.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import mn.electrical.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ModemDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModemDTO.class);
        ModemDTO modemDTO1 = new ModemDTO();
        modemDTO1.setId(1L);
        ModemDTO modemDTO2 = new ModemDTO();
        assertThat(modemDTO1).isNotEqualTo(modemDTO2);
        modemDTO2.setId(modemDTO1.getId());
        assertThat(modemDTO1).isEqualTo(modemDTO2);
        modemDTO2.setId(2L);
        assertThat(modemDTO1).isNotEqualTo(modemDTO2);
        modemDTO1.setId(null);
        assertThat(modemDTO1).isNotEqualTo(modemDTO2);
    }
}
