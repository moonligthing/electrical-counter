package mn.electrical.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import mn.electrical.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CommonCodeDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CommonCodeDTO.class);
        CommonCodeDTO commonCodeDTO1 = new CommonCodeDTO();
        commonCodeDTO1.setId(1L);
        CommonCodeDTO commonCodeDTO2 = new CommonCodeDTO();
        assertThat(commonCodeDTO1).isNotEqualTo(commonCodeDTO2);
        commonCodeDTO2.setId(commonCodeDTO1.getId());
        assertThat(commonCodeDTO1).isEqualTo(commonCodeDTO2);
        commonCodeDTO2.setId(2L);
        assertThat(commonCodeDTO1).isNotEqualTo(commonCodeDTO2);
        commonCodeDTO1.setId(null);
        assertThat(commonCodeDTO1).isNotEqualTo(commonCodeDTO2);
    }
}
