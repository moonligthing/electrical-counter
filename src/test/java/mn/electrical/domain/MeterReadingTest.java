package mn.electrical.domain;

import static org.assertj.core.api.Assertions.assertThat;

import mn.electrical.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MeterReadingTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MeterReading.class);
        MeterReading meterReading1 = new MeterReading();
        meterReading1.setId(1L);
        MeterReading meterReading2 = new MeterReading();
        meterReading2.setId(meterReading1.getId());
        assertThat(meterReading1).isEqualTo(meterReading2);
        meterReading2.setId(2L);
        assertThat(meterReading1).isNotEqualTo(meterReading2);
        meterReading1.setId(null);
        assertThat(meterReading1).isNotEqualTo(meterReading2);
    }
}
