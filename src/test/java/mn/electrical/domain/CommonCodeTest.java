package mn.electrical.domain;

import static org.assertj.core.api.Assertions.assertThat;

import mn.electrical.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CommonCodeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CommonCode.class);
        CommonCode commonCode1 = new CommonCode();
        commonCode1.setId(1L);
        CommonCode commonCode2 = new CommonCode();
        commonCode2.setId(commonCode1.getId());
        assertThat(commonCode1).isEqualTo(commonCode2);
        commonCode2.setId(2L);
        assertThat(commonCode1).isNotEqualTo(commonCode2);
        commonCode1.setId(null);
        assertThat(commonCode1).isNotEqualTo(commonCode2);
    }
}
