package mn.electrical.domain;

import static org.assertj.core.api.Assertions.assertThat;

import mn.electrical.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CommonValueTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CommonValue.class);
        CommonValue commonValue1 = new CommonValue();
        commonValue1.setId(1L);
        CommonValue commonValue2 = new CommonValue();
        commonValue2.setId(commonValue1.getId());
        assertThat(commonValue1).isEqualTo(commonValue2);
        commonValue2.setId(2L);
        assertThat(commonValue1).isNotEqualTo(commonValue2);
        commonValue1.setId(null);
        assertThat(commonValue1).isNotEqualTo(commonValue2);
    }
}
