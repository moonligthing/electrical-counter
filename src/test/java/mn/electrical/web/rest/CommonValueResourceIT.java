package mn.electrical.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import mn.electrical.IntegrationTest;
import mn.electrical.domain.CommonCode;
import mn.electrical.domain.CommonValue;
import mn.electrical.repository.CommonValueRepository;
import mn.electrical.service.criteria.CommonValueCriteria;
import mn.electrical.service.dto.CommonValueDTO;
import mn.electrical.service.mapper.CommonValueMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CommonValueResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CommonValueResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Long DEFAULT_ORDER = 1L;
    private static final Long UPDATED_ORDER = 2L;
    private static final Long SMALLER_ORDER = 1L - 1L;

    private static final String DEFAULT_DATA_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_DATA_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_DATA_STRING = "AAAAAAAAAA";
    private static final String UPDATED_DATA_STRING = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_DATA_SHORT = "AAAAAAAAAA";
    private static final String UPDATED_DATA_SHORT = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_IS_DELETED = false;
    private static final Boolean UPDATED_IS_DELETED = true;

    private static final String ENTITY_API_URL = "/api/common-values";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CommonValueRepository commonValueRepository;

    @Autowired
    private CommonValueMapper commonValueMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCommonValueMockMvc;

    private CommonValue commonValue;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CommonValue createEntity(EntityManager em) {
        CommonValue commonValue = new CommonValue()
            .name(DEFAULT_NAME)
            .code(DEFAULT_CODE)
            .description(DEFAULT_DESCRIPTION)
            .order(DEFAULT_ORDER)
            .dataType(DEFAULT_DATA_TYPE)
            .dataString(DEFAULT_DATA_STRING)
            .status(DEFAULT_STATUS)
            .dataShort(DEFAULT_DATA_SHORT)
            .createdBy(DEFAULT_CREATED_BY)
            .createdOn(DEFAULT_CREATED_ON)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedOn(DEFAULT_LAST_MODIFIED_ON)
            .isDeleted(DEFAULT_IS_DELETED);
        return commonValue;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CommonValue createUpdatedEntity(EntityManager em) {
        CommonValue commonValue = new CommonValue()
            .name(UPDATED_NAME)
            .code(UPDATED_CODE)
            .description(UPDATED_DESCRIPTION)
            .order(UPDATED_ORDER)
            .dataType(UPDATED_DATA_TYPE)
            .dataString(UPDATED_DATA_STRING)
            .status(UPDATED_STATUS)
            .dataShort(UPDATED_DATA_SHORT)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedOn(UPDATED_LAST_MODIFIED_ON)
            .isDeleted(UPDATED_IS_DELETED);
        return commonValue;
    }

    @BeforeEach
    public void initTest() {
        commonValue = createEntity(em);
    }

    @Test
    @Transactional
    void createCommonValue() throws Exception {
        int databaseSizeBeforeCreate = commonValueRepository.findAll().size();
        // Create the CommonValue
        CommonValueDTO commonValueDTO = commonValueMapper.toDto(commonValue);
        restCommonValueMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commonValueDTO))
            )
            .andExpect(status().isCreated());

        // Validate the CommonValue in the database
        List<CommonValue> commonValueList = commonValueRepository.findAll();
        assertThat(commonValueList).hasSize(databaseSizeBeforeCreate + 1);
        CommonValue testCommonValue = commonValueList.get(commonValueList.size() - 1);
        assertThat(testCommonValue.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCommonValue.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testCommonValue.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testCommonValue.getOrder()).isEqualTo(DEFAULT_ORDER);
        assertThat(testCommonValue.getDataType()).isEqualTo(DEFAULT_DATA_TYPE);
        assertThat(testCommonValue.getDataString()).isEqualTo(DEFAULT_DATA_STRING);
        assertThat(testCommonValue.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testCommonValue.getDataShort()).isEqualTo(DEFAULT_DATA_SHORT);
        assertThat(testCommonValue.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testCommonValue.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testCommonValue.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testCommonValue.getLastModifiedOn()).isEqualTo(DEFAULT_LAST_MODIFIED_ON);
        assertThat(testCommonValue.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    void createCommonValueWithExistingId() throws Exception {
        // Create the CommonValue with an existing ID
        commonValue.setId(1L);
        CommonValueDTO commonValueDTO = commonValueMapper.toDto(commonValue);

        int databaseSizeBeforeCreate = commonValueRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCommonValueMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commonValueDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommonValue in the database
        List<CommonValue> commonValueList = commonValueRepository.findAll();
        assertThat(commonValueList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = commonValueRepository.findAll().size();
        // set the field null
        commonValue.setName(null);

        // Create the CommonValue, which fails.
        CommonValueDTO commonValueDTO = commonValueMapper.toDto(commonValue);

        restCommonValueMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commonValueDTO))
            )
            .andExpect(status().isBadRequest());

        List<CommonValue> commonValueList = commonValueRepository.findAll();
        assertThat(commonValueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = commonValueRepository.findAll().size();
        // set the field null
        commonValue.setCode(null);

        // Create the CommonValue, which fails.
        CommonValueDTO commonValueDTO = commonValueMapper.toDto(commonValue);

        restCommonValueMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commonValueDTO))
            )
            .andExpect(status().isBadRequest());

        List<CommonValue> commonValueList = commonValueRepository.findAll();
        assertThat(commonValueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkDataTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = commonValueRepository.findAll().size();
        // set the field null
        commonValue.setDataType(null);

        // Create the CommonValue, which fails.
        CommonValueDTO commonValueDTO = commonValueMapper.toDto(commonValue);

        restCommonValueMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commonValueDTO))
            )
            .andExpect(status().isBadRequest());

        List<CommonValue> commonValueList = commonValueRepository.findAll();
        assertThat(commonValueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkDataStringIsRequired() throws Exception {
        int databaseSizeBeforeTest = commonValueRepository.findAll().size();
        // set the field null
        commonValue.setDataString(null);

        // Create the CommonValue, which fails.
        CommonValueDTO commonValueDTO = commonValueMapper.toDto(commonValue);

        restCommonValueMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commonValueDTO))
            )
            .andExpect(status().isBadRequest());

        List<CommonValue> commonValueList = commonValueRepository.findAll();
        assertThat(commonValueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCommonValues() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList
        restCommonValueMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(commonValue.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER.intValue())))
            .andExpect(jsonPath("$.[*].dataType").value(hasItem(DEFAULT_DATA_TYPE)))
            .andExpect(jsonPath("$.[*].dataString").value(hasItem(DEFAULT_DATA_STRING)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].dataShort").value(hasItem(DEFAULT_DATA_SHORT)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedOn").value(hasItem(DEFAULT_LAST_MODIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    void getCommonValue() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get the commonValue
        restCommonValueMockMvc
            .perform(get(ENTITY_API_URL_ID, commonValue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(commonValue.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.order").value(DEFAULT_ORDER.intValue()))
            .andExpect(jsonPath("$.dataType").value(DEFAULT_DATA_TYPE))
            .andExpect(jsonPath("$.dataString").value(DEFAULT_DATA_STRING))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.dataShort").value(DEFAULT_DATA_SHORT))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedOn").value(DEFAULT_LAST_MODIFIED_ON.toString()))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    void getCommonValuesByIdFiltering() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        Long id = commonValue.getId();

        defaultCommonValueShouldBeFound("id.equals=" + id);
        defaultCommonValueShouldNotBeFound("id.notEquals=" + id);

        defaultCommonValueShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCommonValueShouldNotBeFound("id.greaterThan=" + id);

        defaultCommonValueShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCommonValueShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllCommonValuesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where name equals to DEFAULT_NAME
        defaultCommonValueShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the commonValueList where name equals to UPDATED_NAME
        defaultCommonValueShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCommonValuesByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where name not equals to DEFAULT_NAME
        defaultCommonValueShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the commonValueList where name not equals to UPDATED_NAME
        defaultCommonValueShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCommonValuesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where name in DEFAULT_NAME or UPDATED_NAME
        defaultCommonValueShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the commonValueList where name equals to UPDATED_NAME
        defaultCommonValueShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCommonValuesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where name is not null
        defaultCommonValueShouldBeFound("name.specified=true");

        // Get all the commonValueList where name is null
        defaultCommonValueShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonValuesByNameContainsSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where name contains DEFAULT_NAME
        defaultCommonValueShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the commonValueList where name contains UPDATED_NAME
        defaultCommonValueShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCommonValuesByNameNotContainsSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where name does not contain DEFAULT_NAME
        defaultCommonValueShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the commonValueList where name does not contain UPDATED_NAME
        defaultCommonValueShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCommonValuesByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where code equals to DEFAULT_CODE
        defaultCommonValueShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the commonValueList where code equals to UPDATED_CODE
        defaultCommonValueShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllCommonValuesByCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where code not equals to DEFAULT_CODE
        defaultCommonValueShouldNotBeFound("code.notEquals=" + DEFAULT_CODE);

        // Get all the commonValueList where code not equals to UPDATED_CODE
        defaultCommonValueShouldBeFound("code.notEquals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllCommonValuesByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where code in DEFAULT_CODE or UPDATED_CODE
        defaultCommonValueShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the commonValueList where code equals to UPDATED_CODE
        defaultCommonValueShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllCommonValuesByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where code is not null
        defaultCommonValueShouldBeFound("code.specified=true");

        // Get all the commonValueList where code is null
        defaultCommonValueShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonValuesByCodeContainsSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where code contains DEFAULT_CODE
        defaultCommonValueShouldBeFound("code.contains=" + DEFAULT_CODE);

        // Get all the commonValueList where code contains UPDATED_CODE
        defaultCommonValueShouldNotBeFound("code.contains=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllCommonValuesByCodeNotContainsSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where code does not contain DEFAULT_CODE
        defaultCommonValueShouldNotBeFound("code.doesNotContain=" + DEFAULT_CODE);

        // Get all the commonValueList where code does not contain UPDATED_CODE
        defaultCommonValueShouldBeFound("code.doesNotContain=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllCommonValuesByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where description equals to DEFAULT_DESCRIPTION
        defaultCommonValueShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the commonValueList where description equals to UPDATED_DESCRIPTION
        defaultCommonValueShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllCommonValuesByDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where description not equals to DEFAULT_DESCRIPTION
        defaultCommonValueShouldNotBeFound("description.notEquals=" + DEFAULT_DESCRIPTION);

        // Get all the commonValueList where description not equals to UPDATED_DESCRIPTION
        defaultCommonValueShouldBeFound("description.notEquals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllCommonValuesByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultCommonValueShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the commonValueList where description equals to UPDATED_DESCRIPTION
        defaultCommonValueShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllCommonValuesByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where description is not null
        defaultCommonValueShouldBeFound("description.specified=true");

        // Get all the commonValueList where description is null
        defaultCommonValueShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonValuesByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where description contains DEFAULT_DESCRIPTION
        defaultCommonValueShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the commonValueList where description contains UPDATED_DESCRIPTION
        defaultCommonValueShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllCommonValuesByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where description does not contain DEFAULT_DESCRIPTION
        defaultCommonValueShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the commonValueList where description does not contain UPDATED_DESCRIPTION
        defaultCommonValueShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllCommonValuesByOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where order equals to DEFAULT_ORDER
        defaultCommonValueShouldBeFound("order.equals=" + DEFAULT_ORDER);

        // Get all the commonValueList where order equals to UPDATED_ORDER
        defaultCommonValueShouldNotBeFound("order.equals=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    void getAllCommonValuesByOrderIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where order not equals to DEFAULT_ORDER
        defaultCommonValueShouldNotBeFound("order.notEquals=" + DEFAULT_ORDER);

        // Get all the commonValueList where order not equals to UPDATED_ORDER
        defaultCommonValueShouldBeFound("order.notEquals=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    void getAllCommonValuesByOrderIsInShouldWork() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where order in DEFAULT_ORDER or UPDATED_ORDER
        defaultCommonValueShouldBeFound("order.in=" + DEFAULT_ORDER + "," + UPDATED_ORDER);

        // Get all the commonValueList where order equals to UPDATED_ORDER
        defaultCommonValueShouldNotBeFound("order.in=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    void getAllCommonValuesByOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where order is not null
        defaultCommonValueShouldBeFound("order.specified=true");

        // Get all the commonValueList where order is null
        defaultCommonValueShouldNotBeFound("order.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonValuesByOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where order is greater than or equal to DEFAULT_ORDER
        defaultCommonValueShouldBeFound("order.greaterThanOrEqual=" + DEFAULT_ORDER);

        // Get all the commonValueList where order is greater than or equal to UPDATED_ORDER
        defaultCommonValueShouldNotBeFound("order.greaterThanOrEqual=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    void getAllCommonValuesByOrderIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where order is less than or equal to DEFAULT_ORDER
        defaultCommonValueShouldBeFound("order.lessThanOrEqual=" + DEFAULT_ORDER);

        // Get all the commonValueList where order is less than or equal to SMALLER_ORDER
        defaultCommonValueShouldNotBeFound("order.lessThanOrEqual=" + SMALLER_ORDER);
    }

    @Test
    @Transactional
    void getAllCommonValuesByOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where order is less than DEFAULT_ORDER
        defaultCommonValueShouldNotBeFound("order.lessThan=" + DEFAULT_ORDER);

        // Get all the commonValueList where order is less than UPDATED_ORDER
        defaultCommonValueShouldBeFound("order.lessThan=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    void getAllCommonValuesByOrderIsGreaterThanSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where order is greater than DEFAULT_ORDER
        defaultCommonValueShouldNotBeFound("order.greaterThan=" + DEFAULT_ORDER);

        // Get all the commonValueList where order is greater than SMALLER_ORDER
        defaultCommonValueShouldBeFound("order.greaterThan=" + SMALLER_ORDER);
    }

    @Test
    @Transactional
    void getAllCommonValuesByDataTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where dataType equals to DEFAULT_DATA_TYPE
        defaultCommonValueShouldBeFound("dataType.equals=" + DEFAULT_DATA_TYPE);

        // Get all the commonValueList where dataType equals to UPDATED_DATA_TYPE
        defaultCommonValueShouldNotBeFound("dataType.equals=" + UPDATED_DATA_TYPE);
    }

    @Test
    @Transactional
    void getAllCommonValuesByDataTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where dataType not equals to DEFAULT_DATA_TYPE
        defaultCommonValueShouldNotBeFound("dataType.notEquals=" + DEFAULT_DATA_TYPE);

        // Get all the commonValueList where dataType not equals to UPDATED_DATA_TYPE
        defaultCommonValueShouldBeFound("dataType.notEquals=" + UPDATED_DATA_TYPE);
    }

    @Test
    @Transactional
    void getAllCommonValuesByDataTypeIsInShouldWork() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where dataType in DEFAULT_DATA_TYPE or UPDATED_DATA_TYPE
        defaultCommonValueShouldBeFound("dataType.in=" + DEFAULT_DATA_TYPE + "," + UPDATED_DATA_TYPE);

        // Get all the commonValueList where dataType equals to UPDATED_DATA_TYPE
        defaultCommonValueShouldNotBeFound("dataType.in=" + UPDATED_DATA_TYPE);
    }

    @Test
    @Transactional
    void getAllCommonValuesByDataTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where dataType is not null
        defaultCommonValueShouldBeFound("dataType.specified=true");

        // Get all the commonValueList where dataType is null
        defaultCommonValueShouldNotBeFound("dataType.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonValuesByDataTypeContainsSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where dataType contains DEFAULT_DATA_TYPE
        defaultCommonValueShouldBeFound("dataType.contains=" + DEFAULT_DATA_TYPE);

        // Get all the commonValueList where dataType contains UPDATED_DATA_TYPE
        defaultCommonValueShouldNotBeFound("dataType.contains=" + UPDATED_DATA_TYPE);
    }

    @Test
    @Transactional
    void getAllCommonValuesByDataTypeNotContainsSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where dataType does not contain DEFAULT_DATA_TYPE
        defaultCommonValueShouldNotBeFound("dataType.doesNotContain=" + DEFAULT_DATA_TYPE);

        // Get all the commonValueList where dataType does not contain UPDATED_DATA_TYPE
        defaultCommonValueShouldBeFound("dataType.doesNotContain=" + UPDATED_DATA_TYPE);
    }

    @Test
    @Transactional
    void getAllCommonValuesByDataStringIsEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where dataString equals to DEFAULT_DATA_STRING
        defaultCommonValueShouldBeFound("dataString.equals=" + DEFAULT_DATA_STRING);

        // Get all the commonValueList where dataString equals to UPDATED_DATA_STRING
        defaultCommonValueShouldNotBeFound("dataString.equals=" + UPDATED_DATA_STRING);
    }

    @Test
    @Transactional
    void getAllCommonValuesByDataStringIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where dataString not equals to DEFAULT_DATA_STRING
        defaultCommonValueShouldNotBeFound("dataString.notEquals=" + DEFAULT_DATA_STRING);

        // Get all the commonValueList where dataString not equals to UPDATED_DATA_STRING
        defaultCommonValueShouldBeFound("dataString.notEquals=" + UPDATED_DATA_STRING);
    }

    @Test
    @Transactional
    void getAllCommonValuesByDataStringIsInShouldWork() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where dataString in DEFAULT_DATA_STRING or UPDATED_DATA_STRING
        defaultCommonValueShouldBeFound("dataString.in=" + DEFAULT_DATA_STRING + "," + UPDATED_DATA_STRING);

        // Get all the commonValueList where dataString equals to UPDATED_DATA_STRING
        defaultCommonValueShouldNotBeFound("dataString.in=" + UPDATED_DATA_STRING);
    }

    @Test
    @Transactional
    void getAllCommonValuesByDataStringIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where dataString is not null
        defaultCommonValueShouldBeFound("dataString.specified=true");

        // Get all the commonValueList where dataString is null
        defaultCommonValueShouldNotBeFound("dataString.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonValuesByDataStringContainsSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where dataString contains DEFAULT_DATA_STRING
        defaultCommonValueShouldBeFound("dataString.contains=" + DEFAULT_DATA_STRING);

        // Get all the commonValueList where dataString contains UPDATED_DATA_STRING
        defaultCommonValueShouldNotBeFound("dataString.contains=" + UPDATED_DATA_STRING);
    }

    @Test
    @Transactional
    void getAllCommonValuesByDataStringNotContainsSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where dataString does not contain DEFAULT_DATA_STRING
        defaultCommonValueShouldNotBeFound("dataString.doesNotContain=" + DEFAULT_DATA_STRING);

        // Get all the commonValueList where dataString does not contain UPDATED_DATA_STRING
        defaultCommonValueShouldBeFound("dataString.doesNotContain=" + UPDATED_DATA_STRING);
    }

    @Test
    @Transactional
    void getAllCommonValuesByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where status equals to DEFAULT_STATUS
        defaultCommonValueShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the commonValueList where status equals to UPDATED_STATUS
        defaultCommonValueShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllCommonValuesByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where status not equals to DEFAULT_STATUS
        defaultCommonValueShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the commonValueList where status not equals to UPDATED_STATUS
        defaultCommonValueShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllCommonValuesByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultCommonValueShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the commonValueList where status equals to UPDATED_STATUS
        defaultCommonValueShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllCommonValuesByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where status is not null
        defaultCommonValueShouldBeFound("status.specified=true");

        // Get all the commonValueList where status is null
        defaultCommonValueShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonValuesByStatusContainsSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where status contains DEFAULT_STATUS
        defaultCommonValueShouldBeFound("status.contains=" + DEFAULT_STATUS);

        // Get all the commonValueList where status contains UPDATED_STATUS
        defaultCommonValueShouldNotBeFound("status.contains=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllCommonValuesByStatusNotContainsSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where status does not contain DEFAULT_STATUS
        defaultCommonValueShouldNotBeFound("status.doesNotContain=" + DEFAULT_STATUS);

        // Get all the commonValueList where status does not contain UPDATED_STATUS
        defaultCommonValueShouldBeFound("status.doesNotContain=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllCommonValuesByDataShortIsEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where dataShort equals to DEFAULT_DATA_SHORT
        defaultCommonValueShouldBeFound("dataShort.equals=" + DEFAULT_DATA_SHORT);

        // Get all the commonValueList where dataShort equals to UPDATED_DATA_SHORT
        defaultCommonValueShouldNotBeFound("dataShort.equals=" + UPDATED_DATA_SHORT);
    }

    @Test
    @Transactional
    void getAllCommonValuesByDataShortIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where dataShort not equals to DEFAULT_DATA_SHORT
        defaultCommonValueShouldNotBeFound("dataShort.notEquals=" + DEFAULT_DATA_SHORT);

        // Get all the commonValueList where dataShort not equals to UPDATED_DATA_SHORT
        defaultCommonValueShouldBeFound("dataShort.notEquals=" + UPDATED_DATA_SHORT);
    }

    @Test
    @Transactional
    void getAllCommonValuesByDataShortIsInShouldWork() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where dataShort in DEFAULT_DATA_SHORT or UPDATED_DATA_SHORT
        defaultCommonValueShouldBeFound("dataShort.in=" + DEFAULT_DATA_SHORT + "," + UPDATED_DATA_SHORT);

        // Get all the commonValueList where dataShort equals to UPDATED_DATA_SHORT
        defaultCommonValueShouldNotBeFound("dataShort.in=" + UPDATED_DATA_SHORT);
    }

    @Test
    @Transactional
    void getAllCommonValuesByDataShortIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where dataShort is not null
        defaultCommonValueShouldBeFound("dataShort.specified=true");

        // Get all the commonValueList where dataShort is null
        defaultCommonValueShouldNotBeFound("dataShort.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonValuesByDataShortContainsSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where dataShort contains DEFAULT_DATA_SHORT
        defaultCommonValueShouldBeFound("dataShort.contains=" + DEFAULT_DATA_SHORT);

        // Get all the commonValueList where dataShort contains UPDATED_DATA_SHORT
        defaultCommonValueShouldNotBeFound("dataShort.contains=" + UPDATED_DATA_SHORT);
    }

    @Test
    @Transactional
    void getAllCommonValuesByDataShortNotContainsSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where dataShort does not contain DEFAULT_DATA_SHORT
        defaultCommonValueShouldNotBeFound("dataShort.doesNotContain=" + DEFAULT_DATA_SHORT);

        // Get all the commonValueList where dataShort does not contain UPDATED_DATA_SHORT
        defaultCommonValueShouldBeFound("dataShort.doesNotContain=" + UPDATED_DATA_SHORT);
    }

    @Test
    @Transactional
    void getAllCommonValuesByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where createdBy equals to DEFAULT_CREATED_BY
        defaultCommonValueShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the commonValueList where createdBy equals to UPDATED_CREATED_BY
        defaultCommonValueShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCommonValuesByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where createdBy not equals to DEFAULT_CREATED_BY
        defaultCommonValueShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the commonValueList where createdBy not equals to UPDATED_CREATED_BY
        defaultCommonValueShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCommonValuesByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultCommonValueShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the commonValueList where createdBy equals to UPDATED_CREATED_BY
        defaultCommonValueShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCommonValuesByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where createdBy is not null
        defaultCommonValueShouldBeFound("createdBy.specified=true");

        // Get all the commonValueList where createdBy is null
        defaultCommonValueShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonValuesByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where createdBy contains DEFAULT_CREATED_BY
        defaultCommonValueShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the commonValueList where createdBy contains UPDATED_CREATED_BY
        defaultCommonValueShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCommonValuesByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where createdBy does not contain DEFAULT_CREATED_BY
        defaultCommonValueShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the commonValueList where createdBy does not contain UPDATED_CREATED_BY
        defaultCommonValueShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCommonValuesByCreatedOnIsEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where createdOn equals to DEFAULT_CREATED_ON
        defaultCommonValueShouldBeFound("createdOn.equals=" + DEFAULT_CREATED_ON);

        // Get all the commonValueList where createdOn equals to UPDATED_CREATED_ON
        defaultCommonValueShouldNotBeFound("createdOn.equals=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    void getAllCommonValuesByCreatedOnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where createdOn not equals to DEFAULT_CREATED_ON
        defaultCommonValueShouldNotBeFound("createdOn.notEquals=" + DEFAULT_CREATED_ON);

        // Get all the commonValueList where createdOn not equals to UPDATED_CREATED_ON
        defaultCommonValueShouldBeFound("createdOn.notEquals=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    void getAllCommonValuesByCreatedOnIsInShouldWork() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where createdOn in DEFAULT_CREATED_ON or UPDATED_CREATED_ON
        defaultCommonValueShouldBeFound("createdOn.in=" + DEFAULT_CREATED_ON + "," + UPDATED_CREATED_ON);

        // Get all the commonValueList where createdOn equals to UPDATED_CREATED_ON
        defaultCommonValueShouldNotBeFound("createdOn.in=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    void getAllCommonValuesByCreatedOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where createdOn is not null
        defaultCommonValueShouldBeFound("createdOn.specified=true");

        // Get all the commonValueList where createdOn is null
        defaultCommonValueShouldNotBeFound("createdOn.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonValuesByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultCommonValueShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the commonValueList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCommonValueShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllCommonValuesByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultCommonValueShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the commonValueList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultCommonValueShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllCommonValuesByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultCommonValueShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the commonValueList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCommonValueShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllCommonValuesByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where lastModifiedBy is not null
        defaultCommonValueShouldBeFound("lastModifiedBy.specified=true");

        // Get all the commonValueList where lastModifiedBy is null
        defaultCommonValueShouldNotBeFound("lastModifiedBy.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonValuesByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultCommonValueShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the commonValueList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultCommonValueShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllCommonValuesByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultCommonValueShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the commonValueList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultCommonValueShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllCommonValuesByLastModifiedOnIsEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where lastModifiedOn equals to DEFAULT_LAST_MODIFIED_ON
        defaultCommonValueShouldBeFound("lastModifiedOn.equals=" + DEFAULT_LAST_MODIFIED_ON);

        // Get all the commonValueList where lastModifiedOn equals to UPDATED_LAST_MODIFIED_ON
        defaultCommonValueShouldNotBeFound("lastModifiedOn.equals=" + UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void getAllCommonValuesByLastModifiedOnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where lastModifiedOn not equals to DEFAULT_LAST_MODIFIED_ON
        defaultCommonValueShouldNotBeFound("lastModifiedOn.notEquals=" + DEFAULT_LAST_MODIFIED_ON);

        // Get all the commonValueList where lastModifiedOn not equals to UPDATED_LAST_MODIFIED_ON
        defaultCommonValueShouldBeFound("lastModifiedOn.notEquals=" + UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void getAllCommonValuesByLastModifiedOnIsInShouldWork() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where lastModifiedOn in DEFAULT_LAST_MODIFIED_ON or UPDATED_LAST_MODIFIED_ON
        defaultCommonValueShouldBeFound("lastModifiedOn.in=" + DEFAULT_LAST_MODIFIED_ON + "," + UPDATED_LAST_MODIFIED_ON);

        // Get all the commonValueList where lastModifiedOn equals to UPDATED_LAST_MODIFIED_ON
        defaultCommonValueShouldNotBeFound("lastModifiedOn.in=" + UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void getAllCommonValuesByLastModifiedOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where lastModifiedOn is not null
        defaultCommonValueShouldBeFound("lastModifiedOn.specified=true");

        // Get all the commonValueList where lastModifiedOn is null
        defaultCommonValueShouldNotBeFound("lastModifiedOn.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonValuesByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where isDeleted equals to DEFAULT_IS_DELETED
        defaultCommonValueShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the commonValueList where isDeleted equals to UPDATED_IS_DELETED
        defaultCommonValueShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    void getAllCommonValuesByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultCommonValueShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the commonValueList where isDeleted not equals to UPDATED_IS_DELETED
        defaultCommonValueShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    void getAllCommonValuesByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultCommonValueShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the commonValueList where isDeleted equals to UPDATED_IS_DELETED
        defaultCommonValueShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    void getAllCommonValuesByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        // Get all the commonValueList where isDeleted is not null
        defaultCommonValueShouldBeFound("isDeleted.specified=true");

        // Get all the commonValueList where isDeleted is null
        defaultCommonValueShouldNotBeFound("isDeleted.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonValuesByParentIsEqualToSomething() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);
        CommonCode parent;
        if (TestUtil.findAll(em, CommonCode.class).isEmpty()) {
            parent = CommonCodeResourceIT.createEntity(em);
            em.persist(parent);
            em.flush();
        } else {
            parent = TestUtil.findAll(em, CommonCode.class).get(0);
        }
        em.persist(parent);
        em.flush();
        commonValue.setParent(parent);
        commonValueRepository.saveAndFlush(commonValue);
        Long parentId = parent.getId();

        // Get all the commonValueList where parent equals to parentId
        defaultCommonValueShouldBeFound("parentId.equals=" + parentId);

        // Get all the commonValueList where parent equals to (parentId + 1)
        defaultCommonValueShouldNotBeFound("parentId.equals=" + (parentId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCommonValueShouldBeFound(String filter) throws Exception {
        restCommonValueMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(commonValue.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER.intValue())))
            .andExpect(jsonPath("$.[*].dataType").value(hasItem(DEFAULT_DATA_TYPE)))
            .andExpect(jsonPath("$.[*].dataString").value(hasItem(DEFAULT_DATA_STRING)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].dataShort").value(hasItem(DEFAULT_DATA_SHORT)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedOn").value(hasItem(DEFAULT_LAST_MODIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED.booleanValue())));

        // Check, that the count call also returns 1
        restCommonValueMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCommonValueShouldNotBeFound(String filter) throws Exception {
        restCommonValueMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCommonValueMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingCommonValue() throws Exception {
        // Get the commonValue
        restCommonValueMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCommonValue() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        int databaseSizeBeforeUpdate = commonValueRepository.findAll().size();

        // Update the commonValue
        CommonValue updatedCommonValue = commonValueRepository.findById(commonValue.getId()).get();
        // Disconnect from session so that the updates on updatedCommonValue are not directly saved in db
        em.detach(updatedCommonValue);
        updatedCommonValue
            .name(UPDATED_NAME)
            .code(UPDATED_CODE)
            .description(UPDATED_DESCRIPTION)
            .order(UPDATED_ORDER)
            .dataType(UPDATED_DATA_TYPE)
            .dataString(UPDATED_DATA_STRING)
            .status(UPDATED_STATUS)
            .dataShort(UPDATED_DATA_SHORT)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedOn(UPDATED_LAST_MODIFIED_ON)
            .isDeleted(UPDATED_IS_DELETED);
        CommonValueDTO commonValueDTO = commonValueMapper.toDto(updatedCommonValue);

        restCommonValueMockMvc
            .perform(
                put(ENTITY_API_URL_ID, commonValueDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commonValueDTO))
            )
            .andExpect(status().isOk());

        // Validate the CommonValue in the database
        List<CommonValue> commonValueList = commonValueRepository.findAll();
        assertThat(commonValueList).hasSize(databaseSizeBeforeUpdate);
        CommonValue testCommonValue = commonValueList.get(commonValueList.size() - 1);
        assertThat(testCommonValue.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCommonValue.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCommonValue.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testCommonValue.getOrder()).isEqualTo(UPDATED_ORDER);
        assertThat(testCommonValue.getDataType()).isEqualTo(UPDATED_DATA_TYPE);
        assertThat(testCommonValue.getDataString()).isEqualTo(UPDATED_DATA_STRING);
        assertThat(testCommonValue.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCommonValue.getDataShort()).isEqualTo(UPDATED_DATA_SHORT);
        assertThat(testCommonValue.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCommonValue.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testCommonValue.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testCommonValue.getLastModifiedOn()).isEqualTo(UPDATED_LAST_MODIFIED_ON);
        assertThat(testCommonValue.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    void putNonExistingCommonValue() throws Exception {
        int databaseSizeBeforeUpdate = commonValueRepository.findAll().size();
        commonValue.setId(count.incrementAndGet());

        // Create the CommonValue
        CommonValueDTO commonValueDTO = commonValueMapper.toDto(commonValue);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCommonValueMockMvc
            .perform(
                put(ENTITY_API_URL_ID, commonValueDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commonValueDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommonValue in the database
        List<CommonValue> commonValueList = commonValueRepository.findAll();
        assertThat(commonValueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCommonValue() throws Exception {
        int databaseSizeBeforeUpdate = commonValueRepository.findAll().size();
        commonValue.setId(count.incrementAndGet());

        // Create the CommonValue
        CommonValueDTO commonValueDTO = commonValueMapper.toDto(commonValue);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommonValueMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commonValueDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommonValue in the database
        List<CommonValue> commonValueList = commonValueRepository.findAll();
        assertThat(commonValueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCommonValue() throws Exception {
        int databaseSizeBeforeUpdate = commonValueRepository.findAll().size();
        commonValue.setId(count.incrementAndGet());

        // Create the CommonValue
        CommonValueDTO commonValueDTO = commonValueMapper.toDto(commonValue);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommonValueMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commonValueDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CommonValue in the database
        List<CommonValue> commonValueList = commonValueRepository.findAll();
        assertThat(commonValueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCommonValueWithPatch() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        int databaseSizeBeforeUpdate = commonValueRepository.findAll().size();

        // Update the commonValue using partial update
        CommonValue partialUpdatedCommonValue = new CommonValue();
        partialUpdatedCommonValue.setId(commonValue.getId());

        partialUpdatedCommonValue
            .name(UPDATED_NAME)
            .code(UPDATED_CODE)
            .order(UPDATED_ORDER)
            .dataType(UPDATED_DATA_TYPE)
            .dataShort(UPDATED_DATA_SHORT)
            .lastModifiedOn(UPDATED_LAST_MODIFIED_ON);

        restCommonValueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCommonValue.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCommonValue))
            )
            .andExpect(status().isOk());

        // Validate the CommonValue in the database
        List<CommonValue> commonValueList = commonValueRepository.findAll();
        assertThat(commonValueList).hasSize(databaseSizeBeforeUpdate);
        CommonValue testCommonValue = commonValueList.get(commonValueList.size() - 1);
        assertThat(testCommonValue.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCommonValue.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCommonValue.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testCommonValue.getOrder()).isEqualTo(UPDATED_ORDER);
        assertThat(testCommonValue.getDataType()).isEqualTo(UPDATED_DATA_TYPE);
        assertThat(testCommonValue.getDataString()).isEqualTo(DEFAULT_DATA_STRING);
        assertThat(testCommonValue.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testCommonValue.getDataShort()).isEqualTo(UPDATED_DATA_SHORT);
        assertThat(testCommonValue.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testCommonValue.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testCommonValue.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testCommonValue.getLastModifiedOn()).isEqualTo(UPDATED_LAST_MODIFIED_ON);
        assertThat(testCommonValue.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    void fullUpdateCommonValueWithPatch() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        int databaseSizeBeforeUpdate = commonValueRepository.findAll().size();

        // Update the commonValue using partial update
        CommonValue partialUpdatedCommonValue = new CommonValue();
        partialUpdatedCommonValue.setId(commonValue.getId());

        partialUpdatedCommonValue
            .name(UPDATED_NAME)
            .code(UPDATED_CODE)
            .description(UPDATED_DESCRIPTION)
            .order(UPDATED_ORDER)
            .dataType(UPDATED_DATA_TYPE)
            .dataString(UPDATED_DATA_STRING)
            .status(UPDATED_STATUS)
            .dataShort(UPDATED_DATA_SHORT)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedOn(UPDATED_LAST_MODIFIED_ON)
            .isDeleted(UPDATED_IS_DELETED);

        restCommonValueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCommonValue.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCommonValue))
            )
            .andExpect(status().isOk());

        // Validate the CommonValue in the database
        List<CommonValue> commonValueList = commonValueRepository.findAll();
        assertThat(commonValueList).hasSize(databaseSizeBeforeUpdate);
        CommonValue testCommonValue = commonValueList.get(commonValueList.size() - 1);
        assertThat(testCommonValue.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCommonValue.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCommonValue.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testCommonValue.getOrder()).isEqualTo(UPDATED_ORDER);
        assertThat(testCommonValue.getDataType()).isEqualTo(UPDATED_DATA_TYPE);
        assertThat(testCommonValue.getDataString()).isEqualTo(UPDATED_DATA_STRING);
        assertThat(testCommonValue.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCommonValue.getDataShort()).isEqualTo(UPDATED_DATA_SHORT);
        assertThat(testCommonValue.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCommonValue.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testCommonValue.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testCommonValue.getLastModifiedOn()).isEqualTo(UPDATED_LAST_MODIFIED_ON);
        assertThat(testCommonValue.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    void patchNonExistingCommonValue() throws Exception {
        int databaseSizeBeforeUpdate = commonValueRepository.findAll().size();
        commonValue.setId(count.incrementAndGet());

        // Create the CommonValue
        CommonValueDTO commonValueDTO = commonValueMapper.toDto(commonValue);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCommonValueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, commonValueDTO.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(commonValueDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommonValue in the database
        List<CommonValue> commonValueList = commonValueRepository.findAll();
        assertThat(commonValueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCommonValue() throws Exception {
        int databaseSizeBeforeUpdate = commonValueRepository.findAll().size();
        commonValue.setId(count.incrementAndGet());

        // Create the CommonValue
        CommonValueDTO commonValueDTO = commonValueMapper.toDto(commonValue);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommonValueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(commonValueDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommonValue in the database
        List<CommonValue> commonValueList = commonValueRepository.findAll();
        assertThat(commonValueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCommonValue() throws Exception {
        int databaseSizeBeforeUpdate = commonValueRepository.findAll().size();
        commonValue.setId(count.incrementAndGet());

        // Create the CommonValue
        CommonValueDTO commonValueDTO = commonValueMapper.toDto(commonValue);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommonValueMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(commonValueDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CommonValue in the database
        List<CommonValue> commonValueList = commonValueRepository.findAll();
        assertThat(commonValueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCommonValue() throws Exception {
        // Initialize the database
        commonValueRepository.saveAndFlush(commonValue);

        int databaseSizeBeforeDelete = commonValueRepository.findAll().size();

        // Delete the commonValue
        restCommonValueMockMvc
            .perform(delete(ENTITY_API_URL_ID, commonValue.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CommonValue> commonValueList = commonValueRepository.findAll();
        assertThat(commonValueList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
