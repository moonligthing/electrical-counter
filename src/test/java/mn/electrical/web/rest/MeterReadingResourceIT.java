package mn.electrical.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import mn.electrical.IntegrationTest;
import mn.electrical.domain.Meter;
import mn.electrical.domain.MeterReading;
import mn.electrical.repository.MeterReadingRepository;
import mn.electrical.service.criteria.MeterReadingCriteria;
import mn.electrical.service.dto.MeterReadingDTO;
import mn.electrical.service.mapper.MeterReadingMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MeterReadingResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MeterReadingResourceIT {

    private static final String DEFAULT_METER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_METER_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_METER_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_METER_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_METER_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_METER_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_CYCLE = "AAAAAAAAAA";
    private static final String UPDATED_CYCLE = "BBBBBBBBBB";

    private static final String DEFAULT_CT_RATIO = "AAAAAAAAAA";
    private static final String UPDATED_CT_RATIO = "BBBBBBBBBB";

    private static final String DEFAULT_VT_RATIO = "AAAAAAAAAA";
    private static final String UPDATED_VT_RATIO = "BBBBBBBBBB";

    private static final String DEFAULT_TOTAL_IMPORT_ACTIVE_ENERGY = "AAAAAAAAAA";
    private static final String UPDATED_TOTAL_IMPORT_ACTIVE_ENERGY = "BBBBBBBBBB";

    private static final String DEFAULT_TOTAL_EXPORT_ACTIVE_ENERGY = "AAAAAAAAAA";
    private static final String UPDATED_TOTAL_EXPORT_ACTIVE_ENERGY = "BBBBBBBBBB";

    private static final String DEFAULT_TOTAL_IMPORT_REACTIVE_ENERGY = "AAAAAAAAAA";
    private static final String UPDATED_TOTAL_IMPORT_REACTIVE_ENERGY = "BBBBBBBBBB";

    private static final String DEFAULT_TOTAL_EXPORT_REACTIVE_ENERGY = "AAAAAAAAAA";
    private static final String UPDATED_TOTAL_EXPORT_REACTIVE_ENERGY = "BBBBBBBBBB";

    private static final String DEFAULT_TOTAL_IMPORT_APPARENT_ENERGY = "AAAAAAAAAA";
    private static final String UPDATED_TOTAL_IMPORT_APPARENT_ENERGY = "BBBBBBBBBB";

    private static final String DEFAULT_TOTAL_EXPORT_APPARENT_ENERGY = "AAAAAAAAAA";
    private static final String UPDATED_TOTAL_EXPORT_APPARENT_ENERGY = "BBBBBBBBBB";

    private static final String DEFAULT_TOTAL_INSTANTANEOUS_ACTIVE_POWER = "AAAAAAAAAA";
    private static final String UPDATED_TOTAL_INSTANTANEOUS_ACTIVE_POWER = "BBBBBBBBBB";

    private static final String DEFAULT_TOTAL_INSTANTANEOUS_REACTIVE_POWER = "AAAAAAAAAA";
    private static final String UPDATED_TOTAL_INSTANTANEOUS_REACTIVE_POWER = "BBBBBBBBBB";

    private static final String DEFAULT_TOTAL_INSTANTANEOUS_APPARENT_POWER = "AAAAAAAAAA";
    private static final String UPDATED_TOTAL_INSTANTANEOUS_APPARENT_POWER = "BBBBBBBBBB";

    private static final String DEFAULT_TOTAL_POWER_FACTOR = "AAAAAAAAAA";
    private static final String UPDATED_TOTAL_POWER_FACTOR = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final Instant DEFAULT_READ_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_READ_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_IMPORT_ACTIVE_ENERGY_1 = "AAAAAAAAAA";
    private static final String UPDATED_IMPORT_ACTIVE_ENERGY_1 = "BBBBBBBBBB";

    private static final String DEFAULT_EXPORT_ACTIVE_ENERGY_1 = "AAAAAAAAAA";
    private static final String UPDATED_EXPORT_ACTIVE_ENERGY_1 = "BBBBBBBBBB";

    private static final String DEFAULT_IMPORT_REACTIVE_ENERGY_1 = "AAAAAAAAAA";
    private static final String UPDATED_IMPORT_REACTIVE_ENERGY_1 = "BBBBBBBBBB";

    private static final String DEFAULT_EXPORT_REACTIVE_ENERGY_1 = "AAAAAAAAAA";
    private static final String UPDATED_EXPORT_REACTIVE_ENERGY_1 = "BBBBBBBBBB";

    private static final String DEFAULT_IMPORT_APPARENT_ENERGY_1 = "AAAAAAAAAA";
    private static final String UPDATED_IMPORT_APPARENT_ENERGY_1 = "BBBBBBBBBB";

    private static final String DEFAULT_EXPORT_APPARENT_ENERGY_1 = "AAAAAAAAAA";
    private static final String UPDATED_EXPORT_APPARENT_ENERGY_1 = "BBBBBBBBBB";

    private static final String DEFAULT_IMPORT_ACTIVE_ENERGY_2 = "AAAAAAAAAA";
    private static final String UPDATED_IMPORT_ACTIVE_ENERGY_2 = "BBBBBBBBBB";

    private static final String DEFAULT_EXPORT_ACTIVE_ENERGY_2 = "AAAAAAAAAA";
    private static final String UPDATED_EXPORT_ACTIVE_ENERGY_2 = "BBBBBBBBBB";

    private static final String DEFAULT_IMPORT_REACTIVE_ENERGY_2 = "AAAAAAAAAA";
    private static final String UPDATED_IMPORT_REACTIVE_ENERGY_2 = "BBBBBBBBBB";

    private static final String DEFAULT_EXPORT_REACTIVE_ENERGY_2 = "AAAAAAAAAA";
    private static final String UPDATED_EXPORT_REACTIVE_ENERGY_2 = "BBBBBBBBBB";

    private static final String DEFAULT_IMPORT_APPARENT_ENERGY_2 = "AAAAAAAAAA";
    private static final String UPDATED_IMPORT_APPARENT_ENERGY_2 = "BBBBBBBBBB";

    private static final String DEFAULT_EXPORT_APPARENT_ENERGY_2 = "AAAAAAAAAA";
    private static final String UPDATED_EXPORT_APPARENT_ENERGY_2 = "BBBBBBBBBB";

    private static final String DEFAULT_IMPORT_ACTIVE_ENERGY_3 = "AAAAAAAAAA";
    private static final String UPDATED_IMPORT_ACTIVE_ENERGY_3 = "BBBBBBBBBB";

    private static final String DEFAULT_EXPORT_ACTIVE_ENERGY_3 = "AAAAAAAAAA";
    private static final String UPDATED_EXPORT_ACTIVE_ENERGY_3 = "BBBBBBBBBB";

    private static final String DEFAULT_IMPORT_REACTIVE_ENERGY_3 = "AAAAAAAAAA";
    private static final String UPDATED_IMPORT_REACTIVE_ENERGY_3 = "BBBBBBBBBB";

    private static final String DEFAULT_EXPORT_REACTIVE_ENERGY_3 = "AAAAAAAAAA";
    private static final String UPDATED_EXPORT_REACTIVE_ENERGY_3 = "BBBBBBBBBB";

    private static final String DEFAULT_IMPORT_APPARENT_ENERGY_3 = "AAAAAAAAAA";
    private static final String UPDATED_IMPORT_APPARENT_ENERGY_3 = "BBBBBBBBBB";

    private static final String DEFAULT_EXPORT_APPARENT_ENERGY_3 = "AAAAAAAAAA";
    private static final String UPDATED_EXPORT_APPARENT_ENERGY_3 = "BBBBBBBBBB";

    private static final String DEFAULT_VOLTAGE_A = "AAAAAAAAAA";
    private static final String UPDATED_VOLTAGE_A = "BBBBBBBBBB";

    private static final String DEFAULT_CURRENT_A = "AAAAAAAAAA";
    private static final String UPDATED_CURRENT_A = "BBBBBBBBBB";

    private static final String DEFAULT_ACTIVE_POWER_A = "AAAAAAAAAA";
    private static final String UPDATED_ACTIVE_POWER_A = "BBBBBBBBBB";

    private static final String DEFAULT_REACTIVE_POWER_A = "AAAAAAAAAA";
    private static final String UPDATED_REACTIVE_POWER_A = "BBBBBBBBBB";

    private static final String DEFAULT_POWER_FACTOR_A = "AAAAAAAAAA";
    private static final String UPDATED_POWER_FACTOR_A = "BBBBBBBBBB";

    private static final String DEFAULT_VOLTAGE_B = "AAAAAAAAAA";
    private static final String UPDATED_VOLTAGE_B = "BBBBBBBBBB";

    private static final String DEFAULT_CURRENT_B = "AAAAAAAAAA";
    private static final String UPDATED_CURRENT_B = "BBBBBBBBBB";

    private static final String DEFAULT_ACTIVE_POWER_B = "AAAAAAAAAA";
    private static final String UPDATED_ACTIVE_POWER_B = "BBBBBBBBBB";

    private static final String DEFAULT_REACTIVE_POWER_B = "AAAAAAAAAA";
    private static final String UPDATED_REACTIVE_POWER_B = "BBBBBBBBBB";

    private static final String DEFAULT_POWER_FACTOR_B = "AAAAAAAAAA";
    private static final String UPDATED_POWER_FACTOR_B = "BBBBBBBBBB";

    private static final String DEFAULT_VOLTAGE_C = "AAAAAAAAAA";
    private static final String UPDATED_VOLTAGE_C = "BBBBBBBBBB";

    private static final String DEFAULT_CURRENT_C = "AAAAAAAAAA";
    private static final String UPDATED_CURRENT_C = "BBBBBBBBBB";

    private static final String DEFAULT_ACTIVE_POWER_C = "AAAAAAAAAA";
    private static final String UPDATED_ACTIVE_POWER_C = "BBBBBBBBBB";

    private static final String DEFAULT_REACTIVE_POWER_C = "AAAAAAAAAA";
    private static final String UPDATED_REACTIVE_POWER_C = "BBBBBBBBBB";

    private static final String DEFAULT_POWER_FACTOR_C = "AAAAAAAAAA";
    private static final String UPDATED_POWER_FACTOR_C = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/meter-readings";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MeterReadingRepository meterReadingRepository;

    @Autowired
    private MeterReadingMapper meterReadingMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMeterReadingMockMvc;

    private MeterReading meterReading;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MeterReading createEntity(EntityManager em) {
        MeterReading meterReading = new MeterReading()
            .meterName(DEFAULT_METER_NAME)
            .meterNumber(DEFAULT_METER_NUMBER)
            .meterAddress(DEFAULT_METER_ADDRESS)
            .cycle(DEFAULT_CYCLE)
            .ctRatio(DEFAULT_CT_RATIO)
            .vtRatio(DEFAULT_VT_RATIO)
            .totalImportActiveEnergy(DEFAULT_TOTAL_IMPORT_ACTIVE_ENERGY)
            .totalExportActiveEnergy(DEFAULT_TOTAL_EXPORT_ACTIVE_ENERGY)
            .totalImportReactiveEnergy(DEFAULT_TOTAL_IMPORT_REACTIVE_ENERGY)
            .totalExportReactiveEnergy(DEFAULT_TOTAL_EXPORT_REACTIVE_ENERGY)
            .totalImportApparentEnergy(DEFAULT_TOTAL_IMPORT_APPARENT_ENERGY)
            .totalExportApparentEnergy(DEFAULT_TOTAL_EXPORT_APPARENT_ENERGY)
            .totalInstantaneousActivePower(DEFAULT_TOTAL_INSTANTANEOUS_ACTIVE_POWER)
            .totalInstantaneousReactivePower(DEFAULT_TOTAL_INSTANTANEOUS_REACTIVE_POWER)
            .totalInstantaneousApparentPower(DEFAULT_TOTAL_INSTANTANEOUS_APPARENT_POWER)
            .totalPowerFactor(DEFAULT_TOTAL_POWER_FACTOR)
            .status(DEFAULT_STATUS)
            .readOn(DEFAULT_READ_ON)
            .importActiveEnergy1(DEFAULT_IMPORT_ACTIVE_ENERGY_1)
            .exportActiveEnergy1(DEFAULT_EXPORT_ACTIVE_ENERGY_1)
            .importReactiveEnergy1(DEFAULT_IMPORT_REACTIVE_ENERGY_1)
            .exportReactiveEnergy1(DEFAULT_EXPORT_REACTIVE_ENERGY_1)
            .importApparentEnergy1(DEFAULT_IMPORT_APPARENT_ENERGY_1)
            .exportApparentEnergy1(DEFAULT_EXPORT_APPARENT_ENERGY_1)
            .importActiveEnergy2(DEFAULT_IMPORT_ACTIVE_ENERGY_2)
            .exportActiveEnergy2(DEFAULT_EXPORT_ACTIVE_ENERGY_2)
            .importReactiveEnergy2(DEFAULT_IMPORT_REACTIVE_ENERGY_2)
            .exportReactiveEnergy2(DEFAULT_EXPORT_REACTIVE_ENERGY_2)
            .importApparentEnergy2(DEFAULT_IMPORT_APPARENT_ENERGY_2)
            .exportApparentEnergy2(DEFAULT_EXPORT_APPARENT_ENERGY_2)
            .importActiveEnergy3(DEFAULT_IMPORT_ACTIVE_ENERGY_3)
            .exportActiveEnergy3(DEFAULT_EXPORT_ACTIVE_ENERGY_3)
            .importReactiveEnergy3(DEFAULT_IMPORT_REACTIVE_ENERGY_3)
            .exportReactiveEnergy3(DEFAULT_EXPORT_REACTIVE_ENERGY_3)
            .importApparentEnergy3(DEFAULT_IMPORT_APPARENT_ENERGY_3)
            .exportApparentEnergy3(DEFAULT_EXPORT_APPARENT_ENERGY_3)
            .voltageA(DEFAULT_VOLTAGE_A)
            .currentA(DEFAULT_CURRENT_A)
            .activePowerA(DEFAULT_ACTIVE_POWER_A)
            .reactivePowerA(DEFAULT_REACTIVE_POWER_A)
            .powerFactorA(DEFAULT_POWER_FACTOR_A)
            .voltageB(DEFAULT_VOLTAGE_B)
            .currentB(DEFAULT_CURRENT_B)
            .activePowerB(DEFAULT_ACTIVE_POWER_B)
            .reactivePowerB(DEFAULT_REACTIVE_POWER_B)
            .powerFactorB(DEFAULT_POWER_FACTOR_B)
            .voltageC(DEFAULT_VOLTAGE_C)
            .currentC(DEFAULT_CURRENT_C)
            .activePowerC(DEFAULT_ACTIVE_POWER_C)
            .reactivePowerC(DEFAULT_REACTIVE_POWER_C)
            .powerFactorC(DEFAULT_POWER_FACTOR_C)
            .createdBy(DEFAULT_CREATED_BY)
            .createdOn(DEFAULT_CREATED_ON)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedOn(DEFAULT_LAST_MODIFIED_ON);
        return meterReading;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MeterReading createUpdatedEntity(EntityManager em) {
        MeterReading meterReading = new MeterReading()
            .meterName(UPDATED_METER_NAME)
            .meterNumber(UPDATED_METER_NUMBER)
            .meterAddress(UPDATED_METER_ADDRESS)
            .cycle(UPDATED_CYCLE)
            .ctRatio(UPDATED_CT_RATIO)
            .vtRatio(UPDATED_VT_RATIO)
            .totalImportActiveEnergy(UPDATED_TOTAL_IMPORT_ACTIVE_ENERGY)
            .totalExportActiveEnergy(UPDATED_TOTAL_EXPORT_ACTIVE_ENERGY)
            .totalImportReactiveEnergy(UPDATED_TOTAL_IMPORT_REACTIVE_ENERGY)
            .totalExportReactiveEnergy(UPDATED_TOTAL_EXPORT_REACTIVE_ENERGY)
            .totalImportApparentEnergy(UPDATED_TOTAL_IMPORT_APPARENT_ENERGY)
            .totalExportApparentEnergy(UPDATED_TOTAL_EXPORT_APPARENT_ENERGY)
            .totalInstantaneousActivePower(UPDATED_TOTAL_INSTANTANEOUS_ACTIVE_POWER)
            .totalInstantaneousReactivePower(UPDATED_TOTAL_INSTANTANEOUS_REACTIVE_POWER)
            .totalInstantaneousApparentPower(UPDATED_TOTAL_INSTANTANEOUS_APPARENT_POWER)
            .totalPowerFactor(UPDATED_TOTAL_POWER_FACTOR)
            .status(UPDATED_STATUS)
            .readOn(UPDATED_READ_ON)
            .importActiveEnergy1(UPDATED_IMPORT_ACTIVE_ENERGY_1)
            .exportActiveEnergy1(UPDATED_EXPORT_ACTIVE_ENERGY_1)
            .importReactiveEnergy1(UPDATED_IMPORT_REACTIVE_ENERGY_1)
            .exportReactiveEnergy1(UPDATED_EXPORT_REACTIVE_ENERGY_1)
            .importApparentEnergy1(UPDATED_IMPORT_APPARENT_ENERGY_1)
            .exportApparentEnergy1(UPDATED_EXPORT_APPARENT_ENERGY_1)
            .importActiveEnergy2(UPDATED_IMPORT_ACTIVE_ENERGY_2)
            .exportActiveEnergy2(UPDATED_EXPORT_ACTIVE_ENERGY_2)
            .importReactiveEnergy2(UPDATED_IMPORT_REACTIVE_ENERGY_2)
            .exportReactiveEnergy2(UPDATED_EXPORT_REACTIVE_ENERGY_2)
            .importApparentEnergy2(UPDATED_IMPORT_APPARENT_ENERGY_2)
            .exportApparentEnergy2(UPDATED_EXPORT_APPARENT_ENERGY_2)
            .importActiveEnergy3(UPDATED_IMPORT_ACTIVE_ENERGY_3)
            .exportActiveEnergy3(UPDATED_EXPORT_ACTIVE_ENERGY_3)
            .importReactiveEnergy3(UPDATED_IMPORT_REACTIVE_ENERGY_3)
            .exportReactiveEnergy3(UPDATED_EXPORT_REACTIVE_ENERGY_3)
            .importApparentEnergy3(UPDATED_IMPORT_APPARENT_ENERGY_3)
            .exportApparentEnergy3(UPDATED_EXPORT_APPARENT_ENERGY_3)
            .voltageA(UPDATED_VOLTAGE_A)
            .currentA(UPDATED_CURRENT_A)
            .activePowerA(UPDATED_ACTIVE_POWER_A)
            .reactivePowerA(UPDATED_REACTIVE_POWER_A)
            .powerFactorA(UPDATED_POWER_FACTOR_A)
            .voltageB(UPDATED_VOLTAGE_B)
            .currentB(UPDATED_CURRENT_B)
            .activePowerB(UPDATED_ACTIVE_POWER_B)
            .reactivePowerB(UPDATED_REACTIVE_POWER_B)
            .powerFactorB(UPDATED_POWER_FACTOR_B)
            .voltageC(UPDATED_VOLTAGE_C)
            .currentC(UPDATED_CURRENT_C)
            .activePowerC(UPDATED_ACTIVE_POWER_C)
            .reactivePowerC(UPDATED_REACTIVE_POWER_C)
            .powerFactorC(UPDATED_POWER_FACTOR_C)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedOn(UPDATED_LAST_MODIFIED_ON);
        return meterReading;
    }

    @BeforeEach
    public void initTest() {
        meterReading = createEntity(em);
    }

    @Test
    @Transactional
    void createMeterReading() throws Exception {
        int databaseSizeBeforeCreate = meterReadingRepository.findAll().size();
        // Create the MeterReading
        MeterReadingDTO meterReadingDTO = meterReadingMapper.toDto(meterReading);
        restMeterReadingMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meterReadingDTO))
            )
            .andExpect(status().isCreated());

        // Validate the MeterReading in the database
        List<MeterReading> meterReadingList = meterReadingRepository.findAll();
        assertThat(meterReadingList).hasSize(databaseSizeBeforeCreate + 1);
        MeterReading testMeterReading = meterReadingList.get(meterReadingList.size() - 1);
        assertThat(testMeterReading.getMeterName()).isEqualTo(DEFAULT_METER_NAME);
        assertThat(testMeterReading.getMeterNumber()).isEqualTo(DEFAULT_METER_NUMBER);
        assertThat(testMeterReading.getMeterAddress()).isEqualTo(DEFAULT_METER_ADDRESS);
        assertThat(testMeterReading.getCycle()).isEqualTo(DEFAULT_CYCLE);
        assertThat(testMeterReading.getCtRatio()).isEqualTo(DEFAULT_CT_RATIO);
        assertThat(testMeterReading.getVtRatio()).isEqualTo(DEFAULT_VT_RATIO);
        assertThat(testMeterReading.getTotalImportActiveEnergy()).isEqualTo(DEFAULT_TOTAL_IMPORT_ACTIVE_ENERGY);
        assertThat(testMeterReading.getTotalExportActiveEnergy()).isEqualTo(DEFAULT_TOTAL_EXPORT_ACTIVE_ENERGY);
        assertThat(testMeterReading.getTotalImportReactiveEnergy()).isEqualTo(DEFAULT_TOTAL_IMPORT_REACTIVE_ENERGY);
        assertThat(testMeterReading.getTotalExportReactiveEnergy()).isEqualTo(DEFAULT_TOTAL_EXPORT_REACTIVE_ENERGY);
        assertThat(testMeterReading.getTotalImportApparentEnergy()).isEqualTo(DEFAULT_TOTAL_IMPORT_APPARENT_ENERGY);
        assertThat(testMeterReading.getTotalExportApparentEnergy()).isEqualTo(DEFAULT_TOTAL_EXPORT_APPARENT_ENERGY);
        assertThat(testMeterReading.getTotalInstantaneousActivePower()).isEqualTo(DEFAULT_TOTAL_INSTANTANEOUS_ACTIVE_POWER);
        assertThat(testMeterReading.getTotalInstantaneousReactivePower()).isEqualTo(DEFAULT_TOTAL_INSTANTANEOUS_REACTIVE_POWER);
        assertThat(testMeterReading.getTotalInstantaneousApparentPower()).isEqualTo(DEFAULT_TOTAL_INSTANTANEOUS_APPARENT_POWER);
        assertThat(testMeterReading.getTotalPowerFactor()).isEqualTo(DEFAULT_TOTAL_POWER_FACTOR);
        assertThat(testMeterReading.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testMeterReading.getReadOn()).isEqualTo(DEFAULT_READ_ON);
        assertThat(testMeterReading.getImportActiveEnergy1()).isEqualTo(DEFAULT_IMPORT_ACTIVE_ENERGY_1);
        assertThat(testMeterReading.getExportActiveEnergy1()).isEqualTo(DEFAULT_EXPORT_ACTIVE_ENERGY_1);
        assertThat(testMeterReading.getImportReactiveEnergy1()).isEqualTo(DEFAULT_IMPORT_REACTIVE_ENERGY_1);
        assertThat(testMeterReading.getExportReactiveEnergy1()).isEqualTo(DEFAULT_EXPORT_REACTIVE_ENERGY_1);
        assertThat(testMeterReading.getImportApparentEnergy1()).isEqualTo(DEFAULT_IMPORT_APPARENT_ENERGY_1);
        assertThat(testMeterReading.getExportApparentEnergy1()).isEqualTo(DEFAULT_EXPORT_APPARENT_ENERGY_1);
        assertThat(testMeterReading.getImportActiveEnergy2()).isEqualTo(DEFAULT_IMPORT_ACTIVE_ENERGY_2);
        assertThat(testMeterReading.getExportActiveEnergy2()).isEqualTo(DEFAULT_EXPORT_ACTIVE_ENERGY_2);
        assertThat(testMeterReading.getImportReactiveEnergy2()).isEqualTo(DEFAULT_IMPORT_REACTIVE_ENERGY_2);
        assertThat(testMeterReading.getExportReactiveEnergy2()).isEqualTo(DEFAULT_EXPORT_REACTIVE_ENERGY_2);
        assertThat(testMeterReading.getImportApparentEnergy2()).isEqualTo(DEFAULT_IMPORT_APPARENT_ENERGY_2);
        assertThat(testMeterReading.getExportApparentEnergy2()).isEqualTo(DEFAULT_EXPORT_APPARENT_ENERGY_2);
        assertThat(testMeterReading.getImportActiveEnergy3()).isEqualTo(DEFAULT_IMPORT_ACTIVE_ENERGY_3);
        assertThat(testMeterReading.getExportActiveEnergy3()).isEqualTo(DEFAULT_EXPORT_ACTIVE_ENERGY_3);
        assertThat(testMeterReading.getImportReactiveEnergy3()).isEqualTo(DEFAULT_IMPORT_REACTIVE_ENERGY_3);
        assertThat(testMeterReading.getExportReactiveEnergy3()).isEqualTo(DEFAULT_EXPORT_REACTIVE_ENERGY_3);
        assertThat(testMeterReading.getImportApparentEnergy3()).isEqualTo(DEFAULT_IMPORT_APPARENT_ENERGY_3);
        assertThat(testMeterReading.getExportApparentEnergy3()).isEqualTo(DEFAULT_EXPORT_APPARENT_ENERGY_3);
        assertThat(testMeterReading.getVoltageA()).isEqualTo(DEFAULT_VOLTAGE_A);
        assertThat(testMeterReading.getCurrentA()).isEqualTo(DEFAULT_CURRENT_A);
        assertThat(testMeterReading.getActivePowerA()).isEqualTo(DEFAULT_ACTIVE_POWER_A);
        assertThat(testMeterReading.getReactivePowerA()).isEqualTo(DEFAULT_REACTIVE_POWER_A);
        assertThat(testMeterReading.getPowerFactorA()).isEqualTo(DEFAULT_POWER_FACTOR_A);
        assertThat(testMeterReading.getVoltageB()).isEqualTo(DEFAULT_VOLTAGE_B);
        assertThat(testMeterReading.getCurrentB()).isEqualTo(DEFAULT_CURRENT_B);
        assertThat(testMeterReading.getActivePowerB()).isEqualTo(DEFAULT_ACTIVE_POWER_B);
        assertThat(testMeterReading.getReactivePowerB()).isEqualTo(DEFAULT_REACTIVE_POWER_B);
        assertThat(testMeterReading.getPowerFactorB()).isEqualTo(DEFAULT_POWER_FACTOR_B);
        assertThat(testMeterReading.getVoltageC()).isEqualTo(DEFAULT_VOLTAGE_C);
        assertThat(testMeterReading.getCurrentC()).isEqualTo(DEFAULT_CURRENT_C);
        assertThat(testMeterReading.getActivePowerC()).isEqualTo(DEFAULT_ACTIVE_POWER_C);
        assertThat(testMeterReading.getReactivePowerC()).isEqualTo(DEFAULT_REACTIVE_POWER_C);
        assertThat(testMeterReading.getPowerFactorC()).isEqualTo(DEFAULT_POWER_FACTOR_C);
        assertThat(testMeterReading.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testMeterReading.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testMeterReading.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testMeterReading.getLastModifiedOn()).isEqualTo(DEFAULT_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void createMeterReadingWithExistingId() throws Exception {
        // Create the MeterReading with an existing ID
        meterReading.setId(1L);
        MeterReadingDTO meterReadingDTO = meterReadingMapper.toDto(meterReading);

        int databaseSizeBeforeCreate = meterReadingRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMeterReadingMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meterReadingDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MeterReading in the database
        List<MeterReading> meterReadingList = meterReadingRepository.findAll();
        assertThat(meterReadingList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllMeterReadings() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList
        restMeterReadingMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(meterReading.getId().intValue())))
            .andExpect(jsonPath("$.[*].meterName").value(hasItem(DEFAULT_METER_NAME)))
            .andExpect(jsonPath("$.[*].meterNumber").value(hasItem(DEFAULT_METER_NUMBER)))
            .andExpect(jsonPath("$.[*].meterAddress").value(hasItem(DEFAULT_METER_ADDRESS)))
            .andExpect(jsonPath("$.[*].cycle").value(hasItem(DEFAULT_CYCLE)))
            .andExpect(jsonPath("$.[*].ctRatio").value(hasItem(DEFAULT_CT_RATIO)))
            .andExpect(jsonPath("$.[*].vtRatio").value(hasItem(DEFAULT_VT_RATIO)))
            .andExpect(jsonPath("$.[*].totalImportActiveEnergy").value(hasItem(DEFAULT_TOTAL_IMPORT_ACTIVE_ENERGY)))
            .andExpect(jsonPath("$.[*].totalExportActiveEnergy").value(hasItem(DEFAULT_TOTAL_EXPORT_ACTIVE_ENERGY)))
            .andExpect(jsonPath("$.[*].totalImportReactiveEnergy").value(hasItem(DEFAULT_TOTAL_IMPORT_REACTIVE_ENERGY)))
            .andExpect(jsonPath("$.[*].totalExportReactiveEnergy").value(hasItem(DEFAULT_TOTAL_EXPORT_REACTIVE_ENERGY)))
            .andExpect(jsonPath("$.[*].totalImportApparentEnergy").value(hasItem(DEFAULT_TOTAL_IMPORT_APPARENT_ENERGY)))
            .andExpect(jsonPath("$.[*].totalExportApparentEnergy").value(hasItem(DEFAULT_TOTAL_EXPORT_APPARENT_ENERGY)))
            .andExpect(jsonPath("$.[*].totalInstantaneousActivePower").value(hasItem(DEFAULT_TOTAL_INSTANTANEOUS_ACTIVE_POWER)))
            .andExpect(jsonPath("$.[*].totalInstantaneousReactivePower").value(hasItem(DEFAULT_TOTAL_INSTANTANEOUS_REACTIVE_POWER)))
            .andExpect(jsonPath("$.[*].totalInstantaneousApparentPower").value(hasItem(DEFAULT_TOTAL_INSTANTANEOUS_APPARENT_POWER)))
            .andExpect(jsonPath("$.[*].totalPowerFactor").value(hasItem(DEFAULT_TOTAL_POWER_FACTOR)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].readOn").value(hasItem(DEFAULT_READ_ON.toString())))
            .andExpect(jsonPath("$.[*].importActiveEnergy1").value(hasItem(DEFAULT_IMPORT_ACTIVE_ENERGY_1)))
            .andExpect(jsonPath("$.[*].exportActiveEnergy1").value(hasItem(DEFAULT_EXPORT_ACTIVE_ENERGY_1)))
            .andExpect(jsonPath("$.[*].importReactiveEnergy1").value(hasItem(DEFAULT_IMPORT_REACTIVE_ENERGY_1)))
            .andExpect(jsonPath("$.[*].exportReactiveEnergy1").value(hasItem(DEFAULT_EXPORT_REACTIVE_ENERGY_1)))
            .andExpect(jsonPath("$.[*].importApparentEnergy1").value(hasItem(DEFAULT_IMPORT_APPARENT_ENERGY_1)))
            .andExpect(jsonPath("$.[*].exportApparentEnergy1").value(hasItem(DEFAULT_EXPORT_APPARENT_ENERGY_1)))
            .andExpect(jsonPath("$.[*].importActiveEnergy2").value(hasItem(DEFAULT_IMPORT_ACTIVE_ENERGY_2)))
            .andExpect(jsonPath("$.[*].exportActiveEnergy2").value(hasItem(DEFAULT_EXPORT_ACTIVE_ENERGY_2)))
            .andExpect(jsonPath("$.[*].importReactiveEnergy2").value(hasItem(DEFAULT_IMPORT_REACTIVE_ENERGY_2)))
            .andExpect(jsonPath("$.[*].exportReactiveEnergy2").value(hasItem(DEFAULT_EXPORT_REACTIVE_ENERGY_2)))
            .andExpect(jsonPath("$.[*].importApparentEnergy2").value(hasItem(DEFAULT_IMPORT_APPARENT_ENERGY_2)))
            .andExpect(jsonPath("$.[*].exportApparentEnergy2").value(hasItem(DEFAULT_EXPORT_APPARENT_ENERGY_2)))
            .andExpect(jsonPath("$.[*].importActiveEnergy3").value(hasItem(DEFAULT_IMPORT_ACTIVE_ENERGY_3)))
            .andExpect(jsonPath("$.[*].exportActiveEnergy3").value(hasItem(DEFAULT_EXPORT_ACTIVE_ENERGY_3)))
            .andExpect(jsonPath("$.[*].importReactiveEnergy3").value(hasItem(DEFAULT_IMPORT_REACTIVE_ENERGY_3)))
            .andExpect(jsonPath("$.[*].exportReactiveEnergy3").value(hasItem(DEFAULT_EXPORT_REACTIVE_ENERGY_3)))
            .andExpect(jsonPath("$.[*].importApparentEnergy3").value(hasItem(DEFAULT_IMPORT_APPARENT_ENERGY_3)))
            .andExpect(jsonPath("$.[*].exportApparentEnergy3").value(hasItem(DEFAULT_EXPORT_APPARENT_ENERGY_3)))
            .andExpect(jsonPath("$.[*].voltageA").value(hasItem(DEFAULT_VOLTAGE_A)))
            .andExpect(jsonPath("$.[*].currentA").value(hasItem(DEFAULT_CURRENT_A)))
            .andExpect(jsonPath("$.[*].activePowerA").value(hasItem(DEFAULT_ACTIVE_POWER_A)))
            .andExpect(jsonPath("$.[*].reactivePowerA").value(hasItem(DEFAULT_REACTIVE_POWER_A)))
            .andExpect(jsonPath("$.[*].powerFactorA").value(hasItem(DEFAULT_POWER_FACTOR_A)))
            .andExpect(jsonPath("$.[*].voltageB").value(hasItem(DEFAULT_VOLTAGE_B)))
            .andExpect(jsonPath("$.[*].currentB").value(hasItem(DEFAULT_CURRENT_B)))
            .andExpect(jsonPath("$.[*].activePowerB").value(hasItem(DEFAULT_ACTIVE_POWER_B)))
            .andExpect(jsonPath("$.[*].reactivePowerB").value(hasItem(DEFAULT_REACTIVE_POWER_B)))
            .andExpect(jsonPath("$.[*].powerFactorB").value(hasItem(DEFAULT_POWER_FACTOR_B)))
            .andExpect(jsonPath("$.[*].voltageC").value(hasItem(DEFAULT_VOLTAGE_C)))
            .andExpect(jsonPath("$.[*].currentC").value(hasItem(DEFAULT_CURRENT_C)))
            .andExpect(jsonPath("$.[*].activePowerC").value(hasItem(DEFAULT_ACTIVE_POWER_C)))
            .andExpect(jsonPath("$.[*].reactivePowerC").value(hasItem(DEFAULT_REACTIVE_POWER_C)))
            .andExpect(jsonPath("$.[*].powerFactorC").value(hasItem(DEFAULT_POWER_FACTOR_C)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedOn").value(hasItem(DEFAULT_LAST_MODIFIED_ON.toString())));
    }

    @Test
    @Transactional
    void getMeterReading() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get the meterReading
        restMeterReadingMockMvc
            .perform(get(ENTITY_API_URL_ID, meterReading.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(meterReading.getId().intValue()))
            .andExpect(jsonPath("$.meterName").value(DEFAULT_METER_NAME))
            .andExpect(jsonPath("$.meterNumber").value(DEFAULT_METER_NUMBER))
            .andExpect(jsonPath("$.meterAddress").value(DEFAULT_METER_ADDRESS))
            .andExpect(jsonPath("$.cycle").value(DEFAULT_CYCLE))
            .andExpect(jsonPath("$.ctRatio").value(DEFAULT_CT_RATIO))
            .andExpect(jsonPath("$.vtRatio").value(DEFAULT_VT_RATIO))
            .andExpect(jsonPath("$.totalImportActiveEnergy").value(DEFAULT_TOTAL_IMPORT_ACTIVE_ENERGY))
            .andExpect(jsonPath("$.totalExportActiveEnergy").value(DEFAULT_TOTAL_EXPORT_ACTIVE_ENERGY))
            .andExpect(jsonPath("$.totalImportReactiveEnergy").value(DEFAULT_TOTAL_IMPORT_REACTIVE_ENERGY))
            .andExpect(jsonPath("$.totalExportReactiveEnergy").value(DEFAULT_TOTAL_EXPORT_REACTIVE_ENERGY))
            .andExpect(jsonPath("$.totalImportApparentEnergy").value(DEFAULT_TOTAL_IMPORT_APPARENT_ENERGY))
            .andExpect(jsonPath("$.totalExportApparentEnergy").value(DEFAULT_TOTAL_EXPORT_APPARENT_ENERGY))
            .andExpect(jsonPath("$.totalInstantaneousActivePower").value(DEFAULT_TOTAL_INSTANTANEOUS_ACTIVE_POWER))
            .andExpect(jsonPath("$.totalInstantaneousReactivePower").value(DEFAULT_TOTAL_INSTANTANEOUS_REACTIVE_POWER))
            .andExpect(jsonPath("$.totalInstantaneousApparentPower").value(DEFAULT_TOTAL_INSTANTANEOUS_APPARENT_POWER))
            .andExpect(jsonPath("$.totalPowerFactor").value(DEFAULT_TOTAL_POWER_FACTOR))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.readOn").value(DEFAULT_READ_ON.toString()))
            .andExpect(jsonPath("$.importActiveEnergy1").value(DEFAULT_IMPORT_ACTIVE_ENERGY_1))
            .andExpect(jsonPath("$.exportActiveEnergy1").value(DEFAULT_EXPORT_ACTIVE_ENERGY_1))
            .andExpect(jsonPath("$.importReactiveEnergy1").value(DEFAULT_IMPORT_REACTIVE_ENERGY_1))
            .andExpect(jsonPath("$.exportReactiveEnergy1").value(DEFAULT_EXPORT_REACTIVE_ENERGY_1))
            .andExpect(jsonPath("$.importApparentEnergy1").value(DEFAULT_IMPORT_APPARENT_ENERGY_1))
            .andExpect(jsonPath("$.exportApparentEnergy1").value(DEFAULT_EXPORT_APPARENT_ENERGY_1))
            .andExpect(jsonPath("$.importActiveEnergy2").value(DEFAULT_IMPORT_ACTIVE_ENERGY_2))
            .andExpect(jsonPath("$.exportActiveEnergy2").value(DEFAULT_EXPORT_ACTIVE_ENERGY_2))
            .andExpect(jsonPath("$.importReactiveEnergy2").value(DEFAULT_IMPORT_REACTIVE_ENERGY_2))
            .andExpect(jsonPath("$.exportReactiveEnergy2").value(DEFAULT_EXPORT_REACTIVE_ENERGY_2))
            .andExpect(jsonPath("$.importApparentEnergy2").value(DEFAULT_IMPORT_APPARENT_ENERGY_2))
            .andExpect(jsonPath("$.exportApparentEnergy2").value(DEFAULT_EXPORT_APPARENT_ENERGY_2))
            .andExpect(jsonPath("$.importActiveEnergy3").value(DEFAULT_IMPORT_ACTIVE_ENERGY_3))
            .andExpect(jsonPath("$.exportActiveEnergy3").value(DEFAULT_EXPORT_ACTIVE_ENERGY_3))
            .andExpect(jsonPath("$.importReactiveEnergy3").value(DEFAULT_IMPORT_REACTIVE_ENERGY_3))
            .andExpect(jsonPath("$.exportReactiveEnergy3").value(DEFAULT_EXPORT_REACTIVE_ENERGY_3))
            .andExpect(jsonPath("$.importApparentEnergy3").value(DEFAULT_IMPORT_APPARENT_ENERGY_3))
            .andExpect(jsonPath("$.exportApparentEnergy3").value(DEFAULT_EXPORT_APPARENT_ENERGY_3))
            .andExpect(jsonPath("$.voltageA").value(DEFAULT_VOLTAGE_A))
            .andExpect(jsonPath("$.currentA").value(DEFAULT_CURRENT_A))
            .andExpect(jsonPath("$.activePowerA").value(DEFAULT_ACTIVE_POWER_A))
            .andExpect(jsonPath("$.reactivePowerA").value(DEFAULT_REACTIVE_POWER_A))
            .andExpect(jsonPath("$.powerFactorA").value(DEFAULT_POWER_FACTOR_A))
            .andExpect(jsonPath("$.voltageB").value(DEFAULT_VOLTAGE_B))
            .andExpect(jsonPath("$.currentB").value(DEFAULT_CURRENT_B))
            .andExpect(jsonPath("$.activePowerB").value(DEFAULT_ACTIVE_POWER_B))
            .andExpect(jsonPath("$.reactivePowerB").value(DEFAULT_REACTIVE_POWER_B))
            .andExpect(jsonPath("$.powerFactorB").value(DEFAULT_POWER_FACTOR_B))
            .andExpect(jsonPath("$.voltageC").value(DEFAULT_VOLTAGE_C))
            .andExpect(jsonPath("$.currentC").value(DEFAULT_CURRENT_C))
            .andExpect(jsonPath("$.activePowerC").value(DEFAULT_ACTIVE_POWER_C))
            .andExpect(jsonPath("$.reactivePowerC").value(DEFAULT_REACTIVE_POWER_C))
            .andExpect(jsonPath("$.powerFactorC").value(DEFAULT_POWER_FACTOR_C))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedOn").value(DEFAULT_LAST_MODIFIED_ON.toString()));
    }

    @Test
    @Transactional
    void getMeterReadingsByIdFiltering() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        Long id = meterReading.getId();

        defaultMeterReadingShouldBeFound("id.equals=" + id);
        defaultMeterReadingShouldNotBeFound("id.notEquals=" + id);

        defaultMeterReadingShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultMeterReadingShouldNotBeFound("id.greaterThan=" + id);

        defaultMeterReadingShouldBeFound("id.lessThanOrEqual=" + id);
        defaultMeterReadingShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByMeterNameIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where meterName equals to DEFAULT_METER_NAME
        defaultMeterReadingShouldBeFound("meterName.equals=" + DEFAULT_METER_NAME);

        // Get all the meterReadingList where meterName equals to UPDATED_METER_NAME
        defaultMeterReadingShouldNotBeFound("meterName.equals=" + UPDATED_METER_NAME);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByMeterNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where meterName not equals to DEFAULT_METER_NAME
        defaultMeterReadingShouldNotBeFound("meterName.notEquals=" + DEFAULT_METER_NAME);

        // Get all the meterReadingList where meterName not equals to UPDATED_METER_NAME
        defaultMeterReadingShouldBeFound("meterName.notEquals=" + UPDATED_METER_NAME);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByMeterNameIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where meterName in DEFAULT_METER_NAME or UPDATED_METER_NAME
        defaultMeterReadingShouldBeFound("meterName.in=" + DEFAULT_METER_NAME + "," + UPDATED_METER_NAME);

        // Get all the meterReadingList where meterName equals to UPDATED_METER_NAME
        defaultMeterReadingShouldNotBeFound("meterName.in=" + UPDATED_METER_NAME);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByMeterNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where meterName is not null
        defaultMeterReadingShouldBeFound("meterName.specified=true");

        // Get all the meterReadingList where meterName is null
        defaultMeterReadingShouldNotBeFound("meterName.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByMeterNameContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where meterName contains DEFAULT_METER_NAME
        defaultMeterReadingShouldBeFound("meterName.contains=" + DEFAULT_METER_NAME);

        // Get all the meterReadingList where meterName contains UPDATED_METER_NAME
        defaultMeterReadingShouldNotBeFound("meterName.contains=" + UPDATED_METER_NAME);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByMeterNameNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where meterName does not contain DEFAULT_METER_NAME
        defaultMeterReadingShouldNotBeFound("meterName.doesNotContain=" + DEFAULT_METER_NAME);

        // Get all the meterReadingList where meterName does not contain UPDATED_METER_NAME
        defaultMeterReadingShouldBeFound("meterName.doesNotContain=" + UPDATED_METER_NAME);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByMeterNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where meterNumber equals to DEFAULT_METER_NUMBER
        defaultMeterReadingShouldBeFound("meterNumber.equals=" + DEFAULT_METER_NUMBER);

        // Get all the meterReadingList where meterNumber equals to UPDATED_METER_NUMBER
        defaultMeterReadingShouldNotBeFound("meterNumber.equals=" + UPDATED_METER_NUMBER);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByMeterNumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where meterNumber not equals to DEFAULT_METER_NUMBER
        defaultMeterReadingShouldNotBeFound("meterNumber.notEquals=" + DEFAULT_METER_NUMBER);

        // Get all the meterReadingList where meterNumber not equals to UPDATED_METER_NUMBER
        defaultMeterReadingShouldBeFound("meterNumber.notEquals=" + UPDATED_METER_NUMBER);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByMeterNumberIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where meterNumber in DEFAULT_METER_NUMBER or UPDATED_METER_NUMBER
        defaultMeterReadingShouldBeFound("meterNumber.in=" + DEFAULT_METER_NUMBER + "," + UPDATED_METER_NUMBER);

        // Get all the meterReadingList where meterNumber equals to UPDATED_METER_NUMBER
        defaultMeterReadingShouldNotBeFound("meterNumber.in=" + UPDATED_METER_NUMBER);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByMeterNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where meterNumber is not null
        defaultMeterReadingShouldBeFound("meterNumber.specified=true");

        // Get all the meterReadingList where meterNumber is null
        defaultMeterReadingShouldNotBeFound("meterNumber.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByMeterNumberContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where meterNumber contains DEFAULT_METER_NUMBER
        defaultMeterReadingShouldBeFound("meterNumber.contains=" + DEFAULT_METER_NUMBER);

        // Get all the meterReadingList where meterNumber contains UPDATED_METER_NUMBER
        defaultMeterReadingShouldNotBeFound("meterNumber.contains=" + UPDATED_METER_NUMBER);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByMeterNumberNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where meterNumber does not contain DEFAULT_METER_NUMBER
        defaultMeterReadingShouldNotBeFound("meterNumber.doesNotContain=" + DEFAULT_METER_NUMBER);

        // Get all the meterReadingList where meterNumber does not contain UPDATED_METER_NUMBER
        defaultMeterReadingShouldBeFound("meterNumber.doesNotContain=" + UPDATED_METER_NUMBER);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByMeterAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where meterAddress equals to DEFAULT_METER_ADDRESS
        defaultMeterReadingShouldBeFound("meterAddress.equals=" + DEFAULT_METER_ADDRESS);

        // Get all the meterReadingList where meterAddress equals to UPDATED_METER_ADDRESS
        defaultMeterReadingShouldNotBeFound("meterAddress.equals=" + UPDATED_METER_ADDRESS);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByMeterAddressIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where meterAddress not equals to DEFAULT_METER_ADDRESS
        defaultMeterReadingShouldNotBeFound("meterAddress.notEquals=" + DEFAULT_METER_ADDRESS);

        // Get all the meterReadingList where meterAddress not equals to UPDATED_METER_ADDRESS
        defaultMeterReadingShouldBeFound("meterAddress.notEquals=" + UPDATED_METER_ADDRESS);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByMeterAddressIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where meterAddress in DEFAULT_METER_ADDRESS or UPDATED_METER_ADDRESS
        defaultMeterReadingShouldBeFound("meterAddress.in=" + DEFAULT_METER_ADDRESS + "," + UPDATED_METER_ADDRESS);

        // Get all the meterReadingList where meterAddress equals to UPDATED_METER_ADDRESS
        defaultMeterReadingShouldNotBeFound("meterAddress.in=" + UPDATED_METER_ADDRESS);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByMeterAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where meterAddress is not null
        defaultMeterReadingShouldBeFound("meterAddress.specified=true");

        // Get all the meterReadingList where meterAddress is null
        defaultMeterReadingShouldNotBeFound("meterAddress.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByMeterAddressContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where meterAddress contains DEFAULT_METER_ADDRESS
        defaultMeterReadingShouldBeFound("meterAddress.contains=" + DEFAULT_METER_ADDRESS);

        // Get all the meterReadingList where meterAddress contains UPDATED_METER_ADDRESS
        defaultMeterReadingShouldNotBeFound("meterAddress.contains=" + UPDATED_METER_ADDRESS);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByMeterAddressNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where meterAddress does not contain DEFAULT_METER_ADDRESS
        defaultMeterReadingShouldNotBeFound("meterAddress.doesNotContain=" + DEFAULT_METER_ADDRESS);

        // Get all the meterReadingList where meterAddress does not contain UPDATED_METER_ADDRESS
        defaultMeterReadingShouldBeFound("meterAddress.doesNotContain=" + UPDATED_METER_ADDRESS);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCycleIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where cycle equals to DEFAULT_CYCLE
        defaultMeterReadingShouldBeFound("cycle.equals=" + DEFAULT_CYCLE);

        // Get all the meterReadingList where cycle equals to UPDATED_CYCLE
        defaultMeterReadingShouldNotBeFound("cycle.equals=" + UPDATED_CYCLE);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCycleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where cycle not equals to DEFAULT_CYCLE
        defaultMeterReadingShouldNotBeFound("cycle.notEquals=" + DEFAULT_CYCLE);

        // Get all the meterReadingList where cycle not equals to UPDATED_CYCLE
        defaultMeterReadingShouldBeFound("cycle.notEquals=" + UPDATED_CYCLE);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCycleIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where cycle in DEFAULT_CYCLE or UPDATED_CYCLE
        defaultMeterReadingShouldBeFound("cycle.in=" + DEFAULT_CYCLE + "," + UPDATED_CYCLE);

        // Get all the meterReadingList where cycle equals to UPDATED_CYCLE
        defaultMeterReadingShouldNotBeFound("cycle.in=" + UPDATED_CYCLE);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCycleIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where cycle is not null
        defaultMeterReadingShouldBeFound("cycle.specified=true");

        // Get all the meterReadingList where cycle is null
        defaultMeterReadingShouldNotBeFound("cycle.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCycleContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where cycle contains DEFAULT_CYCLE
        defaultMeterReadingShouldBeFound("cycle.contains=" + DEFAULT_CYCLE);

        // Get all the meterReadingList where cycle contains UPDATED_CYCLE
        defaultMeterReadingShouldNotBeFound("cycle.contains=" + UPDATED_CYCLE);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCycleNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where cycle does not contain DEFAULT_CYCLE
        defaultMeterReadingShouldNotBeFound("cycle.doesNotContain=" + DEFAULT_CYCLE);

        // Get all the meterReadingList where cycle does not contain UPDATED_CYCLE
        defaultMeterReadingShouldBeFound("cycle.doesNotContain=" + UPDATED_CYCLE);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCtRatioIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where ctRatio equals to DEFAULT_CT_RATIO
        defaultMeterReadingShouldBeFound("ctRatio.equals=" + DEFAULT_CT_RATIO);

        // Get all the meterReadingList where ctRatio equals to UPDATED_CT_RATIO
        defaultMeterReadingShouldNotBeFound("ctRatio.equals=" + UPDATED_CT_RATIO);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCtRatioIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where ctRatio not equals to DEFAULT_CT_RATIO
        defaultMeterReadingShouldNotBeFound("ctRatio.notEquals=" + DEFAULT_CT_RATIO);

        // Get all the meterReadingList where ctRatio not equals to UPDATED_CT_RATIO
        defaultMeterReadingShouldBeFound("ctRatio.notEquals=" + UPDATED_CT_RATIO);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCtRatioIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where ctRatio in DEFAULT_CT_RATIO or UPDATED_CT_RATIO
        defaultMeterReadingShouldBeFound("ctRatio.in=" + DEFAULT_CT_RATIO + "," + UPDATED_CT_RATIO);

        // Get all the meterReadingList where ctRatio equals to UPDATED_CT_RATIO
        defaultMeterReadingShouldNotBeFound("ctRatio.in=" + UPDATED_CT_RATIO);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCtRatioIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where ctRatio is not null
        defaultMeterReadingShouldBeFound("ctRatio.specified=true");

        // Get all the meterReadingList where ctRatio is null
        defaultMeterReadingShouldNotBeFound("ctRatio.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCtRatioContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where ctRatio contains DEFAULT_CT_RATIO
        defaultMeterReadingShouldBeFound("ctRatio.contains=" + DEFAULT_CT_RATIO);

        // Get all the meterReadingList where ctRatio contains UPDATED_CT_RATIO
        defaultMeterReadingShouldNotBeFound("ctRatio.contains=" + UPDATED_CT_RATIO);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCtRatioNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where ctRatio does not contain DEFAULT_CT_RATIO
        defaultMeterReadingShouldNotBeFound("ctRatio.doesNotContain=" + DEFAULT_CT_RATIO);

        // Get all the meterReadingList where ctRatio does not contain UPDATED_CT_RATIO
        defaultMeterReadingShouldBeFound("ctRatio.doesNotContain=" + UPDATED_CT_RATIO);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVtRatioIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where vtRatio equals to DEFAULT_VT_RATIO
        defaultMeterReadingShouldBeFound("vtRatio.equals=" + DEFAULT_VT_RATIO);

        // Get all the meterReadingList where vtRatio equals to UPDATED_VT_RATIO
        defaultMeterReadingShouldNotBeFound("vtRatio.equals=" + UPDATED_VT_RATIO);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVtRatioIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where vtRatio not equals to DEFAULT_VT_RATIO
        defaultMeterReadingShouldNotBeFound("vtRatio.notEquals=" + DEFAULT_VT_RATIO);

        // Get all the meterReadingList where vtRatio not equals to UPDATED_VT_RATIO
        defaultMeterReadingShouldBeFound("vtRatio.notEquals=" + UPDATED_VT_RATIO);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVtRatioIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where vtRatio in DEFAULT_VT_RATIO or UPDATED_VT_RATIO
        defaultMeterReadingShouldBeFound("vtRatio.in=" + DEFAULT_VT_RATIO + "," + UPDATED_VT_RATIO);

        // Get all the meterReadingList where vtRatio equals to UPDATED_VT_RATIO
        defaultMeterReadingShouldNotBeFound("vtRatio.in=" + UPDATED_VT_RATIO);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVtRatioIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where vtRatio is not null
        defaultMeterReadingShouldBeFound("vtRatio.specified=true");

        // Get all the meterReadingList where vtRatio is null
        defaultMeterReadingShouldNotBeFound("vtRatio.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVtRatioContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where vtRatio contains DEFAULT_VT_RATIO
        defaultMeterReadingShouldBeFound("vtRatio.contains=" + DEFAULT_VT_RATIO);

        // Get all the meterReadingList where vtRatio contains UPDATED_VT_RATIO
        defaultMeterReadingShouldNotBeFound("vtRatio.contains=" + UPDATED_VT_RATIO);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVtRatioNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where vtRatio does not contain DEFAULT_VT_RATIO
        defaultMeterReadingShouldNotBeFound("vtRatio.doesNotContain=" + DEFAULT_VT_RATIO);

        // Get all the meterReadingList where vtRatio does not contain UPDATED_VT_RATIO
        defaultMeterReadingShouldBeFound("vtRatio.doesNotContain=" + UPDATED_VT_RATIO);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalImportActiveEnergyIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalImportActiveEnergy equals to DEFAULT_TOTAL_IMPORT_ACTIVE_ENERGY
        defaultMeterReadingShouldBeFound("totalImportActiveEnergy.equals=" + DEFAULT_TOTAL_IMPORT_ACTIVE_ENERGY);

        // Get all the meterReadingList where totalImportActiveEnergy equals to UPDATED_TOTAL_IMPORT_ACTIVE_ENERGY
        defaultMeterReadingShouldNotBeFound("totalImportActiveEnergy.equals=" + UPDATED_TOTAL_IMPORT_ACTIVE_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalImportActiveEnergyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalImportActiveEnergy not equals to DEFAULT_TOTAL_IMPORT_ACTIVE_ENERGY
        defaultMeterReadingShouldNotBeFound("totalImportActiveEnergy.notEquals=" + DEFAULT_TOTAL_IMPORT_ACTIVE_ENERGY);

        // Get all the meterReadingList where totalImportActiveEnergy not equals to UPDATED_TOTAL_IMPORT_ACTIVE_ENERGY
        defaultMeterReadingShouldBeFound("totalImportActiveEnergy.notEquals=" + UPDATED_TOTAL_IMPORT_ACTIVE_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalImportActiveEnergyIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalImportActiveEnergy in DEFAULT_TOTAL_IMPORT_ACTIVE_ENERGY or UPDATED_TOTAL_IMPORT_ACTIVE_ENERGY
        defaultMeterReadingShouldBeFound(
            "totalImportActiveEnergy.in=" + DEFAULT_TOTAL_IMPORT_ACTIVE_ENERGY + "," + UPDATED_TOTAL_IMPORT_ACTIVE_ENERGY
        );

        // Get all the meterReadingList where totalImportActiveEnergy equals to UPDATED_TOTAL_IMPORT_ACTIVE_ENERGY
        defaultMeterReadingShouldNotBeFound("totalImportActiveEnergy.in=" + UPDATED_TOTAL_IMPORT_ACTIVE_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalImportActiveEnergyIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalImportActiveEnergy is not null
        defaultMeterReadingShouldBeFound("totalImportActiveEnergy.specified=true");

        // Get all the meterReadingList where totalImportActiveEnergy is null
        defaultMeterReadingShouldNotBeFound("totalImportActiveEnergy.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalImportActiveEnergyContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalImportActiveEnergy contains DEFAULT_TOTAL_IMPORT_ACTIVE_ENERGY
        defaultMeterReadingShouldBeFound("totalImportActiveEnergy.contains=" + DEFAULT_TOTAL_IMPORT_ACTIVE_ENERGY);

        // Get all the meterReadingList where totalImportActiveEnergy contains UPDATED_TOTAL_IMPORT_ACTIVE_ENERGY
        defaultMeterReadingShouldNotBeFound("totalImportActiveEnergy.contains=" + UPDATED_TOTAL_IMPORT_ACTIVE_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalImportActiveEnergyNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalImportActiveEnergy does not contain DEFAULT_TOTAL_IMPORT_ACTIVE_ENERGY
        defaultMeterReadingShouldNotBeFound("totalImportActiveEnergy.doesNotContain=" + DEFAULT_TOTAL_IMPORT_ACTIVE_ENERGY);

        // Get all the meterReadingList where totalImportActiveEnergy does not contain UPDATED_TOTAL_IMPORT_ACTIVE_ENERGY
        defaultMeterReadingShouldBeFound("totalImportActiveEnergy.doesNotContain=" + UPDATED_TOTAL_IMPORT_ACTIVE_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalExportActiveEnergyIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalExportActiveEnergy equals to DEFAULT_TOTAL_EXPORT_ACTIVE_ENERGY
        defaultMeterReadingShouldBeFound("totalExportActiveEnergy.equals=" + DEFAULT_TOTAL_EXPORT_ACTIVE_ENERGY);

        // Get all the meterReadingList where totalExportActiveEnergy equals to UPDATED_TOTAL_EXPORT_ACTIVE_ENERGY
        defaultMeterReadingShouldNotBeFound("totalExportActiveEnergy.equals=" + UPDATED_TOTAL_EXPORT_ACTIVE_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalExportActiveEnergyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalExportActiveEnergy not equals to DEFAULT_TOTAL_EXPORT_ACTIVE_ENERGY
        defaultMeterReadingShouldNotBeFound("totalExportActiveEnergy.notEquals=" + DEFAULT_TOTAL_EXPORT_ACTIVE_ENERGY);

        // Get all the meterReadingList where totalExportActiveEnergy not equals to UPDATED_TOTAL_EXPORT_ACTIVE_ENERGY
        defaultMeterReadingShouldBeFound("totalExportActiveEnergy.notEquals=" + UPDATED_TOTAL_EXPORT_ACTIVE_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalExportActiveEnergyIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalExportActiveEnergy in DEFAULT_TOTAL_EXPORT_ACTIVE_ENERGY or UPDATED_TOTAL_EXPORT_ACTIVE_ENERGY
        defaultMeterReadingShouldBeFound(
            "totalExportActiveEnergy.in=" + DEFAULT_TOTAL_EXPORT_ACTIVE_ENERGY + "," + UPDATED_TOTAL_EXPORT_ACTIVE_ENERGY
        );

        // Get all the meterReadingList where totalExportActiveEnergy equals to UPDATED_TOTAL_EXPORT_ACTIVE_ENERGY
        defaultMeterReadingShouldNotBeFound("totalExportActiveEnergy.in=" + UPDATED_TOTAL_EXPORT_ACTIVE_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalExportActiveEnergyIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalExportActiveEnergy is not null
        defaultMeterReadingShouldBeFound("totalExportActiveEnergy.specified=true");

        // Get all the meterReadingList where totalExportActiveEnergy is null
        defaultMeterReadingShouldNotBeFound("totalExportActiveEnergy.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalExportActiveEnergyContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalExportActiveEnergy contains DEFAULT_TOTAL_EXPORT_ACTIVE_ENERGY
        defaultMeterReadingShouldBeFound("totalExportActiveEnergy.contains=" + DEFAULT_TOTAL_EXPORT_ACTIVE_ENERGY);

        // Get all the meterReadingList where totalExportActiveEnergy contains UPDATED_TOTAL_EXPORT_ACTIVE_ENERGY
        defaultMeterReadingShouldNotBeFound("totalExportActiveEnergy.contains=" + UPDATED_TOTAL_EXPORT_ACTIVE_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalExportActiveEnergyNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalExportActiveEnergy does not contain DEFAULT_TOTAL_EXPORT_ACTIVE_ENERGY
        defaultMeterReadingShouldNotBeFound("totalExportActiveEnergy.doesNotContain=" + DEFAULT_TOTAL_EXPORT_ACTIVE_ENERGY);

        // Get all the meterReadingList where totalExportActiveEnergy does not contain UPDATED_TOTAL_EXPORT_ACTIVE_ENERGY
        defaultMeterReadingShouldBeFound("totalExportActiveEnergy.doesNotContain=" + UPDATED_TOTAL_EXPORT_ACTIVE_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalImportReactiveEnergyIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalImportReactiveEnergy equals to DEFAULT_TOTAL_IMPORT_REACTIVE_ENERGY
        defaultMeterReadingShouldBeFound("totalImportReactiveEnergy.equals=" + DEFAULT_TOTAL_IMPORT_REACTIVE_ENERGY);

        // Get all the meterReadingList where totalImportReactiveEnergy equals to UPDATED_TOTAL_IMPORT_REACTIVE_ENERGY
        defaultMeterReadingShouldNotBeFound("totalImportReactiveEnergy.equals=" + UPDATED_TOTAL_IMPORT_REACTIVE_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalImportReactiveEnergyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalImportReactiveEnergy not equals to DEFAULT_TOTAL_IMPORT_REACTIVE_ENERGY
        defaultMeterReadingShouldNotBeFound("totalImportReactiveEnergy.notEquals=" + DEFAULT_TOTAL_IMPORT_REACTIVE_ENERGY);

        // Get all the meterReadingList where totalImportReactiveEnergy not equals to UPDATED_TOTAL_IMPORT_REACTIVE_ENERGY
        defaultMeterReadingShouldBeFound("totalImportReactiveEnergy.notEquals=" + UPDATED_TOTAL_IMPORT_REACTIVE_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalImportReactiveEnergyIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalImportReactiveEnergy in DEFAULT_TOTAL_IMPORT_REACTIVE_ENERGY or UPDATED_TOTAL_IMPORT_REACTIVE_ENERGY
        defaultMeterReadingShouldBeFound(
            "totalImportReactiveEnergy.in=" + DEFAULT_TOTAL_IMPORT_REACTIVE_ENERGY + "," + UPDATED_TOTAL_IMPORT_REACTIVE_ENERGY
        );

        // Get all the meterReadingList where totalImportReactiveEnergy equals to UPDATED_TOTAL_IMPORT_REACTIVE_ENERGY
        defaultMeterReadingShouldNotBeFound("totalImportReactiveEnergy.in=" + UPDATED_TOTAL_IMPORT_REACTIVE_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalImportReactiveEnergyIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalImportReactiveEnergy is not null
        defaultMeterReadingShouldBeFound("totalImportReactiveEnergy.specified=true");

        // Get all the meterReadingList where totalImportReactiveEnergy is null
        defaultMeterReadingShouldNotBeFound("totalImportReactiveEnergy.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalImportReactiveEnergyContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalImportReactiveEnergy contains DEFAULT_TOTAL_IMPORT_REACTIVE_ENERGY
        defaultMeterReadingShouldBeFound("totalImportReactiveEnergy.contains=" + DEFAULT_TOTAL_IMPORT_REACTIVE_ENERGY);

        // Get all the meterReadingList where totalImportReactiveEnergy contains UPDATED_TOTAL_IMPORT_REACTIVE_ENERGY
        defaultMeterReadingShouldNotBeFound("totalImportReactiveEnergy.contains=" + UPDATED_TOTAL_IMPORT_REACTIVE_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalImportReactiveEnergyNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalImportReactiveEnergy does not contain DEFAULT_TOTAL_IMPORT_REACTIVE_ENERGY
        defaultMeterReadingShouldNotBeFound("totalImportReactiveEnergy.doesNotContain=" + DEFAULT_TOTAL_IMPORT_REACTIVE_ENERGY);

        // Get all the meterReadingList where totalImportReactiveEnergy does not contain UPDATED_TOTAL_IMPORT_REACTIVE_ENERGY
        defaultMeterReadingShouldBeFound("totalImportReactiveEnergy.doesNotContain=" + UPDATED_TOTAL_IMPORT_REACTIVE_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalExportReactiveEnergyIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalExportReactiveEnergy equals to DEFAULT_TOTAL_EXPORT_REACTIVE_ENERGY
        defaultMeterReadingShouldBeFound("totalExportReactiveEnergy.equals=" + DEFAULT_TOTAL_EXPORT_REACTIVE_ENERGY);

        // Get all the meterReadingList where totalExportReactiveEnergy equals to UPDATED_TOTAL_EXPORT_REACTIVE_ENERGY
        defaultMeterReadingShouldNotBeFound("totalExportReactiveEnergy.equals=" + UPDATED_TOTAL_EXPORT_REACTIVE_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalExportReactiveEnergyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalExportReactiveEnergy not equals to DEFAULT_TOTAL_EXPORT_REACTIVE_ENERGY
        defaultMeterReadingShouldNotBeFound("totalExportReactiveEnergy.notEquals=" + DEFAULT_TOTAL_EXPORT_REACTIVE_ENERGY);

        // Get all the meterReadingList where totalExportReactiveEnergy not equals to UPDATED_TOTAL_EXPORT_REACTIVE_ENERGY
        defaultMeterReadingShouldBeFound("totalExportReactiveEnergy.notEquals=" + UPDATED_TOTAL_EXPORT_REACTIVE_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalExportReactiveEnergyIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalExportReactiveEnergy in DEFAULT_TOTAL_EXPORT_REACTIVE_ENERGY or UPDATED_TOTAL_EXPORT_REACTIVE_ENERGY
        defaultMeterReadingShouldBeFound(
            "totalExportReactiveEnergy.in=" + DEFAULT_TOTAL_EXPORT_REACTIVE_ENERGY + "," + UPDATED_TOTAL_EXPORT_REACTIVE_ENERGY
        );

        // Get all the meterReadingList where totalExportReactiveEnergy equals to UPDATED_TOTAL_EXPORT_REACTIVE_ENERGY
        defaultMeterReadingShouldNotBeFound("totalExportReactiveEnergy.in=" + UPDATED_TOTAL_EXPORT_REACTIVE_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalExportReactiveEnergyIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalExportReactiveEnergy is not null
        defaultMeterReadingShouldBeFound("totalExportReactiveEnergy.specified=true");

        // Get all the meterReadingList where totalExportReactiveEnergy is null
        defaultMeterReadingShouldNotBeFound("totalExportReactiveEnergy.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalExportReactiveEnergyContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalExportReactiveEnergy contains DEFAULT_TOTAL_EXPORT_REACTIVE_ENERGY
        defaultMeterReadingShouldBeFound("totalExportReactiveEnergy.contains=" + DEFAULT_TOTAL_EXPORT_REACTIVE_ENERGY);

        // Get all the meterReadingList where totalExportReactiveEnergy contains UPDATED_TOTAL_EXPORT_REACTIVE_ENERGY
        defaultMeterReadingShouldNotBeFound("totalExportReactiveEnergy.contains=" + UPDATED_TOTAL_EXPORT_REACTIVE_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalExportReactiveEnergyNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalExportReactiveEnergy does not contain DEFAULT_TOTAL_EXPORT_REACTIVE_ENERGY
        defaultMeterReadingShouldNotBeFound("totalExportReactiveEnergy.doesNotContain=" + DEFAULT_TOTAL_EXPORT_REACTIVE_ENERGY);

        // Get all the meterReadingList where totalExportReactiveEnergy does not contain UPDATED_TOTAL_EXPORT_REACTIVE_ENERGY
        defaultMeterReadingShouldBeFound("totalExportReactiveEnergy.doesNotContain=" + UPDATED_TOTAL_EXPORT_REACTIVE_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalImportApparentEnergyIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalImportApparentEnergy equals to DEFAULT_TOTAL_IMPORT_APPARENT_ENERGY
        defaultMeterReadingShouldBeFound("totalImportApparentEnergy.equals=" + DEFAULT_TOTAL_IMPORT_APPARENT_ENERGY);

        // Get all the meterReadingList where totalImportApparentEnergy equals to UPDATED_TOTAL_IMPORT_APPARENT_ENERGY
        defaultMeterReadingShouldNotBeFound("totalImportApparentEnergy.equals=" + UPDATED_TOTAL_IMPORT_APPARENT_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalImportApparentEnergyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalImportApparentEnergy not equals to DEFAULT_TOTAL_IMPORT_APPARENT_ENERGY
        defaultMeterReadingShouldNotBeFound("totalImportApparentEnergy.notEquals=" + DEFAULT_TOTAL_IMPORT_APPARENT_ENERGY);

        // Get all the meterReadingList where totalImportApparentEnergy not equals to UPDATED_TOTAL_IMPORT_APPARENT_ENERGY
        defaultMeterReadingShouldBeFound("totalImportApparentEnergy.notEquals=" + UPDATED_TOTAL_IMPORT_APPARENT_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalImportApparentEnergyIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalImportApparentEnergy in DEFAULT_TOTAL_IMPORT_APPARENT_ENERGY or UPDATED_TOTAL_IMPORT_APPARENT_ENERGY
        defaultMeterReadingShouldBeFound(
            "totalImportApparentEnergy.in=" + DEFAULT_TOTAL_IMPORT_APPARENT_ENERGY + "," + UPDATED_TOTAL_IMPORT_APPARENT_ENERGY
        );

        // Get all the meterReadingList where totalImportApparentEnergy equals to UPDATED_TOTAL_IMPORT_APPARENT_ENERGY
        defaultMeterReadingShouldNotBeFound("totalImportApparentEnergy.in=" + UPDATED_TOTAL_IMPORT_APPARENT_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalImportApparentEnergyIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalImportApparentEnergy is not null
        defaultMeterReadingShouldBeFound("totalImportApparentEnergy.specified=true");

        // Get all the meterReadingList where totalImportApparentEnergy is null
        defaultMeterReadingShouldNotBeFound("totalImportApparentEnergy.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalImportApparentEnergyContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalImportApparentEnergy contains DEFAULT_TOTAL_IMPORT_APPARENT_ENERGY
        defaultMeterReadingShouldBeFound("totalImportApparentEnergy.contains=" + DEFAULT_TOTAL_IMPORT_APPARENT_ENERGY);

        // Get all the meterReadingList where totalImportApparentEnergy contains UPDATED_TOTAL_IMPORT_APPARENT_ENERGY
        defaultMeterReadingShouldNotBeFound("totalImportApparentEnergy.contains=" + UPDATED_TOTAL_IMPORT_APPARENT_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalImportApparentEnergyNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalImportApparentEnergy does not contain DEFAULT_TOTAL_IMPORT_APPARENT_ENERGY
        defaultMeterReadingShouldNotBeFound("totalImportApparentEnergy.doesNotContain=" + DEFAULT_TOTAL_IMPORT_APPARENT_ENERGY);

        // Get all the meterReadingList where totalImportApparentEnergy does not contain UPDATED_TOTAL_IMPORT_APPARENT_ENERGY
        defaultMeterReadingShouldBeFound("totalImportApparentEnergy.doesNotContain=" + UPDATED_TOTAL_IMPORT_APPARENT_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalExportApparentEnergyIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalExportApparentEnergy equals to DEFAULT_TOTAL_EXPORT_APPARENT_ENERGY
        defaultMeterReadingShouldBeFound("totalExportApparentEnergy.equals=" + DEFAULT_TOTAL_EXPORT_APPARENT_ENERGY);

        // Get all the meterReadingList where totalExportApparentEnergy equals to UPDATED_TOTAL_EXPORT_APPARENT_ENERGY
        defaultMeterReadingShouldNotBeFound("totalExportApparentEnergy.equals=" + UPDATED_TOTAL_EXPORT_APPARENT_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalExportApparentEnergyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalExportApparentEnergy not equals to DEFAULT_TOTAL_EXPORT_APPARENT_ENERGY
        defaultMeterReadingShouldNotBeFound("totalExportApparentEnergy.notEquals=" + DEFAULT_TOTAL_EXPORT_APPARENT_ENERGY);

        // Get all the meterReadingList where totalExportApparentEnergy not equals to UPDATED_TOTAL_EXPORT_APPARENT_ENERGY
        defaultMeterReadingShouldBeFound("totalExportApparentEnergy.notEquals=" + UPDATED_TOTAL_EXPORT_APPARENT_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalExportApparentEnergyIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalExportApparentEnergy in DEFAULT_TOTAL_EXPORT_APPARENT_ENERGY or UPDATED_TOTAL_EXPORT_APPARENT_ENERGY
        defaultMeterReadingShouldBeFound(
            "totalExportApparentEnergy.in=" + DEFAULT_TOTAL_EXPORT_APPARENT_ENERGY + "," + UPDATED_TOTAL_EXPORT_APPARENT_ENERGY
        );

        // Get all the meterReadingList where totalExportApparentEnergy equals to UPDATED_TOTAL_EXPORT_APPARENT_ENERGY
        defaultMeterReadingShouldNotBeFound("totalExportApparentEnergy.in=" + UPDATED_TOTAL_EXPORT_APPARENT_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalExportApparentEnergyIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalExportApparentEnergy is not null
        defaultMeterReadingShouldBeFound("totalExportApparentEnergy.specified=true");

        // Get all the meterReadingList where totalExportApparentEnergy is null
        defaultMeterReadingShouldNotBeFound("totalExportApparentEnergy.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalExportApparentEnergyContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalExportApparentEnergy contains DEFAULT_TOTAL_EXPORT_APPARENT_ENERGY
        defaultMeterReadingShouldBeFound("totalExportApparentEnergy.contains=" + DEFAULT_TOTAL_EXPORT_APPARENT_ENERGY);

        // Get all the meterReadingList where totalExportApparentEnergy contains UPDATED_TOTAL_EXPORT_APPARENT_ENERGY
        defaultMeterReadingShouldNotBeFound("totalExportApparentEnergy.contains=" + UPDATED_TOTAL_EXPORT_APPARENT_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalExportApparentEnergyNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalExportApparentEnergy does not contain DEFAULT_TOTAL_EXPORT_APPARENT_ENERGY
        defaultMeterReadingShouldNotBeFound("totalExportApparentEnergy.doesNotContain=" + DEFAULT_TOTAL_EXPORT_APPARENT_ENERGY);

        // Get all the meterReadingList where totalExportApparentEnergy does not contain UPDATED_TOTAL_EXPORT_APPARENT_ENERGY
        defaultMeterReadingShouldBeFound("totalExportApparentEnergy.doesNotContain=" + UPDATED_TOTAL_EXPORT_APPARENT_ENERGY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalInstantaneousActivePowerIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalInstantaneousActivePower equals to DEFAULT_TOTAL_INSTANTANEOUS_ACTIVE_POWER
        defaultMeterReadingShouldBeFound("totalInstantaneousActivePower.equals=" + DEFAULT_TOTAL_INSTANTANEOUS_ACTIVE_POWER);

        // Get all the meterReadingList where totalInstantaneousActivePower equals to UPDATED_TOTAL_INSTANTANEOUS_ACTIVE_POWER
        defaultMeterReadingShouldNotBeFound("totalInstantaneousActivePower.equals=" + UPDATED_TOTAL_INSTANTANEOUS_ACTIVE_POWER);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalInstantaneousActivePowerIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalInstantaneousActivePower not equals to DEFAULT_TOTAL_INSTANTANEOUS_ACTIVE_POWER
        defaultMeterReadingShouldNotBeFound("totalInstantaneousActivePower.notEquals=" + DEFAULT_TOTAL_INSTANTANEOUS_ACTIVE_POWER);

        // Get all the meterReadingList where totalInstantaneousActivePower not equals to UPDATED_TOTAL_INSTANTANEOUS_ACTIVE_POWER
        defaultMeterReadingShouldBeFound("totalInstantaneousActivePower.notEquals=" + UPDATED_TOTAL_INSTANTANEOUS_ACTIVE_POWER);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalInstantaneousActivePowerIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalInstantaneousActivePower in DEFAULT_TOTAL_INSTANTANEOUS_ACTIVE_POWER or UPDATED_TOTAL_INSTANTANEOUS_ACTIVE_POWER
        defaultMeterReadingShouldBeFound(
            "totalInstantaneousActivePower.in=" + DEFAULT_TOTAL_INSTANTANEOUS_ACTIVE_POWER + "," + UPDATED_TOTAL_INSTANTANEOUS_ACTIVE_POWER
        );

        // Get all the meterReadingList where totalInstantaneousActivePower equals to UPDATED_TOTAL_INSTANTANEOUS_ACTIVE_POWER
        defaultMeterReadingShouldNotBeFound("totalInstantaneousActivePower.in=" + UPDATED_TOTAL_INSTANTANEOUS_ACTIVE_POWER);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalInstantaneousActivePowerIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalInstantaneousActivePower is not null
        defaultMeterReadingShouldBeFound("totalInstantaneousActivePower.specified=true");

        // Get all the meterReadingList where totalInstantaneousActivePower is null
        defaultMeterReadingShouldNotBeFound("totalInstantaneousActivePower.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalInstantaneousActivePowerContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalInstantaneousActivePower contains DEFAULT_TOTAL_INSTANTANEOUS_ACTIVE_POWER
        defaultMeterReadingShouldBeFound("totalInstantaneousActivePower.contains=" + DEFAULT_TOTAL_INSTANTANEOUS_ACTIVE_POWER);

        // Get all the meterReadingList where totalInstantaneousActivePower contains UPDATED_TOTAL_INSTANTANEOUS_ACTIVE_POWER
        defaultMeterReadingShouldNotBeFound("totalInstantaneousActivePower.contains=" + UPDATED_TOTAL_INSTANTANEOUS_ACTIVE_POWER);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalInstantaneousActivePowerNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalInstantaneousActivePower does not contain DEFAULT_TOTAL_INSTANTANEOUS_ACTIVE_POWER
        defaultMeterReadingShouldNotBeFound("totalInstantaneousActivePower.doesNotContain=" + DEFAULT_TOTAL_INSTANTANEOUS_ACTIVE_POWER);

        // Get all the meterReadingList where totalInstantaneousActivePower does not contain UPDATED_TOTAL_INSTANTANEOUS_ACTIVE_POWER
        defaultMeterReadingShouldBeFound("totalInstantaneousActivePower.doesNotContain=" + UPDATED_TOTAL_INSTANTANEOUS_ACTIVE_POWER);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalInstantaneousReactivePowerIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalInstantaneousReactivePower equals to DEFAULT_TOTAL_INSTANTANEOUS_REACTIVE_POWER
        defaultMeterReadingShouldBeFound("totalInstantaneousReactivePower.equals=" + DEFAULT_TOTAL_INSTANTANEOUS_REACTIVE_POWER);

        // Get all the meterReadingList where totalInstantaneousReactivePower equals to UPDATED_TOTAL_INSTANTANEOUS_REACTIVE_POWER
        defaultMeterReadingShouldNotBeFound("totalInstantaneousReactivePower.equals=" + UPDATED_TOTAL_INSTANTANEOUS_REACTIVE_POWER);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalInstantaneousReactivePowerIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalInstantaneousReactivePower not equals to DEFAULT_TOTAL_INSTANTANEOUS_REACTIVE_POWER
        defaultMeterReadingShouldNotBeFound("totalInstantaneousReactivePower.notEquals=" + DEFAULT_TOTAL_INSTANTANEOUS_REACTIVE_POWER);

        // Get all the meterReadingList where totalInstantaneousReactivePower not equals to UPDATED_TOTAL_INSTANTANEOUS_REACTIVE_POWER
        defaultMeterReadingShouldBeFound("totalInstantaneousReactivePower.notEquals=" + UPDATED_TOTAL_INSTANTANEOUS_REACTIVE_POWER);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalInstantaneousReactivePowerIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalInstantaneousReactivePower in DEFAULT_TOTAL_INSTANTANEOUS_REACTIVE_POWER or UPDATED_TOTAL_INSTANTANEOUS_REACTIVE_POWER
        defaultMeterReadingShouldBeFound(
            "totalInstantaneousReactivePower.in=" +
            DEFAULT_TOTAL_INSTANTANEOUS_REACTIVE_POWER +
            "," +
            UPDATED_TOTAL_INSTANTANEOUS_REACTIVE_POWER
        );

        // Get all the meterReadingList where totalInstantaneousReactivePower equals to UPDATED_TOTAL_INSTANTANEOUS_REACTIVE_POWER
        defaultMeterReadingShouldNotBeFound("totalInstantaneousReactivePower.in=" + UPDATED_TOTAL_INSTANTANEOUS_REACTIVE_POWER);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalInstantaneousReactivePowerIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalInstantaneousReactivePower is not null
        defaultMeterReadingShouldBeFound("totalInstantaneousReactivePower.specified=true");

        // Get all the meterReadingList where totalInstantaneousReactivePower is null
        defaultMeterReadingShouldNotBeFound("totalInstantaneousReactivePower.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalInstantaneousReactivePowerContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalInstantaneousReactivePower contains DEFAULT_TOTAL_INSTANTANEOUS_REACTIVE_POWER
        defaultMeterReadingShouldBeFound("totalInstantaneousReactivePower.contains=" + DEFAULT_TOTAL_INSTANTANEOUS_REACTIVE_POWER);

        // Get all the meterReadingList where totalInstantaneousReactivePower contains UPDATED_TOTAL_INSTANTANEOUS_REACTIVE_POWER
        defaultMeterReadingShouldNotBeFound("totalInstantaneousReactivePower.contains=" + UPDATED_TOTAL_INSTANTANEOUS_REACTIVE_POWER);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalInstantaneousReactivePowerNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalInstantaneousReactivePower does not contain DEFAULT_TOTAL_INSTANTANEOUS_REACTIVE_POWER
        defaultMeterReadingShouldNotBeFound("totalInstantaneousReactivePower.doesNotContain=" + DEFAULT_TOTAL_INSTANTANEOUS_REACTIVE_POWER);

        // Get all the meterReadingList where totalInstantaneousReactivePower does not contain UPDATED_TOTAL_INSTANTANEOUS_REACTIVE_POWER
        defaultMeterReadingShouldBeFound("totalInstantaneousReactivePower.doesNotContain=" + UPDATED_TOTAL_INSTANTANEOUS_REACTIVE_POWER);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalInstantaneousApparentPowerIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalInstantaneousApparentPower equals to DEFAULT_TOTAL_INSTANTANEOUS_APPARENT_POWER
        defaultMeterReadingShouldBeFound("totalInstantaneousApparentPower.equals=" + DEFAULT_TOTAL_INSTANTANEOUS_APPARENT_POWER);

        // Get all the meterReadingList where totalInstantaneousApparentPower equals to UPDATED_TOTAL_INSTANTANEOUS_APPARENT_POWER
        defaultMeterReadingShouldNotBeFound("totalInstantaneousApparentPower.equals=" + UPDATED_TOTAL_INSTANTANEOUS_APPARENT_POWER);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalInstantaneousApparentPowerIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalInstantaneousApparentPower not equals to DEFAULT_TOTAL_INSTANTANEOUS_APPARENT_POWER
        defaultMeterReadingShouldNotBeFound("totalInstantaneousApparentPower.notEquals=" + DEFAULT_TOTAL_INSTANTANEOUS_APPARENT_POWER);

        // Get all the meterReadingList where totalInstantaneousApparentPower not equals to UPDATED_TOTAL_INSTANTANEOUS_APPARENT_POWER
        defaultMeterReadingShouldBeFound("totalInstantaneousApparentPower.notEquals=" + UPDATED_TOTAL_INSTANTANEOUS_APPARENT_POWER);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalInstantaneousApparentPowerIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalInstantaneousApparentPower in DEFAULT_TOTAL_INSTANTANEOUS_APPARENT_POWER or UPDATED_TOTAL_INSTANTANEOUS_APPARENT_POWER
        defaultMeterReadingShouldBeFound(
            "totalInstantaneousApparentPower.in=" +
            DEFAULT_TOTAL_INSTANTANEOUS_APPARENT_POWER +
            "," +
            UPDATED_TOTAL_INSTANTANEOUS_APPARENT_POWER
        );

        // Get all the meterReadingList where totalInstantaneousApparentPower equals to UPDATED_TOTAL_INSTANTANEOUS_APPARENT_POWER
        defaultMeterReadingShouldNotBeFound("totalInstantaneousApparentPower.in=" + UPDATED_TOTAL_INSTANTANEOUS_APPARENT_POWER);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalInstantaneousApparentPowerIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalInstantaneousApparentPower is not null
        defaultMeterReadingShouldBeFound("totalInstantaneousApparentPower.specified=true");

        // Get all the meterReadingList where totalInstantaneousApparentPower is null
        defaultMeterReadingShouldNotBeFound("totalInstantaneousApparentPower.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalInstantaneousApparentPowerContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalInstantaneousApparentPower contains DEFAULT_TOTAL_INSTANTANEOUS_APPARENT_POWER
        defaultMeterReadingShouldBeFound("totalInstantaneousApparentPower.contains=" + DEFAULT_TOTAL_INSTANTANEOUS_APPARENT_POWER);

        // Get all the meterReadingList where totalInstantaneousApparentPower contains UPDATED_TOTAL_INSTANTANEOUS_APPARENT_POWER
        defaultMeterReadingShouldNotBeFound("totalInstantaneousApparentPower.contains=" + UPDATED_TOTAL_INSTANTANEOUS_APPARENT_POWER);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalInstantaneousApparentPowerNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalInstantaneousApparentPower does not contain DEFAULT_TOTAL_INSTANTANEOUS_APPARENT_POWER
        defaultMeterReadingShouldNotBeFound("totalInstantaneousApparentPower.doesNotContain=" + DEFAULT_TOTAL_INSTANTANEOUS_APPARENT_POWER);

        // Get all the meterReadingList where totalInstantaneousApparentPower does not contain UPDATED_TOTAL_INSTANTANEOUS_APPARENT_POWER
        defaultMeterReadingShouldBeFound("totalInstantaneousApparentPower.doesNotContain=" + UPDATED_TOTAL_INSTANTANEOUS_APPARENT_POWER);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalPowerFactorIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalPowerFactor equals to DEFAULT_TOTAL_POWER_FACTOR
        defaultMeterReadingShouldBeFound("totalPowerFactor.equals=" + DEFAULT_TOTAL_POWER_FACTOR);

        // Get all the meterReadingList where totalPowerFactor equals to UPDATED_TOTAL_POWER_FACTOR
        defaultMeterReadingShouldNotBeFound("totalPowerFactor.equals=" + UPDATED_TOTAL_POWER_FACTOR);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalPowerFactorIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalPowerFactor not equals to DEFAULT_TOTAL_POWER_FACTOR
        defaultMeterReadingShouldNotBeFound("totalPowerFactor.notEquals=" + DEFAULT_TOTAL_POWER_FACTOR);

        // Get all the meterReadingList where totalPowerFactor not equals to UPDATED_TOTAL_POWER_FACTOR
        defaultMeterReadingShouldBeFound("totalPowerFactor.notEquals=" + UPDATED_TOTAL_POWER_FACTOR);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalPowerFactorIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalPowerFactor in DEFAULT_TOTAL_POWER_FACTOR or UPDATED_TOTAL_POWER_FACTOR
        defaultMeterReadingShouldBeFound("totalPowerFactor.in=" + DEFAULT_TOTAL_POWER_FACTOR + "," + UPDATED_TOTAL_POWER_FACTOR);

        // Get all the meterReadingList where totalPowerFactor equals to UPDATED_TOTAL_POWER_FACTOR
        defaultMeterReadingShouldNotBeFound("totalPowerFactor.in=" + UPDATED_TOTAL_POWER_FACTOR);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalPowerFactorIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalPowerFactor is not null
        defaultMeterReadingShouldBeFound("totalPowerFactor.specified=true");

        // Get all the meterReadingList where totalPowerFactor is null
        defaultMeterReadingShouldNotBeFound("totalPowerFactor.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalPowerFactorContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalPowerFactor contains DEFAULT_TOTAL_POWER_FACTOR
        defaultMeterReadingShouldBeFound("totalPowerFactor.contains=" + DEFAULT_TOTAL_POWER_FACTOR);

        // Get all the meterReadingList where totalPowerFactor contains UPDATED_TOTAL_POWER_FACTOR
        defaultMeterReadingShouldNotBeFound("totalPowerFactor.contains=" + UPDATED_TOTAL_POWER_FACTOR);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByTotalPowerFactorNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where totalPowerFactor does not contain DEFAULT_TOTAL_POWER_FACTOR
        defaultMeterReadingShouldNotBeFound("totalPowerFactor.doesNotContain=" + DEFAULT_TOTAL_POWER_FACTOR);

        // Get all the meterReadingList where totalPowerFactor does not contain UPDATED_TOTAL_POWER_FACTOR
        defaultMeterReadingShouldBeFound("totalPowerFactor.doesNotContain=" + UPDATED_TOTAL_POWER_FACTOR);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where status equals to DEFAULT_STATUS
        defaultMeterReadingShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the meterReadingList where status equals to UPDATED_STATUS
        defaultMeterReadingShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where status not equals to DEFAULT_STATUS
        defaultMeterReadingShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the meterReadingList where status not equals to UPDATED_STATUS
        defaultMeterReadingShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultMeterReadingShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the meterReadingList where status equals to UPDATED_STATUS
        defaultMeterReadingShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where status is not null
        defaultMeterReadingShouldBeFound("status.specified=true");

        // Get all the meterReadingList where status is null
        defaultMeterReadingShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByStatusContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where status contains DEFAULT_STATUS
        defaultMeterReadingShouldBeFound("status.contains=" + DEFAULT_STATUS);

        // Get all the meterReadingList where status contains UPDATED_STATUS
        defaultMeterReadingShouldNotBeFound("status.contains=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByStatusNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where status does not contain DEFAULT_STATUS
        defaultMeterReadingShouldNotBeFound("status.doesNotContain=" + DEFAULT_STATUS);

        // Get all the meterReadingList where status does not contain UPDATED_STATUS
        defaultMeterReadingShouldBeFound("status.doesNotContain=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReadOnIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where readOn equals to DEFAULT_READ_ON
        defaultMeterReadingShouldBeFound("readOn.equals=" + DEFAULT_READ_ON);

        // Get all the meterReadingList where readOn equals to UPDATED_READ_ON
        defaultMeterReadingShouldNotBeFound("readOn.equals=" + UPDATED_READ_ON);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReadOnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where readOn not equals to DEFAULT_READ_ON
        defaultMeterReadingShouldNotBeFound("readOn.notEquals=" + DEFAULT_READ_ON);

        // Get all the meterReadingList where readOn not equals to UPDATED_READ_ON
        defaultMeterReadingShouldBeFound("readOn.notEquals=" + UPDATED_READ_ON);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReadOnIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where readOn in DEFAULT_READ_ON or UPDATED_READ_ON
        defaultMeterReadingShouldBeFound("readOn.in=" + DEFAULT_READ_ON + "," + UPDATED_READ_ON);

        // Get all the meterReadingList where readOn equals to UPDATED_READ_ON
        defaultMeterReadingShouldNotBeFound("readOn.in=" + UPDATED_READ_ON);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReadOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where readOn is not null
        defaultMeterReadingShouldBeFound("readOn.specified=true");

        // Get all the meterReadingList where readOn is null
        defaultMeterReadingShouldNotBeFound("readOn.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportActiveEnergy1IsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importActiveEnergy1 equals to DEFAULT_IMPORT_ACTIVE_ENERGY_1
        defaultMeterReadingShouldBeFound("importActiveEnergy1.equals=" + DEFAULT_IMPORT_ACTIVE_ENERGY_1);

        // Get all the meterReadingList where importActiveEnergy1 equals to UPDATED_IMPORT_ACTIVE_ENERGY_1
        defaultMeterReadingShouldNotBeFound("importActiveEnergy1.equals=" + UPDATED_IMPORT_ACTIVE_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportActiveEnergy1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importActiveEnergy1 not equals to DEFAULT_IMPORT_ACTIVE_ENERGY_1
        defaultMeterReadingShouldNotBeFound("importActiveEnergy1.notEquals=" + DEFAULT_IMPORT_ACTIVE_ENERGY_1);

        // Get all the meterReadingList where importActiveEnergy1 not equals to UPDATED_IMPORT_ACTIVE_ENERGY_1
        defaultMeterReadingShouldBeFound("importActiveEnergy1.notEquals=" + UPDATED_IMPORT_ACTIVE_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportActiveEnergy1IsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importActiveEnergy1 in DEFAULT_IMPORT_ACTIVE_ENERGY_1 or UPDATED_IMPORT_ACTIVE_ENERGY_1
        defaultMeterReadingShouldBeFound("importActiveEnergy1.in=" + DEFAULT_IMPORT_ACTIVE_ENERGY_1 + "," + UPDATED_IMPORT_ACTIVE_ENERGY_1);

        // Get all the meterReadingList where importActiveEnergy1 equals to UPDATED_IMPORT_ACTIVE_ENERGY_1
        defaultMeterReadingShouldNotBeFound("importActiveEnergy1.in=" + UPDATED_IMPORT_ACTIVE_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportActiveEnergy1IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importActiveEnergy1 is not null
        defaultMeterReadingShouldBeFound("importActiveEnergy1.specified=true");

        // Get all the meterReadingList where importActiveEnergy1 is null
        defaultMeterReadingShouldNotBeFound("importActiveEnergy1.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportActiveEnergy1ContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importActiveEnergy1 contains DEFAULT_IMPORT_ACTIVE_ENERGY_1
        defaultMeterReadingShouldBeFound("importActiveEnergy1.contains=" + DEFAULT_IMPORT_ACTIVE_ENERGY_1);

        // Get all the meterReadingList where importActiveEnergy1 contains UPDATED_IMPORT_ACTIVE_ENERGY_1
        defaultMeterReadingShouldNotBeFound("importActiveEnergy1.contains=" + UPDATED_IMPORT_ACTIVE_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportActiveEnergy1NotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importActiveEnergy1 does not contain DEFAULT_IMPORT_ACTIVE_ENERGY_1
        defaultMeterReadingShouldNotBeFound("importActiveEnergy1.doesNotContain=" + DEFAULT_IMPORT_ACTIVE_ENERGY_1);

        // Get all the meterReadingList where importActiveEnergy1 does not contain UPDATED_IMPORT_ACTIVE_ENERGY_1
        defaultMeterReadingShouldBeFound("importActiveEnergy1.doesNotContain=" + UPDATED_IMPORT_ACTIVE_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportActiveEnergy1IsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportActiveEnergy1 equals to DEFAULT_EXPORT_ACTIVE_ENERGY_1
        defaultMeterReadingShouldBeFound("exportActiveEnergy1.equals=" + DEFAULT_EXPORT_ACTIVE_ENERGY_1);

        // Get all the meterReadingList where exportActiveEnergy1 equals to UPDATED_EXPORT_ACTIVE_ENERGY_1
        defaultMeterReadingShouldNotBeFound("exportActiveEnergy1.equals=" + UPDATED_EXPORT_ACTIVE_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportActiveEnergy1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportActiveEnergy1 not equals to DEFAULT_EXPORT_ACTIVE_ENERGY_1
        defaultMeterReadingShouldNotBeFound("exportActiveEnergy1.notEquals=" + DEFAULT_EXPORT_ACTIVE_ENERGY_1);

        // Get all the meterReadingList where exportActiveEnergy1 not equals to UPDATED_EXPORT_ACTIVE_ENERGY_1
        defaultMeterReadingShouldBeFound("exportActiveEnergy1.notEquals=" + UPDATED_EXPORT_ACTIVE_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportActiveEnergy1IsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportActiveEnergy1 in DEFAULT_EXPORT_ACTIVE_ENERGY_1 or UPDATED_EXPORT_ACTIVE_ENERGY_1
        defaultMeterReadingShouldBeFound("exportActiveEnergy1.in=" + DEFAULT_EXPORT_ACTIVE_ENERGY_1 + "," + UPDATED_EXPORT_ACTIVE_ENERGY_1);

        // Get all the meterReadingList where exportActiveEnergy1 equals to UPDATED_EXPORT_ACTIVE_ENERGY_1
        defaultMeterReadingShouldNotBeFound("exportActiveEnergy1.in=" + UPDATED_EXPORT_ACTIVE_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportActiveEnergy1IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportActiveEnergy1 is not null
        defaultMeterReadingShouldBeFound("exportActiveEnergy1.specified=true");

        // Get all the meterReadingList where exportActiveEnergy1 is null
        defaultMeterReadingShouldNotBeFound("exportActiveEnergy1.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportActiveEnergy1ContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportActiveEnergy1 contains DEFAULT_EXPORT_ACTIVE_ENERGY_1
        defaultMeterReadingShouldBeFound("exportActiveEnergy1.contains=" + DEFAULT_EXPORT_ACTIVE_ENERGY_1);

        // Get all the meterReadingList where exportActiveEnergy1 contains UPDATED_EXPORT_ACTIVE_ENERGY_1
        defaultMeterReadingShouldNotBeFound("exportActiveEnergy1.contains=" + UPDATED_EXPORT_ACTIVE_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportActiveEnergy1NotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportActiveEnergy1 does not contain DEFAULT_EXPORT_ACTIVE_ENERGY_1
        defaultMeterReadingShouldNotBeFound("exportActiveEnergy1.doesNotContain=" + DEFAULT_EXPORT_ACTIVE_ENERGY_1);

        // Get all the meterReadingList where exportActiveEnergy1 does not contain UPDATED_EXPORT_ACTIVE_ENERGY_1
        defaultMeterReadingShouldBeFound("exportActiveEnergy1.doesNotContain=" + UPDATED_EXPORT_ACTIVE_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportReactiveEnergy1IsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importReactiveEnergy1 equals to DEFAULT_IMPORT_REACTIVE_ENERGY_1
        defaultMeterReadingShouldBeFound("importReactiveEnergy1.equals=" + DEFAULT_IMPORT_REACTIVE_ENERGY_1);

        // Get all the meterReadingList where importReactiveEnergy1 equals to UPDATED_IMPORT_REACTIVE_ENERGY_1
        defaultMeterReadingShouldNotBeFound("importReactiveEnergy1.equals=" + UPDATED_IMPORT_REACTIVE_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportReactiveEnergy1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importReactiveEnergy1 not equals to DEFAULT_IMPORT_REACTIVE_ENERGY_1
        defaultMeterReadingShouldNotBeFound("importReactiveEnergy1.notEquals=" + DEFAULT_IMPORT_REACTIVE_ENERGY_1);

        // Get all the meterReadingList where importReactiveEnergy1 not equals to UPDATED_IMPORT_REACTIVE_ENERGY_1
        defaultMeterReadingShouldBeFound("importReactiveEnergy1.notEquals=" + UPDATED_IMPORT_REACTIVE_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportReactiveEnergy1IsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importReactiveEnergy1 in DEFAULT_IMPORT_REACTIVE_ENERGY_1 or UPDATED_IMPORT_REACTIVE_ENERGY_1
        defaultMeterReadingShouldBeFound(
            "importReactiveEnergy1.in=" + DEFAULT_IMPORT_REACTIVE_ENERGY_1 + "," + UPDATED_IMPORT_REACTIVE_ENERGY_1
        );

        // Get all the meterReadingList where importReactiveEnergy1 equals to UPDATED_IMPORT_REACTIVE_ENERGY_1
        defaultMeterReadingShouldNotBeFound("importReactiveEnergy1.in=" + UPDATED_IMPORT_REACTIVE_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportReactiveEnergy1IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importReactiveEnergy1 is not null
        defaultMeterReadingShouldBeFound("importReactiveEnergy1.specified=true");

        // Get all the meterReadingList where importReactiveEnergy1 is null
        defaultMeterReadingShouldNotBeFound("importReactiveEnergy1.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportReactiveEnergy1ContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importReactiveEnergy1 contains DEFAULT_IMPORT_REACTIVE_ENERGY_1
        defaultMeterReadingShouldBeFound("importReactiveEnergy1.contains=" + DEFAULT_IMPORT_REACTIVE_ENERGY_1);

        // Get all the meterReadingList where importReactiveEnergy1 contains UPDATED_IMPORT_REACTIVE_ENERGY_1
        defaultMeterReadingShouldNotBeFound("importReactiveEnergy1.contains=" + UPDATED_IMPORT_REACTIVE_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportReactiveEnergy1NotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importReactiveEnergy1 does not contain DEFAULT_IMPORT_REACTIVE_ENERGY_1
        defaultMeterReadingShouldNotBeFound("importReactiveEnergy1.doesNotContain=" + DEFAULT_IMPORT_REACTIVE_ENERGY_1);

        // Get all the meterReadingList where importReactiveEnergy1 does not contain UPDATED_IMPORT_REACTIVE_ENERGY_1
        defaultMeterReadingShouldBeFound("importReactiveEnergy1.doesNotContain=" + UPDATED_IMPORT_REACTIVE_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportReactiveEnergy1IsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportReactiveEnergy1 equals to DEFAULT_EXPORT_REACTIVE_ENERGY_1
        defaultMeterReadingShouldBeFound("exportReactiveEnergy1.equals=" + DEFAULT_EXPORT_REACTIVE_ENERGY_1);

        // Get all the meterReadingList where exportReactiveEnergy1 equals to UPDATED_EXPORT_REACTIVE_ENERGY_1
        defaultMeterReadingShouldNotBeFound("exportReactiveEnergy1.equals=" + UPDATED_EXPORT_REACTIVE_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportReactiveEnergy1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportReactiveEnergy1 not equals to DEFAULT_EXPORT_REACTIVE_ENERGY_1
        defaultMeterReadingShouldNotBeFound("exportReactiveEnergy1.notEquals=" + DEFAULT_EXPORT_REACTIVE_ENERGY_1);

        // Get all the meterReadingList where exportReactiveEnergy1 not equals to UPDATED_EXPORT_REACTIVE_ENERGY_1
        defaultMeterReadingShouldBeFound("exportReactiveEnergy1.notEquals=" + UPDATED_EXPORT_REACTIVE_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportReactiveEnergy1IsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportReactiveEnergy1 in DEFAULT_EXPORT_REACTIVE_ENERGY_1 or UPDATED_EXPORT_REACTIVE_ENERGY_1
        defaultMeterReadingShouldBeFound(
            "exportReactiveEnergy1.in=" + DEFAULT_EXPORT_REACTIVE_ENERGY_1 + "," + UPDATED_EXPORT_REACTIVE_ENERGY_1
        );

        // Get all the meterReadingList where exportReactiveEnergy1 equals to UPDATED_EXPORT_REACTIVE_ENERGY_1
        defaultMeterReadingShouldNotBeFound("exportReactiveEnergy1.in=" + UPDATED_EXPORT_REACTIVE_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportReactiveEnergy1IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportReactiveEnergy1 is not null
        defaultMeterReadingShouldBeFound("exportReactiveEnergy1.specified=true");

        // Get all the meterReadingList where exportReactiveEnergy1 is null
        defaultMeterReadingShouldNotBeFound("exportReactiveEnergy1.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportReactiveEnergy1ContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportReactiveEnergy1 contains DEFAULT_EXPORT_REACTIVE_ENERGY_1
        defaultMeterReadingShouldBeFound("exportReactiveEnergy1.contains=" + DEFAULT_EXPORT_REACTIVE_ENERGY_1);

        // Get all the meterReadingList where exportReactiveEnergy1 contains UPDATED_EXPORT_REACTIVE_ENERGY_1
        defaultMeterReadingShouldNotBeFound("exportReactiveEnergy1.contains=" + UPDATED_EXPORT_REACTIVE_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportReactiveEnergy1NotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportReactiveEnergy1 does not contain DEFAULT_EXPORT_REACTIVE_ENERGY_1
        defaultMeterReadingShouldNotBeFound("exportReactiveEnergy1.doesNotContain=" + DEFAULT_EXPORT_REACTIVE_ENERGY_1);

        // Get all the meterReadingList where exportReactiveEnergy1 does not contain UPDATED_EXPORT_REACTIVE_ENERGY_1
        defaultMeterReadingShouldBeFound("exportReactiveEnergy1.doesNotContain=" + UPDATED_EXPORT_REACTIVE_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportApparentEnergy1IsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importApparentEnergy1 equals to DEFAULT_IMPORT_APPARENT_ENERGY_1
        defaultMeterReadingShouldBeFound("importApparentEnergy1.equals=" + DEFAULT_IMPORT_APPARENT_ENERGY_1);

        // Get all the meterReadingList where importApparentEnergy1 equals to UPDATED_IMPORT_APPARENT_ENERGY_1
        defaultMeterReadingShouldNotBeFound("importApparentEnergy1.equals=" + UPDATED_IMPORT_APPARENT_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportApparentEnergy1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importApparentEnergy1 not equals to DEFAULT_IMPORT_APPARENT_ENERGY_1
        defaultMeterReadingShouldNotBeFound("importApparentEnergy1.notEquals=" + DEFAULT_IMPORT_APPARENT_ENERGY_1);

        // Get all the meterReadingList where importApparentEnergy1 not equals to UPDATED_IMPORT_APPARENT_ENERGY_1
        defaultMeterReadingShouldBeFound("importApparentEnergy1.notEquals=" + UPDATED_IMPORT_APPARENT_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportApparentEnergy1IsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importApparentEnergy1 in DEFAULT_IMPORT_APPARENT_ENERGY_1 or UPDATED_IMPORT_APPARENT_ENERGY_1
        defaultMeterReadingShouldBeFound(
            "importApparentEnergy1.in=" + DEFAULT_IMPORT_APPARENT_ENERGY_1 + "," + UPDATED_IMPORT_APPARENT_ENERGY_1
        );

        // Get all the meterReadingList where importApparentEnergy1 equals to UPDATED_IMPORT_APPARENT_ENERGY_1
        defaultMeterReadingShouldNotBeFound("importApparentEnergy1.in=" + UPDATED_IMPORT_APPARENT_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportApparentEnergy1IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importApparentEnergy1 is not null
        defaultMeterReadingShouldBeFound("importApparentEnergy1.specified=true");

        // Get all the meterReadingList where importApparentEnergy1 is null
        defaultMeterReadingShouldNotBeFound("importApparentEnergy1.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportApparentEnergy1ContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importApparentEnergy1 contains DEFAULT_IMPORT_APPARENT_ENERGY_1
        defaultMeterReadingShouldBeFound("importApparentEnergy1.contains=" + DEFAULT_IMPORT_APPARENT_ENERGY_1);

        // Get all the meterReadingList where importApparentEnergy1 contains UPDATED_IMPORT_APPARENT_ENERGY_1
        defaultMeterReadingShouldNotBeFound("importApparentEnergy1.contains=" + UPDATED_IMPORT_APPARENT_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportApparentEnergy1NotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importApparentEnergy1 does not contain DEFAULT_IMPORT_APPARENT_ENERGY_1
        defaultMeterReadingShouldNotBeFound("importApparentEnergy1.doesNotContain=" + DEFAULT_IMPORT_APPARENT_ENERGY_1);

        // Get all the meterReadingList where importApparentEnergy1 does not contain UPDATED_IMPORT_APPARENT_ENERGY_1
        defaultMeterReadingShouldBeFound("importApparentEnergy1.doesNotContain=" + UPDATED_IMPORT_APPARENT_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportApparentEnergy1IsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportApparentEnergy1 equals to DEFAULT_EXPORT_APPARENT_ENERGY_1
        defaultMeterReadingShouldBeFound("exportApparentEnergy1.equals=" + DEFAULT_EXPORT_APPARENT_ENERGY_1);

        // Get all the meterReadingList where exportApparentEnergy1 equals to UPDATED_EXPORT_APPARENT_ENERGY_1
        defaultMeterReadingShouldNotBeFound("exportApparentEnergy1.equals=" + UPDATED_EXPORT_APPARENT_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportApparentEnergy1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportApparentEnergy1 not equals to DEFAULT_EXPORT_APPARENT_ENERGY_1
        defaultMeterReadingShouldNotBeFound("exportApparentEnergy1.notEquals=" + DEFAULT_EXPORT_APPARENT_ENERGY_1);

        // Get all the meterReadingList where exportApparentEnergy1 not equals to UPDATED_EXPORT_APPARENT_ENERGY_1
        defaultMeterReadingShouldBeFound("exportApparentEnergy1.notEquals=" + UPDATED_EXPORT_APPARENT_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportApparentEnergy1IsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportApparentEnergy1 in DEFAULT_EXPORT_APPARENT_ENERGY_1 or UPDATED_EXPORT_APPARENT_ENERGY_1
        defaultMeterReadingShouldBeFound(
            "exportApparentEnergy1.in=" + DEFAULT_EXPORT_APPARENT_ENERGY_1 + "," + UPDATED_EXPORT_APPARENT_ENERGY_1
        );

        // Get all the meterReadingList where exportApparentEnergy1 equals to UPDATED_EXPORT_APPARENT_ENERGY_1
        defaultMeterReadingShouldNotBeFound("exportApparentEnergy1.in=" + UPDATED_EXPORT_APPARENT_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportApparentEnergy1IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportApparentEnergy1 is not null
        defaultMeterReadingShouldBeFound("exportApparentEnergy1.specified=true");

        // Get all the meterReadingList where exportApparentEnergy1 is null
        defaultMeterReadingShouldNotBeFound("exportApparentEnergy1.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportApparentEnergy1ContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportApparentEnergy1 contains DEFAULT_EXPORT_APPARENT_ENERGY_1
        defaultMeterReadingShouldBeFound("exportApparentEnergy1.contains=" + DEFAULT_EXPORT_APPARENT_ENERGY_1);

        // Get all the meterReadingList where exportApparentEnergy1 contains UPDATED_EXPORT_APPARENT_ENERGY_1
        defaultMeterReadingShouldNotBeFound("exportApparentEnergy1.contains=" + UPDATED_EXPORT_APPARENT_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportApparentEnergy1NotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportApparentEnergy1 does not contain DEFAULT_EXPORT_APPARENT_ENERGY_1
        defaultMeterReadingShouldNotBeFound("exportApparentEnergy1.doesNotContain=" + DEFAULT_EXPORT_APPARENT_ENERGY_1);

        // Get all the meterReadingList where exportApparentEnergy1 does not contain UPDATED_EXPORT_APPARENT_ENERGY_1
        defaultMeterReadingShouldBeFound("exportApparentEnergy1.doesNotContain=" + UPDATED_EXPORT_APPARENT_ENERGY_1);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportActiveEnergy2IsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importActiveEnergy2 equals to DEFAULT_IMPORT_ACTIVE_ENERGY_2
        defaultMeterReadingShouldBeFound("importActiveEnergy2.equals=" + DEFAULT_IMPORT_ACTIVE_ENERGY_2);

        // Get all the meterReadingList where importActiveEnergy2 equals to UPDATED_IMPORT_ACTIVE_ENERGY_2
        defaultMeterReadingShouldNotBeFound("importActiveEnergy2.equals=" + UPDATED_IMPORT_ACTIVE_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportActiveEnergy2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importActiveEnergy2 not equals to DEFAULT_IMPORT_ACTIVE_ENERGY_2
        defaultMeterReadingShouldNotBeFound("importActiveEnergy2.notEquals=" + DEFAULT_IMPORT_ACTIVE_ENERGY_2);

        // Get all the meterReadingList where importActiveEnergy2 not equals to UPDATED_IMPORT_ACTIVE_ENERGY_2
        defaultMeterReadingShouldBeFound("importActiveEnergy2.notEquals=" + UPDATED_IMPORT_ACTIVE_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportActiveEnergy2IsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importActiveEnergy2 in DEFAULT_IMPORT_ACTIVE_ENERGY_2 or UPDATED_IMPORT_ACTIVE_ENERGY_2
        defaultMeterReadingShouldBeFound("importActiveEnergy2.in=" + DEFAULT_IMPORT_ACTIVE_ENERGY_2 + "," + UPDATED_IMPORT_ACTIVE_ENERGY_2);

        // Get all the meterReadingList where importActiveEnergy2 equals to UPDATED_IMPORT_ACTIVE_ENERGY_2
        defaultMeterReadingShouldNotBeFound("importActiveEnergy2.in=" + UPDATED_IMPORT_ACTIVE_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportActiveEnergy2IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importActiveEnergy2 is not null
        defaultMeterReadingShouldBeFound("importActiveEnergy2.specified=true");

        // Get all the meterReadingList where importActiveEnergy2 is null
        defaultMeterReadingShouldNotBeFound("importActiveEnergy2.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportActiveEnergy2ContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importActiveEnergy2 contains DEFAULT_IMPORT_ACTIVE_ENERGY_2
        defaultMeterReadingShouldBeFound("importActiveEnergy2.contains=" + DEFAULT_IMPORT_ACTIVE_ENERGY_2);

        // Get all the meterReadingList where importActiveEnergy2 contains UPDATED_IMPORT_ACTIVE_ENERGY_2
        defaultMeterReadingShouldNotBeFound("importActiveEnergy2.contains=" + UPDATED_IMPORT_ACTIVE_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportActiveEnergy2NotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importActiveEnergy2 does not contain DEFAULT_IMPORT_ACTIVE_ENERGY_2
        defaultMeterReadingShouldNotBeFound("importActiveEnergy2.doesNotContain=" + DEFAULT_IMPORT_ACTIVE_ENERGY_2);

        // Get all the meterReadingList where importActiveEnergy2 does not contain UPDATED_IMPORT_ACTIVE_ENERGY_2
        defaultMeterReadingShouldBeFound("importActiveEnergy2.doesNotContain=" + UPDATED_IMPORT_ACTIVE_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportActiveEnergy2IsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportActiveEnergy2 equals to DEFAULT_EXPORT_ACTIVE_ENERGY_2
        defaultMeterReadingShouldBeFound("exportActiveEnergy2.equals=" + DEFAULT_EXPORT_ACTIVE_ENERGY_2);

        // Get all the meterReadingList where exportActiveEnergy2 equals to UPDATED_EXPORT_ACTIVE_ENERGY_2
        defaultMeterReadingShouldNotBeFound("exportActiveEnergy2.equals=" + UPDATED_EXPORT_ACTIVE_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportActiveEnergy2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportActiveEnergy2 not equals to DEFAULT_EXPORT_ACTIVE_ENERGY_2
        defaultMeterReadingShouldNotBeFound("exportActiveEnergy2.notEquals=" + DEFAULT_EXPORT_ACTIVE_ENERGY_2);

        // Get all the meterReadingList where exportActiveEnergy2 not equals to UPDATED_EXPORT_ACTIVE_ENERGY_2
        defaultMeterReadingShouldBeFound("exportActiveEnergy2.notEquals=" + UPDATED_EXPORT_ACTIVE_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportActiveEnergy2IsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportActiveEnergy2 in DEFAULT_EXPORT_ACTIVE_ENERGY_2 or UPDATED_EXPORT_ACTIVE_ENERGY_2
        defaultMeterReadingShouldBeFound("exportActiveEnergy2.in=" + DEFAULT_EXPORT_ACTIVE_ENERGY_2 + "," + UPDATED_EXPORT_ACTIVE_ENERGY_2);

        // Get all the meterReadingList where exportActiveEnergy2 equals to UPDATED_EXPORT_ACTIVE_ENERGY_2
        defaultMeterReadingShouldNotBeFound("exportActiveEnergy2.in=" + UPDATED_EXPORT_ACTIVE_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportActiveEnergy2IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportActiveEnergy2 is not null
        defaultMeterReadingShouldBeFound("exportActiveEnergy2.specified=true");

        // Get all the meterReadingList where exportActiveEnergy2 is null
        defaultMeterReadingShouldNotBeFound("exportActiveEnergy2.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportActiveEnergy2ContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportActiveEnergy2 contains DEFAULT_EXPORT_ACTIVE_ENERGY_2
        defaultMeterReadingShouldBeFound("exportActiveEnergy2.contains=" + DEFAULT_EXPORT_ACTIVE_ENERGY_2);

        // Get all the meterReadingList where exportActiveEnergy2 contains UPDATED_EXPORT_ACTIVE_ENERGY_2
        defaultMeterReadingShouldNotBeFound("exportActiveEnergy2.contains=" + UPDATED_EXPORT_ACTIVE_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportActiveEnergy2NotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportActiveEnergy2 does not contain DEFAULT_EXPORT_ACTIVE_ENERGY_2
        defaultMeterReadingShouldNotBeFound("exportActiveEnergy2.doesNotContain=" + DEFAULT_EXPORT_ACTIVE_ENERGY_2);

        // Get all the meterReadingList where exportActiveEnergy2 does not contain UPDATED_EXPORT_ACTIVE_ENERGY_2
        defaultMeterReadingShouldBeFound("exportActiveEnergy2.doesNotContain=" + UPDATED_EXPORT_ACTIVE_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportReactiveEnergy2IsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importReactiveEnergy2 equals to DEFAULT_IMPORT_REACTIVE_ENERGY_2
        defaultMeterReadingShouldBeFound("importReactiveEnergy2.equals=" + DEFAULT_IMPORT_REACTIVE_ENERGY_2);

        // Get all the meterReadingList where importReactiveEnergy2 equals to UPDATED_IMPORT_REACTIVE_ENERGY_2
        defaultMeterReadingShouldNotBeFound("importReactiveEnergy2.equals=" + UPDATED_IMPORT_REACTIVE_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportReactiveEnergy2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importReactiveEnergy2 not equals to DEFAULT_IMPORT_REACTIVE_ENERGY_2
        defaultMeterReadingShouldNotBeFound("importReactiveEnergy2.notEquals=" + DEFAULT_IMPORT_REACTIVE_ENERGY_2);

        // Get all the meterReadingList where importReactiveEnergy2 not equals to UPDATED_IMPORT_REACTIVE_ENERGY_2
        defaultMeterReadingShouldBeFound("importReactiveEnergy2.notEquals=" + UPDATED_IMPORT_REACTIVE_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportReactiveEnergy2IsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importReactiveEnergy2 in DEFAULT_IMPORT_REACTIVE_ENERGY_2 or UPDATED_IMPORT_REACTIVE_ENERGY_2
        defaultMeterReadingShouldBeFound(
            "importReactiveEnergy2.in=" + DEFAULT_IMPORT_REACTIVE_ENERGY_2 + "," + UPDATED_IMPORT_REACTIVE_ENERGY_2
        );

        // Get all the meterReadingList where importReactiveEnergy2 equals to UPDATED_IMPORT_REACTIVE_ENERGY_2
        defaultMeterReadingShouldNotBeFound("importReactiveEnergy2.in=" + UPDATED_IMPORT_REACTIVE_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportReactiveEnergy2IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importReactiveEnergy2 is not null
        defaultMeterReadingShouldBeFound("importReactiveEnergy2.specified=true");

        // Get all the meterReadingList where importReactiveEnergy2 is null
        defaultMeterReadingShouldNotBeFound("importReactiveEnergy2.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportReactiveEnergy2ContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importReactiveEnergy2 contains DEFAULT_IMPORT_REACTIVE_ENERGY_2
        defaultMeterReadingShouldBeFound("importReactiveEnergy2.contains=" + DEFAULT_IMPORT_REACTIVE_ENERGY_2);

        // Get all the meterReadingList where importReactiveEnergy2 contains UPDATED_IMPORT_REACTIVE_ENERGY_2
        defaultMeterReadingShouldNotBeFound("importReactiveEnergy2.contains=" + UPDATED_IMPORT_REACTIVE_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportReactiveEnergy2NotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importReactiveEnergy2 does not contain DEFAULT_IMPORT_REACTIVE_ENERGY_2
        defaultMeterReadingShouldNotBeFound("importReactiveEnergy2.doesNotContain=" + DEFAULT_IMPORT_REACTIVE_ENERGY_2);

        // Get all the meterReadingList where importReactiveEnergy2 does not contain UPDATED_IMPORT_REACTIVE_ENERGY_2
        defaultMeterReadingShouldBeFound("importReactiveEnergy2.doesNotContain=" + UPDATED_IMPORT_REACTIVE_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportReactiveEnergy2IsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportReactiveEnergy2 equals to DEFAULT_EXPORT_REACTIVE_ENERGY_2
        defaultMeterReadingShouldBeFound("exportReactiveEnergy2.equals=" + DEFAULT_EXPORT_REACTIVE_ENERGY_2);

        // Get all the meterReadingList where exportReactiveEnergy2 equals to UPDATED_EXPORT_REACTIVE_ENERGY_2
        defaultMeterReadingShouldNotBeFound("exportReactiveEnergy2.equals=" + UPDATED_EXPORT_REACTIVE_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportReactiveEnergy2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportReactiveEnergy2 not equals to DEFAULT_EXPORT_REACTIVE_ENERGY_2
        defaultMeterReadingShouldNotBeFound("exportReactiveEnergy2.notEquals=" + DEFAULT_EXPORT_REACTIVE_ENERGY_2);

        // Get all the meterReadingList where exportReactiveEnergy2 not equals to UPDATED_EXPORT_REACTIVE_ENERGY_2
        defaultMeterReadingShouldBeFound("exportReactiveEnergy2.notEquals=" + UPDATED_EXPORT_REACTIVE_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportReactiveEnergy2IsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportReactiveEnergy2 in DEFAULT_EXPORT_REACTIVE_ENERGY_2 or UPDATED_EXPORT_REACTIVE_ENERGY_2
        defaultMeterReadingShouldBeFound(
            "exportReactiveEnergy2.in=" + DEFAULT_EXPORT_REACTIVE_ENERGY_2 + "," + UPDATED_EXPORT_REACTIVE_ENERGY_2
        );

        // Get all the meterReadingList where exportReactiveEnergy2 equals to UPDATED_EXPORT_REACTIVE_ENERGY_2
        defaultMeterReadingShouldNotBeFound("exportReactiveEnergy2.in=" + UPDATED_EXPORT_REACTIVE_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportReactiveEnergy2IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportReactiveEnergy2 is not null
        defaultMeterReadingShouldBeFound("exportReactiveEnergy2.specified=true");

        // Get all the meterReadingList where exportReactiveEnergy2 is null
        defaultMeterReadingShouldNotBeFound("exportReactiveEnergy2.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportReactiveEnergy2ContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportReactiveEnergy2 contains DEFAULT_EXPORT_REACTIVE_ENERGY_2
        defaultMeterReadingShouldBeFound("exportReactiveEnergy2.contains=" + DEFAULT_EXPORT_REACTIVE_ENERGY_2);

        // Get all the meterReadingList where exportReactiveEnergy2 contains UPDATED_EXPORT_REACTIVE_ENERGY_2
        defaultMeterReadingShouldNotBeFound("exportReactiveEnergy2.contains=" + UPDATED_EXPORT_REACTIVE_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportReactiveEnergy2NotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportReactiveEnergy2 does not contain DEFAULT_EXPORT_REACTIVE_ENERGY_2
        defaultMeterReadingShouldNotBeFound("exportReactiveEnergy2.doesNotContain=" + DEFAULT_EXPORT_REACTIVE_ENERGY_2);

        // Get all the meterReadingList where exportReactiveEnergy2 does not contain UPDATED_EXPORT_REACTIVE_ENERGY_2
        defaultMeterReadingShouldBeFound("exportReactiveEnergy2.doesNotContain=" + UPDATED_EXPORT_REACTIVE_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportApparentEnergy2IsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importApparentEnergy2 equals to DEFAULT_IMPORT_APPARENT_ENERGY_2
        defaultMeterReadingShouldBeFound("importApparentEnergy2.equals=" + DEFAULT_IMPORT_APPARENT_ENERGY_2);

        // Get all the meterReadingList where importApparentEnergy2 equals to UPDATED_IMPORT_APPARENT_ENERGY_2
        defaultMeterReadingShouldNotBeFound("importApparentEnergy2.equals=" + UPDATED_IMPORT_APPARENT_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportApparentEnergy2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importApparentEnergy2 not equals to DEFAULT_IMPORT_APPARENT_ENERGY_2
        defaultMeterReadingShouldNotBeFound("importApparentEnergy2.notEquals=" + DEFAULT_IMPORT_APPARENT_ENERGY_2);

        // Get all the meterReadingList where importApparentEnergy2 not equals to UPDATED_IMPORT_APPARENT_ENERGY_2
        defaultMeterReadingShouldBeFound("importApparentEnergy2.notEquals=" + UPDATED_IMPORT_APPARENT_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportApparentEnergy2IsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importApparentEnergy2 in DEFAULT_IMPORT_APPARENT_ENERGY_2 or UPDATED_IMPORT_APPARENT_ENERGY_2
        defaultMeterReadingShouldBeFound(
            "importApparentEnergy2.in=" + DEFAULT_IMPORT_APPARENT_ENERGY_2 + "," + UPDATED_IMPORT_APPARENT_ENERGY_2
        );

        // Get all the meterReadingList where importApparentEnergy2 equals to UPDATED_IMPORT_APPARENT_ENERGY_2
        defaultMeterReadingShouldNotBeFound("importApparentEnergy2.in=" + UPDATED_IMPORT_APPARENT_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportApparentEnergy2IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importApparentEnergy2 is not null
        defaultMeterReadingShouldBeFound("importApparentEnergy2.specified=true");

        // Get all the meterReadingList where importApparentEnergy2 is null
        defaultMeterReadingShouldNotBeFound("importApparentEnergy2.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportApparentEnergy2ContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importApparentEnergy2 contains DEFAULT_IMPORT_APPARENT_ENERGY_2
        defaultMeterReadingShouldBeFound("importApparentEnergy2.contains=" + DEFAULT_IMPORT_APPARENT_ENERGY_2);

        // Get all the meterReadingList where importApparentEnergy2 contains UPDATED_IMPORT_APPARENT_ENERGY_2
        defaultMeterReadingShouldNotBeFound("importApparentEnergy2.contains=" + UPDATED_IMPORT_APPARENT_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportApparentEnergy2NotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importApparentEnergy2 does not contain DEFAULT_IMPORT_APPARENT_ENERGY_2
        defaultMeterReadingShouldNotBeFound("importApparentEnergy2.doesNotContain=" + DEFAULT_IMPORT_APPARENT_ENERGY_2);

        // Get all the meterReadingList where importApparentEnergy2 does not contain UPDATED_IMPORT_APPARENT_ENERGY_2
        defaultMeterReadingShouldBeFound("importApparentEnergy2.doesNotContain=" + UPDATED_IMPORT_APPARENT_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportApparentEnergy2IsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportApparentEnergy2 equals to DEFAULT_EXPORT_APPARENT_ENERGY_2
        defaultMeterReadingShouldBeFound("exportApparentEnergy2.equals=" + DEFAULT_EXPORT_APPARENT_ENERGY_2);

        // Get all the meterReadingList where exportApparentEnergy2 equals to UPDATED_EXPORT_APPARENT_ENERGY_2
        defaultMeterReadingShouldNotBeFound("exportApparentEnergy2.equals=" + UPDATED_EXPORT_APPARENT_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportApparentEnergy2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportApparentEnergy2 not equals to DEFAULT_EXPORT_APPARENT_ENERGY_2
        defaultMeterReadingShouldNotBeFound("exportApparentEnergy2.notEquals=" + DEFAULT_EXPORT_APPARENT_ENERGY_2);

        // Get all the meterReadingList where exportApparentEnergy2 not equals to UPDATED_EXPORT_APPARENT_ENERGY_2
        defaultMeterReadingShouldBeFound("exportApparentEnergy2.notEquals=" + UPDATED_EXPORT_APPARENT_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportApparentEnergy2IsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportApparentEnergy2 in DEFAULT_EXPORT_APPARENT_ENERGY_2 or UPDATED_EXPORT_APPARENT_ENERGY_2
        defaultMeterReadingShouldBeFound(
            "exportApparentEnergy2.in=" + DEFAULT_EXPORT_APPARENT_ENERGY_2 + "," + UPDATED_EXPORT_APPARENT_ENERGY_2
        );

        // Get all the meterReadingList where exportApparentEnergy2 equals to UPDATED_EXPORT_APPARENT_ENERGY_2
        defaultMeterReadingShouldNotBeFound("exportApparentEnergy2.in=" + UPDATED_EXPORT_APPARENT_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportApparentEnergy2IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportApparentEnergy2 is not null
        defaultMeterReadingShouldBeFound("exportApparentEnergy2.specified=true");

        // Get all the meterReadingList where exportApparentEnergy2 is null
        defaultMeterReadingShouldNotBeFound("exportApparentEnergy2.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportApparentEnergy2ContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportApparentEnergy2 contains DEFAULT_EXPORT_APPARENT_ENERGY_2
        defaultMeterReadingShouldBeFound("exportApparentEnergy2.contains=" + DEFAULT_EXPORT_APPARENT_ENERGY_2);

        // Get all the meterReadingList where exportApparentEnergy2 contains UPDATED_EXPORT_APPARENT_ENERGY_2
        defaultMeterReadingShouldNotBeFound("exportApparentEnergy2.contains=" + UPDATED_EXPORT_APPARENT_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportApparentEnergy2NotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportApparentEnergy2 does not contain DEFAULT_EXPORT_APPARENT_ENERGY_2
        defaultMeterReadingShouldNotBeFound("exportApparentEnergy2.doesNotContain=" + DEFAULT_EXPORT_APPARENT_ENERGY_2);

        // Get all the meterReadingList where exportApparentEnergy2 does not contain UPDATED_EXPORT_APPARENT_ENERGY_2
        defaultMeterReadingShouldBeFound("exportApparentEnergy2.doesNotContain=" + UPDATED_EXPORT_APPARENT_ENERGY_2);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportActiveEnergy3IsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importActiveEnergy3 equals to DEFAULT_IMPORT_ACTIVE_ENERGY_3
        defaultMeterReadingShouldBeFound("importActiveEnergy3.equals=" + DEFAULT_IMPORT_ACTIVE_ENERGY_3);

        // Get all the meterReadingList where importActiveEnergy3 equals to UPDATED_IMPORT_ACTIVE_ENERGY_3
        defaultMeterReadingShouldNotBeFound("importActiveEnergy3.equals=" + UPDATED_IMPORT_ACTIVE_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportActiveEnergy3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importActiveEnergy3 not equals to DEFAULT_IMPORT_ACTIVE_ENERGY_3
        defaultMeterReadingShouldNotBeFound("importActiveEnergy3.notEquals=" + DEFAULT_IMPORT_ACTIVE_ENERGY_3);

        // Get all the meterReadingList where importActiveEnergy3 not equals to UPDATED_IMPORT_ACTIVE_ENERGY_3
        defaultMeterReadingShouldBeFound("importActiveEnergy3.notEquals=" + UPDATED_IMPORT_ACTIVE_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportActiveEnergy3IsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importActiveEnergy3 in DEFAULT_IMPORT_ACTIVE_ENERGY_3 or UPDATED_IMPORT_ACTIVE_ENERGY_3
        defaultMeterReadingShouldBeFound("importActiveEnergy3.in=" + DEFAULT_IMPORT_ACTIVE_ENERGY_3 + "," + UPDATED_IMPORT_ACTIVE_ENERGY_3);

        // Get all the meterReadingList where importActiveEnergy3 equals to UPDATED_IMPORT_ACTIVE_ENERGY_3
        defaultMeterReadingShouldNotBeFound("importActiveEnergy3.in=" + UPDATED_IMPORT_ACTIVE_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportActiveEnergy3IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importActiveEnergy3 is not null
        defaultMeterReadingShouldBeFound("importActiveEnergy3.specified=true");

        // Get all the meterReadingList where importActiveEnergy3 is null
        defaultMeterReadingShouldNotBeFound("importActiveEnergy3.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportActiveEnergy3ContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importActiveEnergy3 contains DEFAULT_IMPORT_ACTIVE_ENERGY_3
        defaultMeterReadingShouldBeFound("importActiveEnergy3.contains=" + DEFAULT_IMPORT_ACTIVE_ENERGY_3);

        // Get all the meterReadingList where importActiveEnergy3 contains UPDATED_IMPORT_ACTIVE_ENERGY_3
        defaultMeterReadingShouldNotBeFound("importActiveEnergy3.contains=" + UPDATED_IMPORT_ACTIVE_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportActiveEnergy3NotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importActiveEnergy3 does not contain DEFAULT_IMPORT_ACTIVE_ENERGY_3
        defaultMeterReadingShouldNotBeFound("importActiveEnergy3.doesNotContain=" + DEFAULT_IMPORT_ACTIVE_ENERGY_3);

        // Get all the meterReadingList where importActiveEnergy3 does not contain UPDATED_IMPORT_ACTIVE_ENERGY_3
        defaultMeterReadingShouldBeFound("importActiveEnergy3.doesNotContain=" + UPDATED_IMPORT_ACTIVE_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportActiveEnergy3IsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportActiveEnergy3 equals to DEFAULT_EXPORT_ACTIVE_ENERGY_3
        defaultMeterReadingShouldBeFound("exportActiveEnergy3.equals=" + DEFAULT_EXPORT_ACTIVE_ENERGY_3);

        // Get all the meterReadingList where exportActiveEnergy3 equals to UPDATED_EXPORT_ACTIVE_ENERGY_3
        defaultMeterReadingShouldNotBeFound("exportActiveEnergy3.equals=" + UPDATED_EXPORT_ACTIVE_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportActiveEnergy3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportActiveEnergy3 not equals to DEFAULT_EXPORT_ACTIVE_ENERGY_3
        defaultMeterReadingShouldNotBeFound("exportActiveEnergy3.notEquals=" + DEFAULT_EXPORT_ACTIVE_ENERGY_3);

        // Get all the meterReadingList where exportActiveEnergy3 not equals to UPDATED_EXPORT_ACTIVE_ENERGY_3
        defaultMeterReadingShouldBeFound("exportActiveEnergy3.notEquals=" + UPDATED_EXPORT_ACTIVE_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportActiveEnergy3IsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportActiveEnergy3 in DEFAULT_EXPORT_ACTIVE_ENERGY_3 or UPDATED_EXPORT_ACTIVE_ENERGY_3
        defaultMeterReadingShouldBeFound("exportActiveEnergy3.in=" + DEFAULT_EXPORT_ACTIVE_ENERGY_3 + "," + UPDATED_EXPORT_ACTIVE_ENERGY_3);

        // Get all the meterReadingList where exportActiveEnergy3 equals to UPDATED_EXPORT_ACTIVE_ENERGY_3
        defaultMeterReadingShouldNotBeFound("exportActiveEnergy3.in=" + UPDATED_EXPORT_ACTIVE_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportActiveEnergy3IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportActiveEnergy3 is not null
        defaultMeterReadingShouldBeFound("exportActiveEnergy3.specified=true");

        // Get all the meterReadingList where exportActiveEnergy3 is null
        defaultMeterReadingShouldNotBeFound("exportActiveEnergy3.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportActiveEnergy3ContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportActiveEnergy3 contains DEFAULT_EXPORT_ACTIVE_ENERGY_3
        defaultMeterReadingShouldBeFound("exportActiveEnergy3.contains=" + DEFAULT_EXPORT_ACTIVE_ENERGY_3);

        // Get all the meterReadingList where exportActiveEnergy3 contains UPDATED_EXPORT_ACTIVE_ENERGY_3
        defaultMeterReadingShouldNotBeFound("exportActiveEnergy3.contains=" + UPDATED_EXPORT_ACTIVE_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportActiveEnergy3NotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportActiveEnergy3 does not contain DEFAULT_EXPORT_ACTIVE_ENERGY_3
        defaultMeterReadingShouldNotBeFound("exportActiveEnergy3.doesNotContain=" + DEFAULT_EXPORT_ACTIVE_ENERGY_3);

        // Get all the meterReadingList where exportActiveEnergy3 does not contain UPDATED_EXPORT_ACTIVE_ENERGY_3
        defaultMeterReadingShouldBeFound("exportActiveEnergy3.doesNotContain=" + UPDATED_EXPORT_ACTIVE_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportReactiveEnergy3IsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importReactiveEnergy3 equals to DEFAULT_IMPORT_REACTIVE_ENERGY_3
        defaultMeterReadingShouldBeFound("importReactiveEnergy3.equals=" + DEFAULT_IMPORT_REACTIVE_ENERGY_3);

        // Get all the meterReadingList where importReactiveEnergy3 equals to UPDATED_IMPORT_REACTIVE_ENERGY_3
        defaultMeterReadingShouldNotBeFound("importReactiveEnergy3.equals=" + UPDATED_IMPORT_REACTIVE_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportReactiveEnergy3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importReactiveEnergy3 not equals to DEFAULT_IMPORT_REACTIVE_ENERGY_3
        defaultMeterReadingShouldNotBeFound("importReactiveEnergy3.notEquals=" + DEFAULT_IMPORT_REACTIVE_ENERGY_3);

        // Get all the meterReadingList where importReactiveEnergy3 not equals to UPDATED_IMPORT_REACTIVE_ENERGY_3
        defaultMeterReadingShouldBeFound("importReactiveEnergy3.notEquals=" + UPDATED_IMPORT_REACTIVE_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportReactiveEnergy3IsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importReactiveEnergy3 in DEFAULT_IMPORT_REACTIVE_ENERGY_3 or UPDATED_IMPORT_REACTIVE_ENERGY_3
        defaultMeterReadingShouldBeFound(
            "importReactiveEnergy3.in=" + DEFAULT_IMPORT_REACTIVE_ENERGY_3 + "," + UPDATED_IMPORT_REACTIVE_ENERGY_3
        );

        // Get all the meterReadingList where importReactiveEnergy3 equals to UPDATED_IMPORT_REACTIVE_ENERGY_3
        defaultMeterReadingShouldNotBeFound("importReactiveEnergy3.in=" + UPDATED_IMPORT_REACTIVE_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportReactiveEnergy3IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importReactiveEnergy3 is not null
        defaultMeterReadingShouldBeFound("importReactiveEnergy3.specified=true");

        // Get all the meterReadingList where importReactiveEnergy3 is null
        defaultMeterReadingShouldNotBeFound("importReactiveEnergy3.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportReactiveEnergy3ContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importReactiveEnergy3 contains DEFAULT_IMPORT_REACTIVE_ENERGY_3
        defaultMeterReadingShouldBeFound("importReactiveEnergy3.contains=" + DEFAULT_IMPORT_REACTIVE_ENERGY_3);

        // Get all the meterReadingList where importReactiveEnergy3 contains UPDATED_IMPORT_REACTIVE_ENERGY_3
        defaultMeterReadingShouldNotBeFound("importReactiveEnergy3.contains=" + UPDATED_IMPORT_REACTIVE_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportReactiveEnergy3NotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importReactiveEnergy3 does not contain DEFAULT_IMPORT_REACTIVE_ENERGY_3
        defaultMeterReadingShouldNotBeFound("importReactiveEnergy3.doesNotContain=" + DEFAULT_IMPORT_REACTIVE_ENERGY_3);

        // Get all the meterReadingList where importReactiveEnergy3 does not contain UPDATED_IMPORT_REACTIVE_ENERGY_3
        defaultMeterReadingShouldBeFound("importReactiveEnergy3.doesNotContain=" + UPDATED_IMPORT_REACTIVE_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportReactiveEnergy3IsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportReactiveEnergy3 equals to DEFAULT_EXPORT_REACTIVE_ENERGY_3
        defaultMeterReadingShouldBeFound("exportReactiveEnergy3.equals=" + DEFAULT_EXPORT_REACTIVE_ENERGY_3);

        // Get all the meterReadingList where exportReactiveEnergy3 equals to UPDATED_EXPORT_REACTIVE_ENERGY_3
        defaultMeterReadingShouldNotBeFound("exportReactiveEnergy3.equals=" + UPDATED_EXPORT_REACTIVE_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportReactiveEnergy3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportReactiveEnergy3 not equals to DEFAULT_EXPORT_REACTIVE_ENERGY_3
        defaultMeterReadingShouldNotBeFound("exportReactiveEnergy3.notEquals=" + DEFAULT_EXPORT_REACTIVE_ENERGY_3);

        // Get all the meterReadingList where exportReactiveEnergy3 not equals to UPDATED_EXPORT_REACTIVE_ENERGY_3
        defaultMeterReadingShouldBeFound("exportReactiveEnergy3.notEquals=" + UPDATED_EXPORT_REACTIVE_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportReactiveEnergy3IsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportReactiveEnergy3 in DEFAULT_EXPORT_REACTIVE_ENERGY_3 or UPDATED_EXPORT_REACTIVE_ENERGY_3
        defaultMeterReadingShouldBeFound(
            "exportReactiveEnergy3.in=" + DEFAULT_EXPORT_REACTIVE_ENERGY_3 + "," + UPDATED_EXPORT_REACTIVE_ENERGY_3
        );

        // Get all the meterReadingList where exportReactiveEnergy3 equals to UPDATED_EXPORT_REACTIVE_ENERGY_3
        defaultMeterReadingShouldNotBeFound("exportReactiveEnergy3.in=" + UPDATED_EXPORT_REACTIVE_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportReactiveEnergy3IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportReactiveEnergy3 is not null
        defaultMeterReadingShouldBeFound("exportReactiveEnergy3.specified=true");

        // Get all the meterReadingList where exportReactiveEnergy3 is null
        defaultMeterReadingShouldNotBeFound("exportReactiveEnergy3.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportReactiveEnergy3ContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportReactiveEnergy3 contains DEFAULT_EXPORT_REACTIVE_ENERGY_3
        defaultMeterReadingShouldBeFound("exportReactiveEnergy3.contains=" + DEFAULT_EXPORT_REACTIVE_ENERGY_3);

        // Get all the meterReadingList where exportReactiveEnergy3 contains UPDATED_EXPORT_REACTIVE_ENERGY_3
        defaultMeterReadingShouldNotBeFound("exportReactiveEnergy3.contains=" + UPDATED_EXPORT_REACTIVE_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportReactiveEnergy3NotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportReactiveEnergy3 does not contain DEFAULT_EXPORT_REACTIVE_ENERGY_3
        defaultMeterReadingShouldNotBeFound("exportReactiveEnergy3.doesNotContain=" + DEFAULT_EXPORT_REACTIVE_ENERGY_3);

        // Get all the meterReadingList where exportReactiveEnergy3 does not contain UPDATED_EXPORT_REACTIVE_ENERGY_3
        defaultMeterReadingShouldBeFound("exportReactiveEnergy3.doesNotContain=" + UPDATED_EXPORT_REACTIVE_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportApparentEnergy3IsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importApparentEnergy3 equals to DEFAULT_IMPORT_APPARENT_ENERGY_3
        defaultMeterReadingShouldBeFound("importApparentEnergy3.equals=" + DEFAULT_IMPORT_APPARENT_ENERGY_3);

        // Get all the meterReadingList where importApparentEnergy3 equals to UPDATED_IMPORT_APPARENT_ENERGY_3
        defaultMeterReadingShouldNotBeFound("importApparentEnergy3.equals=" + UPDATED_IMPORT_APPARENT_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportApparentEnergy3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importApparentEnergy3 not equals to DEFAULT_IMPORT_APPARENT_ENERGY_3
        defaultMeterReadingShouldNotBeFound("importApparentEnergy3.notEquals=" + DEFAULT_IMPORT_APPARENT_ENERGY_3);

        // Get all the meterReadingList where importApparentEnergy3 not equals to UPDATED_IMPORT_APPARENT_ENERGY_3
        defaultMeterReadingShouldBeFound("importApparentEnergy3.notEquals=" + UPDATED_IMPORT_APPARENT_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportApparentEnergy3IsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importApparentEnergy3 in DEFAULT_IMPORT_APPARENT_ENERGY_3 or UPDATED_IMPORT_APPARENT_ENERGY_3
        defaultMeterReadingShouldBeFound(
            "importApparentEnergy3.in=" + DEFAULT_IMPORT_APPARENT_ENERGY_3 + "," + UPDATED_IMPORT_APPARENT_ENERGY_3
        );

        // Get all the meterReadingList where importApparentEnergy3 equals to UPDATED_IMPORT_APPARENT_ENERGY_3
        defaultMeterReadingShouldNotBeFound("importApparentEnergy3.in=" + UPDATED_IMPORT_APPARENT_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportApparentEnergy3IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importApparentEnergy3 is not null
        defaultMeterReadingShouldBeFound("importApparentEnergy3.specified=true");

        // Get all the meterReadingList where importApparentEnergy3 is null
        defaultMeterReadingShouldNotBeFound("importApparentEnergy3.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportApparentEnergy3ContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importApparentEnergy3 contains DEFAULT_IMPORT_APPARENT_ENERGY_3
        defaultMeterReadingShouldBeFound("importApparentEnergy3.contains=" + DEFAULT_IMPORT_APPARENT_ENERGY_3);

        // Get all the meterReadingList where importApparentEnergy3 contains UPDATED_IMPORT_APPARENT_ENERGY_3
        defaultMeterReadingShouldNotBeFound("importApparentEnergy3.contains=" + UPDATED_IMPORT_APPARENT_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByImportApparentEnergy3NotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where importApparentEnergy3 does not contain DEFAULT_IMPORT_APPARENT_ENERGY_3
        defaultMeterReadingShouldNotBeFound("importApparentEnergy3.doesNotContain=" + DEFAULT_IMPORT_APPARENT_ENERGY_3);

        // Get all the meterReadingList where importApparentEnergy3 does not contain UPDATED_IMPORT_APPARENT_ENERGY_3
        defaultMeterReadingShouldBeFound("importApparentEnergy3.doesNotContain=" + UPDATED_IMPORT_APPARENT_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportApparentEnergy3IsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportApparentEnergy3 equals to DEFAULT_EXPORT_APPARENT_ENERGY_3
        defaultMeterReadingShouldBeFound("exportApparentEnergy3.equals=" + DEFAULT_EXPORT_APPARENT_ENERGY_3);

        // Get all the meterReadingList where exportApparentEnergy3 equals to UPDATED_EXPORT_APPARENT_ENERGY_3
        defaultMeterReadingShouldNotBeFound("exportApparentEnergy3.equals=" + UPDATED_EXPORT_APPARENT_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportApparentEnergy3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportApparentEnergy3 not equals to DEFAULT_EXPORT_APPARENT_ENERGY_3
        defaultMeterReadingShouldNotBeFound("exportApparentEnergy3.notEquals=" + DEFAULT_EXPORT_APPARENT_ENERGY_3);

        // Get all the meterReadingList where exportApparentEnergy3 not equals to UPDATED_EXPORT_APPARENT_ENERGY_3
        defaultMeterReadingShouldBeFound("exportApparentEnergy3.notEquals=" + UPDATED_EXPORT_APPARENT_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportApparentEnergy3IsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportApparentEnergy3 in DEFAULT_EXPORT_APPARENT_ENERGY_3 or UPDATED_EXPORT_APPARENT_ENERGY_3
        defaultMeterReadingShouldBeFound(
            "exportApparentEnergy3.in=" + DEFAULT_EXPORT_APPARENT_ENERGY_3 + "," + UPDATED_EXPORT_APPARENT_ENERGY_3
        );

        // Get all the meterReadingList where exportApparentEnergy3 equals to UPDATED_EXPORT_APPARENT_ENERGY_3
        defaultMeterReadingShouldNotBeFound("exportApparentEnergy3.in=" + UPDATED_EXPORT_APPARENT_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportApparentEnergy3IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportApparentEnergy3 is not null
        defaultMeterReadingShouldBeFound("exportApparentEnergy3.specified=true");

        // Get all the meterReadingList where exportApparentEnergy3 is null
        defaultMeterReadingShouldNotBeFound("exportApparentEnergy3.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportApparentEnergy3ContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportApparentEnergy3 contains DEFAULT_EXPORT_APPARENT_ENERGY_3
        defaultMeterReadingShouldBeFound("exportApparentEnergy3.contains=" + DEFAULT_EXPORT_APPARENT_ENERGY_3);

        // Get all the meterReadingList where exportApparentEnergy3 contains UPDATED_EXPORT_APPARENT_ENERGY_3
        defaultMeterReadingShouldNotBeFound("exportApparentEnergy3.contains=" + UPDATED_EXPORT_APPARENT_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByExportApparentEnergy3NotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where exportApparentEnergy3 does not contain DEFAULT_EXPORT_APPARENT_ENERGY_3
        defaultMeterReadingShouldNotBeFound("exportApparentEnergy3.doesNotContain=" + DEFAULT_EXPORT_APPARENT_ENERGY_3);

        // Get all the meterReadingList where exportApparentEnergy3 does not contain UPDATED_EXPORT_APPARENT_ENERGY_3
        defaultMeterReadingShouldBeFound("exportApparentEnergy3.doesNotContain=" + UPDATED_EXPORT_APPARENT_ENERGY_3);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVoltageAIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where voltageA equals to DEFAULT_VOLTAGE_A
        defaultMeterReadingShouldBeFound("voltageA.equals=" + DEFAULT_VOLTAGE_A);

        // Get all the meterReadingList where voltageA equals to UPDATED_VOLTAGE_A
        defaultMeterReadingShouldNotBeFound("voltageA.equals=" + UPDATED_VOLTAGE_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVoltageAIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where voltageA not equals to DEFAULT_VOLTAGE_A
        defaultMeterReadingShouldNotBeFound("voltageA.notEquals=" + DEFAULT_VOLTAGE_A);

        // Get all the meterReadingList where voltageA not equals to UPDATED_VOLTAGE_A
        defaultMeterReadingShouldBeFound("voltageA.notEquals=" + UPDATED_VOLTAGE_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVoltageAIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where voltageA in DEFAULT_VOLTAGE_A or UPDATED_VOLTAGE_A
        defaultMeterReadingShouldBeFound("voltageA.in=" + DEFAULT_VOLTAGE_A + "," + UPDATED_VOLTAGE_A);

        // Get all the meterReadingList where voltageA equals to UPDATED_VOLTAGE_A
        defaultMeterReadingShouldNotBeFound("voltageA.in=" + UPDATED_VOLTAGE_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVoltageAIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where voltageA is not null
        defaultMeterReadingShouldBeFound("voltageA.specified=true");

        // Get all the meterReadingList where voltageA is null
        defaultMeterReadingShouldNotBeFound("voltageA.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVoltageAContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where voltageA contains DEFAULT_VOLTAGE_A
        defaultMeterReadingShouldBeFound("voltageA.contains=" + DEFAULT_VOLTAGE_A);

        // Get all the meterReadingList where voltageA contains UPDATED_VOLTAGE_A
        defaultMeterReadingShouldNotBeFound("voltageA.contains=" + UPDATED_VOLTAGE_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVoltageANotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where voltageA does not contain DEFAULT_VOLTAGE_A
        defaultMeterReadingShouldNotBeFound("voltageA.doesNotContain=" + DEFAULT_VOLTAGE_A);

        // Get all the meterReadingList where voltageA does not contain UPDATED_VOLTAGE_A
        defaultMeterReadingShouldBeFound("voltageA.doesNotContain=" + UPDATED_VOLTAGE_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCurrentAIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where currentA equals to DEFAULT_CURRENT_A
        defaultMeterReadingShouldBeFound("currentA.equals=" + DEFAULT_CURRENT_A);

        // Get all the meterReadingList where currentA equals to UPDATED_CURRENT_A
        defaultMeterReadingShouldNotBeFound("currentA.equals=" + UPDATED_CURRENT_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCurrentAIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where currentA not equals to DEFAULT_CURRENT_A
        defaultMeterReadingShouldNotBeFound("currentA.notEquals=" + DEFAULT_CURRENT_A);

        // Get all the meterReadingList where currentA not equals to UPDATED_CURRENT_A
        defaultMeterReadingShouldBeFound("currentA.notEquals=" + UPDATED_CURRENT_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCurrentAIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where currentA in DEFAULT_CURRENT_A or UPDATED_CURRENT_A
        defaultMeterReadingShouldBeFound("currentA.in=" + DEFAULT_CURRENT_A + "," + UPDATED_CURRENT_A);

        // Get all the meterReadingList where currentA equals to UPDATED_CURRENT_A
        defaultMeterReadingShouldNotBeFound("currentA.in=" + UPDATED_CURRENT_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCurrentAIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where currentA is not null
        defaultMeterReadingShouldBeFound("currentA.specified=true");

        // Get all the meterReadingList where currentA is null
        defaultMeterReadingShouldNotBeFound("currentA.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCurrentAContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where currentA contains DEFAULT_CURRENT_A
        defaultMeterReadingShouldBeFound("currentA.contains=" + DEFAULT_CURRENT_A);

        // Get all the meterReadingList where currentA contains UPDATED_CURRENT_A
        defaultMeterReadingShouldNotBeFound("currentA.contains=" + UPDATED_CURRENT_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCurrentANotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where currentA does not contain DEFAULT_CURRENT_A
        defaultMeterReadingShouldNotBeFound("currentA.doesNotContain=" + DEFAULT_CURRENT_A);

        // Get all the meterReadingList where currentA does not contain UPDATED_CURRENT_A
        defaultMeterReadingShouldBeFound("currentA.doesNotContain=" + UPDATED_CURRENT_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByActivePowerAIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where activePowerA equals to DEFAULT_ACTIVE_POWER_A
        defaultMeterReadingShouldBeFound("activePowerA.equals=" + DEFAULT_ACTIVE_POWER_A);

        // Get all the meterReadingList where activePowerA equals to UPDATED_ACTIVE_POWER_A
        defaultMeterReadingShouldNotBeFound("activePowerA.equals=" + UPDATED_ACTIVE_POWER_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByActivePowerAIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where activePowerA not equals to DEFAULT_ACTIVE_POWER_A
        defaultMeterReadingShouldNotBeFound("activePowerA.notEquals=" + DEFAULT_ACTIVE_POWER_A);

        // Get all the meterReadingList where activePowerA not equals to UPDATED_ACTIVE_POWER_A
        defaultMeterReadingShouldBeFound("activePowerA.notEquals=" + UPDATED_ACTIVE_POWER_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByActivePowerAIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where activePowerA in DEFAULT_ACTIVE_POWER_A or UPDATED_ACTIVE_POWER_A
        defaultMeterReadingShouldBeFound("activePowerA.in=" + DEFAULT_ACTIVE_POWER_A + "," + UPDATED_ACTIVE_POWER_A);

        // Get all the meterReadingList where activePowerA equals to UPDATED_ACTIVE_POWER_A
        defaultMeterReadingShouldNotBeFound("activePowerA.in=" + UPDATED_ACTIVE_POWER_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByActivePowerAIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where activePowerA is not null
        defaultMeterReadingShouldBeFound("activePowerA.specified=true");

        // Get all the meterReadingList where activePowerA is null
        defaultMeterReadingShouldNotBeFound("activePowerA.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByActivePowerAContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where activePowerA contains DEFAULT_ACTIVE_POWER_A
        defaultMeterReadingShouldBeFound("activePowerA.contains=" + DEFAULT_ACTIVE_POWER_A);

        // Get all the meterReadingList where activePowerA contains UPDATED_ACTIVE_POWER_A
        defaultMeterReadingShouldNotBeFound("activePowerA.contains=" + UPDATED_ACTIVE_POWER_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByActivePowerANotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where activePowerA does not contain DEFAULT_ACTIVE_POWER_A
        defaultMeterReadingShouldNotBeFound("activePowerA.doesNotContain=" + DEFAULT_ACTIVE_POWER_A);

        // Get all the meterReadingList where activePowerA does not contain UPDATED_ACTIVE_POWER_A
        defaultMeterReadingShouldBeFound("activePowerA.doesNotContain=" + UPDATED_ACTIVE_POWER_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReactivePowerAIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where reactivePowerA equals to DEFAULT_REACTIVE_POWER_A
        defaultMeterReadingShouldBeFound("reactivePowerA.equals=" + DEFAULT_REACTIVE_POWER_A);

        // Get all the meterReadingList where reactivePowerA equals to UPDATED_REACTIVE_POWER_A
        defaultMeterReadingShouldNotBeFound("reactivePowerA.equals=" + UPDATED_REACTIVE_POWER_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReactivePowerAIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where reactivePowerA not equals to DEFAULT_REACTIVE_POWER_A
        defaultMeterReadingShouldNotBeFound("reactivePowerA.notEquals=" + DEFAULT_REACTIVE_POWER_A);

        // Get all the meterReadingList where reactivePowerA not equals to UPDATED_REACTIVE_POWER_A
        defaultMeterReadingShouldBeFound("reactivePowerA.notEquals=" + UPDATED_REACTIVE_POWER_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReactivePowerAIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where reactivePowerA in DEFAULT_REACTIVE_POWER_A or UPDATED_REACTIVE_POWER_A
        defaultMeterReadingShouldBeFound("reactivePowerA.in=" + DEFAULT_REACTIVE_POWER_A + "," + UPDATED_REACTIVE_POWER_A);

        // Get all the meterReadingList where reactivePowerA equals to UPDATED_REACTIVE_POWER_A
        defaultMeterReadingShouldNotBeFound("reactivePowerA.in=" + UPDATED_REACTIVE_POWER_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReactivePowerAIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where reactivePowerA is not null
        defaultMeterReadingShouldBeFound("reactivePowerA.specified=true");

        // Get all the meterReadingList where reactivePowerA is null
        defaultMeterReadingShouldNotBeFound("reactivePowerA.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReactivePowerAContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where reactivePowerA contains DEFAULT_REACTIVE_POWER_A
        defaultMeterReadingShouldBeFound("reactivePowerA.contains=" + DEFAULT_REACTIVE_POWER_A);

        // Get all the meterReadingList where reactivePowerA contains UPDATED_REACTIVE_POWER_A
        defaultMeterReadingShouldNotBeFound("reactivePowerA.contains=" + UPDATED_REACTIVE_POWER_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReactivePowerANotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where reactivePowerA does not contain DEFAULT_REACTIVE_POWER_A
        defaultMeterReadingShouldNotBeFound("reactivePowerA.doesNotContain=" + DEFAULT_REACTIVE_POWER_A);

        // Get all the meterReadingList where reactivePowerA does not contain UPDATED_REACTIVE_POWER_A
        defaultMeterReadingShouldBeFound("reactivePowerA.doesNotContain=" + UPDATED_REACTIVE_POWER_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByPowerFactorAIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where powerFactorA equals to DEFAULT_POWER_FACTOR_A
        defaultMeterReadingShouldBeFound("powerFactorA.equals=" + DEFAULT_POWER_FACTOR_A);

        // Get all the meterReadingList where powerFactorA equals to UPDATED_POWER_FACTOR_A
        defaultMeterReadingShouldNotBeFound("powerFactorA.equals=" + UPDATED_POWER_FACTOR_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByPowerFactorAIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where powerFactorA not equals to DEFAULT_POWER_FACTOR_A
        defaultMeterReadingShouldNotBeFound("powerFactorA.notEquals=" + DEFAULT_POWER_FACTOR_A);

        // Get all the meterReadingList where powerFactorA not equals to UPDATED_POWER_FACTOR_A
        defaultMeterReadingShouldBeFound("powerFactorA.notEquals=" + UPDATED_POWER_FACTOR_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByPowerFactorAIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where powerFactorA in DEFAULT_POWER_FACTOR_A or UPDATED_POWER_FACTOR_A
        defaultMeterReadingShouldBeFound("powerFactorA.in=" + DEFAULT_POWER_FACTOR_A + "," + UPDATED_POWER_FACTOR_A);

        // Get all the meterReadingList where powerFactorA equals to UPDATED_POWER_FACTOR_A
        defaultMeterReadingShouldNotBeFound("powerFactorA.in=" + UPDATED_POWER_FACTOR_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByPowerFactorAIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where powerFactorA is not null
        defaultMeterReadingShouldBeFound("powerFactorA.specified=true");

        // Get all the meterReadingList where powerFactorA is null
        defaultMeterReadingShouldNotBeFound("powerFactorA.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByPowerFactorAContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where powerFactorA contains DEFAULT_POWER_FACTOR_A
        defaultMeterReadingShouldBeFound("powerFactorA.contains=" + DEFAULT_POWER_FACTOR_A);

        // Get all the meterReadingList where powerFactorA contains UPDATED_POWER_FACTOR_A
        defaultMeterReadingShouldNotBeFound("powerFactorA.contains=" + UPDATED_POWER_FACTOR_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByPowerFactorANotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where powerFactorA does not contain DEFAULT_POWER_FACTOR_A
        defaultMeterReadingShouldNotBeFound("powerFactorA.doesNotContain=" + DEFAULT_POWER_FACTOR_A);

        // Get all the meterReadingList where powerFactorA does not contain UPDATED_POWER_FACTOR_A
        defaultMeterReadingShouldBeFound("powerFactorA.doesNotContain=" + UPDATED_POWER_FACTOR_A);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVoltageBIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where voltageB equals to DEFAULT_VOLTAGE_B
        defaultMeterReadingShouldBeFound("voltageB.equals=" + DEFAULT_VOLTAGE_B);

        // Get all the meterReadingList where voltageB equals to UPDATED_VOLTAGE_B
        defaultMeterReadingShouldNotBeFound("voltageB.equals=" + UPDATED_VOLTAGE_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVoltageBIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where voltageB not equals to DEFAULT_VOLTAGE_B
        defaultMeterReadingShouldNotBeFound("voltageB.notEquals=" + DEFAULT_VOLTAGE_B);

        // Get all the meterReadingList where voltageB not equals to UPDATED_VOLTAGE_B
        defaultMeterReadingShouldBeFound("voltageB.notEquals=" + UPDATED_VOLTAGE_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVoltageBIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where voltageB in DEFAULT_VOLTAGE_B or UPDATED_VOLTAGE_B
        defaultMeterReadingShouldBeFound("voltageB.in=" + DEFAULT_VOLTAGE_B + "," + UPDATED_VOLTAGE_B);

        // Get all the meterReadingList where voltageB equals to UPDATED_VOLTAGE_B
        defaultMeterReadingShouldNotBeFound("voltageB.in=" + UPDATED_VOLTAGE_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVoltageBIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where voltageB is not null
        defaultMeterReadingShouldBeFound("voltageB.specified=true");

        // Get all the meterReadingList where voltageB is null
        defaultMeterReadingShouldNotBeFound("voltageB.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVoltageBContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where voltageB contains DEFAULT_VOLTAGE_B
        defaultMeterReadingShouldBeFound("voltageB.contains=" + DEFAULT_VOLTAGE_B);

        // Get all the meterReadingList where voltageB contains UPDATED_VOLTAGE_B
        defaultMeterReadingShouldNotBeFound("voltageB.contains=" + UPDATED_VOLTAGE_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVoltageBNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where voltageB does not contain DEFAULT_VOLTAGE_B
        defaultMeterReadingShouldNotBeFound("voltageB.doesNotContain=" + DEFAULT_VOLTAGE_B);

        // Get all the meterReadingList where voltageB does not contain UPDATED_VOLTAGE_B
        defaultMeterReadingShouldBeFound("voltageB.doesNotContain=" + UPDATED_VOLTAGE_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCurrentBIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where currentB equals to DEFAULT_CURRENT_B
        defaultMeterReadingShouldBeFound("currentB.equals=" + DEFAULT_CURRENT_B);

        // Get all the meterReadingList where currentB equals to UPDATED_CURRENT_B
        defaultMeterReadingShouldNotBeFound("currentB.equals=" + UPDATED_CURRENT_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCurrentBIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where currentB not equals to DEFAULT_CURRENT_B
        defaultMeterReadingShouldNotBeFound("currentB.notEquals=" + DEFAULT_CURRENT_B);

        // Get all the meterReadingList where currentB not equals to UPDATED_CURRENT_B
        defaultMeterReadingShouldBeFound("currentB.notEquals=" + UPDATED_CURRENT_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCurrentBIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where currentB in DEFAULT_CURRENT_B or UPDATED_CURRENT_B
        defaultMeterReadingShouldBeFound("currentB.in=" + DEFAULT_CURRENT_B + "," + UPDATED_CURRENT_B);

        // Get all the meterReadingList where currentB equals to UPDATED_CURRENT_B
        defaultMeterReadingShouldNotBeFound("currentB.in=" + UPDATED_CURRENT_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCurrentBIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where currentB is not null
        defaultMeterReadingShouldBeFound("currentB.specified=true");

        // Get all the meterReadingList where currentB is null
        defaultMeterReadingShouldNotBeFound("currentB.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCurrentBContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where currentB contains DEFAULT_CURRENT_B
        defaultMeterReadingShouldBeFound("currentB.contains=" + DEFAULT_CURRENT_B);

        // Get all the meterReadingList where currentB contains UPDATED_CURRENT_B
        defaultMeterReadingShouldNotBeFound("currentB.contains=" + UPDATED_CURRENT_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCurrentBNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where currentB does not contain DEFAULT_CURRENT_B
        defaultMeterReadingShouldNotBeFound("currentB.doesNotContain=" + DEFAULT_CURRENT_B);

        // Get all the meterReadingList where currentB does not contain UPDATED_CURRENT_B
        defaultMeterReadingShouldBeFound("currentB.doesNotContain=" + UPDATED_CURRENT_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByActivePowerBIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where activePowerB equals to DEFAULT_ACTIVE_POWER_B
        defaultMeterReadingShouldBeFound("activePowerB.equals=" + DEFAULT_ACTIVE_POWER_B);

        // Get all the meterReadingList where activePowerB equals to UPDATED_ACTIVE_POWER_B
        defaultMeterReadingShouldNotBeFound("activePowerB.equals=" + UPDATED_ACTIVE_POWER_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByActivePowerBIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where activePowerB not equals to DEFAULT_ACTIVE_POWER_B
        defaultMeterReadingShouldNotBeFound("activePowerB.notEquals=" + DEFAULT_ACTIVE_POWER_B);

        // Get all the meterReadingList where activePowerB not equals to UPDATED_ACTIVE_POWER_B
        defaultMeterReadingShouldBeFound("activePowerB.notEquals=" + UPDATED_ACTIVE_POWER_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByActivePowerBIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where activePowerB in DEFAULT_ACTIVE_POWER_B or UPDATED_ACTIVE_POWER_B
        defaultMeterReadingShouldBeFound("activePowerB.in=" + DEFAULT_ACTIVE_POWER_B + "," + UPDATED_ACTIVE_POWER_B);

        // Get all the meterReadingList where activePowerB equals to UPDATED_ACTIVE_POWER_B
        defaultMeterReadingShouldNotBeFound("activePowerB.in=" + UPDATED_ACTIVE_POWER_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByActivePowerBIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where activePowerB is not null
        defaultMeterReadingShouldBeFound("activePowerB.specified=true");

        // Get all the meterReadingList where activePowerB is null
        defaultMeterReadingShouldNotBeFound("activePowerB.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByActivePowerBContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where activePowerB contains DEFAULT_ACTIVE_POWER_B
        defaultMeterReadingShouldBeFound("activePowerB.contains=" + DEFAULT_ACTIVE_POWER_B);

        // Get all the meterReadingList where activePowerB contains UPDATED_ACTIVE_POWER_B
        defaultMeterReadingShouldNotBeFound("activePowerB.contains=" + UPDATED_ACTIVE_POWER_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByActivePowerBNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where activePowerB does not contain DEFAULT_ACTIVE_POWER_B
        defaultMeterReadingShouldNotBeFound("activePowerB.doesNotContain=" + DEFAULT_ACTIVE_POWER_B);

        // Get all the meterReadingList where activePowerB does not contain UPDATED_ACTIVE_POWER_B
        defaultMeterReadingShouldBeFound("activePowerB.doesNotContain=" + UPDATED_ACTIVE_POWER_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReactivePowerBIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where reactivePowerB equals to DEFAULT_REACTIVE_POWER_B
        defaultMeterReadingShouldBeFound("reactivePowerB.equals=" + DEFAULT_REACTIVE_POWER_B);

        // Get all the meterReadingList where reactivePowerB equals to UPDATED_REACTIVE_POWER_B
        defaultMeterReadingShouldNotBeFound("reactivePowerB.equals=" + UPDATED_REACTIVE_POWER_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReactivePowerBIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where reactivePowerB not equals to DEFAULT_REACTIVE_POWER_B
        defaultMeterReadingShouldNotBeFound("reactivePowerB.notEquals=" + DEFAULT_REACTIVE_POWER_B);

        // Get all the meterReadingList where reactivePowerB not equals to UPDATED_REACTIVE_POWER_B
        defaultMeterReadingShouldBeFound("reactivePowerB.notEquals=" + UPDATED_REACTIVE_POWER_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReactivePowerBIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where reactivePowerB in DEFAULT_REACTIVE_POWER_B or UPDATED_REACTIVE_POWER_B
        defaultMeterReadingShouldBeFound("reactivePowerB.in=" + DEFAULT_REACTIVE_POWER_B + "," + UPDATED_REACTIVE_POWER_B);

        // Get all the meterReadingList where reactivePowerB equals to UPDATED_REACTIVE_POWER_B
        defaultMeterReadingShouldNotBeFound("reactivePowerB.in=" + UPDATED_REACTIVE_POWER_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReactivePowerBIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where reactivePowerB is not null
        defaultMeterReadingShouldBeFound("reactivePowerB.specified=true");

        // Get all the meterReadingList where reactivePowerB is null
        defaultMeterReadingShouldNotBeFound("reactivePowerB.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReactivePowerBContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where reactivePowerB contains DEFAULT_REACTIVE_POWER_B
        defaultMeterReadingShouldBeFound("reactivePowerB.contains=" + DEFAULT_REACTIVE_POWER_B);

        // Get all the meterReadingList where reactivePowerB contains UPDATED_REACTIVE_POWER_B
        defaultMeterReadingShouldNotBeFound("reactivePowerB.contains=" + UPDATED_REACTIVE_POWER_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReactivePowerBNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where reactivePowerB does not contain DEFAULT_REACTIVE_POWER_B
        defaultMeterReadingShouldNotBeFound("reactivePowerB.doesNotContain=" + DEFAULT_REACTIVE_POWER_B);

        // Get all the meterReadingList where reactivePowerB does not contain UPDATED_REACTIVE_POWER_B
        defaultMeterReadingShouldBeFound("reactivePowerB.doesNotContain=" + UPDATED_REACTIVE_POWER_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByPowerFactorBIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where powerFactorB equals to DEFAULT_POWER_FACTOR_B
        defaultMeterReadingShouldBeFound("powerFactorB.equals=" + DEFAULT_POWER_FACTOR_B);

        // Get all the meterReadingList where powerFactorB equals to UPDATED_POWER_FACTOR_B
        defaultMeterReadingShouldNotBeFound("powerFactorB.equals=" + UPDATED_POWER_FACTOR_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByPowerFactorBIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where powerFactorB not equals to DEFAULT_POWER_FACTOR_B
        defaultMeterReadingShouldNotBeFound("powerFactorB.notEquals=" + DEFAULT_POWER_FACTOR_B);

        // Get all the meterReadingList where powerFactorB not equals to UPDATED_POWER_FACTOR_B
        defaultMeterReadingShouldBeFound("powerFactorB.notEquals=" + UPDATED_POWER_FACTOR_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByPowerFactorBIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where powerFactorB in DEFAULT_POWER_FACTOR_B or UPDATED_POWER_FACTOR_B
        defaultMeterReadingShouldBeFound("powerFactorB.in=" + DEFAULT_POWER_FACTOR_B + "," + UPDATED_POWER_FACTOR_B);

        // Get all the meterReadingList where powerFactorB equals to UPDATED_POWER_FACTOR_B
        defaultMeterReadingShouldNotBeFound("powerFactorB.in=" + UPDATED_POWER_FACTOR_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByPowerFactorBIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where powerFactorB is not null
        defaultMeterReadingShouldBeFound("powerFactorB.specified=true");

        // Get all the meterReadingList where powerFactorB is null
        defaultMeterReadingShouldNotBeFound("powerFactorB.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByPowerFactorBContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where powerFactorB contains DEFAULT_POWER_FACTOR_B
        defaultMeterReadingShouldBeFound("powerFactorB.contains=" + DEFAULT_POWER_FACTOR_B);

        // Get all the meterReadingList where powerFactorB contains UPDATED_POWER_FACTOR_B
        defaultMeterReadingShouldNotBeFound("powerFactorB.contains=" + UPDATED_POWER_FACTOR_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByPowerFactorBNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where powerFactorB does not contain DEFAULT_POWER_FACTOR_B
        defaultMeterReadingShouldNotBeFound("powerFactorB.doesNotContain=" + DEFAULT_POWER_FACTOR_B);

        // Get all the meterReadingList where powerFactorB does not contain UPDATED_POWER_FACTOR_B
        defaultMeterReadingShouldBeFound("powerFactorB.doesNotContain=" + UPDATED_POWER_FACTOR_B);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVoltageCIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where voltageC equals to DEFAULT_VOLTAGE_C
        defaultMeterReadingShouldBeFound("voltageC.equals=" + DEFAULT_VOLTAGE_C);

        // Get all the meterReadingList where voltageC equals to UPDATED_VOLTAGE_C
        defaultMeterReadingShouldNotBeFound("voltageC.equals=" + UPDATED_VOLTAGE_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVoltageCIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where voltageC not equals to DEFAULT_VOLTAGE_C
        defaultMeterReadingShouldNotBeFound("voltageC.notEquals=" + DEFAULT_VOLTAGE_C);

        // Get all the meterReadingList where voltageC not equals to UPDATED_VOLTAGE_C
        defaultMeterReadingShouldBeFound("voltageC.notEquals=" + UPDATED_VOLTAGE_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVoltageCIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where voltageC in DEFAULT_VOLTAGE_C or UPDATED_VOLTAGE_C
        defaultMeterReadingShouldBeFound("voltageC.in=" + DEFAULT_VOLTAGE_C + "," + UPDATED_VOLTAGE_C);

        // Get all the meterReadingList where voltageC equals to UPDATED_VOLTAGE_C
        defaultMeterReadingShouldNotBeFound("voltageC.in=" + UPDATED_VOLTAGE_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVoltageCIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where voltageC is not null
        defaultMeterReadingShouldBeFound("voltageC.specified=true");

        // Get all the meterReadingList where voltageC is null
        defaultMeterReadingShouldNotBeFound("voltageC.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVoltageCContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where voltageC contains DEFAULT_VOLTAGE_C
        defaultMeterReadingShouldBeFound("voltageC.contains=" + DEFAULT_VOLTAGE_C);

        // Get all the meterReadingList where voltageC contains UPDATED_VOLTAGE_C
        defaultMeterReadingShouldNotBeFound("voltageC.contains=" + UPDATED_VOLTAGE_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByVoltageCNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where voltageC does not contain DEFAULT_VOLTAGE_C
        defaultMeterReadingShouldNotBeFound("voltageC.doesNotContain=" + DEFAULT_VOLTAGE_C);

        // Get all the meterReadingList where voltageC does not contain UPDATED_VOLTAGE_C
        defaultMeterReadingShouldBeFound("voltageC.doesNotContain=" + UPDATED_VOLTAGE_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCurrentCIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where currentC equals to DEFAULT_CURRENT_C
        defaultMeterReadingShouldBeFound("currentC.equals=" + DEFAULT_CURRENT_C);

        // Get all the meterReadingList where currentC equals to UPDATED_CURRENT_C
        defaultMeterReadingShouldNotBeFound("currentC.equals=" + UPDATED_CURRENT_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCurrentCIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where currentC not equals to DEFAULT_CURRENT_C
        defaultMeterReadingShouldNotBeFound("currentC.notEquals=" + DEFAULT_CURRENT_C);

        // Get all the meterReadingList where currentC not equals to UPDATED_CURRENT_C
        defaultMeterReadingShouldBeFound("currentC.notEquals=" + UPDATED_CURRENT_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCurrentCIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where currentC in DEFAULT_CURRENT_C or UPDATED_CURRENT_C
        defaultMeterReadingShouldBeFound("currentC.in=" + DEFAULT_CURRENT_C + "," + UPDATED_CURRENT_C);

        // Get all the meterReadingList where currentC equals to UPDATED_CURRENT_C
        defaultMeterReadingShouldNotBeFound("currentC.in=" + UPDATED_CURRENT_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCurrentCIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where currentC is not null
        defaultMeterReadingShouldBeFound("currentC.specified=true");

        // Get all the meterReadingList where currentC is null
        defaultMeterReadingShouldNotBeFound("currentC.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCurrentCContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where currentC contains DEFAULT_CURRENT_C
        defaultMeterReadingShouldBeFound("currentC.contains=" + DEFAULT_CURRENT_C);

        // Get all the meterReadingList where currentC contains UPDATED_CURRENT_C
        defaultMeterReadingShouldNotBeFound("currentC.contains=" + UPDATED_CURRENT_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCurrentCNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where currentC does not contain DEFAULT_CURRENT_C
        defaultMeterReadingShouldNotBeFound("currentC.doesNotContain=" + DEFAULT_CURRENT_C);

        // Get all the meterReadingList where currentC does not contain UPDATED_CURRENT_C
        defaultMeterReadingShouldBeFound("currentC.doesNotContain=" + UPDATED_CURRENT_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByActivePowerCIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where activePowerC equals to DEFAULT_ACTIVE_POWER_C
        defaultMeterReadingShouldBeFound("activePowerC.equals=" + DEFAULT_ACTIVE_POWER_C);

        // Get all the meterReadingList where activePowerC equals to UPDATED_ACTIVE_POWER_C
        defaultMeterReadingShouldNotBeFound("activePowerC.equals=" + UPDATED_ACTIVE_POWER_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByActivePowerCIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where activePowerC not equals to DEFAULT_ACTIVE_POWER_C
        defaultMeterReadingShouldNotBeFound("activePowerC.notEquals=" + DEFAULT_ACTIVE_POWER_C);

        // Get all the meterReadingList where activePowerC not equals to UPDATED_ACTIVE_POWER_C
        defaultMeterReadingShouldBeFound("activePowerC.notEquals=" + UPDATED_ACTIVE_POWER_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByActivePowerCIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where activePowerC in DEFAULT_ACTIVE_POWER_C or UPDATED_ACTIVE_POWER_C
        defaultMeterReadingShouldBeFound("activePowerC.in=" + DEFAULT_ACTIVE_POWER_C + "," + UPDATED_ACTIVE_POWER_C);

        // Get all the meterReadingList where activePowerC equals to UPDATED_ACTIVE_POWER_C
        defaultMeterReadingShouldNotBeFound("activePowerC.in=" + UPDATED_ACTIVE_POWER_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByActivePowerCIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where activePowerC is not null
        defaultMeterReadingShouldBeFound("activePowerC.specified=true");

        // Get all the meterReadingList where activePowerC is null
        defaultMeterReadingShouldNotBeFound("activePowerC.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByActivePowerCContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where activePowerC contains DEFAULT_ACTIVE_POWER_C
        defaultMeterReadingShouldBeFound("activePowerC.contains=" + DEFAULT_ACTIVE_POWER_C);

        // Get all the meterReadingList where activePowerC contains UPDATED_ACTIVE_POWER_C
        defaultMeterReadingShouldNotBeFound("activePowerC.contains=" + UPDATED_ACTIVE_POWER_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByActivePowerCNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where activePowerC does not contain DEFAULT_ACTIVE_POWER_C
        defaultMeterReadingShouldNotBeFound("activePowerC.doesNotContain=" + DEFAULT_ACTIVE_POWER_C);

        // Get all the meterReadingList where activePowerC does not contain UPDATED_ACTIVE_POWER_C
        defaultMeterReadingShouldBeFound("activePowerC.doesNotContain=" + UPDATED_ACTIVE_POWER_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReactivePowerCIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where reactivePowerC equals to DEFAULT_REACTIVE_POWER_C
        defaultMeterReadingShouldBeFound("reactivePowerC.equals=" + DEFAULT_REACTIVE_POWER_C);

        // Get all the meterReadingList where reactivePowerC equals to UPDATED_REACTIVE_POWER_C
        defaultMeterReadingShouldNotBeFound("reactivePowerC.equals=" + UPDATED_REACTIVE_POWER_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReactivePowerCIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where reactivePowerC not equals to DEFAULT_REACTIVE_POWER_C
        defaultMeterReadingShouldNotBeFound("reactivePowerC.notEquals=" + DEFAULT_REACTIVE_POWER_C);

        // Get all the meterReadingList where reactivePowerC not equals to UPDATED_REACTIVE_POWER_C
        defaultMeterReadingShouldBeFound("reactivePowerC.notEquals=" + UPDATED_REACTIVE_POWER_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReactivePowerCIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where reactivePowerC in DEFAULT_REACTIVE_POWER_C or UPDATED_REACTIVE_POWER_C
        defaultMeterReadingShouldBeFound("reactivePowerC.in=" + DEFAULT_REACTIVE_POWER_C + "," + UPDATED_REACTIVE_POWER_C);

        // Get all the meterReadingList where reactivePowerC equals to UPDATED_REACTIVE_POWER_C
        defaultMeterReadingShouldNotBeFound("reactivePowerC.in=" + UPDATED_REACTIVE_POWER_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReactivePowerCIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where reactivePowerC is not null
        defaultMeterReadingShouldBeFound("reactivePowerC.specified=true");

        // Get all the meterReadingList where reactivePowerC is null
        defaultMeterReadingShouldNotBeFound("reactivePowerC.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReactivePowerCContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where reactivePowerC contains DEFAULT_REACTIVE_POWER_C
        defaultMeterReadingShouldBeFound("reactivePowerC.contains=" + DEFAULT_REACTIVE_POWER_C);

        // Get all the meterReadingList where reactivePowerC contains UPDATED_REACTIVE_POWER_C
        defaultMeterReadingShouldNotBeFound("reactivePowerC.contains=" + UPDATED_REACTIVE_POWER_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByReactivePowerCNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where reactivePowerC does not contain DEFAULT_REACTIVE_POWER_C
        defaultMeterReadingShouldNotBeFound("reactivePowerC.doesNotContain=" + DEFAULT_REACTIVE_POWER_C);

        // Get all the meterReadingList where reactivePowerC does not contain UPDATED_REACTIVE_POWER_C
        defaultMeterReadingShouldBeFound("reactivePowerC.doesNotContain=" + UPDATED_REACTIVE_POWER_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByPowerFactorCIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where powerFactorC equals to DEFAULT_POWER_FACTOR_C
        defaultMeterReadingShouldBeFound("powerFactorC.equals=" + DEFAULT_POWER_FACTOR_C);

        // Get all the meterReadingList where powerFactorC equals to UPDATED_POWER_FACTOR_C
        defaultMeterReadingShouldNotBeFound("powerFactorC.equals=" + UPDATED_POWER_FACTOR_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByPowerFactorCIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where powerFactorC not equals to DEFAULT_POWER_FACTOR_C
        defaultMeterReadingShouldNotBeFound("powerFactorC.notEquals=" + DEFAULT_POWER_FACTOR_C);

        // Get all the meterReadingList where powerFactorC not equals to UPDATED_POWER_FACTOR_C
        defaultMeterReadingShouldBeFound("powerFactorC.notEquals=" + UPDATED_POWER_FACTOR_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByPowerFactorCIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where powerFactorC in DEFAULT_POWER_FACTOR_C or UPDATED_POWER_FACTOR_C
        defaultMeterReadingShouldBeFound("powerFactorC.in=" + DEFAULT_POWER_FACTOR_C + "," + UPDATED_POWER_FACTOR_C);

        // Get all the meterReadingList where powerFactorC equals to UPDATED_POWER_FACTOR_C
        defaultMeterReadingShouldNotBeFound("powerFactorC.in=" + UPDATED_POWER_FACTOR_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByPowerFactorCIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where powerFactorC is not null
        defaultMeterReadingShouldBeFound("powerFactorC.specified=true");

        // Get all the meterReadingList where powerFactorC is null
        defaultMeterReadingShouldNotBeFound("powerFactorC.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByPowerFactorCContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where powerFactorC contains DEFAULT_POWER_FACTOR_C
        defaultMeterReadingShouldBeFound("powerFactorC.contains=" + DEFAULT_POWER_FACTOR_C);

        // Get all the meterReadingList where powerFactorC contains UPDATED_POWER_FACTOR_C
        defaultMeterReadingShouldNotBeFound("powerFactorC.contains=" + UPDATED_POWER_FACTOR_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByPowerFactorCNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where powerFactorC does not contain DEFAULT_POWER_FACTOR_C
        defaultMeterReadingShouldNotBeFound("powerFactorC.doesNotContain=" + DEFAULT_POWER_FACTOR_C);

        // Get all the meterReadingList where powerFactorC does not contain UPDATED_POWER_FACTOR_C
        defaultMeterReadingShouldBeFound("powerFactorC.doesNotContain=" + UPDATED_POWER_FACTOR_C);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where createdBy equals to DEFAULT_CREATED_BY
        defaultMeterReadingShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the meterReadingList where createdBy equals to UPDATED_CREATED_BY
        defaultMeterReadingShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where createdBy not equals to DEFAULT_CREATED_BY
        defaultMeterReadingShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the meterReadingList where createdBy not equals to UPDATED_CREATED_BY
        defaultMeterReadingShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultMeterReadingShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the meterReadingList where createdBy equals to UPDATED_CREATED_BY
        defaultMeterReadingShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where createdBy is not null
        defaultMeterReadingShouldBeFound("createdBy.specified=true");

        // Get all the meterReadingList where createdBy is null
        defaultMeterReadingShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where createdBy contains DEFAULT_CREATED_BY
        defaultMeterReadingShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the meterReadingList where createdBy contains UPDATED_CREATED_BY
        defaultMeterReadingShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where createdBy does not contain DEFAULT_CREATED_BY
        defaultMeterReadingShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the meterReadingList where createdBy does not contain UPDATED_CREATED_BY
        defaultMeterReadingShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCreatedOnIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where createdOn equals to DEFAULT_CREATED_ON
        defaultMeterReadingShouldBeFound("createdOn.equals=" + DEFAULT_CREATED_ON);

        // Get all the meterReadingList where createdOn equals to UPDATED_CREATED_ON
        defaultMeterReadingShouldNotBeFound("createdOn.equals=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCreatedOnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where createdOn not equals to DEFAULT_CREATED_ON
        defaultMeterReadingShouldNotBeFound("createdOn.notEquals=" + DEFAULT_CREATED_ON);

        // Get all the meterReadingList where createdOn not equals to UPDATED_CREATED_ON
        defaultMeterReadingShouldBeFound("createdOn.notEquals=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCreatedOnIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where createdOn in DEFAULT_CREATED_ON or UPDATED_CREATED_ON
        defaultMeterReadingShouldBeFound("createdOn.in=" + DEFAULT_CREATED_ON + "," + UPDATED_CREATED_ON);

        // Get all the meterReadingList where createdOn equals to UPDATED_CREATED_ON
        defaultMeterReadingShouldNotBeFound("createdOn.in=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByCreatedOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where createdOn is not null
        defaultMeterReadingShouldBeFound("createdOn.specified=true");

        // Get all the meterReadingList where createdOn is null
        defaultMeterReadingShouldNotBeFound("createdOn.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultMeterReadingShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the meterReadingList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultMeterReadingShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultMeterReadingShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the meterReadingList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultMeterReadingShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultMeterReadingShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the meterReadingList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultMeterReadingShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where lastModifiedBy is not null
        defaultMeterReadingShouldBeFound("lastModifiedBy.specified=true");

        // Get all the meterReadingList where lastModifiedBy is null
        defaultMeterReadingShouldNotBeFound("lastModifiedBy.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultMeterReadingShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the meterReadingList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultMeterReadingShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultMeterReadingShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the meterReadingList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultMeterReadingShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByLastModifiedOnIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where lastModifiedOn equals to DEFAULT_LAST_MODIFIED_ON
        defaultMeterReadingShouldBeFound("lastModifiedOn.equals=" + DEFAULT_LAST_MODIFIED_ON);

        // Get all the meterReadingList where lastModifiedOn equals to UPDATED_LAST_MODIFIED_ON
        defaultMeterReadingShouldNotBeFound("lastModifiedOn.equals=" + UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByLastModifiedOnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where lastModifiedOn not equals to DEFAULT_LAST_MODIFIED_ON
        defaultMeterReadingShouldNotBeFound("lastModifiedOn.notEquals=" + DEFAULT_LAST_MODIFIED_ON);

        // Get all the meterReadingList where lastModifiedOn not equals to UPDATED_LAST_MODIFIED_ON
        defaultMeterReadingShouldBeFound("lastModifiedOn.notEquals=" + UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByLastModifiedOnIsInShouldWork() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where lastModifiedOn in DEFAULT_LAST_MODIFIED_ON or UPDATED_LAST_MODIFIED_ON
        defaultMeterReadingShouldBeFound("lastModifiedOn.in=" + DEFAULT_LAST_MODIFIED_ON + "," + UPDATED_LAST_MODIFIED_ON);

        // Get all the meterReadingList where lastModifiedOn equals to UPDATED_LAST_MODIFIED_ON
        defaultMeterReadingShouldNotBeFound("lastModifiedOn.in=" + UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void getAllMeterReadingsByLastModifiedOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        // Get all the meterReadingList where lastModifiedOn is not null
        defaultMeterReadingShouldBeFound("lastModifiedOn.specified=true");

        // Get all the meterReadingList where lastModifiedOn is null
        defaultMeterReadingShouldNotBeFound("lastModifiedOn.specified=false");
    }

    @Test
    @Transactional
    void getAllMeterReadingsByMeterIsEqualToSomething() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);
        Meter meter;
        if (TestUtil.findAll(em, Meter.class).isEmpty()) {
            meter = MeterResourceIT.createEntity(em);
            em.persist(meter);
            em.flush();
        } else {
            meter = TestUtil.findAll(em, Meter.class).get(0);
        }
        em.persist(meter);
        em.flush();
        meterReading.setMeter(meter);
        meterReadingRepository.saveAndFlush(meterReading);
        Long meterId = meter.getId();

        // Get all the meterReadingList where meter equals to meterId
        defaultMeterReadingShouldBeFound("meterId.equals=" + meterId);

        // Get all the meterReadingList where meter equals to (meterId + 1)
        defaultMeterReadingShouldNotBeFound("meterId.equals=" + (meterId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMeterReadingShouldBeFound(String filter) throws Exception {
        restMeterReadingMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(meterReading.getId().intValue())))
            .andExpect(jsonPath("$.[*].meterName").value(hasItem(DEFAULT_METER_NAME)))
            .andExpect(jsonPath("$.[*].meterNumber").value(hasItem(DEFAULT_METER_NUMBER)))
            .andExpect(jsonPath("$.[*].meterAddress").value(hasItem(DEFAULT_METER_ADDRESS)))
            .andExpect(jsonPath("$.[*].cycle").value(hasItem(DEFAULT_CYCLE)))
            .andExpect(jsonPath("$.[*].ctRatio").value(hasItem(DEFAULT_CT_RATIO)))
            .andExpect(jsonPath("$.[*].vtRatio").value(hasItem(DEFAULT_VT_RATIO)))
            .andExpect(jsonPath("$.[*].totalImportActiveEnergy").value(hasItem(DEFAULT_TOTAL_IMPORT_ACTIVE_ENERGY)))
            .andExpect(jsonPath("$.[*].totalExportActiveEnergy").value(hasItem(DEFAULT_TOTAL_EXPORT_ACTIVE_ENERGY)))
            .andExpect(jsonPath("$.[*].totalImportReactiveEnergy").value(hasItem(DEFAULT_TOTAL_IMPORT_REACTIVE_ENERGY)))
            .andExpect(jsonPath("$.[*].totalExportReactiveEnergy").value(hasItem(DEFAULT_TOTAL_EXPORT_REACTIVE_ENERGY)))
            .andExpect(jsonPath("$.[*].totalImportApparentEnergy").value(hasItem(DEFAULT_TOTAL_IMPORT_APPARENT_ENERGY)))
            .andExpect(jsonPath("$.[*].totalExportApparentEnergy").value(hasItem(DEFAULT_TOTAL_EXPORT_APPARENT_ENERGY)))
            .andExpect(jsonPath("$.[*].totalInstantaneousActivePower").value(hasItem(DEFAULT_TOTAL_INSTANTANEOUS_ACTIVE_POWER)))
            .andExpect(jsonPath("$.[*].totalInstantaneousReactivePower").value(hasItem(DEFAULT_TOTAL_INSTANTANEOUS_REACTIVE_POWER)))
            .andExpect(jsonPath("$.[*].totalInstantaneousApparentPower").value(hasItem(DEFAULT_TOTAL_INSTANTANEOUS_APPARENT_POWER)))
            .andExpect(jsonPath("$.[*].totalPowerFactor").value(hasItem(DEFAULT_TOTAL_POWER_FACTOR)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].readOn").value(hasItem(DEFAULT_READ_ON.toString())))
            .andExpect(jsonPath("$.[*].importActiveEnergy1").value(hasItem(DEFAULT_IMPORT_ACTIVE_ENERGY_1)))
            .andExpect(jsonPath("$.[*].exportActiveEnergy1").value(hasItem(DEFAULT_EXPORT_ACTIVE_ENERGY_1)))
            .andExpect(jsonPath("$.[*].importReactiveEnergy1").value(hasItem(DEFAULT_IMPORT_REACTIVE_ENERGY_1)))
            .andExpect(jsonPath("$.[*].exportReactiveEnergy1").value(hasItem(DEFAULT_EXPORT_REACTIVE_ENERGY_1)))
            .andExpect(jsonPath("$.[*].importApparentEnergy1").value(hasItem(DEFAULT_IMPORT_APPARENT_ENERGY_1)))
            .andExpect(jsonPath("$.[*].exportApparentEnergy1").value(hasItem(DEFAULT_EXPORT_APPARENT_ENERGY_1)))
            .andExpect(jsonPath("$.[*].importActiveEnergy2").value(hasItem(DEFAULT_IMPORT_ACTIVE_ENERGY_2)))
            .andExpect(jsonPath("$.[*].exportActiveEnergy2").value(hasItem(DEFAULT_EXPORT_ACTIVE_ENERGY_2)))
            .andExpect(jsonPath("$.[*].importReactiveEnergy2").value(hasItem(DEFAULT_IMPORT_REACTIVE_ENERGY_2)))
            .andExpect(jsonPath("$.[*].exportReactiveEnergy2").value(hasItem(DEFAULT_EXPORT_REACTIVE_ENERGY_2)))
            .andExpect(jsonPath("$.[*].importApparentEnergy2").value(hasItem(DEFAULT_IMPORT_APPARENT_ENERGY_2)))
            .andExpect(jsonPath("$.[*].exportApparentEnergy2").value(hasItem(DEFAULT_EXPORT_APPARENT_ENERGY_2)))
            .andExpect(jsonPath("$.[*].importActiveEnergy3").value(hasItem(DEFAULT_IMPORT_ACTIVE_ENERGY_3)))
            .andExpect(jsonPath("$.[*].exportActiveEnergy3").value(hasItem(DEFAULT_EXPORT_ACTIVE_ENERGY_3)))
            .andExpect(jsonPath("$.[*].importReactiveEnergy3").value(hasItem(DEFAULT_IMPORT_REACTIVE_ENERGY_3)))
            .andExpect(jsonPath("$.[*].exportReactiveEnergy3").value(hasItem(DEFAULT_EXPORT_REACTIVE_ENERGY_3)))
            .andExpect(jsonPath("$.[*].importApparentEnergy3").value(hasItem(DEFAULT_IMPORT_APPARENT_ENERGY_3)))
            .andExpect(jsonPath("$.[*].exportApparentEnergy3").value(hasItem(DEFAULT_EXPORT_APPARENT_ENERGY_3)))
            .andExpect(jsonPath("$.[*].voltageA").value(hasItem(DEFAULT_VOLTAGE_A)))
            .andExpect(jsonPath("$.[*].currentA").value(hasItem(DEFAULT_CURRENT_A)))
            .andExpect(jsonPath("$.[*].activePowerA").value(hasItem(DEFAULT_ACTIVE_POWER_A)))
            .andExpect(jsonPath("$.[*].reactivePowerA").value(hasItem(DEFAULT_REACTIVE_POWER_A)))
            .andExpect(jsonPath("$.[*].powerFactorA").value(hasItem(DEFAULT_POWER_FACTOR_A)))
            .andExpect(jsonPath("$.[*].voltageB").value(hasItem(DEFAULT_VOLTAGE_B)))
            .andExpect(jsonPath("$.[*].currentB").value(hasItem(DEFAULT_CURRENT_B)))
            .andExpect(jsonPath("$.[*].activePowerB").value(hasItem(DEFAULT_ACTIVE_POWER_B)))
            .andExpect(jsonPath("$.[*].reactivePowerB").value(hasItem(DEFAULT_REACTIVE_POWER_B)))
            .andExpect(jsonPath("$.[*].powerFactorB").value(hasItem(DEFAULT_POWER_FACTOR_B)))
            .andExpect(jsonPath("$.[*].voltageC").value(hasItem(DEFAULT_VOLTAGE_C)))
            .andExpect(jsonPath("$.[*].currentC").value(hasItem(DEFAULT_CURRENT_C)))
            .andExpect(jsonPath("$.[*].activePowerC").value(hasItem(DEFAULT_ACTIVE_POWER_C)))
            .andExpect(jsonPath("$.[*].reactivePowerC").value(hasItem(DEFAULT_REACTIVE_POWER_C)))
            .andExpect(jsonPath("$.[*].powerFactorC").value(hasItem(DEFAULT_POWER_FACTOR_C)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedOn").value(hasItem(DEFAULT_LAST_MODIFIED_ON.toString())));

        // Check, that the count call also returns 1
        restMeterReadingMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMeterReadingShouldNotBeFound(String filter) throws Exception {
        restMeterReadingMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMeterReadingMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingMeterReading() throws Exception {
        // Get the meterReading
        restMeterReadingMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMeterReading() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        int databaseSizeBeforeUpdate = meterReadingRepository.findAll().size();

        // Update the meterReading
        MeterReading updatedMeterReading = meterReadingRepository.findById(meterReading.getId()).get();
        // Disconnect from session so that the updates on updatedMeterReading are not directly saved in db
        em.detach(updatedMeterReading);
        updatedMeterReading
            .meterName(UPDATED_METER_NAME)
            .meterNumber(UPDATED_METER_NUMBER)
            .meterAddress(UPDATED_METER_ADDRESS)
            .cycle(UPDATED_CYCLE)
            .ctRatio(UPDATED_CT_RATIO)
            .vtRatio(UPDATED_VT_RATIO)
            .totalImportActiveEnergy(UPDATED_TOTAL_IMPORT_ACTIVE_ENERGY)
            .totalExportActiveEnergy(UPDATED_TOTAL_EXPORT_ACTIVE_ENERGY)
            .totalImportReactiveEnergy(UPDATED_TOTAL_IMPORT_REACTIVE_ENERGY)
            .totalExportReactiveEnergy(UPDATED_TOTAL_EXPORT_REACTIVE_ENERGY)
            .totalImportApparentEnergy(UPDATED_TOTAL_IMPORT_APPARENT_ENERGY)
            .totalExportApparentEnergy(UPDATED_TOTAL_EXPORT_APPARENT_ENERGY)
            .totalInstantaneousActivePower(UPDATED_TOTAL_INSTANTANEOUS_ACTIVE_POWER)
            .totalInstantaneousReactivePower(UPDATED_TOTAL_INSTANTANEOUS_REACTIVE_POWER)
            .totalInstantaneousApparentPower(UPDATED_TOTAL_INSTANTANEOUS_APPARENT_POWER)
            .totalPowerFactor(UPDATED_TOTAL_POWER_FACTOR)
            .status(UPDATED_STATUS)
            .readOn(UPDATED_READ_ON)
            .importActiveEnergy1(UPDATED_IMPORT_ACTIVE_ENERGY_1)
            .exportActiveEnergy1(UPDATED_EXPORT_ACTIVE_ENERGY_1)
            .importReactiveEnergy1(UPDATED_IMPORT_REACTIVE_ENERGY_1)
            .exportReactiveEnergy1(UPDATED_EXPORT_REACTIVE_ENERGY_1)
            .importApparentEnergy1(UPDATED_IMPORT_APPARENT_ENERGY_1)
            .exportApparentEnergy1(UPDATED_EXPORT_APPARENT_ENERGY_1)
            .importActiveEnergy2(UPDATED_IMPORT_ACTIVE_ENERGY_2)
            .exportActiveEnergy2(UPDATED_EXPORT_ACTIVE_ENERGY_2)
            .importReactiveEnergy2(UPDATED_IMPORT_REACTIVE_ENERGY_2)
            .exportReactiveEnergy2(UPDATED_EXPORT_REACTIVE_ENERGY_2)
            .importApparentEnergy2(UPDATED_IMPORT_APPARENT_ENERGY_2)
            .exportApparentEnergy2(UPDATED_EXPORT_APPARENT_ENERGY_2)
            .importActiveEnergy3(UPDATED_IMPORT_ACTIVE_ENERGY_3)
            .exportActiveEnergy3(UPDATED_EXPORT_ACTIVE_ENERGY_3)
            .importReactiveEnergy3(UPDATED_IMPORT_REACTIVE_ENERGY_3)
            .exportReactiveEnergy3(UPDATED_EXPORT_REACTIVE_ENERGY_3)
            .importApparentEnergy3(UPDATED_IMPORT_APPARENT_ENERGY_3)
            .exportApparentEnergy3(UPDATED_EXPORT_APPARENT_ENERGY_3)
            .voltageA(UPDATED_VOLTAGE_A)
            .currentA(UPDATED_CURRENT_A)
            .activePowerA(UPDATED_ACTIVE_POWER_A)
            .reactivePowerA(UPDATED_REACTIVE_POWER_A)
            .powerFactorA(UPDATED_POWER_FACTOR_A)
            .voltageB(UPDATED_VOLTAGE_B)
            .currentB(UPDATED_CURRENT_B)
            .activePowerB(UPDATED_ACTIVE_POWER_B)
            .reactivePowerB(UPDATED_REACTIVE_POWER_B)
            .powerFactorB(UPDATED_POWER_FACTOR_B)
            .voltageC(UPDATED_VOLTAGE_C)
            .currentC(UPDATED_CURRENT_C)
            .activePowerC(UPDATED_ACTIVE_POWER_C)
            .reactivePowerC(UPDATED_REACTIVE_POWER_C)
            .powerFactorC(UPDATED_POWER_FACTOR_C)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedOn(UPDATED_LAST_MODIFIED_ON);
        MeterReadingDTO meterReadingDTO = meterReadingMapper.toDto(updatedMeterReading);

        restMeterReadingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, meterReadingDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meterReadingDTO))
            )
            .andExpect(status().isOk());

        // Validate the MeterReading in the database
        List<MeterReading> meterReadingList = meterReadingRepository.findAll();
        assertThat(meterReadingList).hasSize(databaseSizeBeforeUpdate);
        MeterReading testMeterReading = meterReadingList.get(meterReadingList.size() - 1);
        assertThat(testMeterReading.getMeterName()).isEqualTo(UPDATED_METER_NAME);
        assertThat(testMeterReading.getMeterNumber()).isEqualTo(UPDATED_METER_NUMBER);
        assertThat(testMeterReading.getMeterAddress()).isEqualTo(UPDATED_METER_ADDRESS);
        assertThat(testMeterReading.getCycle()).isEqualTo(UPDATED_CYCLE);
        assertThat(testMeterReading.getCtRatio()).isEqualTo(UPDATED_CT_RATIO);
        assertThat(testMeterReading.getVtRatio()).isEqualTo(UPDATED_VT_RATIO);
        assertThat(testMeterReading.getTotalImportActiveEnergy()).isEqualTo(UPDATED_TOTAL_IMPORT_ACTIVE_ENERGY);
        assertThat(testMeterReading.getTotalExportActiveEnergy()).isEqualTo(UPDATED_TOTAL_EXPORT_ACTIVE_ENERGY);
        assertThat(testMeterReading.getTotalImportReactiveEnergy()).isEqualTo(UPDATED_TOTAL_IMPORT_REACTIVE_ENERGY);
        assertThat(testMeterReading.getTotalExportReactiveEnergy()).isEqualTo(UPDATED_TOTAL_EXPORT_REACTIVE_ENERGY);
        assertThat(testMeterReading.getTotalImportApparentEnergy()).isEqualTo(UPDATED_TOTAL_IMPORT_APPARENT_ENERGY);
        assertThat(testMeterReading.getTotalExportApparentEnergy()).isEqualTo(UPDATED_TOTAL_EXPORT_APPARENT_ENERGY);
        assertThat(testMeterReading.getTotalInstantaneousActivePower()).isEqualTo(UPDATED_TOTAL_INSTANTANEOUS_ACTIVE_POWER);
        assertThat(testMeterReading.getTotalInstantaneousReactivePower()).isEqualTo(UPDATED_TOTAL_INSTANTANEOUS_REACTIVE_POWER);
        assertThat(testMeterReading.getTotalInstantaneousApparentPower()).isEqualTo(UPDATED_TOTAL_INSTANTANEOUS_APPARENT_POWER);
        assertThat(testMeterReading.getTotalPowerFactor()).isEqualTo(UPDATED_TOTAL_POWER_FACTOR);
        assertThat(testMeterReading.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testMeterReading.getReadOn()).isEqualTo(UPDATED_READ_ON);
        assertThat(testMeterReading.getImportActiveEnergy1()).isEqualTo(UPDATED_IMPORT_ACTIVE_ENERGY_1);
        assertThat(testMeterReading.getExportActiveEnergy1()).isEqualTo(UPDATED_EXPORT_ACTIVE_ENERGY_1);
        assertThat(testMeterReading.getImportReactiveEnergy1()).isEqualTo(UPDATED_IMPORT_REACTIVE_ENERGY_1);
        assertThat(testMeterReading.getExportReactiveEnergy1()).isEqualTo(UPDATED_EXPORT_REACTIVE_ENERGY_1);
        assertThat(testMeterReading.getImportApparentEnergy1()).isEqualTo(UPDATED_IMPORT_APPARENT_ENERGY_1);
        assertThat(testMeterReading.getExportApparentEnergy1()).isEqualTo(UPDATED_EXPORT_APPARENT_ENERGY_1);
        assertThat(testMeterReading.getImportActiveEnergy2()).isEqualTo(UPDATED_IMPORT_ACTIVE_ENERGY_2);
        assertThat(testMeterReading.getExportActiveEnergy2()).isEqualTo(UPDATED_EXPORT_ACTIVE_ENERGY_2);
        assertThat(testMeterReading.getImportReactiveEnergy2()).isEqualTo(UPDATED_IMPORT_REACTIVE_ENERGY_2);
        assertThat(testMeterReading.getExportReactiveEnergy2()).isEqualTo(UPDATED_EXPORT_REACTIVE_ENERGY_2);
        assertThat(testMeterReading.getImportApparentEnergy2()).isEqualTo(UPDATED_IMPORT_APPARENT_ENERGY_2);
        assertThat(testMeterReading.getExportApparentEnergy2()).isEqualTo(UPDATED_EXPORT_APPARENT_ENERGY_2);
        assertThat(testMeterReading.getImportActiveEnergy3()).isEqualTo(UPDATED_IMPORT_ACTIVE_ENERGY_3);
        assertThat(testMeterReading.getExportActiveEnergy3()).isEqualTo(UPDATED_EXPORT_ACTIVE_ENERGY_3);
        assertThat(testMeterReading.getImportReactiveEnergy3()).isEqualTo(UPDATED_IMPORT_REACTIVE_ENERGY_3);
        assertThat(testMeterReading.getExportReactiveEnergy3()).isEqualTo(UPDATED_EXPORT_REACTIVE_ENERGY_3);
        assertThat(testMeterReading.getImportApparentEnergy3()).isEqualTo(UPDATED_IMPORT_APPARENT_ENERGY_3);
        assertThat(testMeterReading.getExportApparentEnergy3()).isEqualTo(UPDATED_EXPORT_APPARENT_ENERGY_3);
        assertThat(testMeterReading.getVoltageA()).isEqualTo(UPDATED_VOLTAGE_A);
        assertThat(testMeterReading.getCurrentA()).isEqualTo(UPDATED_CURRENT_A);
        assertThat(testMeterReading.getActivePowerA()).isEqualTo(UPDATED_ACTIVE_POWER_A);
        assertThat(testMeterReading.getReactivePowerA()).isEqualTo(UPDATED_REACTIVE_POWER_A);
        assertThat(testMeterReading.getPowerFactorA()).isEqualTo(UPDATED_POWER_FACTOR_A);
        assertThat(testMeterReading.getVoltageB()).isEqualTo(UPDATED_VOLTAGE_B);
        assertThat(testMeterReading.getCurrentB()).isEqualTo(UPDATED_CURRENT_B);
        assertThat(testMeterReading.getActivePowerB()).isEqualTo(UPDATED_ACTIVE_POWER_B);
        assertThat(testMeterReading.getReactivePowerB()).isEqualTo(UPDATED_REACTIVE_POWER_B);
        assertThat(testMeterReading.getPowerFactorB()).isEqualTo(UPDATED_POWER_FACTOR_B);
        assertThat(testMeterReading.getVoltageC()).isEqualTo(UPDATED_VOLTAGE_C);
        assertThat(testMeterReading.getCurrentC()).isEqualTo(UPDATED_CURRENT_C);
        assertThat(testMeterReading.getActivePowerC()).isEqualTo(UPDATED_ACTIVE_POWER_C);
        assertThat(testMeterReading.getReactivePowerC()).isEqualTo(UPDATED_REACTIVE_POWER_C);
        assertThat(testMeterReading.getPowerFactorC()).isEqualTo(UPDATED_POWER_FACTOR_C);
        assertThat(testMeterReading.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testMeterReading.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testMeterReading.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testMeterReading.getLastModifiedOn()).isEqualTo(UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void putNonExistingMeterReading() throws Exception {
        int databaseSizeBeforeUpdate = meterReadingRepository.findAll().size();
        meterReading.setId(count.incrementAndGet());

        // Create the MeterReading
        MeterReadingDTO meterReadingDTO = meterReadingMapper.toDto(meterReading);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMeterReadingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, meterReadingDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meterReadingDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MeterReading in the database
        List<MeterReading> meterReadingList = meterReadingRepository.findAll();
        assertThat(meterReadingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMeterReading() throws Exception {
        int databaseSizeBeforeUpdate = meterReadingRepository.findAll().size();
        meterReading.setId(count.incrementAndGet());

        // Create the MeterReading
        MeterReadingDTO meterReadingDTO = meterReadingMapper.toDto(meterReading);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMeterReadingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meterReadingDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MeterReading in the database
        List<MeterReading> meterReadingList = meterReadingRepository.findAll();
        assertThat(meterReadingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMeterReading() throws Exception {
        int databaseSizeBeforeUpdate = meterReadingRepository.findAll().size();
        meterReading.setId(count.incrementAndGet());

        // Create the MeterReading
        MeterReadingDTO meterReadingDTO = meterReadingMapper.toDto(meterReading);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMeterReadingMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meterReadingDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MeterReading in the database
        List<MeterReading> meterReadingList = meterReadingRepository.findAll();
        assertThat(meterReadingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMeterReadingWithPatch() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        int databaseSizeBeforeUpdate = meterReadingRepository.findAll().size();

        // Update the meterReading using partial update
        MeterReading partialUpdatedMeterReading = new MeterReading();
        partialUpdatedMeterReading.setId(meterReading.getId());

        partialUpdatedMeterReading
            .meterNumber(UPDATED_METER_NUMBER)
            .cycle(UPDATED_CYCLE)
            .vtRatio(UPDATED_VT_RATIO)
            .totalExportActiveEnergy(UPDATED_TOTAL_EXPORT_ACTIVE_ENERGY)
            .totalInstantaneousActivePower(UPDATED_TOTAL_INSTANTANEOUS_ACTIVE_POWER)
            .totalInstantaneousReactivePower(UPDATED_TOTAL_INSTANTANEOUS_REACTIVE_POWER)
            .status(UPDATED_STATUS)
            .readOn(UPDATED_READ_ON)
            .exportActiveEnergy1(UPDATED_EXPORT_ACTIVE_ENERGY_1)
            .importReactiveEnergy1(UPDATED_IMPORT_REACTIVE_ENERGY_1)
            .exportReactiveEnergy1(UPDATED_EXPORT_REACTIVE_ENERGY_1)
            .exportApparentEnergy1(UPDATED_EXPORT_APPARENT_ENERGY_1)
            .importActiveEnergy2(UPDATED_IMPORT_ACTIVE_ENERGY_2)
            .importReactiveEnergy2(UPDATED_IMPORT_REACTIVE_ENERGY_2)
            .exportApparentEnergy2(UPDATED_EXPORT_APPARENT_ENERGY_2)
            .importActiveEnergy3(UPDATED_IMPORT_ACTIVE_ENERGY_3)
            .voltageA(UPDATED_VOLTAGE_A)
            .currentA(UPDATED_CURRENT_A)
            .activePowerA(UPDATED_ACTIVE_POWER_A)
            .reactivePowerA(UPDATED_REACTIVE_POWER_A)
            .powerFactorA(UPDATED_POWER_FACTOR_A)
            .reactivePowerB(UPDATED_REACTIVE_POWER_B)
            .powerFactorB(UPDATED_POWER_FACTOR_B)
            .voltageC(UPDATED_VOLTAGE_C)
            .activePowerC(UPDATED_ACTIVE_POWER_C)
            .reactivePowerC(UPDATED_REACTIVE_POWER_C)
            .powerFactorC(UPDATED_POWER_FACTOR_C)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedOn(UPDATED_LAST_MODIFIED_ON);

        restMeterReadingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMeterReading.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMeterReading))
            )
            .andExpect(status().isOk());

        // Validate the MeterReading in the database
        List<MeterReading> meterReadingList = meterReadingRepository.findAll();
        assertThat(meterReadingList).hasSize(databaseSizeBeforeUpdate);
        MeterReading testMeterReading = meterReadingList.get(meterReadingList.size() - 1);
        assertThat(testMeterReading.getMeterName()).isEqualTo(DEFAULT_METER_NAME);
        assertThat(testMeterReading.getMeterNumber()).isEqualTo(UPDATED_METER_NUMBER);
        assertThat(testMeterReading.getMeterAddress()).isEqualTo(DEFAULT_METER_ADDRESS);
        assertThat(testMeterReading.getCycle()).isEqualTo(UPDATED_CYCLE);
        assertThat(testMeterReading.getCtRatio()).isEqualTo(DEFAULT_CT_RATIO);
        assertThat(testMeterReading.getVtRatio()).isEqualTo(UPDATED_VT_RATIO);
        assertThat(testMeterReading.getTotalImportActiveEnergy()).isEqualTo(DEFAULT_TOTAL_IMPORT_ACTIVE_ENERGY);
        assertThat(testMeterReading.getTotalExportActiveEnergy()).isEqualTo(UPDATED_TOTAL_EXPORT_ACTIVE_ENERGY);
        assertThat(testMeterReading.getTotalImportReactiveEnergy()).isEqualTo(DEFAULT_TOTAL_IMPORT_REACTIVE_ENERGY);
        assertThat(testMeterReading.getTotalExportReactiveEnergy()).isEqualTo(DEFAULT_TOTAL_EXPORT_REACTIVE_ENERGY);
        assertThat(testMeterReading.getTotalImportApparentEnergy()).isEqualTo(DEFAULT_TOTAL_IMPORT_APPARENT_ENERGY);
        assertThat(testMeterReading.getTotalExportApparentEnergy()).isEqualTo(DEFAULT_TOTAL_EXPORT_APPARENT_ENERGY);
        assertThat(testMeterReading.getTotalInstantaneousActivePower()).isEqualTo(UPDATED_TOTAL_INSTANTANEOUS_ACTIVE_POWER);
        assertThat(testMeterReading.getTotalInstantaneousReactivePower()).isEqualTo(UPDATED_TOTAL_INSTANTANEOUS_REACTIVE_POWER);
        assertThat(testMeterReading.getTotalInstantaneousApparentPower()).isEqualTo(DEFAULT_TOTAL_INSTANTANEOUS_APPARENT_POWER);
        assertThat(testMeterReading.getTotalPowerFactor()).isEqualTo(DEFAULT_TOTAL_POWER_FACTOR);
        assertThat(testMeterReading.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testMeterReading.getReadOn()).isEqualTo(UPDATED_READ_ON);
        assertThat(testMeterReading.getImportActiveEnergy1()).isEqualTo(DEFAULT_IMPORT_ACTIVE_ENERGY_1);
        assertThat(testMeterReading.getExportActiveEnergy1()).isEqualTo(UPDATED_EXPORT_ACTIVE_ENERGY_1);
        assertThat(testMeterReading.getImportReactiveEnergy1()).isEqualTo(UPDATED_IMPORT_REACTIVE_ENERGY_1);
        assertThat(testMeterReading.getExportReactiveEnergy1()).isEqualTo(UPDATED_EXPORT_REACTIVE_ENERGY_1);
        assertThat(testMeterReading.getImportApparentEnergy1()).isEqualTo(DEFAULT_IMPORT_APPARENT_ENERGY_1);
        assertThat(testMeterReading.getExportApparentEnergy1()).isEqualTo(UPDATED_EXPORT_APPARENT_ENERGY_1);
        assertThat(testMeterReading.getImportActiveEnergy2()).isEqualTo(UPDATED_IMPORT_ACTIVE_ENERGY_2);
        assertThat(testMeterReading.getExportActiveEnergy2()).isEqualTo(DEFAULT_EXPORT_ACTIVE_ENERGY_2);
        assertThat(testMeterReading.getImportReactiveEnergy2()).isEqualTo(UPDATED_IMPORT_REACTIVE_ENERGY_2);
        assertThat(testMeterReading.getExportReactiveEnergy2()).isEqualTo(DEFAULT_EXPORT_REACTIVE_ENERGY_2);
        assertThat(testMeterReading.getImportApparentEnergy2()).isEqualTo(DEFAULT_IMPORT_APPARENT_ENERGY_2);
        assertThat(testMeterReading.getExportApparentEnergy2()).isEqualTo(UPDATED_EXPORT_APPARENT_ENERGY_2);
        assertThat(testMeterReading.getImportActiveEnergy3()).isEqualTo(UPDATED_IMPORT_ACTIVE_ENERGY_3);
        assertThat(testMeterReading.getExportActiveEnergy3()).isEqualTo(DEFAULT_EXPORT_ACTIVE_ENERGY_3);
        assertThat(testMeterReading.getImportReactiveEnergy3()).isEqualTo(DEFAULT_IMPORT_REACTIVE_ENERGY_3);
        assertThat(testMeterReading.getExportReactiveEnergy3()).isEqualTo(DEFAULT_EXPORT_REACTIVE_ENERGY_3);
        assertThat(testMeterReading.getImportApparentEnergy3()).isEqualTo(DEFAULT_IMPORT_APPARENT_ENERGY_3);
        assertThat(testMeterReading.getExportApparentEnergy3()).isEqualTo(DEFAULT_EXPORT_APPARENT_ENERGY_3);
        assertThat(testMeterReading.getVoltageA()).isEqualTo(UPDATED_VOLTAGE_A);
        assertThat(testMeterReading.getCurrentA()).isEqualTo(UPDATED_CURRENT_A);
        assertThat(testMeterReading.getActivePowerA()).isEqualTo(UPDATED_ACTIVE_POWER_A);
        assertThat(testMeterReading.getReactivePowerA()).isEqualTo(UPDATED_REACTIVE_POWER_A);
        assertThat(testMeterReading.getPowerFactorA()).isEqualTo(UPDATED_POWER_FACTOR_A);
        assertThat(testMeterReading.getVoltageB()).isEqualTo(DEFAULT_VOLTAGE_B);
        assertThat(testMeterReading.getCurrentB()).isEqualTo(DEFAULT_CURRENT_B);
        assertThat(testMeterReading.getActivePowerB()).isEqualTo(DEFAULT_ACTIVE_POWER_B);
        assertThat(testMeterReading.getReactivePowerB()).isEqualTo(UPDATED_REACTIVE_POWER_B);
        assertThat(testMeterReading.getPowerFactorB()).isEqualTo(UPDATED_POWER_FACTOR_B);
        assertThat(testMeterReading.getVoltageC()).isEqualTo(UPDATED_VOLTAGE_C);
        assertThat(testMeterReading.getCurrentC()).isEqualTo(DEFAULT_CURRENT_C);
        assertThat(testMeterReading.getActivePowerC()).isEqualTo(UPDATED_ACTIVE_POWER_C);
        assertThat(testMeterReading.getReactivePowerC()).isEqualTo(UPDATED_REACTIVE_POWER_C);
        assertThat(testMeterReading.getPowerFactorC()).isEqualTo(UPDATED_POWER_FACTOR_C);
        assertThat(testMeterReading.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testMeterReading.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testMeterReading.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testMeterReading.getLastModifiedOn()).isEqualTo(UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void fullUpdateMeterReadingWithPatch() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        int databaseSizeBeforeUpdate = meterReadingRepository.findAll().size();

        // Update the meterReading using partial update
        MeterReading partialUpdatedMeterReading = new MeterReading();
        partialUpdatedMeterReading.setId(meterReading.getId());

        partialUpdatedMeterReading
            .meterName(UPDATED_METER_NAME)
            .meterNumber(UPDATED_METER_NUMBER)
            .meterAddress(UPDATED_METER_ADDRESS)
            .cycle(UPDATED_CYCLE)
            .ctRatio(UPDATED_CT_RATIO)
            .vtRatio(UPDATED_VT_RATIO)
            .totalImportActiveEnergy(UPDATED_TOTAL_IMPORT_ACTIVE_ENERGY)
            .totalExportActiveEnergy(UPDATED_TOTAL_EXPORT_ACTIVE_ENERGY)
            .totalImportReactiveEnergy(UPDATED_TOTAL_IMPORT_REACTIVE_ENERGY)
            .totalExportReactiveEnergy(UPDATED_TOTAL_EXPORT_REACTIVE_ENERGY)
            .totalImportApparentEnergy(UPDATED_TOTAL_IMPORT_APPARENT_ENERGY)
            .totalExportApparentEnergy(UPDATED_TOTAL_EXPORT_APPARENT_ENERGY)
            .totalInstantaneousActivePower(UPDATED_TOTAL_INSTANTANEOUS_ACTIVE_POWER)
            .totalInstantaneousReactivePower(UPDATED_TOTAL_INSTANTANEOUS_REACTIVE_POWER)
            .totalInstantaneousApparentPower(UPDATED_TOTAL_INSTANTANEOUS_APPARENT_POWER)
            .totalPowerFactor(UPDATED_TOTAL_POWER_FACTOR)
            .status(UPDATED_STATUS)
            .readOn(UPDATED_READ_ON)
            .importActiveEnergy1(UPDATED_IMPORT_ACTIVE_ENERGY_1)
            .exportActiveEnergy1(UPDATED_EXPORT_ACTIVE_ENERGY_1)
            .importReactiveEnergy1(UPDATED_IMPORT_REACTIVE_ENERGY_1)
            .exportReactiveEnergy1(UPDATED_EXPORT_REACTIVE_ENERGY_1)
            .importApparentEnergy1(UPDATED_IMPORT_APPARENT_ENERGY_1)
            .exportApparentEnergy1(UPDATED_EXPORT_APPARENT_ENERGY_1)
            .importActiveEnergy2(UPDATED_IMPORT_ACTIVE_ENERGY_2)
            .exportActiveEnergy2(UPDATED_EXPORT_ACTIVE_ENERGY_2)
            .importReactiveEnergy2(UPDATED_IMPORT_REACTIVE_ENERGY_2)
            .exportReactiveEnergy2(UPDATED_EXPORT_REACTIVE_ENERGY_2)
            .importApparentEnergy2(UPDATED_IMPORT_APPARENT_ENERGY_2)
            .exportApparentEnergy2(UPDATED_EXPORT_APPARENT_ENERGY_2)
            .importActiveEnergy3(UPDATED_IMPORT_ACTIVE_ENERGY_3)
            .exportActiveEnergy3(UPDATED_EXPORT_ACTIVE_ENERGY_3)
            .importReactiveEnergy3(UPDATED_IMPORT_REACTIVE_ENERGY_3)
            .exportReactiveEnergy3(UPDATED_EXPORT_REACTIVE_ENERGY_3)
            .importApparentEnergy3(UPDATED_IMPORT_APPARENT_ENERGY_3)
            .exportApparentEnergy3(UPDATED_EXPORT_APPARENT_ENERGY_3)
            .voltageA(UPDATED_VOLTAGE_A)
            .currentA(UPDATED_CURRENT_A)
            .activePowerA(UPDATED_ACTIVE_POWER_A)
            .reactivePowerA(UPDATED_REACTIVE_POWER_A)
            .powerFactorA(UPDATED_POWER_FACTOR_A)
            .voltageB(UPDATED_VOLTAGE_B)
            .currentB(UPDATED_CURRENT_B)
            .activePowerB(UPDATED_ACTIVE_POWER_B)
            .reactivePowerB(UPDATED_REACTIVE_POWER_B)
            .powerFactorB(UPDATED_POWER_FACTOR_B)
            .voltageC(UPDATED_VOLTAGE_C)
            .currentC(UPDATED_CURRENT_C)
            .activePowerC(UPDATED_ACTIVE_POWER_C)
            .reactivePowerC(UPDATED_REACTIVE_POWER_C)
            .powerFactorC(UPDATED_POWER_FACTOR_C)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedOn(UPDATED_LAST_MODIFIED_ON);

        restMeterReadingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMeterReading.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMeterReading))
            )
            .andExpect(status().isOk());

        // Validate the MeterReading in the database
        List<MeterReading> meterReadingList = meterReadingRepository.findAll();
        assertThat(meterReadingList).hasSize(databaseSizeBeforeUpdate);
        MeterReading testMeterReading = meterReadingList.get(meterReadingList.size() - 1);
        assertThat(testMeterReading.getMeterName()).isEqualTo(UPDATED_METER_NAME);
        assertThat(testMeterReading.getMeterNumber()).isEqualTo(UPDATED_METER_NUMBER);
        assertThat(testMeterReading.getMeterAddress()).isEqualTo(UPDATED_METER_ADDRESS);
        assertThat(testMeterReading.getCycle()).isEqualTo(UPDATED_CYCLE);
        assertThat(testMeterReading.getCtRatio()).isEqualTo(UPDATED_CT_RATIO);
        assertThat(testMeterReading.getVtRatio()).isEqualTo(UPDATED_VT_RATIO);
        assertThat(testMeterReading.getTotalImportActiveEnergy()).isEqualTo(UPDATED_TOTAL_IMPORT_ACTIVE_ENERGY);
        assertThat(testMeterReading.getTotalExportActiveEnergy()).isEqualTo(UPDATED_TOTAL_EXPORT_ACTIVE_ENERGY);
        assertThat(testMeterReading.getTotalImportReactiveEnergy()).isEqualTo(UPDATED_TOTAL_IMPORT_REACTIVE_ENERGY);
        assertThat(testMeterReading.getTotalExportReactiveEnergy()).isEqualTo(UPDATED_TOTAL_EXPORT_REACTIVE_ENERGY);
        assertThat(testMeterReading.getTotalImportApparentEnergy()).isEqualTo(UPDATED_TOTAL_IMPORT_APPARENT_ENERGY);
        assertThat(testMeterReading.getTotalExportApparentEnergy()).isEqualTo(UPDATED_TOTAL_EXPORT_APPARENT_ENERGY);
        assertThat(testMeterReading.getTotalInstantaneousActivePower()).isEqualTo(UPDATED_TOTAL_INSTANTANEOUS_ACTIVE_POWER);
        assertThat(testMeterReading.getTotalInstantaneousReactivePower()).isEqualTo(UPDATED_TOTAL_INSTANTANEOUS_REACTIVE_POWER);
        assertThat(testMeterReading.getTotalInstantaneousApparentPower()).isEqualTo(UPDATED_TOTAL_INSTANTANEOUS_APPARENT_POWER);
        assertThat(testMeterReading.getTotalPowerFactor()).isEqualTo(UPDATED_TOTAL_POWER_FACTOR);
        assertThat(testMeterReading.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testMeterReading.getReadOn()).isEqualTo(UPDATED_READ_ON);
        assertThat(testMeterReading.getImportActiveEnergy1()).isEqualTo(UPDATED_IMPORT_ACTIVE_ENERGY_1);
        assertThat(testMeterReading.getExportActiveEnergy1()).isEqualTo(UPDATED_EXPORT_ACTIVE_ENERGY_1);
        assertThat(testMeterReading.getImportReactiveEnergy1()).isEqualTo(UPDATED_IMPORT_REACTIVE_ENERGY_1);
        assertThat(testMeterReading.getExportReactiveEnergy1()).isEqualTo(UPDATED_EXPORT_REACTIVE_ENERGY_1);
        assertThat(testMeterReading.getImportApparentEnergy1()).isEqualTo(UPDATED_IMPORT_APPARENT_ENERGY_1);
        assertThat(testMeterReading.getExportApparentEnergy1()).isEqualTo(UPDATED_EXPORT_APPARENT_ENERGY_1);
        assertThat(testMeterReading.getImportActiveEnergy2()).isEqualTo(UPDATED_IMPORT_ACTIVE_ENERGY_2);
        assertThat(testMeterReading.getExportActiveEnergy2()).isEqualTo(UPDATED_EXPORT_ACTIVE_ENERGY_2);
        assertThat(testMeterReading.getImportReactiveEnergy2()).isEqualTo(UPDATED_IMPORT_REACTIVE_ENERGY_2);
        assertThat(testMeterReading.getExportReactiveEnergy2()).isEqualTo(UPDATED_EXPORT_REACTIVE_ENERGY_2);
        assertThat(testMeterReading.getImportApparentEnergy2()).isEqualTo(UPDATED_IMPORT_APPARENT_ENERGY_2);
        assertThat(testMeterReading.getExportApparentEnergy2()).isEqualTo(UPDATED_EXPORT_APPARENT_ENERGY_2);
        assertThat(testMeterReading.getImportActiveEnergy3()).isEqualTo(UPDATED_IMPORT_ACTIVE_ENERGY_3);
        assertThat(testMeterReading.getExportActiveEnergy3()).isEqualTo(UPDATED_EXPORT_ACTIVE_ENERGY_3);
        assertThat(testMeterReading.getImportReactiveEnergy3()).isEqualTo(UPDATED_IMPORT_REACTIVE_ENERGY_3);
        assertThat(testMeterReading.getExportReactiveEnergy3()).isEqualTo(UPDATED_EXPORT_REACTIVE_ENERGY_3);
        assertThat(testMeterReading.getImportApparentEnergy3()).isEqualTo(UPDATED_IMPORT_APPARENT_ENERGY_3);
        assertThat(testMeterReading.getExportApparentEnergy3()).isEqualTo(UPDATED_EXPORT_APPARENT_ENERGY_3);
        assertThat(testMeterReading.getVoltageA()).isEqualTo(UPDATED_VOLTAGE_A);
        assertThat(testMeterReading.getCurrentA()).isEqualTo(UPDATED_CURRENT_A);
        assertThat(testMeterReading.getActivePowerA()).isEqualTo(UPDATED_ACTIVE_POWER_A);
        assertThat(testMeterReading.getReactivePowerA()).isEqualTo(UPDATED_REACTIVE_POWER_A);
        assertThat(testMeterReading.getPowerFactorA()).isEqualTo(UPDATED_POWER_FACTOR_A);
        assertThat(testMeterReading.getVoltageB()).isEqualTo(UPDATED_VOLTAGE_B);
        assertThat(testMeterReading.getCurrentB()).isEqualTo(UPDATED_CURRENT_B);
        assertThat(testMeterReading.getActivePowerB()).isEqualTo(UPDATED_ACTIVE_POWER_B);
        assertThat(testMeterReading.getReactivePowerB()).isEqualTo(UPDATED_REACTIVE_POWER_B);
        assertThat(testMeterReading.getPowerFactorB()).isEqualTo(UPDATED_POWER_FACTOR_B);
        assertThat(testMeterReading.getVoltageC()).isEqualTo(UPDATED_VOLTAGE_C);
        assertThat(testMeterReading.getCurrentC()).isEqualTo(UPDATED_CURRENT_C);
        assertThat(testMeterReading.getActivePowerC()).isEqualTo(UPDATED_ACTIVE_POWER_C);
        assertThat(testMeterReading.getReactivePowerC()).isEqualTo(UPDATED_REACTIVE_POWER_C);
        assertThat(testMeterReading.getPowerFactorC()).isEqualTo(UPDATED_POWER_FACTOR_C);
        assertThat(testMeterReading.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testMeterReading.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testMeterReading.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testMeterReading.getLastModifiedOn()).isEqualTo(UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void patchNonExistingMeterReading() throws Exception {
        int databaseSizeBeforeUpdate = meterReadingRepository.findAll().size();
        meterReading.setId(count.incrementAndGet());

        // Create the MeterReading
        MeterReadingDTO meterReadingDTO = meterReadingMapper.toDto(meterReading);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMeterReadingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, meterReadingDTO.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(meterReadingDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MeterReading in the database
        List<MeterReading> meterReadingList = meterReadingRepository.findAll();
        assertThat(meterReadingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMeterReading() throws Exception {
        int databaseSizeBeforeUpdate = meterReadingRepository.findAll().size();
        meterReading.setId(count.incrementAndGet());

        // Create the MeterReading
        MeterReadingDTO meterReadingDTO = meterReadingMapper.toDto(meterReading);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMeterReadingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(meterReadingDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MeterReading in the database
        List<MeterReading> meterReadingList = meterReadingRepository.findAll();
        assertThat(meterReadingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMeterReading() throws Exception {
        int databaseSizeBeforeUpdate = meterReadingRepository.findAll().size();
        meterReading.setId(count.incrementAndGet());

        // Create the MeterReading
        MeterReadingDTO meterReadingDTO = meterReadingMapper.toDto(meterReading);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMeterReadingMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(meterReadingDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MeterReading in the database
        List<MeterReading> meterReadingList = meterReadingRepository.findAll();
        assertThat(meterReadingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMeterReading() throws Exception {
        // Initialize the database
        meterReadingRepository.saveAndFlush(meterReading);

        int databaseSizeBeforeDelete = meterReadingRepository.findAll().size();

        // Delete the meterReading
        restMeterReadingMockMvc
            .perform(delete(ENTITY_API_URL_ID, meterReading.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MeterReading> meterReadingList = meterReadingRepository.findAll();
        assertThat(meterReadingList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
