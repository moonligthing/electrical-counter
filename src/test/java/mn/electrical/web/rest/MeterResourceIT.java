package mn.electrical.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import mn.electrical.IntegrationTest;
import mn.electrical.domain.CommonValue;
import mn.electrical.domain.Meter;
import mn.electrical.domain.Modem;
import mn.electrical.repository.MeterRepository;
import mn.electrical.service.criteria.MeterCriteria;
import mn.electrical.service.dto.MeterDTO;
import mn.electrical.service.mapper.MeterMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MeterResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MeterResourceIT {

    private static final String DEFAULT_MODEM_NAME = "AAAAAAAAAA";
    private static final String UPDATED_MODEM_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_MODEL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_MODEL_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_METER_TYPE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_METER_TYPE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CURRENT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CURRENT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_VOLTAGE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_VOLTAGE_NAME = "BBBBBBBBBB";

    private static final Instant DEFAULT_INSTALLATION_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_INSTALLATION_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_WARRANTY_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_WARRANTY_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_CT_RATIO = "AAAAAAAAAA";
    private static final String UPDATED_CT_RATIO = "BBBBBBBBBB";

    private static final String DEFAULT_VT_RATIO = "AAAAAAAAAA";
    private static final String UPDATED_VT_RATIO = "BBBBBBBBBB";

    private static final String DEFAULT_DEVICE_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_DEVICE_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_RATE_1 = "AAAAAAAAAA";
    private static final String UPDATED_RATE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_RATE_2 = "AAAAAAAAAA";
    private static final String UPDATED_RATE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_RATE_3 = "AAAAAAAAAA";
    private static final String UPDATED_RATE_3 = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/meters";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MeterRepository meterRepository;

    @Autowired
    private MeterMapper meterMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMeterMockMvc;

    private Meter meter;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Meter createEntity(EntityManager em) {
        Meter meter = new Meter()
            .modemName(DEFAULT_MODEM_NAME)
            .modelName(DEFAULT_MODEL_NAME)
            .meterTypeName(DEFAULT_METER_TYPE_NAME)
            .currentName(DEFAULT_CURRENT_NAME)
            .voltageName(DEFAULT_VOLTAGE_NAME)
            .installationOn(DEFAULT_INSTALLATION_ON)
            .warrantyOn(DEFAULT_WARRANTY_ON)
            .number(DEFAULT_NUMBER)
            .name(DEFAULT_NAME)
            .address(DEFAULT_ADDRESS)
            .ctRatio(DEFAULT_CT_RATIO)
            .vtRatio(DEFAULT_VT_RATIO)
            .deviceAddress(DEFAULT_DEVICE_ADDRESS)
            .status(DEFAULT_STATUS)
            .rate1(DEFAULT_RATE_1)
            .rate2(DEFAULT_RATE_2)
            .rate3(DEFAULT_RATE_3)
            .createdBy(DEFAULT_CREATED_BY)
            .createdOn(DEFAULT_CREATED_ON)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedOn(DEFAULT_LAST_MODIFIED_ON);
        return meter;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Meter createUpdatedEntity(EntityManager em) {
        Meter meter = new Meter()
            .modemName(UPDATED_MODEM_NAME)
            .modelName(UPDATED_MODEL_NAME)
            .meterTypeName(UPDATED_METER_TYPE_NAME)
            .currentName(UPDATED_CURRENT_NAME)
            .voltageName(UPDATED_VOLTAGE_NAME)
            .installationOn(UPDATED_INSTALLATION_ON)
            .warrantyOn(UPDATED_WARRANTY_ON)
            .number(UPDATED_NUMBER)
            .name(UPDATED_NAME)
            .address(UPDATED_ADDRESS)
            .ctRatio(UPDATED_CT_RATIO)
            .vtRatio(UPDATED_VT_RATIO)
            .deviceAddress(UPDATED_DEVICE_ADDRESS)
            .status(UPDATED_STATUS)
            .rate1(UPDATED_RATE_1)
            .rate2(UPDATED_RATE_2)
            .rate3(UPDATED_RATE_3)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedOn(UPDATED_LAST_MODIFIED_ON);
        return meter;
    }

    @BeforeEach
    public void initTest() {
        meter = createEntity(em);
    }

    @Test
    @Transactional
    void createMeter() throws Exception {
        int databaseSizeBeforeCreate = meterRepository.findAll().size();
        // Create the Meter
        MeterDTO meterDTO = meterMapper.toDto(meter);
        restMeterMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meterDTO))
            )
            .andExpect(status().isCreated());

        // Validate the Meter in the database
        List<Meter> meterList = meterRepository.findAll();
        assertThat(meterList).hasSize(databaseSizeBeforeCreate + 1);
        Meter testMeter = meterList.get(meterList.size() - 1);
        assertThat(testMeter.getModemName()).isEqualTo(DEFAULT_MODEM_NAME);
        assertThat(testMeter.getModelName()).isEqualTo(DEFAULT_MODEL_NAME);
        assertThat(testMeter.getMeterTypeName()).isEqualTo(DEFAULT_METER_TYPE_NAME);
        assertThat(testMeter.getCurrentName()).isEqualTo(DEFAULT_CURRENT_NAME);
        assertThat(testMeter.getVoltageName()).isEqualTo(DEFAULT_VOLTAGE_NAME);
        assertThat(testMeter.getInstallationOn()).isEqualTo(DEFAULT_INSTALLATION_ON);
        assertThat(testMeter.getWarrantyOn()).isEqualTo(DEFAULT_WARRANTY_ON);
        assertThat(testMeter.getNumber()).isEqualTo(DEFAULT_NUMBER);
        assertThat(testMeter.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testMeter.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testMeter.getCtRatio()).isEqualTo(DEFAULT_CT_RATIO);
        assertThat(testMeter.getVtRatio()).isEqualTo(DEFAULT_VT_RATIO);
        assertThat(testMeter.getDeviceAddress()).isEqualTo(DEFAULT_DEVICE_ADDRESS);
        assertThat(testMeter.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testMeter.getRate1()).isEqualTo(DEFAULT_RATE_1);
        assertThat(testMeter.getRate2()).isEqualTo(DEFAULT_RATE_2);
        assertThat(testMeter.getRate3()).isEqualTo(DEFAULT_RATE_3);
        assertThat(testMeter.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testMeter.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testMeter.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testMeter.getLastModifiedOn()).isEqualTo(DEFAULT_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void createMeterWithExistingId() throws Exception {
        // Create the Meter with an existing ID
        meter.setId(1L);
        MeterDTO meterDTO = meterMapper.toDto(meter);

        int databaseSizeBeforeCreate = meterRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMeterMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meterDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Meter in the database
        List<Meter> meterList = meterRepository.findAll();
        assertThat(meterList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllMeters() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList
        restMeterMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(meter.getId().intValue())))
            .andExpect(jsonPath("$.[*].modemName").value(hasItem(DEFAULT_MODEM_NAME)))
            .andExpect(jsonPath("$.[*].modelName").value(hasItem(DEFAULT_MODEL_NAME)))
            .andExpect(jsonPath("$.[*].meterTypeName").value(hasItem(DEFAULT_METER_TYPE_NAME)))
            .andExpect(jsonPath("$.[*].currentName").value(hasItem(DEFAULT_CURRENT_NAME)))
            .andExpect(jsonPath("$.[*].voltageName").value(hasItem(DEFAULT_VOLTAGE_NAME)))
            .andExpect(jsonPath("$.[*].installationOn").value(hasItem(DEFAULT_INSTALLATION_ON.toString())))
            .andExpect(jsonPath("$.[*].warrantyOn").value(hasItem(DEFAULT_WARRANTY_ON.toString())))
            .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].ctRatio").value(hasItem(DEFAULT_CT_RATIO)))
            .andExpect(jsonPath("$.[*].vtRatio").value(hasItem(DEFAULT_VT_RATIO)))
            .andExpect(jsonPath("$.[*].deviceAddress").value(hasItem(DEFAULT_DEVICE_ADDRESS)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].rate1").value(hasItem(DEFAULT_RATE_1)))
            .andExpect(jsonPath("$.[*].rate2").value(hasItem(DEFAULT_RATE_2)))
            .andExpect(jsonPath("$.[*].rate3").value(hasItem(DEFAULT_RATE_3)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedOn").value(hasItem(DEFAULT_LAST_MODIFIED_ON.toString())));
    }

    @Test
    @Transactional
    void getMeter() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get the meter
        restMeterMockMvc
            .perform(get(ENTITY_API_URL_ID, meter.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(meter.getId().intValue()))
            .andExpect(jsonPath("$.modemName").value(DEFAULT_MODEM_NAME))
            .andExpect(jsonPath("$.modelName").value(DEFAULT_MODEL_NAME))
            .andExpect(jsonPath("$.meterTypeName").value(DEFAULT_METER_TYPE_NAME))
            .andExpect(jsonPath("$.currentName").value(DEFAULT_CURRENT_NAME))
            .andExpect(jsonPath("$.voltageName").value(DEFAULT_VOLTAGE_NAME))
            .andExpect(jsonPath("$.installationOn").value(DEFAULT_INSTALLATION_ON.toString()))
            .andExpect(jsonPath("$.warrantyOn").value(DEFAULT_WARRANTY_ON.toString()))
            .andExpect(jsonPath("$.number").value(DEFAULT_NUMBER))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.ctRatio").value(DEFAULT_CT_RATIO))
            .andExpect(jsonPath("$.vtRatio").value(DEFAULT_VT_RATIO))
            .andExpect(jsonPath("$.deviceAddress").value(DEFAULT_DEVICE_ADDRESS))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.rate1").value(DEFAULT_RATE_1))
            .andExpect(jsonPath("$.rate2").value(DEFAULT_RATE_2))
            .andExpect(jsonPath("$.rate3").value(DEFAULT_RATE_3))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedOn").value(DEFAULT_LAST_MODIFIED_ON.toString()));
    }

    @Test
    @Transactional
    void getMetersByIdFiltering() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        Long id = meter.getId();

        defaultMeterShouldBeFound("id.equals=" + id);
        defaultMeterShouldNotBeFound("id.notEquals=" + id);

        defaultMeterShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultMeterShouldNotBeFound("id.greaterThan=" + id);

        defaultMeterShouldBeFound("id.lessThanOrEqual=" + id);
        defaultMeterShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllMetersByModemNameIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where modemName equals to DEFAULT_MODEM_NAME
        defaultMeterShouldBeFound("modemName.equals=" + DEFAULT_MODEM_NAME);

        // Get all the meterList where modemName equals to UPDATED_MODEM_NAME
        defaultMeterShouldNotBeFound("modemName.equals=" + UPDATED_MODEM_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByModemNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where modemName not equals to DEFAULT_MODEM_NAME
        defaultMeterShouldNotBeFound("modemName.notEquals=" + DEFAULT_MODEM_NAME);

        // Get all the meterList where modemName not equals to UPDATED_MODEM_NAME
        defaultMeterShouldBeFound("modemName.notEquals=" + UPDATED_MODEM_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByModemNameIsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where modemName in DEFAULT_MODEM_NAME or UPDATED_MODEM_NAME
        defaultMeterShouldBeFound("modemName.in=" + DEFAULT_MODEM_NAME + "," + UPDATED_MODEM_NAME);

        // Get all the meterList where modemName equals to UPDATED_MODEM_NAME
        defaultMeterShouldNotBeFound("modemName.in=" + UPDATED_MODEM_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByModemNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where modemName is not null
        defaultMeterShouldBeFound("modemName.specified=true");

        // Get all the meterList where modemName is null
        defaultMeterShouldNotBeFound("modemName.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByModemNameContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where modemName contains DEFAULT_MODEM_NAME
        defaultMeterShouldBeFound("modemName.contains=" + DEFAULT_MODEM_NAME);

        // Get all the meterList where modemName contains UPDATED_MODEM_NAME
        defaultMeterShouldNotBeFound("modemName.contains=" + UPDATED_MODEM_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByModemNameNotContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where modemName does not contain DEFAULT_MODEM_NAME
        defaultMeterShouldNotBeFound("modemName.doesNotContain=" + DEFAULT_MODEM_NAME);

        // Get all the meterList where modemName does not contain UPDATED_MODEM_NAME
        defaultMeterShouldBeFound("modemName.doesNotContain=" + UPDATED_MODEM_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByModelNameIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where modelName equals to DEFAULT_MODEL_NAME
        defaultMeterShouldBeFound("modelName.equals=" + DEFAULT_MODEL_NAME);

        // Get all the meterList where modelName equals to UPDATED_MODEL_NAME
        defaultMeterShouldNotBeFound("modelName.equals=" + UPDATED_MODEL_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByModelNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where modelName not equals to DEFAULT_MODEL_NAME
        defaultMeterShouldNotBeFound("modelName.notEquals=" + DEFAULT_MODEL_NAME);

        // Get all the meterList where modelName not equals to UPDATED_MODEL_NAME
        defaultMeterShouldBeFound("modelName.notEquals=" + UPDATED_MODEL_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByModelNameIsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where modelName in DEFAULT_MODEL_NAME or UPDATED_MODEL_NAME
        defaultMeterShouldBeFound("modelName.in=" + DEFAULT_MODEL_NAME + "," + UPDATED_MODEL_NAME);

        // Get all the meterList where modelName equals to UPDATED_MODEL_NAME
        defaultMeterShouldNotBeFound("modelName.in=" + UPDATED_MODEL_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByModelNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where modelName is not null
        defaultMeterShouldBeFound("modelName.specified=true");

        // Get all the meterList where modelName is null
        defaultMeterShouldNotBeFound("modelName.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByModelNameContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where modelName contains DEFAULT_MODEL_NAME
        defaultMeterShouldBeFound("modelName.contains=" + DEFAULT_MODEL_NAME);

        // Get all the meterList where modelName contains UPDATED_MODEL_NAME
        defaultMeterShouldNotBeFound("modelName.contains=" + UPDATED_MODEL_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByModelNameNotContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where modelName does not contain DEFAULT_MODEL_NAME
        defaultMeterShouldNotBeFound("modelName.doesNotContain=" + DEFAULT_MODEL_NAME);

        // Get all the meterList where modelName does not contain UPDATED_MODEL_NAME
        defaultMeterShouldBeFound("modelName.doesNotContain=" + UPDATED_MODEL_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByMeterTypeNameIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where meterTypeName equals to DEFAULT_METER_TYPE_NAME
        defaultMeterShouldBeFound("meterTypeName.equals=" + DEFAULT_METER_TYPE_NAME);

        // Get all the meterList where meterTypeName equals to UPDATED_METER_TYPE_NAME
        defaultMeterShouldNotBeFound("meterTypeName.equals=" + UPDATED_METER_TYPE_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByMeterTypeNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where meterTypeName not equals to DEFAULT_METER_TYPE_NAME
        defaultMeterShouldNotBeFound("meterTypeName.notEquals=" + DEFAULT_METER_TYPE_NAME);

        // Get all the meterList where meterTypeName not equals to UPDATED_METER_TYPE_NAME
        defaultMeterShouldBeFound("meterTypeName.notEquals=" + UPDATED_METER_TYPE_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByMeterTypeNameIsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where meterTypeName in DEFAULT_METER_TYPE_NAME or UPDATED_METER_TYPE_NAME
        defaultMeterShouldBeFound("meterTypeName.in=" + DEFAULT_METER_TYPE_NAME + "," + UPDATED_METER_TYPE_NAME);

        // Get all the meterList where meterTypeName equals to UPDATED_METER_TYPE_NAME
        defaultMeterShouldNotBeFound("meterTypeName.in=" + UPDATED_METER_TYPE_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByMeterTypeNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where meterTypeName is not null
        defaultMeterShouldBeFound("meterTypeName.specified=true");

        // Get all the meterList where meterTypeName is null
        defaultMeterShouldNotBeFound("meterTypeName.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByMeterTypeNameContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where meterTypeName contains DEFAULT_METER_TYPE_NAME
        defaultMeterShouldBeFound("meterTypeName.contains=" + DEFAULT_METER_TYPE_NAME);

        // Get all the meterList where meterTypeName contains UPDATED_METER_TYPE_NAME
        defaultMeterShouldNotBeFound("meterTypeName.contains=" + UPDATED_METER_TYPE_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByMeterTypeNameNotContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where meterTypeName does not contain DEFAULT_METER_TYPE_NAME
        defaultMeterShouldNotBeFound("meterTypeName.doesNotContain=" + DEFAULT_METER_TYPE_NAME);

        // Get all the meterList where meterTypeName does not contain UPDATED_METER_TYPE_NAME
        defaultMeterShouldBeFound("meterTypeName.doesNotContain=" + UPDATED_METER_TYPE_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByCurrentNameIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where currentName equals to DEFAULT_CURRENT_NAME
        defaultMeterShouldBeFound("currentName.equals=" + DEFAULT_CURRENT_NAME);

        // Get all the meterList where currentName equals to UPDATED_CURRENT_NAME
        defaultMeterShouldNotBeFound("currentName.equals=" + UPDATED_CURRENT_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByCurrentNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where currentName not equals to DEFAULT_CURRENT_NAME
        defaultMeterShouldNotBeFound("currentName.notEquals=" + DEFAULT_CURRENT_NAME);

        // Get all the meterList where currentName not equals to UPDATED_CURRENT_NAME
        defaultMeterShouldBeFound("currentName.notEquals=" + UPDATED_CURRENT_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByCurrentNameIsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where currentName in DEFAULT_CURRENT_NAME or UPDATED_CURRENT_NAME
        defaultMeterShouldBeFound("currentName.in=" + DEFAULT_CURRENT_NAME + "," + UPDATED_CURRENT_NAME);

        // Get all the meterList where currentName equals to UPDATED_CURRENT_NAME
        defaultMeterShouldNotBeFound("currentName.in=" + UPDATED_CURRENT_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByCurrentNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where currentName is not null
        defaultMeterShouldBeFound("currentName.specified=true");

        // Get all the meterList where currentName is null
        defaultMeterShouldNotBeFound("currentName.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByCurrentNameContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where currentName contains DEFAULT_CURRENT_NAME
        defaultMeterShouldBeFound("currentName.contains=" + DEFAULT_CURRENT_NAME);

        // Get all the meterList where currentName contains UPDATED_CURRENT_NAME
        defaultMeterShouldNotBeFound("currentName.contains=" + UPDATED_CURRENT_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByCurrentNameNotContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where currentName does not contain DEFAULT_CURRENT_NAME
        defaultMeterShouldNotBeFound("currentName.doesNotContain=" + DEFAULT_CURRENT_NAME);

        // Get all the meterList where currentName does not contain UPDATED_CURRENT_NAME
        defaultMeterShouldBeFound("currentName.doesNotContain=" + UPDATED_CURRENT_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByVoltageNameIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where voltageName equals to DEFAULT_VOLTAGE_NAME
        defaultMeterShouldBeFound("voltageName.equals=" + DEFAULT_VOLTAGE_NAME);

        // Get all the meterList where voltageName equals to UPDATED_VOLTAGE_NAME
        defaultMeterShouldNotBeFound("voltageName.equals=" + UPDATED_VOLTAGE_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByVoltageNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where voltageName not equals to DEFAULT_VOLTAGE_NAME
        defaultMeterShouldNotBeFound("voltageName.notEquals=" + DEFAULT_VOLTAGE_NAME);

        // Get all the meterList where voltageName not equals to UPDATED_VOLTAGE_NAME
        defaultMeterShouldBeFound("voltageName.notEquals=" + UPDATED_VOLTAGE_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByVoltageNameIsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where voltageName in DEFAULT_VOLTAGE_NAME or UPDATED_VOLTAGE_NAME
        defaultMeterShouldBeFound("voltageName.in=" + DEFAULT_VOLTAGE_NAME + "," + UPDATED_VOLTAGE_NAME);

        // Get all the meterList where voltageName equals to UPDATED_VOLTAGE_NAME
        defaultMeterShouldNotBeFound("voltageName.in=" + UPDATED_VOLTAGE_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByVoltageNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where voltageName is not null
        defaultMeterShouldBeFound("voltageName.specified=true");

        // Get all the meterList where voltageName is null
        defaultMeterShouldNotBeFound("voltageName.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByVoltageNameContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where voltageName contains DEFAULT_VOLTAGE_NAME
        defaultMeterShouldBeFound("voltageName.contains=" + DEFAULT_VOLTAGE_NAME);

        // Get all the meterList where voltageName contains UPDATED_VOLTAGE_NAME
        defaultMeterShouldNotBeFound("voltageName.contains=" + UPDATED_VOLTAGE_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByVoltageNameNotContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where voltageName does not contain DEFAULT_VOLTAGE_NAME
        defaultMeterShouldNotBeFound("voltageName.doesNotContain=" + DEFAULT_VOLTAGE_NAME);

        // Get all the meterList where voltageName does not contain UPDATED_VOLTAGE_NAME
        defaultMeterShouldBeFound("voltageName.doesNotContain=" + UPDATED_VOLTAGE_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByInstallationOnIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where installationOn equals to DEFAULT_INSTALLATION_ON
        defaultMeterShouldBeFound("installationOn.equals=" + DEFAULT_INSTALLATION_ON);

        // Get all the meterList where installationOn equals to UPDATED_INSTALLATION_ON
        defaultMeterShouldNotBeFound("installationOn.equals=" + UPDATED_INSTALLATION_ON);
    }

    @Test
    @Transactional
    void getAllMetersByInstallationOnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where installationOn not equals to DEFAULT_INSTALLATION_ON
        defaultMeterShouldNotBeFound("installationOn.notEquals=" + DEFAULT_INSTALLATION_ON);

        // Get all the meterList where installationOn not equals to UPDATED_INSTALLATION_ON
        defaultMeterShouldBeFound("installationOn.notEquals=" + UPDATED_INSTALLATION_ON);
    }

    @Test
    @Transactional
    void getAllMetersByInstallationOnIsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where installationOn in DEFAULT_INSTALLATION_ON or UPDATED_INSTALLATION_ON
        defaultMeterShouldBeFound("installationOn.in=" + DEFAULT_INSTALLATION_ON + "," + UPDATED_INSTALLATION_ON);

        // Get all the meterList where installationOn equals to UPDATED_INSTALLATION_ON
        defaultMeterShouldNotBeFound("installationOn.in=" + UPDATED_INSTALLATION_ON);
    }

    @Test
    @Transactional
    void getAllMetersByInstallationOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where installationOn is not null
        defaultMeterShouldBeFound("installationOn.specified=true");

        // Get all the meterList where installationOn is null
        defaultMeterShouldNotBeFound("installationOn.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByWarrantyOnIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where warrantyOn equals to DEFAULT_WARRANTY_ON
        defaultMeterShouldBeFound("warrantyOn.equals=" + DEFAULT_WARRANTY_ON);

        // Get all the meterList where warrantyOn equals to UPDATED_WARRANTY_ON
        defaultMeterShouldNotBeFound("warrantyOn.equals=" + UPDATED_WARRANTY_ON);
    }

    @Test
    @Transactional
    void getAllMetersByWarrantyOnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where warrantyOn not equals to DEFAULT_WARRANTY_ON
        defaultMeterShouldNotBeFound("warrantyOn.notEquals=" + DEFAULT_WARRANTY_ON);

        // Get all the meterList where warrantyOn not equals to UPDATED_WARRANTY_ON
        defaultMeterShouldBeFound("warrantyOn.notEquals=" + UPDATED_WARRANTY_ON);
    }

    @Test
    @Transactional
    void getAllMetersByWarrantyOnIsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where warrantyOn in DEFAULT_WARRANTY_ON or UPDATED_WARRANTY_ON
        defaultMeterShouldBeFound("warrantyOn.in=" + DEFAULT_WARRANTY_ON + "," + UPDATED_WARRANTY_ON);

        // Get all the meterList where warrantyOn equals to UPDATED_WARRANTY_ON
        defaultMeterShouldNotBeFound("warrantyOn.in=" + UPDATED_WARRANTY_ON);
    }

    @Test
    @Transactional
    void getAllMetersByWarrantyOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where warrantyOn is not null
        defaultMeterShouldBeFound("warrantyOn.specified=true");

        // Get all the meterList where warrantyOn is null
        defaultMeterShouldNotBeFound("warrantyOn.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where number equals to DEFAULT_NUMBER
        defaultMeterShouldBeFound("number.equals=" + DEFAULT_NUMBER);

        // Get all the meterList where number equals to UPDATED_NUMBER
        defaultMeterShouldNotBeFound("number.equals=" + UPDATED_NUMBER);
    }

    @Test
    @Transactional
    void getAllMetersByNumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where number not equals to DEFAULT_NUMBER
        defaultMeterShouldNotBeFound("number.notEquals=" + DEFAULT_NUMBER);

        // Get all the meterList where number not equals to UPDATED_NUMBER
        defaultMeterShouldBeFound("number.notEquals=" + UPDATED_NUMBER);
    }

    @Test
    @Transactional
    void getAllMetersByNumberIsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where number in DEFAULT_NUMBER or UPDATED_NUMBER
        defaultMeterShouldBeFound("number.in=" + DEFAULT_NUMBER + "," + UPDATED_NUMBER);

        // Get all the meterList where number equals to UPDATED_NUMBER
        defaultMeterShouldNotBeFound("number.in=" + UPDATED_NUMBER);
    }

    @Test
    @Transactional
    void getAllMetersByNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where number is not null
        defaultMeterShouldBeFound("number.specified=true");

        // Get all the meterList where number is null
        defaultMeterShouldNotBeFound("number.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByNumberContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where number contains DEFAULT_NUMBER
        defaultMeterShouldBeFound("number.contains=" + DEFAULT_NUMBER);

        // Get all the meterList where number contains UPDATED_NUMBER
        defaultMeterShouldNotBeFound("number.contains=" + UPDATED_NUMBER);
    }

    @Test
    @Transactional
    void getAllMetersByNumberNotContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where number does not contain DEFAULT_NUMBER
        defaultMeterShouldNotBeFound("number.doesNotContain=" + DEFAULT_NUMBER);

        // Get all the meterList where number does not contain UPDATED_NUMBER
        defaultMeterShouldBeFound("number.doesNotContain=" + UPDATED_NUMBER);
    }

    @Test
    @Transactional
    void getAllMetersByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where name equals to DEFAULT_NAME
        defaultMeterShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the meterList where name equals to UPDATED_NAME
        defaultMeterShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where name not equals to DEFAULT_NAME
        defaultMeterShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the meterList where name not equals to UPDATED_NAME
        defaultMeterShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByNameIsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where name in DEFAULT_NAME or UPDATED_NAME
        defaultMeterShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the meterList where name equals to UPDATED_NAME
        defaultMeterShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where name is not null
        defaultMeterShouldBeFound("name.specified=true");

        // Get all the meterList where name is null
        defaultMeterShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByNameContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where name contains DEFAULT_NAME
        defaultMeterShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the meterList where name contains UPDATED_NAME
        defaultMeterShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByNameNotContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where name does not contain DEFAULT_NAME
        defaultMeterShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the meterList where name does not contain UPDATED_NAME
        defaultMeterShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllMetersByAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where address equals to DEFAULT_ADDRESS
        defaultMeterShouldBeFound("address.equals=" + DEFAULT_ADDRESS);

        // Get all the meterList where address equals to UPDATED_ADDRESS
        defaultMeterShouldNotBeFound("address.equals=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    void getAllMetersByAddressIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where address not equals to DEFAULT_ADDRESS
        defaultMeterShouldNotBeFound("address.notEquals=" + DEFAULT_ADDRESS);

        // Get all the meterList where address not equals to UPDATED_ADDRESS
        defaultMeterShouldBeFound("address.notEquals=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    void getAllMetersByAddressIsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where address in DEFAULT_ADDRESS or UPDATED_ADDRESS
        defaultMeterShouldBeFound("address.in=" + DEFAULT_ADDRESS + "," + UPDATED_ADDRESS);

        // Get all the meterList where address equals to UPDATED_ADDRESS
        defaultMeterShouldNotBeFound("address.in=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    void getAllMetersByAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where address is not null
        defaultMeterShouldBeFound("address.specified=true");

        // Get all the meterList where address is null
        defaultMeterShouldNotBeFound("address.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByAddressContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where address contains DEFAULT_ADDRESS
        defaultMeterShouldBeFound("address.contains=" + DEFAULT_ADDRESS);

        // Get all the meterList where address contains UPDATED_ADDRESS
        defaultMeterShouldNotBeFound("address.contains=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    void getAllMetersByAddressNotContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where address does not contain DEFAULT_ADDRESS
        defaultMeterShouldNotBeFound("address.doesNotContain=" + DEFAULT_ADDRESS);

        // Get all the meterList where address does not contain UPDATED_ADDRESS
        defaultMeterShouldBeFound("address.doesNotContain=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    void getAllMetersByCtRatioIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where ctRatio equals to DEFAULT_CT_RATIO
        defaultMeterShouldBeFound("ctRatio.equals=" + DEFAULT_CT_RATIO);

        // Get all the meterList where ctRatio equals to UPDATED_CT_RATIO
        defaultMeterShouldNotBeFound("ctRatio.equals=" + UPDATED_CT_RATIO);
    }

    @Test
    @Transactional
    void getAllMetersByCtRatioIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where ctRatio not equals to DEFAULT_CT_RATIO
        defaultMeterShouldNotBeFound("ctRatio.notEquals=" + DEFAULT_CT_RATIO);

        // Get all the meterList where ctRatio not equals to UPDATED_CT_RATIO
        defaultMeterShouldBeFound("ctRatio.notEquals=" + UPDATED_CT_RATIO);
    }

    @Test
    @Transactional
    void getAllMetersByCtRatioIsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where ctRatio in DEFAULT_CT_RATIO or UPDATED_CT_RATIO
        defaultMeterShouldBeFound("ctRatio.in=" + DEFAULT_CT_RATIO + "," + UPDATED_CT_RATIO);

        // Get all the meterList where ctRatio equals to UPDATED_CT_RATIO
        defaultMeterShouldNotBeFound("ctRatio.in=" + UPDATED_CT_RATIO);
    }

    @Test
    @Transactional
    void getAllMetersByCtRatioIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where ctRatio is not null
        defaultMeterShouldBeFound("ctRatio.specified=true");

        // Get all the meterList where ctRatio is null
        defaultMeterShouldNotBeFound("ctRatio.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByCtRatioContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where ctRatio contains DEFAULT_CT_RATIO
        defaultMeterShouldBeFound("ctRatio.contains=" + DEFAULT_CT_RATIO);

        // Get all the meterList where ctRatio contains UPDATED_CT_RATIO
        defaultMeterShouldNotBeFound("ctRatio.contains=" + UPDATED_CT_RATIO);
    }

    @Test
    @Transactional
    void getAllMetersByCtRatioNotContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where ctRatio does not contain DEFAULT_CT_RATIO
        defaultMeterShouldNotBeFound("ctRatio.doesNotContain=" + DEFAULT_CT_RATIO);

        // Get all the meterList where ctRatio does not contain UPDATED_CT_RATIO
        defaultMeterShouldBeFound("ctRatio.doesNotContain=" + UPDATED_CT_RATIO);
    }

    @Test
    @Transactional
    void getAllMetersByVtRatioIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where vtRatio equals to DEFAULT_VT_RATIO
        defaultMeterShouldBeFound("vtRatio.equals=" + DEFAULT_VT_RATIO);

        // Get all the meterList where vtRatio equals to UPDATED_VT_RATIO
        defaultMeterShouldNotBeFound("vtRatio.equals=" + UPDATED_VT_RATIO);
    }

    @Test
    @Transactional
    void getAllMetersByVtRatioIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where vtRatio not equals to DEFAULT_VT_RATIO
        defaultMeterShouldNotBeFound("vtRatio.notEquals=" + DEFAULT_VT_RATIO);

        // Get all the meterList where vtRatio not equals to UPDATED_VT_RATIO
        defaultMeterShouldBeFound("vtRatio.notEquals=" + UPDATED_VT_RATIO);
    }

    @Test
    @Transactional
    void getAllMetersByVtRatioIsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where vtRatio in DEFAULT_VT_RATIO or UPDATED_VT_RATIO
        defaultMeterShouldBeFound("vtRatio.in=" + DEFAULT_VT_RATIO + "," + UPDATED_VT_RATIO);

        // Get all the meterList where vtRatio equals to UPDATED_VT_RATIO
        defaultMeterShouldNotBeFound("vtRatio.in=" + UPDATED_VT_RATIO);
    }

    @Test
    @Transactional
    void getAllMetersByVtRatioIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where vtRatio is not null
        defaultMeterShouldBeFound("vtRatio.specified=true");

        // Get all the meterList where vtRatio is null
        defaultMeterShouldNotBeFound("vtRatio.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByVtRatioContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where vtRatio contains DEFAULT_VT_RATIO
        defaultMeterShouldBeFound("vtRatio.contains=" + DEFAULT_VT_RATIO);

        // Get all the meterList where vtRatio contains UPDATED_VT_RATIO
        defaultMeterShouldNotBeFound("vtRatio.contains=" + UPDATED_VT_RATIO);
    }

    @Test
    @Transactional
    void getAllMetersByVtRatioNotContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where vtRatio does not contain DEFAULT_VT_RATIO
        defaultMeterShouldNotBeFound("vtRatio.doesNotContain=" + DEFAULT_VT_RATIO);

        // Get all the meterList where vtRatio does not contain UPDATED_VT_RATIO
        defaultMeterShouldBeFound("vtRatio.doesNotContain=" + UPDATED_VT_RATIO);
    }

    @Test
    @Transactional
    void getAllMetersByDeviceAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where deviceAddress equals to DEFAULT_DEVICE_ADDRESS
        defaultMeterShouldBeFound("deviceAddress.equals=" + DEFAULT_DEVICE_ADDRESS);

        // Get all the meterList where deviceAddress equals to UPDATED_DEVICE_ADDRESS
        defaultMeterShouldNotBeFound("deviceAddress.equals=" + UPDATED_DEVICE_ADDRESS);
    }

    @Test
    @Transactional
    void getAllMetersByDeviceAddressIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where deviceAddress not equals to DEFAULT_DEVICE_ADDRESS
        defaultMeterShouldNotBeFound("deviceAddress.notEquals=" + DEFAULT_DEVICE_ADDRESS);

        // Get all the meterList where deviceAddress not equals to UPDATED_DEVICE_ADDRESS
        defaultMeterShouldBeFound("deviceAddress.notEquals=" + UPDATED_DEVICE_ADDRESS);
    }

    @Test
    @Transactional
    void getAllMetersByDeviceAddressIsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where deviceAddress in DEFAULT_DEVICE_ADDRESS or UPDATED_DEVICE_ADDRESS
        defaultMeterShouldBeFound("deviceAddress.in=" + DEFAULT_DEVICE_ADDRESS + "," + UPDATED_DEVICE_ADDRESS);

        // Get all the meterList where deviceAddress equals to UPDATED_DEVICE_ADDRESS
        defaultMeterShouldNotBeFound("deviceAddress.in=" + UPDATED_DEVICE_ADDRESS);
    }

    @Test
    @Transactional
    void getAllMetersByDeviceAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where deviceAddress is not null
        defaultMeterShouldBeFound("deviceAddress.specified=true");

        // Get all the meterList where deviceAddress is null
        defaultMeterShouldNotBeFound("deviceAddress.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByDeviceAddressContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where deviceAddress contains DEFAULT_DEVICE_ADDRESS
        defaultMeterShouldBeFound("deviceAddress.contains=" + DEFAULT_DEVICE_ADDRESS);

        // Get all the meterList where deviceAddress contains UPDATED_DEVICE_ADDRESS
        defaultMeterShouldNotBeFound("deviceAddress.contains=" + UPDATED_DEVICE_ADDRESS);
    }

    @Test
    @Transactional
    void getAllMetersByDeviceAddressNotContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where deviceAddress does not contain DEFAULT_DEVICE_ADDRESS
        defaultMeterShouldNotBeFound("deviceAddress.doesNotContain=" + DEFAULT_DEVICE_ADDRESS);

        // Get all the meterList where deviceAddress does not contain UPDATED_DEVICE_ADDRESS
        defaultMeterShouldBeFound("deviceAddress.doesNotContain=" + UPDATED_DEVICE_ADDRESS);
    }

    @Test
    @Transactional
    void getAllMetersByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where status equals to DEFAULT_STATUS
        defaultMeterShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the meterList where status equals to UPDATED_STATUS
        defaultMeterShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllMetersByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where status not equals to DEFAULT_STATUS
        defaultMeterShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the meterList where status not equals to UPDATED_STATUS
        defaultMeterShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllMetersByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultMeterShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the meterList where status equals to UPDATED_STATUS
        defaultMeterShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllMetersByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where status is not null
        defaultMeterShouldBeFound("status.specified=true");

        // Get all the meterList where status is null
        defaultMeterShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByStatusContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where status contains DEFAULT_STATUS
        defaultMeterShouldBeFound("status.contains=" + DEFAULT_STATUS);

        // Get all the meterList where status contains UPDATED_STATUS
        defaultMeterShouldNotBeFound("status.contains=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllMetersByStatusNotContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where status does not contain DEFAULT_STATUS
        defaultMeterShouldNotBeFound("status.doesNotContain=" + DEFAULT_STATUS);

        // Get all the meterList where status does not contain UPDATED_STATUS
        defaultMeterShouldBeFound("status.doesNotContain=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllMetersByRate1IsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where rate1 equals to DEFAULT_RATE_1
        defaultMeterShouldBeFound("rate1.equals=" + DEFAULT_RATE_1);

        // Get all the meterList where rate1 equals to UPDATED_RATE_1
        defaultMeterShouldNotBeFound("rate1.equals=" + UPDATED_RATE_1);
    }

    @Test
    @Transactional
    void getAllMetersByRate1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where rate1 not equals to DEFAULT_RATE_1
        defaultMeterShouldNotBeFound("rate1.notEquals=" + DEFAULT_RATE_1);

        // Get all the meterList where rate1 not equals to UPDATED_RATE_1
        defaultMeterShouldBeFound("rate1.notEquals=" + UPDATED_RATE_1);
    }

    @Test
    @Transactional
    void getAllMetersByRate1IsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where rate1 in DEFAULT_RATE_1 or UPDATED_RATE_1
        defaultMeterShouldBeFound("rate1.in=" + DEFAULT_RATE_1 + "," + UPDATED_RATE_1);

        // Get all the meterList where rate1 equals to UPDATED_RATE_1
        defaultMeterShouldNotBeFound("rate1.in=" + UPDATED_RATE_1);
    }

    @Test
    @Transactional
    void getAllMetersByRate1IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where rate1 is not null
        defaultMeterShouldBeFound("rate1.specified=true");

        // Get all the meterList where rate1 is null
        defaultMeterShouldNotBeFound("rate1.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByRate1ContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where rate1 contains DEFAULT_RATE_1
        defaultMeterShouldBeFound("rate1.contains=" + DEFAULT_RATE_1);

        // Get all the meterList where rate1 contains UPDATED_RATE_1
        defaultMeterShouldNotBeFound("rate1.contains=" + UPDATED_RATE_1);
    }

    @Test
    @Transactional
    void getAllMetersByRate1NotContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where rate1 does not contain DEFAULT_RATE_1
        defaultMeterShouldNotBeFound("rate1.doesNotContain=" + DEFAULT_RATE_1);

        // Get all the meterList where rate1 does not contain UPDATED_RATE_1
        defaultMeterShouldBeFound("rate1.doesNotContain=" + UPDATED_RATE_1);
    }

    @Test
    @Transactional
    void getAllMetersByRate2IsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where rate2 equals to DEFAULT_RATE_2
        defaultMeterShouldBeFound("rate2.equals=" + DEFAULT_RATE_2);

        // Get all the meterList where rate2 equals to UPDATED_RATE_2
        defaultMeterShouldNotBeFound("rate2.equals=" + UPDATED_RATE_2);
    }

    @Test
    @Transactional
    void getAllMetersByRate2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where rate2 not equals to DEFAULT_RATE_2
        defaultMeterShouldNotBeFound("rate2.notEquals=" + DEFAULT_RATE_2);

        // Get all the meterList where rate2 not equals to UPDATED_RATE_2
        defaultMeterShouldBeFound("rate2.notEquals=" + UPDATED_RATE_2);
    }

    @Test
    @Transactional
    void getAllMetersByRate2IsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where rate2 in DEFAULT_RATE_2 or UPDATED_RATE_2
        defaultMeterShouldBeFound("rate2.in=" + DEFAULT_RATE_2 + "," + UPDATED_RATE_2);

        // Get all the meterList where rate2 equals to UPDATED_RATE_2
        defaultMeterShouldNotBeFound("rate2.in=" + UPDATED_RATE_2);
    }

    @Test
    @Transactional
    void getAllMetersByRate2IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where rate2 is not null
        defaultMeterShouldBeFound("rate2.specified=true");

        // Get all the meterList where rate2 is null
        defaultMeterShouldNotBeFound("rate2.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByRate2ContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where rate2 contains DEFAULT_RATE_2
        defaultMeterShouldBeFound("rate2.contains=" + DEFAULT_RATE_2);

        // Get all the meterList where rate2 contains UPDATED_RATE_2
        defaultMeterShouldNotBeFound("rate2.contains=" + UPDATED_RATE_2);
    }

    @Test
    @Transactional
    void getAllMetersByRate2NotContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where rate2 does not contain DEFAULT_RATE_2
        defaultMeterShouldNotBeFound("rate2.doesNotContain=" + DEFAULT_RATE_2);

        // Get all the meterList where rate2 does not contain UPDATED_RATE_2
        defaultMeterShouldBeFound("rate2.doesNotContain=" + UPDATED_RATE_2);
    }

    @Test
    @Transactional
    void getAllMetersByRate3IsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where rate3 equals to DEFAULT_RATE_3
        defaultMeterShouldBeFound("rate3.equals=" + DEFAULT_RATE_3);

        // Get all the meterList where rate3 equals to UPDATED_RATE_3
        defaultMeterShouldNotBeFound("rate3.equals=" + UPDATED_RATE_3);
    }

    @Test
    @Transactional
    void getAllMetersByRate3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where rate3 not equals to DEFAULT_RATE_3
        defaultMeterShouldNotBeFound("rate3.notEquals=" + DEFAULT_RATE_3);

        // Get all the meterList where rate3 not equals to UPDATED_RATE_3
        defaultMeterShouldBeFound("rate3.notEquals=" + UPDATED_RATE_3);
    }

    @Test
    @Transactional
    void getAllMetersByRate3IsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where rate3 in DEFAULT_RATE_3 or UPDATED_RATE_3
        defaultMeterShouldBeFound("rate3.in=" + DEFAULT_RATE_3 + "," + UPDATED_RATE_3);

        // Get all the meterList where rate3 equals to UPDATED_RATE_3
        defaultMeterShouldNotBeFound("rate3.in=" + UPDATED_RATE_3);
    }

    @Test
    @Transactional
    void getAllMetersByRate3IsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where rate3 is not null
        defaultMeterShouldBeFound("rate3.specified=true");

        // Get all the meterList where rate3 is null
        defaultMeterShouldNotBeFound("rate3.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByRate3ContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where rate3 contains DEFAULT_RATE_3
        defaultMeterShouldBeFound("rate3.contains=" + DEFAULT_RATE_3);

        // Get all the meterList where rate3 contains UPDATED_RATE_3
        defaultMeterShouldNotBeFound("rate3.contains=" + UPDATED_RATE_3);
    }

    @Test
    @Transactional
    void getAllMetersByRate3NotContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where rate3 does not contain DEFAULT_RATE_3
        defaultMeterShouldNotBeFound("rate3.doesNotContain=" + DEFAULT_RATE_3);

        // Get all the meterList where rate3 does not contain UPDATED_RATE_3
        defaultMeterShouldBeFound("rate3.doesNotContain=" + UPDATED_RATE_3);
    }

    @Test
    @Transactional
    void getAllMetersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where createdBy equals to DEFAULT_CREATED_BY
        defaultMeterShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the meterList where createdBy equals to UPDATED_CREATED_BY
        defaultMeterShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllMetersByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where createdBy not equals to DEFAULT_CREATED_BY
        defaultMeterShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the meterList where createdBy not equals to UPDATED_CREATED_BY
        defaultMeterShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllMetersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultMeterShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the meterList where createdBy equals to UPDATED_CREATED_BY
        defaultMeterShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllMetersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where createdBy is not null
        defaultMeterShouldBeFound("createdBy.specified=true");

        // Get all the meterList where createdBy is null
        defaultMeterShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where createdBy contains DEFAULT_CREATED_BY
        defaultMeterShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the meterList where createdBy contains UPDATED_CREATED_BY
        defaultMeterShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllMetersByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where createdBy does not contain DEFAULT_CREATED_BY
        defaultMeterShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the meterList where createdBy does not contain UPDATED_CREATED_BY
        defaultMeterShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllMetersByCreatedOnIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where createdOn equals to DEFAULT_CREATED_ON
        defaultMeterShouldBeFound("createdOn.equals=" + DEFAULT_CREATED_ON);

        // Get all the meterList where createdOn equals to UPDATED_CREATED_ON
        defaultMeterShouldNotBeFound("createdOn.equals=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    void getAllMetersByCreatedOnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where createdOn not equals to DEFAULT_CREATED_ON
        defaultMeterShouldNotBeFound("createdOn.notEquals=" + DEFAULT_CREATED_ON);

        // Get all the meterList where createdOn not equals to UPDATED_CREATED_ON
        defaultMeterShouldBeFound("createdOn.notEquals=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    void getAllMetersByCreatedOnIsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where createdOn in DEFAULT_CREATED_ON or UPDATED_CREATED_ON
        defaultMeterShouldBeFound("createdOn.in=" + DEFAULT_CREATED_ON + "," + UPDATED_CREATED_ON);

        // Get all the meterList where createdOn equals to UPDATED_CREATED_ON
        defaultMeterShouldNotBeFound("createdOn.in=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    void getAllMetersByCreatedOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where createdOn is not null
        defaultMeterShouldBeFound("createdOn.specified=true");

        // Get all the meterList where createdOn is null
        defaultMeterShouldNotBeFound("createdOn.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultMeterShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the meterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultMeterShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllMetersByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultMeterShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the meterList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultMeterShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllMetersByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultMeterShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the meterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultMeterShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllMetersByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where lastModifiedBy is not null
        defaultMeterShouldBeFound("lastModifiedBy.specified=true");

        // Get all the meterList where lastModifiedBy is null
        defaultMeterShouldNotBeFound("lastModifiedBy.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultMeterShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the meterList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultMeterShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllMetersByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultMeterShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the meterList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultMeterShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllMetersByLastModifiedOnIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where lastModifiedOn equals to DEFAULT_LAST_MODIFIED_ON
        defaultMeterShouldBeFound("lastModifiedOn.equals=" + DEFAULT_LAST_MODIFIED_ON);

        // Get all the meterList where lastModifiedOn equals to UPDATED_LAST_MODIFIED_ON
        defaultMeterShouldNotBeFound("lastModifiedOn.equals=" + UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void getAllMetersByLastModifiedOnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where lastModifiedOn not equals to DEFAULT_LAST_MODIFIED_ON
        defaultMeterShouldNotBeFound("lastModifiedOn.notEquals=" + DEFAULT_LAST_MODIFIED_ON);

        // Get all the meterList where lastModifiedOn not equals to UPDATED_LAST_MODIFIED_ON
        defaultMeterShouldBeFound("lastModifiedOn.notEquals=" + UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void getAllMetersByLastModifiedOnIsInShouldWork() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where lastModifiedOn in DEFAULT_LAST_MODIFIED_ON or UPDATED_LAST_MODIFIED_ON
        defaultMeterShouldBeFound("lastModifiedOn.in=" + DEFAULT_LAST_MODIFIED_ON + "," + UPDATED_LAST_MODIFIED_ON);

        // Get all the meterList where lastModifiedOn equals to UPDATED_LAST_MODIFIED_ON
        defaultMeterShouldNotBeFound("lastModifiedOn.in=" + UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void getAllMetersByLastModifiedOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        // Get all the meterList where lastModifiedOn is not null
        defaultMeterShouldBeFound("lastModifiedOn.specified=true");

        // Get all the meterList where lastModifiedOn is null
        defaultMeterShouldNotBeFound("lastModifiedOn.specified=false");
    }

    @Test
    @Transactional
    void getAllMetersByModemIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);
        Modem modem;
        if (TestUtil.findAll(em, Modem.class).isEmpty()) {
            modem = ModemResourceIT.createEntity(em);
            em.persist(modem);
            em.flush();
        } else {
            modem = TestUtil.findAll(em, Modem.class).get(0);
        }
        em.persist(modem);
        em.flush();
        meter.setModem(modem);
        meterRepository.saveAndFlush(meter);
        Long modemId = modem.getId();

        // Get all the meterList where modem equals to modemId
        defaultMeterShouldBeFound("modemId.equals=" + modemId);

        // Get all the meterList where modem equals to (modemId + 1)
        defaultMeterShouldNotBeFound("modemId.equals=" + (modemId + 1));
    }

    @Test
    @Transactional
    void getAllMetersByMeterTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);
        CommonValue meterType;
        if (TestUtil.findAll(em, CommonValue.class).isEmpty()) {
            meterType = CommonValueResourceIT.createEntity(em);
            em.persist(meterType);
            em.flush();
        } else {
            meterType = TestUtil.findAll(em, CommonValue.class).get(0);
        }
        em.persist(meterType);
        em.flush();
        meter.setMeterType(meterType);
        meterRepository.saveAndFlush(meter);
        Long meterTypeId = meterType.getId();

        // Get all the meterList where meterType equals to meterTypeId
        defaultMeterShouldBeFound("meterTypeId.equals=" + meterTypeId);

        // Get all the meterList where meterType equals to (meterTypeId + 1)
        defaultMeterShouldNotBeFound("meterTypeId.equals=" + (meterTypeId + 1));
    }

    @Test
    @Transactional
    void getAllMetersByCurrentIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);
        CommonValue current;
        if (TestUtil.findAll(em, CommonValue.class).isEmpty()) {
            current = CommonValueResourceIT.createEntity(em);
            em.persist(current);
            em.flush();
        } else {
            current = TestUtil.findAll(em, CommonValue.class).get(0);
        }
        em.persist(current);
        em.flush();
        meter.setCurrent(current);
        meterRepository.saveAndFlush(meter);
        Long currentId = current.getId();

        // Get all the meterList where current equals to currentId
        defaultMeterShouldBeFound("currentId.equals=" + currentId);

        // Get all the meterList where current equals to (currentId + 1)
        defaultMeterShouldNotBeFound("currentId.equals=" + (currentId + 1));
    }

    @Test
    @Transactional
    void getAllMetersByVoltageIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);
        CommonValue voltage;
        if (TestUtil.findAll(em, CommonValue.class).isEmpty()) {
            voltage = CommonValueResourceIT.createEntity(em);
            em.persist(voltage);
            em.flush();
        } else {
            voltage = TestUtil.findAll(em, CommonValue.class).get(0);
        }
        em.persist(voltage);
        em.flush();
        meter.setVoltage(voltage);
        meterRepository.saveAndFlush(meter);
        Long voltageId = voltage.getId();

        // Get all the meterList where voltage equals to voltageId
        defaultMeterShouldBeFound("voltageId.equals=" + voltageId);

        // Get all the meterList where voltage equals to (voltageId + 1)
        defaultMeterShouldNotBeFound("voltageId.equals=" + (voltageId + 1));
    }

    @Test
    @Transactional
    void getAllMetersByModelIsEqualToSomething() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);
        CommonValue model;
        if (TestUtil.findAll(em, CommonValue.class).isEmpty()) {
            model = CommonValueResourceIT.createEntity(em);
            em.persist(model);
            em.flush();
        } else {
            model = TestUtil.findAll(em, CommonValue.class).get(0);
        }
        em.persist(model);
        em.flush();
        meter.setModel(model);
        meterRepository.saveAndFlush(meter);
        Long modelId = model.getId();

        // Get all the meterList where model equals to modelId
        defaultMeterShouldBeFound("modelId.equals=" + modelId);

        // Get all the meterList where model equals to (modelId + 1)
        defaultMeterShouldNotBeFound("modelId.equals=" + (modelId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMeterShouldBeFound(String filter) throws Exception {
        restMeterMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(meter.getId().intValue())))
            .andExpect(jsonPath("$.[*].modemName").value(hasItem(DEFAULT_MODEM_NAME)))
            .andExpect(jsonPath("$.[*].modelName").value(hasItem(DEFAULT_MODEL_NAME)))
            .andExpect(jsonPath("$.[*].meterTypeName").value(hasItem(DEFAULT_METER_TYPE_NAME)))
            .andExpect(jsonPath("$.[*].currentName").value(hasItem(DEFAULT_CURRENT_NAME)))
            .andExpect(jsonPath("$.[*].voltageName").value(hasItem(DEFAULT_VOLTAGE_NAME)))
            .andExpect(jsonPath("$.[*].installationOn").value(hasItem(DEFAULT_INSTALLATION_ON.toString())))
            .andExpect(jsonPath("$.[*].warrantyOn").value(hasItem(DEFAULT_WARRANTY_ON.toString())))
            .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].ctRatio").value(hasItem(DEFAULT_CT_RATIO)))
            .andExpect(jsonPath("$.[*].vtRatio").value(hasItem(DEFAULT_VT_RATIO)))
            .andExpect(jsonPath("$.[*].deviceAddress").value(hasItem(DEFAULT_DEVICE_ADDRESS)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].rate1").value(hasItem(DEFAULT_RATE_1)))
            .andExpect(jsonPath("$.[*].rate2").value(hasItem(DEFAULT_RATE_2)))
            .andExpect(jsonPath("$.[*].rate3").value(hasItem(DEFAULT_RATE_3)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedOn").value(hasItem(DEFAULT_LAST_MODIFIED_ON.toString())));

        // Check, that the count call also returns 1
        restMeterMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMeterShouldNotBeFound(String filter) throws Exception {
        restMeterMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMeterMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingMeter() throws Exception {
        // Get the meter
        restMeterMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMeter() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        int databaseSizeBeforeUpdate = meterRepository.findAll().size();

        // Update the meter
        Meter updatedMeter = meterRepository.findById(meter.getId()).get();
        // Disconnect from session so that the updates on updatedMeter are not directly saved in db
        em.detach(updatedMeter);
        updatedMeter
            .modemName(UPDATED_MODEM_NAME)
            .modelName(UPDATED_MODEL_NAME)
            .meterTypeName(UPDATED_METER_TYPE_NAME)
            .currentName(UPDATED_CURRENT_NAME)
            .voltageName(UPDATED_VOLTAGE_NAME)
            .installationOn(UPDATED_INSTALLATION_ON)
            .warrantyOn(UPDATED_WARRANTY_ON)
            .number(UPDATED_NUMBER)
            .name(UPDATED_NAME)
            .address(UPDATED_ADDRESS)
            .ctRatio(UPDATED_CT_RATIO)
            .vtRatio(UPDATED_VT_RATIO)
            .deviceAddress(UPDATED_DEVICE_ADDRESS)
            .status(UPDATED_STATUS)
            .rate1(UPDATED_RATE_1)
            .rate2(UPDATED_RATE_2)
            .rate3(UPDATED_RATE_3)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedOn(UPDATED_LAST_MODIFIED_ON);
        MeterDTO meterDTO = meterMapper.toDto(updatedMeter);

        restMeterMockMvc
            .perform(
                put(ENTITY_API_URL_ID, meterDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meterDTO))
            )
            .andExpect(status().isOk());

        // Validate the Meter in the database
        List<Meter> meterList = meterRepository.findAll();
        assertThat(meterList).hasSize(databaseSizeBeforeUpdate);
        Meter testMeter = meterList.get(meterList.size() - 1);
        assertThat(testMeter.getModemName()).isEqualTo(UPDATED_MODEM_NAME);
        assertThat(testMeter.getModelName()).isEqualTo(UPDATED_MODEL_NAME);
        assertThat(testMeter.getMeterTypeName()).isEqualTo(UPDATED_METER_TYPE_NAME);
        assertThat(testMeter.getCurrentName()).isEqualTo(UPDATED_CURRENT_NAME);
        assertThat(testMeter.getVoltageName()).isEqualTo(UPDATED_VOLTAGE_NAME);
        assertThat(testMeter.getInstallationOn()).isEqualTo(UPDATED_INSTALLATION_ON);
        assertThat(testMeter.getWarrantyOn()).isEqualTo(UPDATED_WARRANTY_ON);
        assertThat(testMeter.getNumber()).isEqualTo(UPDATED_NUMBER);
        assertThat(testMeter.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testMeter.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testMeter.getCtRatio()).isEqualTo(UPDATED_CT_RATIO);
        assertThat(testMeter.getVtRatio()).isEqualTo(UPDATED_VT_RATIO);
        assertThat(testMeter.getDeviceAddress()).isEqualTo(UPDATED_DEVICE_ADDRESS);
        assertThat(testMeter.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testMeter.getRate1()).isEqualTo(UPDATED_RATE_1);
        assertThat(testMeter.getRate2()).isEqualTo(UPDATED_RATE_2);
        assertThat(testMeter.getRate3()).isEqualTo(UPDATED_RATE_3);
        assertThat(testMeter.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testMeter.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testMeter.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testMeter.getLastModifiedOn()).isEqualTo(UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void putNonExistingMeter() throws Exception {
        int databaseSizeBeforeUpdate = meterRepository.findAll().size();
        meter.setId(count.incrementAndGet());

        // Create the Meter
        MeterDTO meterDTO = meterMapper.toDto(meter);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMeterMockMvc
            .perform(
                put(ENTITY_API_URL_ID, meterDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meterDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Meter in the database
        List<Meter> meterList = meterRepository.findAll();
        assertThat(meterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMeter() throws Exception {
        int databaseSizeBeforeUpdate = meterRepository.findAll().size();
        meter.setId(count.incrementAndGet());

        // Create the Meter
        MeterDTO meterDTO = meterMapper.toDto(meter);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMeterMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meterDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Meter in the database
        List<Meter> meterList = meterRepository.findAll();
        assertThat(meterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMeter() throws Exception {
        int databaseSizeBeforeUpdate = meterRepository.findAll().size();
        meter.setId(count.incrementAndGet());

        // Create the Meter
        MeterDTO meterDTO = meterMapper.toDto(meter);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMeterMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meterDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Meter in the database
        List<Meter> meterList = meterRepository.findAll();
        assertThat(meterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMeterWithPatch() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        int databaseSizeBeforeUpdate = meterRepository.findAll().size();

        // Update the meter using partial update
        Meter partialUpdatedMeter = new Meter();
        partialUpdatedMeter.setId(meter.getId());

        partialUpdatedMeter
            .installationOn(UPDATED_INSTALLATION_ON)
            .number(UPDATED_NUMBER)
            .address(UPDATED_ADDRESS)
            .vtRatio(UPDATED_VT_RATIO)
            .deviceAddress(UPDATED_DEVICE_ADDRESS)
            .status(UPDATED_STATUS)
            .rate1(UPDATED_RATE_1)
            .rate2(UPDATED_RATE_2)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedOn(UPDATED_LAST_MODIFIED_ON);

        restMeterMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMeter.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMeter))
            )
            .andExpect(status().isOk());

        // Validate the Meter in the database
        List<Meter> meterList = meterRepository.findAll();
        assertThat(meterList).hasSize(databaseSizeBeforeUpdate);
        Meter testMeter = meterList.get(meterList.size() - 1);
        assertThat(testMeter.getModemName()).isEqualTo(DEFAULT_MODEM_NAME);
        assertThat(testMeter.getModelName()).isEqualTo(DEFAULT_MODEL_NAME);
        assertThat(testMeter.getMeterTypeName()).isEqualTo(DEFAULT_METER_TYPE_NAME);
        assertThat(testMeter.getCurrentName()).isEqualTo(DEFAULT_CURRENT_NAME);
        assertThat(testMeter.getVoltageName()).isEqualTo(DEFAULT_VOLTAGE_NAME);
        assertThat(testMeter.getInstallationOn()).isEqualTo(UPDATED_INSTALLATION_ON);
        assertThat(testMeter.getWarrantyOn()).isEqualTo(DEFAULT_WARRANTY_ON);
        assertThat(testMeter.getNumber()).isEqualTo(UPDATED_NUMBER);
        assertThat(testMeter.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testMeter.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testMeter.getCtRatio()).isEqualTo(DEFAULT_CT_RATIO);
        assertThat(testMeter.getVtRatio()).isEqualTo(UPDATED_VT_RATIO);
        assertThat(testMeter.getDeviceAddress()).isEqualTo(UPDATED_DEVICE_ADDRESS);
        assertThat(testMeter.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testMeter.getRate1()).isEqualTo(UPDATED_RATE_1);
        assertThat(testMeter.getRate2()).isEqualTo(UPDATED_RATE_2);
        assertThat(testMeter.getRate3()).isEqualTo(DEFAULT_RATE_3);
        assertThat(testMeter.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testMeter.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testMeter.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testMeter.getLastModifiedOn()).isEqualTo(UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void fullUpdateMeterWithPatch() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        int databaseSizeBeforeUpdate = meterRepository.findAll().size();

        // Update the meter using partial update
        Meter partialUpdatedMeter = new Meter();
        partialUpdatedMeter.setId(meter.getId());

        partialUpdatedMeter
            .modemName(UPDATED_MODEM_NAME)
            .modelName(UPDATED_MODEL_NAME)
            .meterTypeName(UPDATED_METER_TYPE_NAME)
            .currentName(UPDATED_CURRENT_NAME)
            .voltageName(UPDATED_VOLTAGE_NAME)
            .installationOn(UPDATED_INSTALLATION_ON)
            .warrantyOn(UPDATED_WARRANTY_ON)
            .number(UPDATED_NUMBER)
            .name(UPDATED_NAME)
            .address(UPDATED_ADDRESS)
            .ctRatio(UPDATED_CT_RATIO)
            .vtRatio(UPDATED_VT_RATIO)
            .deviceAddress(UPDATED_DEVICE_ADDRESS)
            .status(UPDATED_STATUS)
            .rate1(UPDATED_RATE_1)
            .rate2(UPDATED_RATE_2)
            .rate3(UPDATED_RATE_3)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedOn(UPDATED_LAST_MODIFIED_ON);

        restMeterMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMeter.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMeter))
            )
            .andExpect(status().isOk());

        // Validate the Meter in the database
        List<Meter> meterList = meterRepository.findAll();
        assertThat(meterList).hasSize(databaseSizeBeforeUpdate);
        Meter testMeter = meterList.get(meterList.size() - 1);
        assertThat(testMeter.getModemName()).isEqualTo(UPDATED_MODEM_NAME);
        assertThat(testMeter.getModelName()).isEqualTo(UPDATED_MODEL_NAME);
        assertThat(testMeter.getMeterTypeName()).isEqualTo(UPDATED_METER_TYPE_NAME);
        assertThat(testMeter.getCurrentName()).isEqualTo(UPDATED_CURRENT_NAME);
        assertThat(testMeter.getVoltageName()).isEqualTo(UPDATED_VOLTAGE_NAME);
        assertThat(testMeter.getInstallationOn()).isEqualTo(UPDATED_INSTALLATION_ON);
        assertThat(testMeter.getWarrantyOn()).isEqualTo(UPDATED_WARRANTY_ON);
        assertThat(testMeter.getNumber()).isEqualTo(UPDATED_NUMBER);
        assertThat(testMeter.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testMeter.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testMeter.getCtRatio()).isEqualTo(UPDATED_CT_RATIO);
        assertThat(testMeter.getVtRatio()).isEqualTo(UPDATED_VT_RATIO);
        assertThat(testMeter.getDeviceAddress()).isEqualTo(UPDATED_DEVICE_ADDRESS);
        assertThat(testMeter.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testMeter.getRate1()).isEqualTo(UPDATED_RATE_1);
        assertThat(testMeter.getRate2()).isEqualTo(UPDATED_RATE_2);
        assertThat(testMeter.getRate3()).isEqualTo(UPDATED_RATE_3);
        assertThat(testMeter.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testMeter.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testMeter.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testMeter.getLastModifiedOn()).isEqualTo(UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void patchNonExistingMeter() throws Exception {
        int databaseSizeBeforeUpdate = meterRepository.findAll().size();
        meter.setId(count.incrementAndGet());

        // Create the Meter
        MeterDTO meterDTO = meterMapper.toDto(meter);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMeterMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, meterDTO.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(meterDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Meter in the database
        List<Meter> meterList = meterRepository.findAll();
        assertThat(meterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMeter() throws Exception {
        int databaseSizeBeforeUpdate = meterRepository.findAll().size();
        meter.setId(count.incrementAndGet());

        // Create the Meter
        MeterDTO meterDTO = meterMapper.toDto(meter);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMeterMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(meterDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Meter in the database
        List<Meter> meterList = meterRepository.findAll();
        assertThat(meterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMeter() throws Exception {
        int databaseSizeBeforeUpdate = meterRepository.findAll().size();
        meter.setId(count.incrementAndGet());

        // Create the Meter
        MeterDTO meterDTO = meterMapper.toDto(meter);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMeterMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(meterDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Meter in the database
        List<Meter> meterList = meterRepository.findAll();
        assertThat(meterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMeter() throws Exception {
        // Initialize the database
        meterRepository.saveAndFlush(meter);

        int databaseSizeBeforeDelete = meterRepository.findAll().size();

        // Delete the meter
        restMeterMockMvc
            .perform(delete(ENTITY_API_URL_ID, meter.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Meter> meterList = meterRepository.findAll();
        assertThat(meterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
