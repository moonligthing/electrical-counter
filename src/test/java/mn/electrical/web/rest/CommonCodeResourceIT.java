package mn.electrical.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import mn.electrical.IntegrationTest;
import mn.electrical.domain.CommonCode;
import mn.electrical.repository.CommonCodeRepository;
import mn.electrical.service.criteria.CommonCodeCriteria;
import mn.electrical.service.dto.CommonCodeDTO;
import mn.electrical.service.mapper.CommonCodeMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CommonCodeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CommonCodeResourceIT {

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_IS_DELETED = false;
    private static final Boolean UPDATED_IS_DELETED = true;

    private static final String ENTITY_API_URL = "/api/common-codes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CommonCodeRepository commonCodeRepository;

    @Autowired
    private CommonCodeMapper commonCodeMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCommonCodeMockMvc;

    private CommonCode commonCode;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CommonCode createEntity(EntityManager em) {
        CommonCode commonCode = new CommonCode()
            .status(DEFAULT_STATUS)
            .name(DEFAULT_NAME)
            .code(DEFAULT_CODE)
            .description(DEFAULT_DESCRIPTION)
            .createdBy(DEFAULT_CREATED_BY)
            .createdOn(DEFAULT_CREATED_ON)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedOn(DEFAULT_LAST_MODIFIED_ON)
            .isDeleted(DEFAULT_IS_DELETED);
        return commonCode;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CommonCode createUpdatedEntity(EntityManager em) {
        CommonCode commonCode = new CommonCode()
            .status(UPDATED_STATUS)
            .name(UPDATED_NAME)
            .code(UPDATED_CODE)
            .description(UPDATED_DESCRIPTION)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedOn(UPDATED_LAST_MODIFIED_ON)
            .isDeleted(UPDATED_IS_DELETED);
        return commonCode;
    }

    @BeforeEach
    public void initTest() {
        commonCode = createEntity(em);
    }

    @Test
    @Transactional
    void createCommonCode() throws Exception {
        int databaseSizeBeforeCreate = commonCodeRepository.findAll().size();
        // Create the CommonCode
        CommonCodeDTO commonCodeDTO = commonCodeMapper.toDto(commonCode);
        restCommonCodeMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commonCodeDTO))
            )
            .andExpect(status().isCreated());

        // Validate the CommonCode in the database
        List<CommonCode> commonCodeList = commonCodeRepository.findAll();
        assertThat(commonCodeList).hasSize(databaseSizeBeforeCreate + 1);
        CommonCode testCommonCode = commonCodeList.get(commonCodeList.size() - 1);
        assertThat(testCommonCode.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testCommonCode.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCommonCode.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testCommonCode.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testCommonCode.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testCommonCode.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testCommonCode.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testCommonCode.getLastModifiedOn()).isEqualTo(DEFAULT_LAST_MODIFIED_ON);
        assertThat(testCommonCode.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    void createCommonCodeWithExistingId() throws Exception {
        // Create the CommonCode with an existing ID
        commonCode.setId(1L);
        CommonCodeDTO commonCodeDTO = commonCodeMapper.toDto(commonCode);

        int databaseSizeBeforeCreate = commonCodeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCommonCodeMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commonCodeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommonCode in the database
        List<CommonCode> commonCodeList = commonCodeRepository.findAll();
        assertThat(commonCodeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = commonCodeRepository.findAll().size();
        // set the field null
        commonCode.setStatus(null);

        // Create the CommonCode, which fails.
        CommonCodeDTO commonCodeDTO = commonCodeMapper.toDto(commonCode);

        restCommonCodeMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commonCodeDTO))
            )
            .andExpect(status().isBadRequest());

        List<CommonCode> commonCodeList = commonCodeRepository.findAll();
        assertThat(commonCodeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = commonCodeRepository.findAll().size();
        // set the field null
        commonCode.setName(null);

        // Create the CommonCode, which fails.
        CommonCodeDTO commonCodeDTO = commonCodeMapper.toDto(commonCode);

        restCommonCodeMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commonCodeDTO))
            )
            .andExpect(status().isBadRequest());

        List<CommonCode> commonCodeList = commonCodeRepository.findAll();
        assertThat(commonCodeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = commonCodeRepository.findAll().size();
        // set the field null
        commonCode.setCode(null);

        // Create the CommonCode, which fails.
        CommonCodeDTO commonCodeDTO = commonCodeMapper.toDto(commonCode);

        restCommonCodeMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commonCodeDTO))
            )
            .andExpect(status().isBadRequest());

        List<CommonCode> commonCodeList = commonCodeRepository.findAll();
        assertThat(commonCodeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCommonCodes() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList
        restCommonCodeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(commonCode.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedOn").value(hasItem(DEFAULT_LAST_MODIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    void getCommonCode() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get the commonCode
        restCommonCodeMockMvc
            .perform(get(ENTITY_API_URL_ID, commonCode.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(commonCode.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedOn").value(DEFAULT_LAST_MODIFIED_ON.toString()))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    void getCommonCodesByIdFiltering() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        Long id = commonCode.getId();

        defaultCommonCodeShouldBeFound("id.equals=" + id);
        defaultCommonCodeShouldNotBeFound("id.notEquals=" + id);

        defaultCommonCodeShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCommonCodeShouldNotBeFound("id.greaterThan=" + id);

        defaultCommonCodeShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCommonCodeShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllCommonCodesByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where status equals to DEFAULT_STATUS
        defaultCommonCodeShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the commonCodeList where status equals to UPDATED_STATUS
        defaultCommonCodeShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllCommonCodesByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where status not equals to DEFAULT_STATUS
        defaultCommonCodeShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the commonCodeList where status not equals to UPDATED_STATUS
        defaultCommonCodeShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllCommonCodesByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultCommonCodeShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the commonCodeList where status equals to UPDATED_STATUS
        defaultCommonCodeShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllCommonCodesByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where status is not null
        defaultCommonCodeShouldBeFound("status.specified=true");

        // Get all the commonCodeList where status is null
        defaultCommonCodeShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonCodesByStatusContainsSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where status contains DEFAULT_STATUS
        defaultCommonCodeShouldBeFound("status.contains=" + DEFAULT_STATUS);

        // Get all the commonCodeList where status contains UPDATED_STATUS
        defaultCommonCodeShouldNotBeFound("status.contains=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllCommonCodesByStatusNotContainsSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where status does not contain DEFAULT_STATUS
        defaultCommonCodeShouldNotBeFound("status.doesNotContain=" + DEFAULT_STATUS);

        // Get all the commonCodeList where status does not contain UPDATED_STATUS
        defaultCommonCodeShouldBeFound("status.doesNotContain=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    void getAllCommonCodesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where name equals to DEFAULT_NAME
        defaultCommonCodeShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the commonCodeList where name equals to UPDATED_NAME
        defaultCommonCodeShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCommonCodesByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where name not equals to DEFAULT_NAME
        defaultCommonCodeShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the commonCodeList where name not equals to UPDATED_NAME
        defaultCommonCodeShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCommonCodesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where name in DEFAULT_NAME or UPDATED_NAME
        defaultCommonCodeShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the commonCodeList where name equals to UPDATED_NAME
        defaultCommonCodeShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCommonCodesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where name is not null
        defaultCommonCodeShouldBeFound("name.specified=true");

        // Get all the commonCodeList where name is null
        defaultCommonCodeShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonCodesByNameContainsSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where name contains DEFAULT_NAME
        defaultCommonCodeShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the commonCodeList where name contains UPDATED_NAME
        defaultCommonCodeShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCommonCodesByNameNotContainsSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where name does not contain DEFAULT_NAME
        defaultCommonCodeShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the commonCodeList where name does not contain UPDATED_NAME
        defaultCommonCodeShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCommonCodesByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where code equals to DEFAULT_CODE
        defaultCommonCodeShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the commonCodeList where code equals to UPDATED_CODE
        defaultCommonCodeShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllCommonCodesByCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where code not equals to DEFAULT_CODE
        defaultCommonCodeShouldNotBeFound("code.notEquals=" + DEFAULT_CODE);

        // Get all the commonCodeList where code not equals to UPDATED_CODE
        defaultCommonCodeShouldBeFound("code.notEquals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllCommonCodesByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where code in DEFAULT_CODE or UPDATED_CODE
        defaultCommonCodeShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the commonCodeList where code equals to UPDATED_CODE
        defaultCommonCodeShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllCommonCodesByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where code is not null
        defaultCommonCodeShouldBeFound("code.specified=true");

        // Get all the commonCodeList where code is null
        defaultCommonCodeShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonCodesByCodeContainsSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where code contains DEFAULT_CODE
        defaultCommonCodeShouldBeFound("code.contains=" + DEFAULT_CODE);

        // Get all the commonCodeList where code contains UPDATED_CODE
        defaultCommonCodeShouldNotBeFound("code.contains=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllCommonCodesByCodeNotContainsSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where code does not contain DEFAULT_CODE
        defaultCommonCodeShouldNotBeFound("code.doesNotContain=" + DEFAULT_CODE);

        // Get all the commonCodeList where code does not contain UPDATED_CODE
        defaultCommonCodeShouldBeFound("code.doesNotContain=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllCommonCodesByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where description equals to DEFAULT_DESCRIPTION
        defaultCommonCodeShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the commonCodeList where description equals to UPDATED_DESCRIPTION
        defaultCommonCodeShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllCommonCodesByDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where description not equals to DEFAULT_DESCRIPTION
        defaultCommonCodeShouldNotBeFound("description.notEquals=" + DEFAULT_DESCRIPTION);

        // Get all the commonCodeList where description not equals to UPDATED_DESCRIPTION
        defaultCommonCodeShouldBeFound("description.notEquals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllCommonCodesByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultCommonCodeShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the commonCodeList where description equals to UPDATED_DESCRIPTION
        defaultCommonCodeShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllCommonCodesByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where description is not null
        defaultCommonCodeShouldBeFound("description.specified=true");

        // Get all the commonCodeList where description is null
        defaultCommonCodeShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonCodesByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where description contains DEFAULT_DESCRIPTION
        defaultCommonCodeShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the commonCodeList where description contains UPDATED_DESCRIPTION
        defaultCommonCodeShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllCommonCodesByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where description does not contain DEFAULT_DESCRIPTION
        defaultCommonCodeShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the commonCodeList where description does not contain UPDATED_DESCRIPTION
        defaultCommonCodeShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllCommonCodesByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where createdBy equals to DEFAULT_CREATED_BY
        defaultCommonCodeShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the commonCodeList where createdBy equals to UPDATED_CREATED_BY
        defaultCommonCodeShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCommonCodesByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where createdBy not equals to DEFAULT_CREATED_BY
        defaultCommonCodeShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the commonCodeList where createdBy not equals to UPDATED_CREATED_BY
        defaultCommonCodeShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCommonCodesByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultCommonCodeShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the commonCodeList where createdBy equals to UPDATED_CREATED_BY
        defaultCommonCodeShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCommonCodesByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where createdBy is not null
        defaultCommonCodeShouldBeFound("createdBy.specified=true");

        // Get all the commonCodeList where createdBy is null
        defaultCommonCodeShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonCodesByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where createdBy contains DEFAULT_CREATED_BY
        defaultCommonCodeShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the commonCodeList where createdBy contains UPDATED_CREATED_BY
        defaultCommonCodeShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCommonCodesByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where createdBy does not contain DEFAULT_CREATED_BY
        defaultCommonCodeShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the commonCodeList where createdBy does not contain UPDATED_CREATED_BY
        defaultCommonCodeShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCommonCodesByCreatedOnIsEqualToSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where createdOn equals to DEFAULT_CREATED_ON
        defaultCommonCodeShouldBeFound("createdOn.equals=" + DEFAULT_CREATED_ON);

        // Get all the commonCodeList where createdOn equals to UPDATED_CREATED_ON
        defaultCommonCodeShouldNotBeFound("createdOn.equals=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    void getAllCommonCodesByCreatedOnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where createdOn not equals to DEFAULT_CREATED_ON
        defaultCommonCodeShouldNotBeFound("createdOn.notEquals=" + DEFAULT_CREATED_ON);

        // Get all the commonCodeList where createdOn not equals to UPDATED_CREATED_ON
        defaultCommonCodeShouldBeFound("createdOn.notEquals=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    void getAllCommonCodesByCreatedOnIsInShouldWork() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where createdOn in DEFAULT_CREATED_ON or UPDATED_CREATED_ON
        defaultCommonCodeShouldBeFound("createdOn.in=" + DEFAULT_CREATED_ON + "," + UPDATED_CREATED_ON);

        // Get all the commonCodeList where createdOn equals to UPDATED_CREATED_ON
        defaultCommonCodeShouldNotBeFound("createdOn.in=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    void getAllCommonCodesByCreatedOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where createdOn is not null
        defaultCommonCodeShouldBeFound("createdOn.specified=true");

        // Get all the commonCodeList where createdOn is null
        defaultCommonCodeShouldNotBeFound("createdOn.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonCodesByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultCommonCodeShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the commonCodeList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCommonCodeShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllCommonCodesByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultCommonCodeShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the commonCodeList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultCommonCodeShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllCommonCodesByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultCommonCodeShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the commonCodeList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCommonCodeShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllCommonCodesByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where lastModifiedBy is not null
        defaultCommonCodeShouldBeFound("lastModifiedBy.specified=true");

        // Get all the commonCodeList where lastModifiedBy is null
        defaultCommonCodeShouldNotBeFound("lastModifiedBy.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonCodesByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultCommonCodeShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the commonCodeList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultCommonCodeShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllCommonCodesByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultCommonCodeShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the commonCodeList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultCommonCodeShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllCommonCodesByLastModifiedOnIsEqualToSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where lastModifiedOn equals to DEFAULT_LAST_MODIFIED_ON
        defaultCommonCodeShouldBeFound("lastModifiedOn.equals=" + DEFAULT_LAST_MODIFIED_ON);

        // Get all the commonCodeList where lastModifiedOn equals to UPDATED_LAST_MODIFIED_ON
        defaultCommonCodeShouldNotBeFound("lastModifiedOn.equals=" + UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void getAllCommonCodesByLastModifiedOnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where lastModifiedOn not equals to DEFAULT_LAST_MODIFIED_ON
        defaultCommonCodeShouldNotBeFound("lastModifiedOn.notEquals=" + DEFAULT_LAST_MODIFIED_ON);

        // Get all the commonCodeList where lastModifiedOn not equals to UPDATED_LAST_MODIFIED_ON
        defaultCommonCodeShouldBeFound("lastModifiedOn.notEquals=" + UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void getAllCommonCodesByLastModifiedOnIsInShouldWork() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where lastModifiedOn in DEFAULT_LAST_MODIFIED_ON or UPDATED_LAST_MODIFIED_ON
        defaultCommonCodeShouldBeFound("lastModifiedOn.in=" + DEFAULT_LAST_MODIFIED_ON + "," + UPDATED_LAST_MODIFIED_ON);

        // Get all the commonCodeList where lastModifiedOn equals to UPDATED_LAST_MODIFIED_ON
        defaultCommonCodeShouldNotBeFound("lastModifiedOn.in=" + UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void getAllCommonCodesByLastModifiedOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where lastModifiedOn is not null
        defaultCommonCodeShouldBeFound("lastModifiedOn.specified=true");

        // Get all the commonCodeList where lastModifiedOn is null
        defaultCommonCodeShouldNotBeFound("lastModifiedOn.specified=false");
    }

    @Test
    @Transactional
    void getAllCommonCodesByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where isDeleted equals to DEFAULT_IS_DELETED
        defaultCommonCodeShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the commonCodeList where isDeleted equals to UPDATED_IS_DELETED
        defaultCommonCodeShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    void getAllCommonCodesByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultCommonCodeShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the commonCodeList where isDeleted not equals to UPDATED_IS_DELETED
        defaultCommonCodeShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    void getAllCommonCodesByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultCommonCodeShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the commonCodeList where isDeleted equals to UPDATED_IS_DELETED
        defaultCommonCodeShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    void getAllCommonCodesByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        // Get all the commonCodeList where isDeleted is not null
        defaultCommonCodeShouldBeFound("isDeleted.specified=true");

        // Get all the commonCodeList where isDeleted is null
        defaultCommonCodeShouldNotBeFound("isDeleted.specified=false");
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCommonCodeShouldBeFound(String filter) throws Exception {
        restCommonCodeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(commonCode.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedOn").value(hasItem(DEFAULT_LAST_MODIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED.booleanValue())));

        // Check, that the count call also returns 1
        restCommonCodeMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCommonCodeShouldNotBeFound(String filter) throws Exception {
        restCommonCodeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCommonCodeMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingCommonCode() throws Exception {
        // Get the commonCode
        restCommonCodeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCommonCode() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        int databaseSizeBeforeUpdate = commonCodeRepository.findAll().size();

        // Update the commonCode
        CommonCode updatedCommonCode = commonCodeRepository.findById(commonCode.getId()).get();
        // Disconnect from session so that the updates on updatedCommonCode are not directly saved in db
        em.detach(updatedCommonCode);
        updatedCommonCode
            .status(UPDATED_STATUS)
            .name(UPDATED_NAME)
            .code(UPDATED_CODE)
            .description(UPDATED_DESCRIPTION)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedOn(UPDATED_LAST_MODIFIED_ON)
            .isDeleted(UPDATED_IS_DELETED);
        CommonCodeDTO commonCodeDTO = commonCodeMapper.toDto(updatedCommonCode);

        restCommonCodeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, commonCodeDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commonCodeDTO))
            )
            .andExpect(status().isOk());

        // Validate the CommonCode in the database
        List<CommonCode> commonCodeList = commonCodeRepository.findAll();
        assertThat(commonCodeList).hasSize(databaseSizeBeforeUpdate);
        CommonCode testCommonCode = commonCodeList.get(commonCodeList.size() - 1);
        assertThat(testCommonCode.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCommonCode.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCommonCode.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCommonCode.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testCommonCode.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCommonCode.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testCommonCode.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testCommonCode.getLastModifiedOn()).isEqualTo(UPDATED_LAST_MODIFIED_ON);
        assertThat(testCommonCode.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    void putNonExistingCommonCode() throws Exception {
        int databaseSizeBeforeUpdate = commonCodeRepository.findAll().size();
        commonCode.setId(count.incrementAndGet());

        // Create the CommonCode
        CommonCodeDTO commonCodeDTO = commonCodeMapper.toDto(commonCode);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCommonCodeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, commonCodeDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commonCodeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommonCode in the database
        List<CommonCode> commonCodeList = commonCodeRepository.findAll();
        assertThat(commonCodeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCommonCode() throws Exception {
        int databaseSizeBeforeUpdate = commonCodeRepository.findAll().size();
        commonCode.setId(count.incrementAndGet());

        // Create the CommonCode
        CommonCodeDTO commonCodeDTO = commonCodeMapper.toDto(commonCode);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommonCodeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commonCodeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommonCode in the database
        List<CommonCode> commonCodeList = commonCodeRepository.findAll();
        assertThat(commonCodeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCommonCode() throws Exception {
        int databaseSizeBeforeUpdate = commonCodeRepository.findAll().size();
        commonCode.setId(count.incrementAndGet());

        // Create the CommonCode
        CommonCodeDTO commonCodeDTO = commonCodeMapper.toDto(commonCode);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommonCodeMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commonCodeDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CommonCode in the database
        List<CommonCode> commonCodeList = commonCodeRepository.findAll();
        assertThat(commonCodeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCommonCodeWithPatch() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        int databaseSizeBeforeUpdate = commonCodeRepository.findAll().size();

        // Update the commonCode using partial update
        CommonCode partialUpdatedCommonCode = new CommonCode();
        partialUpdatedCommonCode.setId(commonCode.getId());

        partialUpdatedCommonCode
            .name(UPDATED_NAME)
            .createdOn(UPDATED_CREATED_ON)
            .lastModifiedOn(UPDATED_LAST_MODIFIED_ON)
            .isDeleted(UPDATED_IS_DELETED);

        restCommonCodeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCommonCode.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCommonCode))
            )
            .andExpect(status().isOk());

        // Validate the CommonCode in the database
        List<CommonCode> commonCodeList = commonCodeRepository.findAll();
        assertThat(commonCodeList).hasSize(databaseSizeBeforeUpdate);
        CommonCode testCommonCode = commonCodeList.get(commonCodeList.size() - 1);
        assertThat(testCommonCode.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testCommonCode.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCommonCode.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testCommonCode.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testCommonCode.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testCommonCode.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testCommonCode.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testCommonCode.getLastModifiedOn()).isEqualTo(UPDATED_LAST_MODIFIED_ON);
        assertThat(testCommonCode.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    void fullUpdateCommonCodeWithPatch() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        int databaseSizeBeforeUpdate = commonCodeRepository.findAll().size();

        // Update the commonCode using partial update
        CommonCode partialUpdatedCommonCode = new CommonCode();
        partialUpdatedCommonCode.setId(commonCode.getId());

        partialUpdatedCommonCode
            .status(UPDATED_STATUS)
            .name(UPDATED_NAME)
            .code(UPDATED_CODE)
            .description(UPDATED_DESCRIPTION)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedOn(UPDATED_LAST_MODIFIED_ON)
            .isDeleted(UPDATED_IS_DELETED);

        restCommonCodeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCommonCode.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCommonCode))
            )
            .andExpect(status().isOk());

        // Validate the CommonCode in the database
        List<CommonCode> commonCodeList = commonCodeRepository.findAll();
        assertThat(commonCodeList).hasSize(databaseSizeBeforeUpdate);
        CommonCode testCommonCode = commonCodeList.get(commonCodeList.size() - 1);
        assertThat(testCommonCode.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCommonCode.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCommonCode.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCommonCode.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testCommonCode.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCommonCode.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testCommonCode.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testCommonCode.getLastModifiedOn()).isEqualTo(UPDATED_LAST_MODIFIED_ON);
        assertThat(testCommonCode.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    void patchNonExistingCommonCode() throws Exception {
        int databaseSizeBeforeUpdate = commonCodeRepository.findAll().size();
        commonCode.setId(count.incrementAndGet());

        // Create the CommonCode
        CommonCodeDTO commonCodeDTO = commonCodeMapper.toDto(commonCode);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCommonCodeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, commonCodeDTO.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(commonCodeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommonCode in the database
        List<CommonCode> commonCodeList = commonCodeRepository.findAll();
        assertThat(commonCodeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCommonCode() throws Exception {
        int databaseSizeBeforeUpdate = commonCodeRepository.findAll().size();
        commonCode.setId(count.incrementAndGet());

        // Create the CommonCode
        CommonCodeDTO commonCodeDTO = commonCodeMapper.toDto(commonCode);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommonCodeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(commonCodeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommonCode in the database
        List<CommonCode> commonCodeList = commonCodeRepository.findAll();
        assertThat(commonCodeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCommonCode() throws Exception {
        int databaseSizeBeforeUpdate = commonCodeRepository.findAll().size();
        commonCode.setId(count.incrementAndGet());

        // Create the CommonCode
        CommonCodeDTO commonCodeDTO = commonCodeMapper.toDto(commonCode);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommonCodeMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(commonCodeDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CommonCode in the database
        List<CommonCode> commonCodeList = commonCodeRepository.findAll();
        assertThat(commonCodeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCommonCode() throws Exception {
        // Initialize the database
        commonCodeRepository.saveAndFlush(commonCode);

        int databaseSizeBeforeDelete = commonCodeRepository.findAll().size();

        // Delete the commonCode
        restCommonCodeMockMvc
            .perform(delete(ENTITY_API_URL_ID, commonCode.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CommonCode> commonCodeList = commonCodeRepository.findAll();
        assertThat(commonCodeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
