package mn.electrical.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import mn.electrical.IntegrationTest;
import mn.electrical.domain.CommonValue;
import mn.electrical.domain.Modem;
import mn.electrical.repository.ModemRepository;
import mn.electrical.service.criteria.ModemCriteria;
import mn.electrical.service.dto.ModemDTO;
import mn.electrical.service.mapper.ModemMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ModemResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ModemResourceIT {

    private static final String DEFAULT_MODEL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_MODEL_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PROTOCOL_TYPE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_PROTOCOL_TYPE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_IP_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_IP_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_PORT = "AAAAAAAAAA";
    private static final String UPDATED_PORT = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION = "BBBBBBBBBB";

    private static final String DEFAULT_SIM_INFO = "AAAAAAAAAA";
    private static final String UPDATED_SIM_INFO = "BBBBBBBBBB";

    private static final String DEFAULT_CRON_TAB = "AAAAAAAAAA";
    private static final String UPDATED_CRON_TAB = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_ON = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_ON = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_IS_DELETED = false;
    private static final Boolean UPDATED_IS_DELETED = true;

    private static final Boolean DEFAULT_FLAG = false;
    private static final Boolean UPDATED_FLAG = true;

    private static final String ENTITY_API_URL = "/api/modems";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ModemRepository modemRepository;

    @Autowired
    private ModemMapper modemMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restModemMockMvc;

    private Modem modem;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Modem createEntity(EntityManager em) {
        Modem modem = new Modem()
            .modelName(DEFAULT_MODEL_NAME)
            .protocolTypeName(DEFAULT_PROTOCOL_TYPE_NAME)
            .ipAddress(DEFAULT_IP_ADDRESS)
            .port(DEFAULT_PORT)
            .name(DEFAULT_NAME)
            .address(DEFAULT_ADDRESS)
            .location(DEFAULT_LOCATION)
            .simInfo(DEFAULT_SIM_INFO)
            .cronTab(DEFAULT_CRON_TAB)
            .createdBy(DEFAULT_CREATED_BY)
            .createdOn(DEFAULT_CREATED_ON)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedOn(DEFAULT_LAST_MODIFIED_ON)
            .isDeleted(DEFAULT_IS_DELETED)
            .flag(DEFAULT_FLAG);
        return modem;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Modem createUpdatedEntity(EntityManager em) {
        Modem modem = new Modem()
            .modelName(UPDATED_MODEL_NAME)
            .protocolTypeName(UPDATED_PROTOCOL_TYPE_NAME)
            .ipAddress(UPDATED_IP_ADDRESS)
            .port(UPDATED_PORT)
            .name(UPDATED_NAME)
            .address(UPDATED_ADDRESS)
            .location(UPDATED_LOCATION)
            .simInfo(UPDATED_SIM_INFO)
            .cronTab(UPDATED_CRON_TAB)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedOn(UPDATED_LAST_MODIFIED_ON)
            .isDeleted(UPDATED_IS_DELETED)
            .flag(UPDATED_FLAG);
        return modem;
    }

    @BeforeEach
    public void initTest() {
        modem = createEntity(em);
    }

    @Test
    @Transactional
    void createModem() throws Exception {
        int databaseSizeBeforeCreate = modemRepository.findAll().size();
        // Create the Modem
        ModemDTO modemDTO = modemMapper.toDto(modem);
        restModemMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(modemDTO))
            )
            .andExpect(status().isCreated());

        // Validate the Modem in the database
        List<Modem> modemList = modemRepository.findAll();
        assertThat(modemList).hasSize(databaseSizeBeforeCreate + 1);
        Modem testModem = modemList.get(modemList.size() - 1);
        assertThat(testModem.getModelName()).isEqualTo(DEFAULT_MODEL_NAME);
        assertThat(testModem.getProtocolTypeName()).isEqualTo(DEFAULT_PROTOCOL_TYPE_NAME);
        assertThat(testModem.getIpAddress()).isEqualTo(DEFAULT_IP_ADDRESS);
        assertThat(testModem.getPort()).isEqualTo(DEFAULT_PORT);
        assertThat(testModem.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testModem.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testModem.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testModem.getSimInfo()).isEqualTo(DEFAULT_SIM_INFO);
        assertThat(testModem.getCronTab()).isEqualTo(DEFAULT_CRON_TAB);
        assertThat(testModem.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testModem.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testModem.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testModem.getLastModifiedOn()).isEqualTo(DEFAULT_LAST_MODIFIED_ON);
        assertThat(testModem.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testModem.getFlag()).isEqualTo(DEFAULT_FLAG);
    }

    @Test
    @Transactional
    void createModemWithExistingId() throws Exception {
        // Create the Modem with an existing ID
        modem.setId(1L);
        ModemDTO modemDTO = modemMapper.toDto(modem);

        int databaseSizeBeforeCreate = modemRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restModemMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(modemDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Modem in the database
        List<Modem> modemList = modemRepository.findAll();
        assertThat(modemList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllModems() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList
        restModemMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(modem.getId().intValue())))
            .andExpect(jsonPath("$.[*].modelName").value(hasItem(DEFAULT_MODEL_NAME)))
            .andExpect(jsonPath("$.[*].protocolTypeName").value(hasItem(DEFAULT_PROTOCOL_TYPE_NAME)))
            .andExpect(jsonPath("$.[*].ipAddress").value(hasItem(DEFAULT_IP_ADDRESS)))
            .andExpect(jsonPath("$.[*].port").value(hasItem(DEFAULT_PORT)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION)))
            .andExpect(jsonPath("$.[*].simInfo").value(hasItem(DEFAULT_SIM_INFO)))
            .andExpect(jsonPath("$.[*].cronTab").value(hasItem(DEFAULT_CRON_TAB)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedOn").value(hasItem(DEFAULT_LAST_MODIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].flag").value(hasItem(DEFAULT_FLAG.booleanValue())));
    }

    @Test
    @Transactional
    void getModem() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get the modem
        restModemMockMvc
            .perform(get(ENTITY_API_URL_ID, modem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(modem.getId().intValue()))
            .andExpect(jsonPath("$.modelName").value(DEFAULT_MODEL_NAME))
            .andExpect(jsonPath("$.protocolTypeName").value(DEFAULT_PROTOCOL_TYPE_NAME))
            .andExpect(jsonPath("$.ipAddress").value(DEFAULT_IP_ADDRESS))
            .andExpect(jsonPath("$.port").value(DEFAULT_PORT))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION))
            .andExpect(jsonPath("$.simInfo").value(DEFAULT_SIM_INFO))
            .andExpect(jsonPath("$.cronTab").value(DEFAULT_CRON_TAB))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedOn").value(DEFAULT_LAST_MODIFIED_ON.toString()))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED.booleanValue()))
            .andExpect(jsonPath("$.flag").value(DEFAULT_FLAG.booleanValue()));
    }

    @Test
    @Transactional
    void getModemsByIdFiltering() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        Long id = modem.getId();

        defaultModemShouldBeFound("id.equals=" + id);
        defaultModemShouldNotBeFound("id.notEquals=" + id);

        defaultModemShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultModemShouldNotBeFound("id.greaterThan=" + id);

        defaultModemShouldBeFound("id.lessThanOrEqual=" + id);
        defaultModemShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllModemsByModelNameIsEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where modelName equals to DEFAULT_MODEL_NAME
        defaultModemShouldBeFound("modelName.equals=" + DEFAULT_MODEL_NAME);

        // Get all the modemList where modelName equals to UPDATED_MODEL_NAME
        defaultModemShouldNotBeFound("modelName.equals=" + UPDATED_MODEL_NAME);
    }

    @Test
    @Transactional
    void getAllModemsByModelNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where modelName not equals to DEFAULT_MODEL_NAME
        defaultModemShouldNotBeFound("modelName.notEquals=" + DEFAULT_MODEL_NAME);

        // Get all the modemList where modelName not equals to UPDATED_MODEL_NAME
        defaultModemShouldBeFound("modelName.notEquals=" + UPDATED_MODEL_NAME);
    }

    @Test
    @Transactional
    void getAllModemsByModelNameIsInShouldWork() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where modelName in DEFAULT_MODEL_NAME or UPDATED_MODEL_NAME
        defaultModemShouldBeFound("modelName.in=" + DEFAULT_MODEL_NAME + "," + UPDATED_MODEL_NAME);

        // Get all the modemList where modelName equals to UPDATED_MODEL_NAME
        defaultModemShouldNotBeFound("modelName.in=" + UPDATED_MODEL_NAME);
    }

    @Test
    @Transactional
    void getAllModemsByModelNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where modelName is not null
        defaultModemShouldBeFound("modelName.specified=true");

        // Get all the modemList where modelName is null
        defaultModemShouldNotBeFound("modelName.specified=false");
    }

    @Test
    @Transactional
    void getAllModemsByModelNameContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where modelName contains DEFAULT_MODEL_NAME
        defaultModemShouldBeFound("modelName.contains=" + DEFAULT_MODEL_NAME);

        // Get all the modemList where modelName contains UPDATED_MODEL_NAME
        defaultModemShouldNotBeFound("modelName.contains=" + UPDATED_MODEL_NAME);
    }

    @Test
    @Transactional
    void getAllModemsByModelNameNotContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where modelName does not contain DEFAULT_MODEL_NAME
        defaultModemShouldNotBeFound("modelName.doesNotContain=" + DEFAULT_MODEL_NAME);

        // Get all the modemList where modelName does not contain UPDATED_MODEL_NAME
        defaultModemShouldBeFound("modelName.doesNotContain=" + UPDATED_MODEL_NAME);
    }

    @Test
    @Transactional
    void getAllModemsByProtocolTypeNameIsEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where protocolTypeName equals to DEFAULT_PROTOCOL_TYPE_NAME
        defaultModemShouldBeFound("protocolTypeName.equals=" + DEFAULT_PROTOCOL_TYPE_NAME);

        // Get all the modemList where protocolTypeName equals to UPDATED_PROTOCOL_TYPE_NAME
        defaultModemShouldNotBeFound("protocolTypeName.equals=" + UPDATED_PROTOCOL_TYPE_NAME);
    }

    @Test
    @Transactional
    void getAllModemsByProtocolTypeNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where protocolTypeName not equals to DEFAULT_PROTOCOL_TYPE_NAME
        defaultModemShouldNotBeFound("protocolTypeName.notEquals=" + DEFAULT_PROTOCOL_TYPE_NAME);

        // Get all the modemList where protocolTypeName not equals to UPDATED_PROTOCOL_TYPE_NAME
        defaultModemShouldBeFound("protocolTypeName.notEquals=" + UPDATED_PROTOCOL_TYPE_NAME);
    }

    @Test
    @Transactional
    void getAllModemsByProtocolTypeNameIsInShouldWork() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where protocolTypeName in DEFAULT_PROTOCOL_TYPE_NAME or UPDATED_PROTOCOL_TYPE_NAME
        defaultModemShouldBeFound("protocolTypeName.in=" + DEFAULT_PROTOCOL_TYPE_NAME + "," + UPDATED_PROTOCOL_TYPE_NAME);

        // Get all the modemList where protocolTypeName equals to UPDATED_PROTOCOL_TYPE_NAME
        defaultModemShouldNotBeFound("protocolTypeName.in=" + UPDATED_PROTOCOL_TYPE_NAME);
    }

    @Test
    @Transactional
    void getAllModemsByProtocolTypeNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where protocolTypeName is not null
        defaultModemShouldBeFound("protocolTypeName.specified=true");

        // Get all the modemList where protocolTypeName is null
        defaultModemShouldNotBeFound("protocolTypeName.specified=false");
    }

    @Test
    @Transactional
    void getAllModemsByProtocolTypeNameContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where protocolTypeName contains DEFAULT_PROTOCOL_TYPE_NAME
        defaultModemShouldBeFound("protocolTypeName.contains=" + DEFAULT_PROTOCOL_TYPE_NAME);

        // Get all the modemList where protocolTypeName contains UPDATED_PROTOCOL_TYPE_NAME
        defaultModemShouldNotBeFound("protocolTypeName.contains=" + UPDATED_PROTOCOL_TYPE_NAME);
    }

    @Test
    @Transactional
    void getAllModemsByProtocolTypeNameNotContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where protocolTypeName does not contain DEFAULT_PROTOCOL_TYPE_NAME
        defaultModemShouldNotBeFound("protocolTypeName.doesNotContain=" + DEFAULT_PROTOCOL_TYPE_NAME);

        // Get all the modemList where protocolTypeName does not contain UPDATED_PROTOCOL_TYPE_NAME
        defaultModemShouldBeFound("protocolTypeName.doesNotContain=" + UPDATED_PROTOCOL_TYPE_NAME);
    }

    @Test
    @Transactional
    void getAllModemsByIpAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where ipAddress equals to DEFAULT_IP_ADDRESS
        defaultModemShouldBeFound("ipAddress.equals=" + DEFAULT_IP_ADDRESS);

        // Get all the modemList where ipAddress equals to UPDATED_IP_ADDRESS
        defaultModemShouldNotBeFound("ipAddress.equals=" + UPDATED_IP_ADDRESS);
    }

    @Test
    @Transactional
    void getAllModemsByIpAddressIsNotEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where ipAddress not equals to DEFAULT_IP_ADDRESS
        defaultModemShouldNotBeFound("ipAddress.notEquals=" + DEFAULT_IP_ADDRESS);

        // Get all the modemList where ipAddress not equals to UPDATED_IP_ADDRESS
        defaultModemShouldBeFound("ipAddress.notEquals=" + UPDATED_IP_ADDRESS);
    }

    @Test
    @Transactional
    void getAllModemsByIpAddressIsInShouldWork() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where ipAddress in DEFAULT_IP_ADDRESS or UPDATED_IP_ADDRESS
        defaultModemShouldBeFound("ipAddress.in=" + DEFAULT_IP_ADDRESS + "," + UPDATED_IP_ADDRESS);

        // Get all the modemList where ipAddress equals to UPDATED_IP_ADDRESS
        defaultModemShouldNotBeFound("ipAddress.in=" + UPDATED_IP_ADDRESS);
    }

    @Test
    @Transactional
    void getAllModemsByIpAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where ipAddress is not null
        defaultModemShouldBeFound("ipAddress.specified=true");

        // Get all the modemList where ipAddress is null
        defaultModemShouldNotBeFound("ipAddress.specified=false");
    }

    @Test
    @Transactional
    void getAllModemsByIpAddressContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where ipAddress contains DEFAULT_IP_ADDRESS
        defaultModemShouldBeFound("ipAddress.contains=" + DEFAULT_IP_ADDRESS);

        // Get all the modemList where ipAddress contains UPDATED_IP_ADDRESS
        defaultModemShouldNotBeFound("ipAddress.contains=" + UPDATED_IP_ADDRESS);
    }

    @Test
    @Transactional
    void getAllModemsByIpAddressNotContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where ipAddress does not contain DEFAULT_IP_ADDRESS
        defaultModemShouldNotBeFound("ipAddress.doesNotContain=" + DEFAULT_IP_ADDRESS);

        // Get all the modemList where ipAddress does not contain UPDATED_IP_ADDRESS
        defaultModemShouldBeFound("ipAddress.doesNotContain=" + UPDATED_IP_ADDRESS);
    }

    @Test
    @Transactional
    void getAllModemsByPortIsEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where port equals to DEFAULT_PORT
        defaultModemShouldBeFound("port.equals=" + DEFAULT_PORT);

        // Get all the modemList where port equals to UPDATED_PORT
        defaultModemShouldNotBeFound("port.equals=" + UPDATED_PORT);
    }

    @Test
    @Transactional
    void getAllModemsByPortIsNotEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where port not equals to DEFAULT_PORT
        defaultModemShouldNotBeFound("port.notEquals=" + DEFAULT_PORT);

        // Get all the modemList where port not equals to UPDATED_PORT
        defaultModemShouldBeFound("port.notEquals=" + UPDATED_PORT);
    }

    @Test
    @Transactional
    void getAllModemsByPortIsInShouldWork() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where port in DEFAULT_PORT or UPDATED_PORT
        defaultModemShouldBeFound("port.in=" + DEFAULT_PORT + "," + UPDATED_PORT);

        // Get all the modemList where port equals to UPDATED_PORT
        defaultModemShouldNotBeFound("port.in=" + UPDATED_PORT);
    }

    @Test
    @Transactional
    void getAllModemsByPortIsNullOrNotNull() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where port is not null
        defaultModemShouldBeFound("port.specified=true");

        // Get all the modemList where port is null
        defaultModemShouldNotBeFound("port.specified=false");
    }

    @Test
    @Transactional
    void getAllModemsByPortContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where port contains DEFAULT_PORT
        defaultModemShouldBeFound("port.contains=" + DEFAULT_PORT);

        // Get all the modemList where port contains UPDATED_PORT
        defaultModemShouldNotBeFound("port.contains=" + UPDATED_PORT);
    }

    @Test
    @Transactional
    void getAllModemsByPortNotContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where port does not contain DEFAULT_PORT
        defaultModemShouldNotBeFound("port.doesNotContain=" + DEFAULT_PORT);

        // Get all the modemList where port does not contain UPDATED_PORT
        defaultModemShouldBeFound("port.doesNotContain=" + UPDATED_PORT);
    }

    @Test
    @Transactional
    void getAllModemsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where name equals to DEFAULT_NAME
        defaultModemShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the modemList where name equals to UPDATED_NAME
        defaultModemShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllModemsByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where name not equals to DEFAULT_NAME
        defaultModemShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the modemList where name not equals to UPDATED_NAME
        defaultModemShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllModemsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where name in DEFAULT_NAME or UPDATED_NAME
        defaultModemShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the modemList where name equals to UPDATED_NAME
        defaultModemShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllModemsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where name is not null
        defaultModemShouldBeFound("name.specified=true");

        // Get all the modemList where name is null
        defaultModemShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllModemsByNameContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where name contains DEFAULT_NAME
        defaultModemShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the modemList where name contains UPDATED_NAME
        defaultModemShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllModemsByNameNotContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where name does not contain DEFAULT_NAME
        defaultModemShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the modemList where name does not contain UPDATED_NAME
        defaultModemShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllModemsByAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where address equals to DEFAULT_ADDRESS
        defaultModemShouldBeFound("address.equals=" + DEFAULT_ADDRESS);

        // Get all the modemList where address equals to UPDATED_ADDRESS
        defaultModemShouldNotBeFound("address.equals=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    void getAllModemsByAddressIsNotEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where address not equals to DEFAULT_ADDRESS
        defaultModemShouldNotBeFound("address.notEquals=" + DEFAULT_ADDRESS);

        // Get all the modemList where address not equals to UPDATED_ADDRESS
        defaultModemShouldBeFound("address.notEquals=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    void getAllModemsByAddressIsInShouldWork() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where address in DEFAULT_ADDRESS or UPDATED_ADDRESS
        defaultModemShouldBeFound("address.in=" + DEFAULT_ADDRESS + "," + UPDATED_ADDRESS);

        // Get all the modemList where address equals to UPDATED_ADDRESS
        defaultModemShouldNotBeFound("address.in=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    void getAllModemsByAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where address is not null
        defaultModemShouldBeFound("address.specified=true");

        // Get all the modemList where address is null
        defaultModemShouldNotBeFound("address.specified=false");
    }

    @Test
    @Transactional
    void getAllModemsByAddressContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where address contains DEFAULT_ADDRESS
        defaultModemShouldBeFound("address.contains=" + DEFAULT_ADDRESS);

        // Get all the modemList where address contains UPDATED_ADDRESS
        defaultModemShouldNotBeFound("address.contains=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    void getAllModemsByAddressNotContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where address does not contain DEFAULT_ADDRESS
        defaultModemShouldNotBeFound("address.doesNotContain=" + DEFAULT_ADDRESS);

        // Get all the modemList where address does not contain UPDATED_ADDRESS
        defaultModemShouldBeFound("address.doesNotContain=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    void getAllModemsByLocationIsEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where location equals to DEFAULT_LOCATION
        defaultModemShouldBeFound("location.equals=" + DEFAULT_LOCATION);

        // Get all the modemList where location equals to UPDATED_LOCATION
        defaultModemShouldNotBeFound("location.equals=" + UPDATED_LOCATION);
    }

    @Test
    @Transactional
    void getAllModemsByLocationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where location not equals to DEFAULT_LOCATION
        defaultModemShouldNotBeFound("location.notEquals=" + DEFAULT_LOCATION);

        // Get all the modemList where location not equals to UPDATED_LOCATION
        defaultModemShouldBeFound("location.notEquals=" + UPDATED_LOCATION);
    }

    @Test
    @Transactional
    void getAllModemsByLocationIsInShouldWork() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where location in DEFAULT_LOCATION or UPDATED_LOCATION
        defaultModemShouldBeFound("location.in=" + DEFAULT_LOCATION + "," + UPDATED_LOCATION);

        // Get all the modemList where location equals to UPDATED_LOCATION
        defaultModemShouldNotBeFound("location.in=" + UPDATED_LOCATION);
    }

    @Test
    @Transactional
    void getAllModemsByLocationIsNullOrNotNull() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where location is not null
        defaultModemShouldBeFound("location.specified=true");

        // Get all the modemList where location is null
        defaultModemShouldNotBeFound("location.specified=false");
    }

    @Test
    @Transactional
    void getAllModemsByLocationContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where location contains DEFAULT_LOCATION
        defaultModemShouldBeFound("location.contains=" + DEFAULT_LOCATION);

        // Get all the modemList where location contains UPDATED_LOCATION
        defaultModemShouldNotBeFound("location.contains=" + UPDATED_LOCATION);
    }

    @Test
    @Transactional
    void getAllModemsByLocationNotContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where location does not contain DEFAULT_LOCATION
        defaultModemShouldNotBeFound("location.doesNotContain=" + DEFAULT_LOCATION);

        // Get all the modemList where location does not contain UPDATED_LOCATION
        defaultModemShouldBeFound("location.doesNotContain=" + UPDATED_LOCATION);
    }

    @Test
    @Transactional
    void getAllModemsBySimInfoIsEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where simInfo equals to DEFAULT_SIM_INFO
        defaultModemShouldBeFound("simInfo.equals=" + DEFAULT_SIM_INFO);

        // Get all the modemList where simInfo equals to UPDATED_SIM_INFO
        defaultModemShouldNotBeFound("simInfo.equals=" + UPDATED_SIM_INFO);
    }

    @Test
    @Transactional
    void getAllModemsBySimInfoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where simInfo not equals to DEFAULT_SIM_INFO
        defaultModemShouldNotBeFound("simInfo.notEquals=" + DEFAULT_SIM_INFO);

        // Get all the modemList where simInfo not equals to UPDATED_SIM_INFO
        defaultModemShouldBeFound("simInfo.notEquals=" + UPDATED_SIM_INFO);
    }

    @Test
    @Transactional
    void getAllModemsBySimInfoIsInShouldWork() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where simInfo in DEFAULT_SIM_INFO or UPDATED_SIM_INFO
        defaultModemShouldBeFound("simInfo.in=" + DEFAULT_SIM_INFO + "," + UPDATED_SIM_INFO);

        // Get all the modemList where simInfo equals to UPDATED_SIM_INFO
        defaultModemShouldNotBeFound("simInfo.in=" + UPDATED_SIM_INFO);
    }

    @Test
    @Transactional
    void getAllModemsBySimInfoIsNullOrNotNull() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where simInfo is not null
        defaultModemShouldBeFound("simInfo.specified=true");

        // Get all the modemList where simInfo is null
        defaultModemShouldNotBeFound("simInfo.specified=false");
    }

    @Test
    @Transactional
    void getAllModemsBySimInfoContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where simInfo contains DEFAULT_SIM_INFO
        defaultModemShouldBeFound("simInfo.contains=" + DEFAULT_SIM_INFO);

        // Get all the modemList where simInfo contains UPDATED_SIM_INFO
        defaultModemShouldNotBeFound("simInfo.contains=" + UPDATED_SIM_INFO);
    }

    @Test
    @Transactional
    void getAllModemsBySimInfoNotContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where simInfo does not contain DEFAULT_SIM_INFO
        defaultModemShouldNotBeFound("simInfo.doesNotContain=" + DEFAULT_SIM_INFO);

        // Get all the modemList where simInfo does not contain UPDATED_SIM_INFO
        defaultModemShouldBeFound("simInfo.doesNotContain=" + UPDATED_SIM_INFO);
    }

    @Test
    @Transactional
    void getAllModemsByCronTabIsEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where cronTab equals to DEFAULT_CRON_TAB
        defaultModemShouldBeFound("cronTab.equals=" + DEFAULT_CRON_TAB);

        // Get all the modemList where cronTab equals to UPDATED_CRON_TAB
        defaultModemShouldNotBeFound("cronTab.equals=" + UPDATED_CRON_TAB);
    }

    @Test
    @Transactional
    void getAllModemsByCronTabIsNotEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where cronTab not equals to DEFAULT_CRON_TAB
        defaultModemShouldNotBeFound("cronTab.notEquals=" + DEFAULT_CRON_TAB);

        // Get all the modemList where cronTab not equals to UPDATED_CRON_TAB
        defaultModemShouldBeFound("cronTab.notEquals=" + UPDATED_CRON_TAB);
    }

    @Test
    @Transactional
    void getAllModemsByCronTabIsInShouldWork() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where cronTab in DEFAULT_CRON_TAB or UPDATED_CRON_TAB
        defaultModemShouldBeFound("cronTab.in=" + DEFAULT_CRON_TAB + "," + UPDATED_CRON_TAB);

        // Get all the modemList where cronTab equals to UPDATED_CRON_TAB
        defaultModemShouldNotBeFound("cronTab.in=" + UPDATED_CRON_TAB);
    }

    @Test
    @Transactional
    void getAllModemsByCronTabIsNullOrNotNull() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where cronTab is not null
        defaultModemShouldBeFound("cronTab.specified=true");

        // Get all the modemList where cronTab is null
        defaultModemShouldNotBeFound("cronTab.specified=false");
    }

    @Test
    @Transactional
    void getAllModemsByCronTabContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where cronTab contains DEFAULT_CRON_TAB
        defaultModemShouldBeFound("cronTab.contains=" + DEFAULT_CRON_TAB);

        // Get all the modemList where cronTab contains UPDATED_CRON_TAB
        defaultModemShouldNotBeFound("cronTab.contains=" + UPDATED_CRON_TAB);
    }

    @Test
    @Transactional
    void getAllModemsByCronTabNotContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where cronTab does not contain DEFAULT_CRON_TAB
        defaultModemShouldNotBeFound("cronTab.doesNotContain=" + DEFAULT_CRON_TAB);

        // Get all the modemList where cronTab does not contain UPDATED_CRON_TAB
        defaultModemShouldBeFound("cronTab.doesNotContain=" + UPDATED_CRON_TAB);
    }

    @Test
    @Transactional
    void getAllModemsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where createdBy equals to DEFAULT_CREATED_BY
        defaultModemShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the modemList where createdBy equals to UPDATED_CREATED_BY
        defaultModemShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllModemsByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where createdBy not equals to DEFAULT_CREATED_BY
        defaultModemShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the modemList where createdBy not equals to UPDATED_CREATED_BY
        defaultModemShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllModemsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultModemShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the modemList where createdBy equals to UPDATED_CREATED_BY
        defaultModemShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllModemsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where createdBy is not null
        defaultModemShouldBeFound("createdBy.specified=true");

        // Get all the modemList where createdBy is null
        defaultModemShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllModemsByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where createdBy contains DEFAULT_CREATED_BY
        defaultModemShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the modemList where createdBy contains UPDATED_CREATED_BY
        defaultModemShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllModemsByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where createdBy does not contain DEFAULT_CREATED_BY
        defaultModemShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the modemList where createdBy does not contain UPDATED_CREATED_BY
        defaultModemShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllModemsByCreatedOnIsEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where createdOn equals to DEFAULT_CREATED_ON
        defaultModemShouldBeFound("createdOn.equals=" + DEFAULT_CREATED_ON);

        // Get all the modemList where createdOn equals to UPDATED_CREATED_ON
        defaultModemShouldNotBeFound("createdOn.equals=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    void getAllModemsByCreatedOnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where createdOn not equals to DEFAULT_CREATED_ON
        defaultModemShouldNotBeFound("createdOn.notEquals=" + DEFAULT_CREATED_ON);

        // Get all the modemList where createdOn not equals to UPDATED_CREATED_ON
        defaultModemShouldBeFound("createdOn.notEquals=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    void getAllModemsByCreatedOnIsInShouldWork() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where createdOn in DEFAULT_CREATED_ON or UPDATED_CREATED_ON
        defaultModemShouldBeFound("createdOn.in=" + DEFAULT_CREATED_ON + "," + UPDATED_CREATED_ON);

        // Get all the modemList where createdOn equals to UPDATED_CREATED_ON
        defaultModemShouldNotBeFound("createdOn.in=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    void getAllModemsByCreatedOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where createdOn is not null
        defaultModemShouldBeFound("createdOn.specified=true");

        // Get all the modemList where createdOn is null
        defaultModemShouldNotBeFound("createdOn.specified=false");
    }

    @Test
    @Transactional
    void getAllModemsByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultModemShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the modemList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultModemShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllModemsByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultModemShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the modemList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultModemShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllModemsByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultModemShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the modemList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultModemShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllModemsByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where lastModifiedBy is not null
        defaultModemShouldBeFound("lastModifiedBy.specified=true");

        // Get all the modemList where lastModifiedBy is null
        defaultModemShouldNotBeFound("lastModifiedBy.specified=false");
    }

    @Test
    @Transactional
    void getAllModemsByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultModemShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the modemList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultModemShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllModemsByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultModemShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the modemList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultModemShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllModemsByLastModifiedOnIsEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where lastModifiedOn equals to DEFAULT_LAST_MODIFIED_ON
        defaultModemShouldBeFound("lastModifiedOn.equals=" + DEFAULT_LAST_MODIFIED_ON);

        // Get all the modemList where lastModifiedOn equals to UPDATED_LAST_MODIFIED_ON
        defaultModemShouldNotBeFound("lastModifiedOn.equals=" + UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void getAllModemsByLastModifiedOnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where lastModifiedOn not equals to DEFAULT_LAST_MODIFIED_ON
        defaultModemShouldNotBeFound("lastModifiedOn.notEquals=" + DEFAULT_LAST_MODIFIED_ON);

        // Get all the modemList where lastModifiedOn not equals to UPDATED_LAST_MODIFIED_ON
        defaultModemShouldBeFound("lastModifiedOn.notEquals=" + UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void getAllModemsByLastModifiedOnIsInShouldWork() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where lastModifiedOn in DEFAULT_LAST_MODIFIED_ON or UPDATED_LAST_MODIFIED_ON
        defaultModemShouldBeFound("lastModifiedOn.in=" + DEFAULT_LAST_MODIFIED_ON + "," + UPDATED_LAST_MODIFIED_ON);

        // Get all the modemList where lastModifiedOn equals to UPDATED_LAST_MODIFIED_ON
        defaultModemShouldNotBeFound("lastModifiedOn.in=" + UPDATED_LAST_MODIFIED_ON);
    }

    @Test
    @Transactional
    void getAllModemsByLastModifiedOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where lastModifiedOn is not null
        defaultModemShouldBeFound("lastModifiedOn.specified=true");

        // Get all the modemList where lastModifiedOn is null
        defaultModemShouldNotBeFound("lastModifiedOn.specified=false");
    }

    @Test
    @Transactional
    void getAllModemsByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where isDeleted equals to DEFAULT_IS_DELETED
        defaultModemShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the modemList where isDeleted equals to UPDATED_IS_DELETED
        defaultModemShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    void getAllModemsByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultModemShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the modemList where isDeleted not equals to UPDATED_IS_DELETED
        defaultModemShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    void getAllModemsByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultModemShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the modemList where isDeleted equals to UPDATED_IS_DELETED
        defaultModemShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    void getAllModemsByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where isDeleted is not null
        defaultModemShouldBeFound("isDeleted.specified=true");

        // Get all the modemList where isDeleted is null
        defaultModemShouldNotBeFound("isDeleted.specified=false");
    }

    @Test
    @Transactional
    void getAllModemsByFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where flag equals to DEFAULT_FLAG
        defaultModemShouldBeFound("flag.equals=" + DEFAULT_FLAG);

        // Get all the modemList where flag equals to UPDATED_FLAG
        defaultModemShouldNotBeFound("flag.equals=" + UPDATED_FLAG);
    }

    @Test
    @Transactional
    void getAllModemsByFlagIsNotEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where flag not equals to DEFAULT_FLAG
        defaultModemShouldNotBeFound("flag.notEquals=" + DEFAULT_FLAG);

        // Get all the modemList where flag not equals to UPDATED_FLAG
        defaultModemShouldBeFound("flag.notEquals=" + UPDATED_FLAG);
    }

    @Test
    @Transactional
    void getAllModemsByFlagIsInShouldWork() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where flag in DEFAULT_FLAG or UPDATED_FLAG
        defaultModemShouldBeFound("flag.in=" + DEFAULT_FLAG + "," + UPDATED_FLAG);

        // Get all the modemList where flag equals to UPDATED_FLAG
        defaultModemShouldNotBeFound("flag.in=" + UPDATED_FLAG);
    }

    @Test
    @Transactional
    void getAllModemsByFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        // Get all the modemList where flag is not null
        defaultModemShouldBeFound("flag.specified=true");

        // Get all the modemList where flag is null
        defaultModemShouldNotBeFound("flag.specified=false");
    }

    @Test
    @Transactional
    void getAllModemsByModelIsEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);
        CommonValue model;
        if (TestUtil.findAll(em, CommonValue.class).isEmpty()) {
            model = CommonValueResourceIT.createEntity(em);
            em.persist(model);
            em.flush();
        } else {
            model = TestUtil.findAll(em, CommonValue.class).get(0);
        }
        em.persist(model);
        em.flush();
        modem.setModel(model);
        modemRepository.saveAndFlush(modem);
        Long modelId = model.getId();

        // Get all the modemList where model equals to modelId
        defaultModemShouldBeFound("modelId.equals=" + modelId);

        // Get all the modemList where model equals to (modelId + 1)
        defaultModemShouldNotBeFound("modelId.equals=" + (modelId + 1));
    }

    @Test
    @Transactional
    void getAllModemsByProtocolTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);
        CommonValue protocolType;
        if (TestUtil.findAll(em, CommonValue.class).isEmpty()) {
            protocolType = CommonValueResourceIT.createEntity(em);
            em.persist(protocolType);
            em.flush();
        } else {
            protocolType = TestUtil.findAll(em, CommonValue.class).get(0);
        }
        em.persist(protocolType);
        em.flush();
        modem.setProtocolType(protocolType);
        modemRepository.saveAndFlush(modem);
        Long protocolTypeId = protocolType.getId();

        // Get all the modemList where protocolType equals to protocolTypeId
        defaultModemShouldBeFound("protocolTypeId.equals=" + protocolTypeId);

        // Get all the modemList where protocolType equals to (protocolTypeId + 1)
        defaultModemShouldNotBeFound("protocolTypeId.equals=" + (protocolTypeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultModemShouldBeFound(String filter) throws Exception {
        restModemMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(modem.getId().intValue())))
            .andExpect(jsonPath("$.[*].modelName").value(hasItem(DEFAULT_MODEL_NAME)))
            .andExpect(jsonPath("$.[*].protocolTypeName").value(hasItem(DEFAULT_PROTOCOL_TYPE_NAME)))
            .andExpect(jsonPath("$.[*].ipAddress").value(hasItem(DEFAULT_IP_ADDRESS)))
            .andExpect(jsonPath("$.[*].port").value(hasItem(DEFAULT_PORT)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION)))
            .andExpect(jsonPath("$.[*].simInfo").value(hasItem(DEFAULT_SIM_INFO)))
            .andExpect(jsonPath("$.[*].cronTab").value(hasItem(DEFAULT_CRON_TAB)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedOn").value(hasItem(DEFAULT_LAST_MODIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].flag").value(hasItem(DEFAULT_FLAG.booleanValue())));

        // Check, that the count call also returns 1
        restModemMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultModemShouldNotBeFound(String filter) throws Exception {
        restModemMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restModemMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingModem() throws Exception {
        // Get the modem
        restModemMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewModem() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        int databaseSizeBeforeUpdate = modemRepository.findAll().size();

        // Update the modem
        Modem updatedModem = modemRepository.findById(modem.getId()).get();
        // Disconnect from session so that the updates on updatedModem are not directly saved in db
        em.detach(updatedModem);
        updatedModem
            .modelName(UPDATED_MODEL_NAME)
            .protocolTypeName(UPDATED_PROTOCOL_TYPE_NAME)
            .ipAddress(UPDATED_IP_ADDRESS)
            .port(UPDATED_PORT)
            .name(UPDATED_NAME)
            .address(UPDATED_ADDRESS)
            .location(UPDATED_LOCATION)
            .simInfo(UPDATED_SIM_INFO)
            .cronTab(UPDATED_CRON_TAB)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedOn(UPDATED_LAST_MODIFIED_ON)
            .isDeleted(UPDATED_IS_DELETED)
            .flag(UPDATED_FLAG);
        ModemDTO modemDTO = modemMapper.toDto(updatedModem);

        restModemMockMvc
            .perform(
                put(ENTITY_API_URL_ID, modemDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(modemDTO))
            )
            .andExpect(status().isOk());

        // Validate the Modem in the database
        List<Modem> modemList = modemRepository.findAll();
        assertThat(modemList).hasSize(databaseSizeBeforeUpdate);
        Modem testModem = modemList.get(modemList.size() - 1);
        assertThat(testModem.getModelName()).isEqualTo(UPDATED_MODEL_NAME);
        assertThat(testModem.getProtocolTypeName()).isEqualTo(UPDATED_PROTOCOL_TYPE_NAME);
        assertThat(testModem.getIpAddress()).isEqualTo(UPDATED_IP_ADDRESS);
        assertThat(testModem.getPort()).isEqualTo(UPDATED_PORT);
        assertThat(testModem.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testModem.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testModem.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testModem.getSimInfo()).isEqualTo(UPDATED_SIM_INFO);
        assertThat(testModem.getCronTab()).isEqualTo(UPDATED_CRON_TAB);
        assertThat(testModem.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testModem.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testModem.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testModem.getLastModifiedOn()).isEqualTo(UPDATED_LAST_MODIFIED_ON);
        assertThat(testModem.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testModem.getFlag()).isEqualTo(UPDATED_FLAG);
    }

    @Test
    @Transactional
    void putNonExistingModem() throws Exception {
        int databaseSizeBeforeUpdate = modemRepository.findAll().size();
        modem.setId(count.incrementAndGet());

        // Create the Modem
        ModemDTO modemDTO = modemMapper.toDto(modem);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restModemMockMvc
            .perform(
                put(ENTITY_API_URL_ID, modemDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(modemDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Modem in the database
        List<Modem> modemList = modemRepository.findAll();
        assertThat(modemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchModem() throws Exception {
        int databaseSizeBeforeUpdate = modemRepository.findAll().size();
        modem.setId(count.incrementAndGet());

        // Create the Modem
        ModemDTO modemDTO = modemMapper.toDto(modem);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restModemMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(modemDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Modem in the database
        List<Modem> modemList = modemRepository.findAll();
        assertThat(modemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamModem() throws Exception {
        int databaseSizeBeforeUpdate = modemRepository.findAll().size();
        modem.setId(count.incrementAndGet());

        // Create the Modem
        ModemDTO modemDTO = modemMapper.toDto(modem);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restModemMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(modemDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Modem in the database
        List<Modem> modemList = modemRepository.findAll();
        assertThat(modemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateModemWithPatch() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        int databaseSizeBeforeUpdate = modemRepository.findAll().size();

        // Update the modem using partial update
        Modem partialUpdatedModem = new Modem();
        partialUpdatedModem.setId(modem.getId());

        partialUpdatedModem
            .protocolTypeName(UPDATED_PROTOCOL_TYPE_NAME)
            .ipAddress(UPDATED_IP_ADDRESS)
            .name(UPDATED_NAME)
            .simInfo(UPDATED_SIM_INFO)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedOn(UPDATED_LAST_MODIFIED_ON)
            .flag(UPDATED_FLAG);

        restModemMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedModem.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedModem))
            )
            .andExpect(status().isOk());

        // Validate the Modem in the database
        List<Modem> modemList = modemRepository.findAll();
        assertThat(modemList).hasSize(databaseSizeBeforeUpdate);
        Modem testModem = modemList.get(modemList.size() - 1);
        assertThat(testModem.getModelName()).isEqualTo(DEFAULT_MODEL_NAME);
        assertThat(testModem.getProtocolTypeName()).isEqualTo(UPDATED_PROTOCOL_TYPE_NAME);
        assertThat(testModem.getIpAddress()).isEqualTo(UPDATED_IP_ADDRESS);
        assertThat(testModem.getPort()).isEqualTo(DEFAULT_PORT);
        assertThat(testModem.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testModem.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testModem.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testModem.getSimInfo()).isEqualTo(UPDATED_SIM_INFO);
        assertThat(testModem.getCronTab()).isEqualTo(DEFAULT_CRON_TAB);
        assertThat(testModem.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testModem.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testModem.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testModem.getLastModifiedOn()).isEqualTo(UPDATED_LAST_MODIFIED_ON);
        assertThat(testModem.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testModem.getFlag()).isEqualTo(UPDATED_FLAG);
    }

    @Test
    @Transactional
    void fullUpdateModemWithPatch() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        int databaseSizeBeforeUpdate = modemRepository.findAll().size();

        // Update the modem using partial update
        Modem partialUpdatedModem = new Modem();
        partialUpdatedModem.setId(modem.getId());

        partialUpdatedModem
            .modelName(UPDATED_MODEL_NAME)
            .protocolTypeName(UPDATED_PROTOCOL_TYPE_NAME)
            .ipAddress(UPDATED_IP_ADDRESS)
            .port(UPDATED_PORT)
            .name(UPDATED_NAME)
            .address(UPDATED_ADDRESS)
            .location(UPDATED_LOCATION)
            .simInfo(UPDATED_SIM_INFO)
            .cronTab(UPDATED_CRON_TAB)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedOn(UPDATED_LAST_MODIFIED_ON)
            .isDeleted(UPDATED_IS_DELETED)
            .flag(UPDATED_FLAG);

        restModemMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedModem.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedModem))
            )
            .andExpect(status().isOk());

        // Validate the Modem in the database
        List<Modem> modemList = modemRepository.findAll();
        assertThat(modemList).hasSize(databaseSizeBeforeUpdate);
        Modem testModem = modemList.get(modemList.size() - 1);
        assertThat(testModem.getModelName()).isEqualTo(UPDATED_MODEL_NAME);
        assertThat(testModem.getProtocolTypeName()).isEqualTo(UPDATED_PROTOCOL_TYPE_NAME);
        assertThat(testModem.getIpAddress()).isEqualTo(UPDATED_IP_ADDRESS);
        assertThat(testModem.getPort()).isEqualTo(UPDATED_PORT);
        assertThat(testModem.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testModem.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testModem.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testModem.getSimInfo()).isEqualTo(UPDATED_SIM_INFO);
        assertThat(testModem.getCronTab()).isEqualTo(UPDATED_CRON_TAB);
        assertThat(testModem.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testModem.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testModem.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testModem.getLastModifiedOn()).isEqualTo(UPDATED_LAST_MODIFIED_ON);
        assertThat(testModem.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testModem.getFlag()).isEqualTo(UPDATED_FLAG);
    }

    @Test
    @Transactional
    void patchNonExistingModem() throws Exception {
        int databaseSizeBeforeUpdate = modemRepository.findAll().size();
        modem.setId(count.incrementAndGet());

        // Create the Modem
        ModemDTO modemDTO = modemMapper.toDto(modem);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restModemMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, modemDTO.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(modemDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Modem in the database
        List<Modem> modemList = modemRepository.findAll();
        assertThat(modemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchModem() throws Exception {
        int databaseSizeBeforeUpdate = modemRepository.findAll().size();
        modem.setId(count.incrementAndGet());

        // Create the Modem
        ModemDTO modemDTO = modemMapper.toDto(modem);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restModemMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(modemDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Modem in the database
        List<Modem> modemList = modemRepository.findAll();
        assertThat(modemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamModem() throws Exception {
        int databaseSizeBeforeUpdate = modemRepository.findAll().size();
        modem.setId(count.incrementAndGet());

        // Create the Modem
        ModemDTO modemDTO = modemMapper.toDto(modem);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restModemMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(modemDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Modem in the database
        List<Modem> modemList = modemRepository.findAll();
        assertThat(modemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteModem() throws Exception {
        // Initialize the database
        modemRepository.saveAndFlush(modem);

        int databaseSizeBeforeDelete = modemRepository.findAll().size();

        // Delete the modem
        restModemMockMvc
            .perform(delete(ENTITY_API_URL_ID, modem.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Modem> modemList = modemRepository.findAll();
        assertThat(modemList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
