package mn.electrical.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import mn.electrical.domain.*; // for static metamodels
import mn.electrical.domain.CommonValue;
import mn.electrical.repository.CommonValueRepository;
import mn.electrical.service.criteria.CommonValueCriteria;
import mn.electrical.service.dto.CommonValueDTO;
import mn.electrical.service.mapper.CommonValueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link CommonValue} entities in the database.
 * The main input is a {@link CommonValueCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CommonValueDTO} or a {@link Page} of {@link CommonValueDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CommonValueQueryService extends QueryService<CommonValue> {

    private final Logger log = LoggerFactory.getLogger(CommonValueQueryService.class);

    private final CommonValueRepository commonValueRepository;

    private final CommonValueMapper commonValueMapper;

    public CommonValueQueryService(CommonValueRepository commonValueRepository, CommonValueMapper commonValueMapper) {
        this.commonValueRepository = commonValueRepository;
        this.commonValueMapper = commonValueMapper;
    }

    /**
     * Return a {@link List} of {@link CommonValueDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CommonValueDTO> findByCriteria(CommonValueCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CommonValue> specification = createSpecification(criteria);
        return commonValueMapper.toDto(commonValueRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CommonValueDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CommonValueDTO> findByCriteria(CommonValueCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CommonValue> specification = createSpecification(criteria);
        return commonValueRepository.findAll(specification, page).map(commonValueMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CommonValueCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CommonValue> specification = createSpecification(criteria);
        return commonValueRepository.count(specification);
    }

    /**
     * Function to convert {@link CommonValueCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CommonValue> createSpecification(CommonValueCriteria criteria) {
        Specification<CommonValue> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), CommonValue_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), CommonValue_.name));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), CommonValue_.code));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), CommonValue_.description));
            }
            if (criteria.getOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getOrder(), CommonValue_.order));
            }
            if (criteria.getDataType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDataType(), CommonValue_.dataType));
            }
            if (criteria.getDataString() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDataString(), CommonValue_.dataString));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), CommonValue_.status));
            }
            if (criteria.getDataShort() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDataShort(), CommonValue_.dataShort));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), CommonValue_.createdBy));
            }
            if (criteria.getCreatedOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedOn(), CommonValue_.createdOn));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), CommonValue_.lastModifiedBy));
            }
            if (criteria.getLastModifiedOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedOn(), CommonValue_.lastModifiedOn));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildSpecification(criteria.getIsDeleted(), CommonValue_.isDeleted));
            }
            if (criteria.getParentId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getParentId(),
                            root -> root.join(CommonValue_.parent, JoinType.LEFT).get(CommonCode_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
