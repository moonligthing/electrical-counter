package mn.electrical.service;

import java.util.Optional;
import mn.electrical.domain.MeterReading;
import mn.electrical.repository.MeterReadingRepository;
import mn.electrical.service.dto.MeterReadingDTO;
import mn.electrical.service.mapper.MeterReadingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link MeterReading}.
 */
@Service
@Transactional
public class MeterReadingService {

    private final Logger log = LoggerFactory.getLogger(MeterReadingService.class);

    private final MeterReadingRepository meterReadingRepository;

    private final MeterReadingMapper meterReadingMapper;

    public MeterReadingService(MeterReadingRepository meterReadingRepository, MeterReadingMapper meterReadingMapper) {
        this.meterReadingRepository = meterReadingRepository;
        this.meterReadingMapper = meterReadingMapper;
    }

    /**
     * Save a meterReading.
     *
     * @param meterReadingDTO the entity to save.
     * @return the persisted entity.
     */
    public MeterReadingDTO save(MeterReadingDTO meterReadingDTO) {
        log.debug("Request to save MeterReading : {}", meterReadingDTO);
        MeterReading meterReading = meterReadingMapper.toEntity(meterReadingDTO);
        meterReading = meterReadingRepository.save(meterReading);
        return meterReadingMapper.toDto(meterReading);
    }

    /**
     * Partially update a meterReading.
     *
     * @param meterReadingDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<MeterReadingDTO> partialUpdate(MeterReadingDTO meterReadingDTO) {
        log.debug("Request to partially update MeterReading : {}", meterReadingDTO);

        return meterReadingRepository
            .findById(meterReadingDTO.getId())
            .map(existingMeterReading -> {
                meterReadingMapper.partialUpdate(existingMeterReading, meterReadingDTO);

                return existingMeterReading;
            })
            .map(meterReadingRepository::save)
            .map(meterReadingMapper::toDto);
    }

    /**
     * Get all the meterReadings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MeterReadingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MeterReadings");
        return meterReadingRepository.findAll(pageable).map(meterReadingMapper::toDto);
    }

    /**
     * Get one meterReading by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MeterReadingDTO> findOne(Long id) {
        log.debug("Request to get MeterReading : {}", id);
        return meterReadingRepository.findById(id).map(meterReadingMapper::toDto);
    }

    /**
     * Delete the meterReading by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete MeterReading : {}", id);
        meterReadingRepository.deleteById(id);
    }
}
