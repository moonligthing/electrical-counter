package mn.electrical.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import mn.electrical.domain.*; // for static metamodels
import mn.electrical.domain.Meter;
import mn.electrical.repository.MeterRepository;
import mn.electrical.service.criteria.MeterCriteria;
import mn.electrical.service.dto.MeterDTO;
import mn.electrical.service.mapper.MeterMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Meter} entities in the database.
 * The main input is a {@link MeterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MeterDTO} or a {@link Page} of {@link MeterDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MeterQueryService extends QueryService<Meter> {

    private final Logger log = LoggerFactory.getLogger(MeterQueryService.class);

    private final MeterRepository meterRepository;

    private final MeterMapper meterMapper;

    public MeterQueryService(MeterRepository meterRepository, MeterMapper meterMapper) {
        this.meterRepository = meterRepository;
        this.meterMapper = meterMapper;
    }

    /**
     * Return a {@link List} of {@link MeterDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MeterDTO> findByCriteria(MeterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Meter> specification = createSpecification(criteria);
        return meterMapper.toDto(meterRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link MeterDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MeterDTO> findByCriteria(MeterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Meter> specification = createSpecification(criteria);
        return meterRepository.findAll(specification, page).map(meterMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MeterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Meter> specification = createSpecification(criteria);
        return meterRepository.count(specification);
    }

    /**
     * Function to convert {@link MeterCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Meter> createSpecification(MeterCriteria criteria) {
        Specification<Meter> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Meter_.id));
            }
            if (criteria.getModemName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getModemName(), Meter_.modemName));
            }
            if (criteria.getModelName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getModelName(), Meter_.modelName));
            }
            if (criteria.getMeterTypeName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMeterTypeName(), Meter_.meterTypeName));
            }
            if (criteria.getCurrentName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCurrentName(), Meter_.currentName));
            }
            if (criteria.getVoltageName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVoltageName(), Meter_.voltageName));
            }
            if (criteria.getInstallationOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getInstallationOn(), Meter_.installationOn));
            }
            if (criteria.getWarrantyOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getWarrantyOn(), Meter_.warrantyOn));
            }
            if (criteria.getNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNumber(), Meter_.number));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Meter_.name));
            }
            if (criteria.getAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress(), Meter_.address));
            }
            if (criteria.getCtRatio() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCtRatio(), Meter_.ctRatio));
            }
            if (criteria.getVtRatio() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVtRatio(), Meter_.vtRatio));
            }
            if (criteria.getDeviceAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDeviceAddress(), Meter_.deviceAddress));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), Meter_.status));
            }
            if (criteria.getRate1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRate1(), Meter_.rate1));
            }
            if (criteria.getRate2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRate2(), Meter_.rate2));
            }
            if (criteria.getRate3() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRate3(), Meter_.rate3));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), Meter_.createdBy));
            }
            if (criteria.getCreatedOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedOn(), Meter_.createdOn));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), Meter_.lastModifiedBy));
            }
            if (criteria.getLastModifiedOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedOn(), Meter_.lastModifiedOn));
            }
            if (criteria.getModemId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getModemId(), root -> root.join(Meter_.modem, JoinType.LEFT).get(Modem_.id))
                    );
            }
            if (criteria.getMeterTypeId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getMeterTypeId(),
                            root -> root.join(Meter_.meterType, JoinType.LEFT).get(CommonValue_.id)
                        )
                    );
            }
            if (criteria.getCurrentId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getCurrentId(), root -> root.join(Meter_.current, JoinType.LEFT).get(CommonValue_.id))
                    );
            }
            if (criteria.getVoltageId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getVoltageId(), root -> root.join(Meter_.voltage, JoinType.LEFT).get(CommonValue_.id))
                    );
            }
            if (criteria.getModelId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getModelId(), root -> root.join(Meter_.model, JoinType.LEFT).get(CommonValue_.id))
                    );
            }
        }
        return specification;
    }
}
