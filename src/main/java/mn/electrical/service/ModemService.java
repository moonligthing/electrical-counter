package mn.electrical.service;

import java.util.Optional;
import mn.electrical.domain.Modem;
import mn.electrical.repository.ModemRepository;
import mn.electrical.service.dto.ModemDTO;
import mn.electrical.service.mapper.ModemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Modem}.
 */
@Service
@Transactional
public class ModemService {

    private final Logger log = LoggerFactory.getLogger(ModemService.class);

    private final ModemRepository modemRepository;

    private final ModemMapper modemMapper;

    public ModemService(ModemRepository modemRepository, ModemMapper modemMapper) {
        this.modemRepository = modemRepository;
        this.modemMapper = modemMapper;
    }

    /**
     * Save a modem.
     *
     * @param modemDTO the entity to save.
     * @return the persisted entity.
     */
    public ModemDTO save(ModemDTO modemDTO) {
        log.debug("Request to save Modem : {}", modemDTO);
        Modem modem = modemMapper.toEntity(modemDTO);
        modem = modemRepository.save(modem);
        return modemMapper.toDto(modem);
    }

    /**
     * Partially update a modem.
     *
     * @param modemDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ModemDTO> partialUpdate(ModemDTO modemDTO) {
        log.debug("Request to partially update Modem : {}", modemDTO);

        return modemRepository
            .findById(modemDTO.getId())
            .map(existingModem -> {
                modemMapper.partialUpdate(existingModem, modemDTO);

                return existingModem;
            })
            .map(modemRepository::save)
            .map(modemMapper::toDto);
    }

    /**
     * Get all the modems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ModemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Modems");
        return modemRepository.findAll(pageable).map(modemMapper::toDto);
    }

    /**
     * Get one modem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ModemDTO> findOne(Long id) {
        log.debug("Request to get Modem : {}", id);
        return modemRepository.findById(id).map(modemMapper::toDto);
    }

    /**
     * Delete the modem by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Modem : {}", id);
        modemRepository.deleteById(id);
    }
}
