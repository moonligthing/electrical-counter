package mn.electrical.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import mn.electrical.domain.*; // for static metamodels
import mn.electrical.domain.MeterReading;
import mn.electrical.repository.MeterReadingRepository;
import mn.electrical.service.criteria.MeterReadingCriteria;
import mn.electrical.service.dto.MeterReadingDTO;
import mn.electrical.service.mapper.MeterReadingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link MeterReading} entities in the database.
 * The main input is a {@link MeterReadingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MeterReadingDTO} or a {@link Page} of {@link MeterReadingDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MeterReadingQueryService extends QueryService<MeterReading> {

    private final Logger log = LoggerFactory.getLogger(MeterReadingQueryService.class);

    private final MeterReadingRepository meterReadingRepository;

    private final MeterReadingMapper meterReadingMapper;

    public MeterReadingQueryService(MeterReadingRepository meterReadingRepository, MeterReadingMapper meterReadingMapper) {
        this.meterReadingRepository = meterReadingRepository;
        this.meterReadingMapper = meterReadingMapper;
    }

    /**
     * Return a {@link List} of {@link MeterReadingDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MeterReadingDTO> findByCriteria(MeterReadingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<MeterReading> specification = createSpecification(criteria);
        return meterReadingMapper.toDto(meterReadingRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link MeterReadingDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MeterReadingDTO> findByCriteria(MeterReadingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<MeterReading> specification = createSpecification(criteria);
        return meterReadingRepository.findAll(specification, page).map(meterReadingMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MeterReadingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<MeterReading> specification = createSpecification(criteria);
        return meterReadingRepository.count(specification);
    }

    /**
     * Function to convert {@link MeterReadingCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<MeterReading> createSpecification(MeterReadingCriteria criteria) {
        Specification<MeterReading> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), MeterReading_.id));
            }
            if (criteria.getMeterName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMeterName(), MeterReading_.meterName));
            }
            if (criteria.getMeterNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMeterNumber(), MeterReading_.meterNumber));
            }
            if (criteria.getMeterAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMeterAddress(), MeterReading_.meterAddress));
            }
            if (criteria.getCycle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCycle(), MeterReading_.cycle));
            }
            if (criteria.getCtRatio() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCtRatio(), MeterReading_.ctRatio));
            }
            if (criteria.getVtRatio() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVtRatio(), MeterReading_.vtRatio));
            }
            if (criteria.getTotalImportActiveEnergy() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getTotalImportActiveEnergy(), MeterReading_.totalImportActiveEnergy)
                    );
            }
            if (criteria.getTotalExportActiveEnergy() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getTotalExportActiveEnergy(), MeterReading_.totalExportActiveEnergy)
                    );
            }
            if (criteria.getTotalImportReactiveEnergy() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getTotalImportReactiveEnergy(), MeterReading_.totalImportReactiveEnergy)
                    );
            }
            if (criteria.getTotalExportReactiveEnergy() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getTotalExportReactiveEnergy(), MeterReading_.totalExportReactiveEnergy)
                    );
            }
            if (criteria.getTotalImportApparentEnergy() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getTotalImportApparentEnergy(), MeterReading_.totalImportApparentEnergy)
                    );
            }
            if (criteria.getTotalExportApparentEnergy() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getTotalExportApparentEnergy(), MeterReading_.totalExportApparentEnergy)
                    );
            }
            if (criteria.getTotalInstantaneousActivePower() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getTotalInstantaneousActivePower(), MeterReading_.totalInstantaneousActivePower)
                    );
            }
            if (criteria.getTotalInstantaneousReactivePower() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(
                            criteria.getTotalInstantaneousReactivePower(),
                            MeterReading_.totalInstantaneousReactivePower
                        )
                    );
            }
            if (criteria.getTotalInstantaneousApparentPower() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(
                            criteria.getTotalInstantaneousApparentPower(),
                            MeterReading_.totalInstantaneousApparentPower
                        )
                    );
            }
            if (criteria.getTotalPowerFactor() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTotalPowerFactor(), MeterReading_.totalPowerFactor));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), MeterReading_.status));
            }
            if (criteria.getReadOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getReadOn(), MeterReading_.readOn));
            }
            if (criteria.getImportActiveEnergy1() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getImportActiveEnergy1(), MeterReading_.importActiveEnergy1));
            }
            if (criteria.getExportActiveEnergy1() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getExportActiveEnergy1(), MeterReading_.exportActiveEnergy1));
            }
            if (criteria.getImportReactiveEnergy1() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getImportReactiveEnergy1(), MeterReading_.importReactiveEnergy1));
            }
            if (criteria.getExportReactiveEnergy1() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getExportReactiveEnergy1(), MeterReading_.exportReactiveEnergy1));
            }
            if (criteria.getImportApparentEnergy1() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getImportApparentEnergy1(), MeterReading_.importApparentEnergy1));
            }
            if (criteria.getExportApparentEnergy1() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getExportApparentEnergy1(), MeterReading_.exportApparentEnergy1));
            }
            if (criteria.getImportActiveEnergy2() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getImportActiveEnergy2(), MeterReading_.importActiveEnergy2));
            }
            if (criteria.getExportActiveEnergy2() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getExportActiveEnergy2(), MeterReading_.exportActiveEnergy2));
            }
            if (criteria.getImportReactiveEnergy2() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getImportReactiveEnergy2(), MeterReading_.importReactiveEnergy2));
            }
            if (criteria.getExportReactiveEnergy2() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getExportReactiveEnergy2(), MeterReading_.exportReactiveEnergy2));
            }
            if (criteria.getImportApparentEnergy2() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getImportApparentEnergy2(), MeterReading_.importApparentEnergy2));
            }
            if (criteria.getExportApparentEnergy2() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getExportApparentEnergy2(), MeterReading_.exportApparentEnergy2));
            }
            if (criteria.getImportActiveEnergy3() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getImportActiveEnergy3(), MeterReading_.importActiveEnergy3));
            }
            if (criteria.getExportActiveEnergy3() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getExportActiveEnergy3(), MeterReading_.exportActiveEnergy3));
            }
            if (criteria.getImportReactiveEnergy3() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getImportReactiveEnergy3(), MeterReading_.importReactiveEnergy3));
            }
            if (criteria.getExportReactiveEnergy3() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getExportReactiveEnergy3(), MeterReading_.exportReactiveEnergy3));
            }
            if (criteria.getImportApparentEnergy3() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getImportApparentEnergy3(), MeterReading_.importApparentEnergy3));
            }
            if (criteria.getExportApparentEnergy3() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getExportApparentEnergy3(), MeterReading_.exportApparentEnergy3));
            }
            if (criteria.getVoltageA() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVoltageA(), MeterReading_.voltageA));
            }
            if (criteria.getCurrentA() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCurrentA(), MeterReading_.currentA));
            }
            if (criteria.getActivePowerA() != null) {
                specification = specification.and(buildStringSpecification(criteria.getActivePowerA(), MeterReading_.activePowerA));
            }
            if (criteria.getReactivePowerA() != null) {
                specification = specification.and(buildStringSpecification(criteria.getReactivePowerA(), MeterReading_.reactivePowerA));
            }
            if (criteria.getPowerFactorA() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPowerFactorA(), MeterReading_.powerFactorA));
            }
            if (criteria.getVoltageB() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVoltageB(), MeterReading_.voltageB));
            }
            if (criteria.getCurrentB() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCurrentB(), MeterReading_.currentB));
            }
            if (criteria.getActivePowerB() != null) {
                specification = specification.and(buildStringSpecification(criteria.getActivePowerB(), MeterReading_.activePowerB));
            }
            if (criteria.getReactivePowerB() != null) {
                specification = specification.and(buildStringSpecification(criteria.getReactivePowerB(), MeterReading_.reactivePowerB));
            }
            if (criteria.getPowerFactorB() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPowerFactorB(), MeterReading_.powerFactorB));
            }
            if (criteria.getVoltageC() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVoltageC(), MeterReading_.voltageC));
            }
            if (criteria.getCurrentC() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCurrentC(), MeterReading_.currentC));
            }
            if (criteria.getActivePowerC() != null) {
                specification = specification.and(buildStringSpecification(criteria.getActivePowerC(), MeterReading_.activePowerC));
            }
            if (criteria.getReactivePowerC() != null) {
                specification = specification.and(buildStringSpecification(criteria.getReactivePowerC(), MeterReading_.reactivePowerC));
            }
            if (criteria.getPowerFactorC() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPowerFactorC(), MeterReading_.powerFactorC));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), MeterReading_.createdBy));
            }
            if (criteria.getCreatedOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedOn(), MeterReading_.createdOn));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), MeterReading_.lastModifiedBy));
            }
            if (criteria.getLastModifiedOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedOn(), MeterReading_.lastModifiedOn));
            }
            if (criteria.getMeterId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getMeterId(), root -> root.join(MeterReading_.meter, JoinType.LEFT).get(Meter_.id))
                    );
            }
        }
        return specification;
    }
}
