package mn.electrical.service.mapper;

import mn.electrical.domain.*;
import mn.electrical.service.dto.MeterReadingDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link MeterReading} and its DTO {@link MeterReadingDTO}.
 */
@Mapper(componentModel = "spring", uses = { MeterMapper.class })
public interface MeterReadingMapper extends EntityMapper<MeterReadingDTO, MeterReading> {
    @Mapping(target = "meter", source = "meter", qualifiedByName = "id")
    MeterReadingDTO toDto(MeterReading s);
}
