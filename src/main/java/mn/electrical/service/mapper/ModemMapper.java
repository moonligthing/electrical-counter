package mn.electrical.service.mapper;

import mn.electrical.domain.*;
import mn.electrical.service.dto.ModemDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Modem} and its DTO {@link ModemDTO}.
 */
@Mapper(componentModel = "spring", uses = { CommonValueMapper.class })
public interface ModemMapper extends EntityMapper<ModemDTO, Modem> {
    @Mapping(target = "model", source = "model", qualifiedByName = "id")
    @Mapping(target = "protocolType", source = "protocolType", qualifiedByName = "id")
    ModemDTO toDto(Modem s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ModemDTO toDtoId(Modem modem);
}
