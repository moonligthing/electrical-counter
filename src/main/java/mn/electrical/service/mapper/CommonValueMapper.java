package mn.electrical.service.mapper;

import mn.electrical.domain.*;
import mn.electrical.service.dto.CommonValueDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link CommonValue} and its DTO {@link CommonValueDTO}.
 */
@Mapper(componentModel = "spring", uses = { CommonCodeMapper.class })
public interface CommonValueMapper extends EntityMapper<CommonValueDTO, CommonValue> {
    @Mapping(target = "parent", source = "parent", qualifiedByName = "code")
    CommonValueDTO toDto(CommonValue s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CommonValueDTO toDtoId(CommonValue commonValue);
}
