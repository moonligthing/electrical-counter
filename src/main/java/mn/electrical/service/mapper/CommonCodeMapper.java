package mn.electrical.service.mapper;

import mn.electrical.domain.*;
import mn.electrical.service.dto.CommonCodeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link CommonCode} and its DTO {@link CommonCodeDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CommonCodeMapper extends EntityMapper<CommonCodeDTO, CommonCode> {
    @Named("code")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "code", source = "code")
    CommonCodeDTO toDtoCode(CommonCode commonCode);
}
