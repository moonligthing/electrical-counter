package mn.electrical.service.mapper;

import mn.electrical.domain.*;
import mn.electrical.service.dto.MeterDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Meter} and its DTO {@link MeterDTO}.
 */
@Mapper(componentModel = "spring", uses = { ModemMapper.class, CommonValueMapper.class })
public interface MeterMapper extends EntityMapper<MeterDTO, Meter> {
    @Mapping(target = "modem", source = "modem", qualifiedByName = "id")
    @Mapping(target = "meterType", source = "meterType", qualifiedByName = "id")
    @Mapping(target = "current", source = "current", qualifiedByName = "id")
    @Mapping(target = "voltage", source = "voltage", qualifiedByName = "id")
    @Mapping(target = "model", source = "model", qualifiedByName = "id")
    MeterDTO toDto(Meter s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    MeterDTO toDtoId(Meter meter);
}
