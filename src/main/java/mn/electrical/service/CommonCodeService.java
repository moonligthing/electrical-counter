package mn.electrical.service;

import java.util.Optional;
import mn.electrical.domain.CommonCode;
import mn.electrical.repository.CommonCodeRepository;
import mn.electrical.service.dto.CommonCodeDTO;
import mn.electrical.service.mapper.CommonCodeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link CommonCode}.
 */
@Service
@Transactional
public class CommonCodeService {

    private final Logger log = LoggerFactory.getLogger(CommonCodeService.class);

    private final CommonCodeRepository commonCodeRepository;

    private final CommonCodeMapper commonCodeMapper;

    public CommonCodeService(CommonCodeRepository commonCodeRepository, CommonCodeMapper commonCodeMapper) {
        this.commonCodeRepository = commonCodeRepository;
        this.commonCodeMapper = commonCodeMapper;
    }

    /**
     * Save a commonCode.
     *
     * @param commonCodeDTO the entity to save.
     * @return the persisted entity.
     */
    public CommonCodeDTO save(CommonCodeDTO commonCodeDTO) {
        log.debug("Request to save CommonCode : {}", commonCodeDTO);
        CommonCode commonCode = commonCodeMapper.toEntity(commonCodeDTO);
        commonCode = commonCodeRepository.save(commonCode);
        return commonCodeMapper.toDto(commonCode);
    }

    /**
     * Partially update a commonCode.
     *
     * @param commonCodeDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<CommonCodeDTO> partialUpdate(CommonCodeDTO commonCodeDTO) {
        log.debug("Request to partially update CommonCode : {}", commonCodeDTO);

        return commonCodeRepository
            .findById(commonCodeDTO.getId())
            .map(existingCommonCode -> {
                commonCodeMapper.partialUpdate(existingCommonCode, commonCodeDTO);

                return existingCommonCode;
            })
            .map(commonCodeRepository::save)
            .map(commonCodeMapper::toDto);
    }

    /**
     * Get all the commonCodes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CommonCodeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CommonCodes");
        return commonCodeRepository.findAll(pageable).map(commonCodeMapper::toDto);
    }

    /**
     * Get one commonCode by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CommonCodeDTO> findOne(Long id) {
        log.debug("Request to get CommonCode : {}", id);
        return commonCodeRepository.findById(id).map(commonCodeMapper::toDto);
    }

    /**
     * Delete the commonCode by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CommonCode : {}", id);
        commonCodeRepository.deleteById(id);
    }
}
