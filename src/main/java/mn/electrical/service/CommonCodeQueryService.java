package mn.electrical.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import mn.electrical.domain.*; // for static metamodels
import mn.electrical.domain.CommonCode;
import mn.electrical.repository.CommonCodeRepository;
import mn.electrical.service.criteria.CommonCodeCriteria;
import mn.electrical.service.dto.CommonCodeDTO;
import mn.electrical.service.mapper.CommonCodeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link CommonCode} entities in the database.
 * The main input is a {@link CommonCodeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CommonCodeDTO} or a {@link Page} of {@link CommonCodeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CommonCodeQueryService extends QueryService<CommonCode> {

    private final Logger log = LoggerFactory.getLogger(CommonCodeQueryService.class);

    private final CommonCodeRepository commonCodeRepository;

    private final CommonCodeMapper commonCodeMapper;

    public CommonCodeQueryService(CommonCodeRepository commonCodeRepository, CommonCodeMapper commonCodeMapper) {
        this.commonCodeRepository = commonCodeRepository;
        this.commonCodeMapper = commonCodeMapper;
    }

    /**
     * Return a {@link List} of {@link CommonCodeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CommonCodeDTO> findByCriteria(CommonCodeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CommonCode> specification = createSpecification(criteria);
        return commonCodeMapper.toDto(commonCodeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CommonCodeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CommonCodeDTO> findByCriteria(CommonCodeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CommonCode> specification = createSpecification(criteria);
        return commonCodeRepository.findAll(specification, page).map(commonCodeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CommonCodeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CommonCode> specification = createSpecification(criteria);
        return commonCodeRepository.count(specification);
    }

    /**
     * Function to convert {@link CommonCodeCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CommonCode> createSpecification(CommonCodeCriteria criteria) {
        Specification<CommonCode> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), CommonCode_.id));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), CommonCode_.status));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), CommonCode_.name));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), CommonCode_.code));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), CommonCode_.description));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), CommonCode_.createdBy));
            }
            if (criteria.getCreatedOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedOn(), CommonCode_.createdOn));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), CommonCode_.lastModifiedBy));
            }
            if (criteria.getLastModifiedOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedOn(), CommonCode_.lastModifiedOn));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildSpecification(criteria.getIsDeleted(), CommonCode_.isDeleted));
            }
        }
        return specification;
    }
}
