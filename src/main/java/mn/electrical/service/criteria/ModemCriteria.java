package mn.electrical.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link mn.electrical.domain.Modem} entity. This class is used
 * in {@link mn.electrical.web.rest.ModemResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /modems?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ModemCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter modelName;

    private StringFilter protocolTypeName;

    private StringFilter ipAddress;

    private StringFilter port;

    private StringFilter name;

    private StringFilter address;

    private StringFilter location;

    private StringFilter simInfo;

    private StringFilter cronTab;

    private StringFilter createdBy;

    private InstantFilter createdOn;

    private StringFilter lastModifiedBy;

    private InstantFilter lastModifiedOn;

    private BooleanFilter isDeleted;

    private BooleanFilter flag;

    private LongFilter modelId;

    private LongFilter protocolTypeId;

    private Boolean distinct;

    public ModemCriteria() {}

    public ModemCriteria(ModemCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.modelName = other.modelName == null ? null : other.modelName.copy();
        this.protocolTypeName = other.protocolTypeName == null ? null : other.protocolTypeName.copy();
        this.ipAddress = other.ipAddress == null ? null : other.ipAddress.copy();
        this.port = other.port == null ? null : other.port.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.address = other.address == null ? null : other.address.copy();
        this.location = other.location == null ? null : other.location.copy();
        this.simInfo = other.simInfo == null ? null : other.simInfo.copy();
        this.cronTab = other.cronTab == null ? null : other.cronTab.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdOn = other.createdOn == null ? null : other.createdOn.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.lastModifiedOn = other.lastModifiedOn == null ? null : other.lastModifiedOn.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.flag = other.flag == null ? null : other.flag.copy();
        this.modelId = other.modelId == null ? null : other.modelId.copy();
        this.protocolTypeId = other.protocolTypeId == null ? null : other.protocolTypeId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public ModemCriteria copy() {
        return new ModemCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getModelName() {
        return modelName;
    }

    public StringFilter modelName() {
        if (modelName == null) {
            modelName = new StringFilter();
        }
        return modelName;
    }

    public void setModelName(StringFilter modelName) {
        this.modelName = modelName;
    }

    public StringFilter getProtocolTypeName() {
        return protocolTypeName;
    }

    public StringFilter protocolTypeName() {
        if (protocolTypeName == null) {
            protocolTypeName = new StringFilter();
        }
        return protocolTypeName;
    }

    public void setProtocolTypeName(StringFilter protocolTypeName) {
        this.protocolTypeName = protocolTypeName;
    }

    public StringFilter getIpAddress() {
        return ipAddress;
    }

    public StringFilter ipAddress() {
        if (ipAddress == null) {
            ipAddress = new StringFilter();
        }
        return ipAddress;
    }

    public void setIpAddress(StringFilter ipAddress) {
        this.ipAddress = ipAddress;
    }

    public StringFilter getPort() {
        return port;
    }

    public StringFilter port() {
        if (port == null) {
            port = new StringFilter();
        }
        return port;
    }

    public void setPort(StringFilter port) {
        this.port = port;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getAddress() {
        return address;
    }

    public StringFilter address() {
        if (address == null) {
            address = new StringFilter();
        }
        return address;
    }

    public void setAddress(StringFilter address) {
        this.address = address;
    }

    public StringFilter getLocation() {
        return location;
    }

    public StringFilter location() {
        if (location == null) {
            location = new StringFilter();
        }
        return location;
    }

    public void setLocation(StringFilter location) {
        this.location = location;
    }

    public StringFilter getSimInfo() {
        return simInfo;
    }

    public StringFilter simInfo() {
        if (simInfo == null) {
            simInfo = new StringFilter();
        }
        return simInfo;
    }

    public void setSimInfo(StringFilter simInfo) {
        this.simInfo = simInfo;
    }

    public StringFilter getCronTab() {
        return cronTab;
    }

    public StringFilter cronTab() {
        if (cronTab == null) {
            cronTab = new StringFilter();
        }
        return cronTab;
    }

    public void setCronTab(StringFilter cronTab) {
        this.cronTab = cronTab;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public StringFilter createdBy() {
        if (createdBy == null) {
            createdBy = new StringFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getCreatedOn() {
        return createdOn;
    }

    public InstantFilter createdOn() {
        if (createdOn == null) {
            createdOn = new InstantFilter();
        }
        return createdOn;
    }

    public void setCreatedOn(InstantFilter createdOn) {
        this.createdOn = createdOn;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public StringFilter lastModifiedBy() {
        if (lastModifiedBy == null) {
            lastModifiedBy = new StringFilter();
        }
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public InstantFilter getLastModifiedOn() {
        return lastModifiedOn;
    }

    public InstantFilter lastModifiedOn() {
        if (lastModifiedOn == null) {
            lastModifiedOn = new InstantFilter();
        }
        return lastModifiedOn;
    }

    public void setLastModifiedOn(InstantFilter lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public BooleanFilter getIsDeleted() {
        return isDeleted;
    }

    public BooleanFilter isDeleted() {
        if (isDeleted == null) {
            isDeleted = new BooleanFilter();
        }
        return isDeleted;
    }

    public void setIsDeleted(BooleanFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public BooleanFilter getFlag() {
        return flag;
    }

    public BooleanFilter flag() {
        if (flag == null) {
            flag = new BooleanFilter();
        }
        return flag;
    }

    public void setFlag(BooleanFilter flag) {
        this.flag = flag;
    }

    public LongFilter getModelId() {
        return modelId;
    }

    public LongFilter modelId() {
        if (modelId == null) {
            modelId = new LongFilter();
        }
        return modelId;
    }

    public void setModelId(LongFilter modelId) {
        this.modelId = modelId;
    }

    public LongFilter getProtocolTypeId() {
        return protocolTypeId;
    }

    public LongFilter protocolTypeId() {
        if (protocolTypeId == null) {
            protocolTypeId = new LongFilter();
        }
        return protocolTypeId;
    }

    public void setProtocolTypeId(LongFilter protocolTypeId) {
        this.protocolTypeId = protocolTypeId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ModemCriteria that = (ModemCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(modelName, that.modelName) &&
            Objects.equals(protocolTypeName, that.protocolTypeName) &&
            Objects.equals(ipAddress, that.ipAddress) &&
            Objects.equals(port, that.port) &&
            Objects.equals(name, that.name) &&
            Objects.equals(address, that.address) &&
            Objects.equals(location, that.location) &&
            Objects.equals(simInfo, that.simInfo) &&
            Objects.equals(cronTab, that.cronTab) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdOn, that.createdOn) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(lastModifiedOn, that.lastModifiedOn) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(flag, that.flag) &&
            Objects.equals(modelId, that.modelId) &&
            Objects.equals(protocolTypeId, that.protocolTypeId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            modelName,
            protocolTypeName,
            ipAddress,
            port,
            name,
            address,
            location,
            simInfo,
            cronTab,
            createdBy,
            createdOn,
            lastModifiedBy,
            lastModifiedOn,
            isDeleted,
            flag,
            modelId,
            protocolTypeId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ModemCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (modelName != null ? "modelName=" + modelName + ", " : "") +
            (protocolTypeName != null ? "protocolTypeName=" + protocolTypeName + ", " : "") +
            (ipAddress != null ? "ipAddress=" + ipAddress + ", " : "") +
            (port != null ? "port=" + port + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (address != null ? "address=" + address + ", " : "") +
            (location != null ? "location=" + location + ", " : "") +
            (simInfo != null ? "simInfo=" + simInfo + ", " : "") +
            (cronTab != null ? "cronTab=" + cronTab + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (createdOn != null ? "createdOn=" + createdOn + ", " : "") +
            (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
            (lastModifiedOn != null ? "lastModifiedOn=" + lastModifiedOn + ", " : "") +
            (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
            (flag != null ? "flag=" + flag + ", " : "") +
            (modelId != null ? "modelId=" + modelId + ", " : "") +
            (protocolTypeId != null ? "protocolTypeId=" + protocolTypeId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
