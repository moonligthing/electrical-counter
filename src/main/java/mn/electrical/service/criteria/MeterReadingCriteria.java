package mn.electrical.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link mn.electrical.domain.MeterReading} entity. This class is used
 * in {@link mn.electrical.web.rest.MeterReadingResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /meter-readings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class MeterReadingCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter meterName;

    private StringFilter meterNumber;

    private StringFilter meterAddress;

    private StringFilter cycle;

    private StringFilter ctRatio;

    private StringFilter vtRatio;

    private StringFilter totalImportActiveEnergy;

    private StringFilter totalExportActiveEnergy;

    private StringFilter totalImportReactiveEnergy;

    private StringFilter totalExportReactiveEnergy;

    private StringFilter totalImportApparentEnergy;

    private StringFilter totalExportApparentEnergy;

    private StringFilter totalInstantaneousActivePower;

    private StringFilter totalInstantaneousReactivePower;

    private StringFilter totalInstantaneousApparentPower;

    private StringFilter totalPowerFactor;

    private StringFilter status;

    private InstantFilter readOn;

    private StringFilter importActiveEnergy1;

    private StringFilter exportActiveEnergy1;

    private StringFilter importReactiveEnergy1;

    private StringFilter exportReactiveEnergy1;

    private StringFilter importApparentEnergy1;

    private StringFilter exportApparentEnergy1;

    private StringFilter importActiveEnergy2;

    private StringFilter exportActiveEnergy2;

    private StringFilter importReactiveEnergy2;

    private StringFilter exportReactiveEnergy2;

    private StringFilter importApparentEnergy2;

    private StringFilter exportApparentEnergy2;

    private StringFilter importActiveEnergy3;

    private StringFilter exportActiveEnergy3;

    private StringFilter importReactiveEnergy3;

    private StringFilter exportReactiveEnergy3;

    private StringFilter importApparentEnergy3;

    private StringFilter exportApparentEnergy3;

    private StringFilter voltageA;

    private StringFilter currentA;

    private StringFilter activePowerA;

    private StringFilter reactivePowerA;

    private StringFilter powerFactorA;

    private StringFilter voltageB;

    private StringFilter currentB;

    private StringFilter activePowerB;

    private StringFilter reactivePowerB;

    private StringFilter powerFactorB;

    private StringFilter voltageC;

    private StringFilter currentC;

    private StringFilter activePowerC;

    private StringFilter reactivePowerC;

    private StringFilter powerFactorC;

    private StringFilter createdBy;

    private InstantFilter createdOn;

    private StringFilter lastModifiedBy;

    private InstantFilter lastModifiedOn;

    private LongFilter meterId;

    private Boolean distinct;

    public MeterReadingCriteria() {}

    public MeterReadingCriteria(MeterReadingCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.meterName = other.meterName == null ? null : other.meterName.copy();
        this.meterNumber = other.meterNumber == null ? null : other.meterNumber.copy();
        this.meterAddress = other.meterAddress == null ? null : other.meterAddress.copy();
        this.cycle = other.cycle == null ? null : other.cycle.copy();
        this.ctRatio = other.ctRatio == null ? null : other.ctRatio.copy();
        this.vtRatio = other.vtRatio == null ? null : other.vtRatio.copy();
        this.totalImportActiveEnergy = other.totalImportActiveEnergy == null ? null : other.totalImportActiveEnergy.copy();
        this.totalExportActiveEnergy = other.totalExportActiveEnergy == null ? null : other.totalExportActiveEnergy.copy();
        this.totalImportReactiveEnergy = other.totalImportReactiveEnergy == null ? null : other.totalImportReactiveEnergy.copy();
        this.totalExportReactiveEnergy = other.totalExportReactiveEnergy == null ? null : other.totalExportReactiveEnergy.copy();
        this.totalImportApparentEnergy = other.totalImportApparentEnergy == null ? null : other.totalImportApparentEnergy.copy();
        this.totalExportApparentEnergy = other.totalExportApparentEnergy == null ? null : other.totalExportApparentEnergy.copy();
        this.totalInstantaneousActivePower =
            other.totalInstantaneousActivePower == null ? null : other.totalInstantaneousActivePower.copy();
        this.totalInstantaneousReactivePower =
            other.totalInstantaneousReactivePower == null ? null : other.totalInstantaneousReactivePower.copy();
        this.totalInstantaneousApparentPower =
            other.totalInstantaneousApparentPower == null ? null : other.totalInstantaneousApparentPower.copy();
        this.totalPowerFactor = other.totalPowerFactor == null ? null : other.totalPowerFactor.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.readOn = other.readOn == null ? null : other.readOn.copy();
        this.importActiveEnergy1 = other.importActiveEnergy1 == null ? null : other.importActiveEnergy1.copy();
        this.exportActiveEnergy1 = other.exportActiveEnergy1 == null ? null : other.exportActiveEnergy1.copy();
        this.importReactiveEnergy1 = other.importReactiveEnergy1 == null ? null : other.importReactiveEnergy1.copy();
        this.exportReactiveEnergy1 = other.exportReactiveEnergy1 == null ? null : other.exportReactiveEnergy1.copy();
        this.importApparentEnergy1 = other.importApparentEnergy1 == null ? null : other.importApparentEnergy1.copy();
        this.exportApparentEnergy1 = other.exportApparentEnergy1 == null ? null : other.exportApparentEnergy1.copy();
        this.importActiveEnergy2 = other.importActiveEnergy2 == null ? null : other.importActiveEnergy2.copy();
        this.exportActiveEnergy2 = other.exportActiveEnergy2 == null ? null : other.exportActiveEnergy2.copy();
        this.importReactiveEnergy2 = other.importReactiveEnergy2 == null ? null : other.importReactiveEnergy2.copy();
        this.exportReactiveEnergy2 = other.exportReactiveEnergy2 == null ? null : other.exportReactiveEnergy2.copy();
        this.importApparentEnergy2 = other.importApparentEnergy2 == null ? null : other.importApparentEnergy2.copy();
        this.exportApparentEnergy2 = other.exportApparentEnergy2 == null ? null : other.exportApparentEnergy2.copy();
        this.importActiveEnergy3 = other.importActiveEnergy3 == null ? null : other.importActiveEnergy3.copy();
        this.exportActiveEnergy3 = other.exportActiveEnergy3 == null ? null : other.exportActiveEnergy3.copy();
        this.importReactiveEnergy3 = other.importReactiveEnergy3 == null ? null : other.importReactiveEnergy3.copy();
        this.exportReactiveEnergy3 = other.exportReactiveEnergy3 == null ? null : other.exportReactiveEnergy3.copy();
        this.importApparentEnergy3 = other.importApparentEnergy3 == null ? null : other.importApparentEnergy3.copy();
        this.exportApparentEnergy3 = other.exportApparentEnergy3 == null ? null : other.exportApparentEnergy3.copy();
        this.voltageA = other.voltageA == null ? null : other.voltageA.copy();
        this.currentA = other.currentA == null ? null : other.currentA.copy();
        this.activePowerA = other.activePowerA == null ? null : other.activePowerA.copy();
        this.reactivePowerA = other.reactivePowerA == null ? null : other.reactivePowerA.copy();
        this.powerFactorA = other.powerFactorA == null ? null : other.powerFactorA.copy();
        this.voltageB = other.voltageB == null ? null : other.voltageB.copy();
        this.currentB = other.currentB == null ? null : other.currentB.copy();
        this.activePowerB = other.activePowerB == null ? null : other.activePowerB.copy();
        this.reactivePowerB = other.reactivePowerB == null ? null : other.reactivePowerB.copy();
        this.powerFactorB = other.powerFactorB == null ? null : other.powerFactorB.copy();
        this.voltageC = other.voltageC == null ? null : other.voltageC.copy();
        this.currentC = other.currentC == null ? null : other.currentC.copy();
        this.activePowerC = other.activePowerC == null ? null : other.activePowerC.copy();
        this.reactivePowerC = other.reactivePowerC == null ? null : other.reactivePowerC.copy();
        this.powerFactorC = other.powerFactorC == null ? null : other.powerFactorC.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdOn = other.createdOn == null ? null : other.createdOn.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.lastModifiedOn = other.lastModifiedOn == null ? null : other.lastModifiedOn.copy();
        this.meterId = other.meterId == null ? null : other.meterId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public MeterReadingCriteria copy() {
        return new MeterReadingCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getMeterName() {
        return meterName;
    }

    public StringFilter meterName() {
        if (meterName == null) {
            meterName = new StringFilter();
        }
        return meterName;
    }

    public void setMeterName(StringFilter meterName) {
        this.meterName = meterName;
    }

    public StringFilter getMeterNumber() {
        return meterNumber;
    }

    public StringFilter meterNumber() {
        if (meterNumber == null) {
            meterNumber = new StringFilter();
        }
        return meterNumber;
    }

    public void setMeterNumber(StringFilter meterNumber) {
        this.meterNumber = meterNumber;
    }

    public StringFilter getMeterAddress() {
        return meterAddress;
    }

    public StringFilter meterAddress() {
        if (meterAddress == null) {
            meterAddress = new StringFilter();
        }
        return meterAddress;
    }

    public void setMeterAddress(StringFilter meterAddress) {
        this.meterAddress = meterAddress;
    }

    public StringFilter getCycle() {
        return cycle;
    }

    public StringFilter cycle() {
        if (cycle == null) {
            cycle = new StringFilter();
        }
        return cycle;
    }

    public void setCycle(StringFilter cycle) {
        this.cycle = cycle;
    }

    public StringFilter getCtRatio() {
        return ctRatio;
    }

    public StringFilter ctRatio() {
        if (ctRatio == null) {
            ctRatio = new StringFilter();
        }
        return ctRatio;
    }

    public void setCtRatio(StringFilter ctRatio) {
        this.ctRatio = ctRatio;
    }

    public StringFilter getVtRatio() {
        return vtRatio;
    }

    public StringFilter vtRatio() {
        if (vtRatio == null) {
            vtRatio = new StringFilter();
        }
        return vtRatio;
    }

    public void setVtRatio(StringFilter vtRatio) {
        this.vtRatio = vtRatio;
    }

    public StringFilter getTotalImportActiveEnergy() {
        return totalImportActiveEnergy;
    }

    public StringFilter totalImportActiveEnergy() {
        if (totalImportActiveEnergy == null) {
            totalImportActiveEnergy = new StringFilter();
        }
        return totalImportActiveEnergy;
    }

    public void setTotalImportActiveEnergy(StringFilter totalImportActiveEnergy) {
        this.totalImportActiveEnergy = totalImportActiveEnergy;
    }

    public StringFilter getTotalExportActiveEnergy() {
        return totalExportActiveEnergy;
    }

    public StringFilter totalExportActiveEnergy() {
        if (totalExportActiveEnergy == null) {
            totalExportActiveEnergy = new StringFilter();
        }
        return totalExportActiveEnergy;
    }

    public void setTotalExportActiveEnergy(StringFilter totalExportActiveEnergy) {
        this.totalExportActiveEnergy = totalExportActiveEnergy;
    }

    public StringFilter getTotalImportReactiveEnergy() {
        return totalImportReactiveEnergy;
    }

    public StringFilter totalImportReactiveEnergy() {
        if (totalImportReactiveEnergy == null) {
            totalImportReactiveEnergy = new StringFilter();
        }
        return totalImportReactiveEnergy;
    }

    public void setTotalImportReactiveEnergy(StringFilter totalImportReactiveEnergy) {
        this.totalImportReactiveEnergy = totalImportReactiveEnergy;
    }

    public StringFilter getTotalExportReactiveEnergy() {
        return totalExportReactiveEnergy;
    }

    public StringFilter totalExportReactiveEnergy() {
        if (totalExportReactiveEnergy == null) {
            totalExportReactiveEnergy = new StringFilter();
        }
        return totalExportReactiveEnergy;
    }

    public void setTotalExportReactiveEnergy(StringFilter totalExportReactiveEnergy) {
        this.totalExportReactiveEnergy = totalExportReactiveEnergy;
    }

    public StringFilter getTotalImportApparentEnergy() {
        return totalImportApparentEnergy;
    }

    public StringFilter totalImportApparentEnergy() {
        if (totalImportApparentEnergy == null) {
            totalImportApparentEnergy = new StringFilter();
        }
        return totalImportApparentEnergy;
    }

    public void setTotalImportApparentEnergy(StringFilter totalImportApparentEnergy) {
        this.totalImportApparentEnergy = totalImportApparentEnergy;
    }

    public StringFilter getTotalExportApparentEnergy() {
        return totalExportApparentEnergy;
    }

    public StringFilter totalExportApparentEnergy() {
        if (totalExportApparentEnergy == null) {
            totalExportApparentEnergy = new StringFilter();
        }
        return totalExportApparentEnergy;
    }

    public void setTotalExportApparentEnergy(StringFilter totalExportApparentEnergy) {
        this.totalExportApparentEnergy = totalExportApparentEnergy;
    }

    public StringFilter getTotalInstantaneousActivePower() {
        return totalInstantaneousActivePower;
    }

    public StringFilter totalInstantaneousActivePower() {
        if (totalInstantaneousActivePower == null) {
            totalInstantaneousActivePower = new StringFilter();
        }
        return totalInstantaneousActivePower;
    }

    public void setTotalInstantaneousActivePower(StringFilter totalInstantaneousActivePower) {
        this.totalInstantaneousActivePower = totalInstantaneousActivePower;
    }

    public StringFilter getTotalInstantaneousReactivePower() {
        return totalInstantaneousReactivePower;
    }

    public StringFilter totalInstantaneousReactivePower() {
        if (totalInstantaneousReactivePower == null) {
            totalInstantaneousReactivePower = new StringFilter();
        }
        return totalInstantaneousReactivePower;
    }

    public void setTotalInstantaneousReactivePower(StringFilter totalInstantaneousReactivePower) {
        this.totalInstantaneousReactivePower = totalInstantaneousReactivePower;
    }

    public StringFilter getTotalInstantaneousApparentPower() {
        return totalInstantaneousApparentPower;
    }

    public StringFilter totalInstantaneousApparentPower() {
        if (totalInstantaneousApparentPower == null) {
            totalInstantaneousApparentPower = new StringFilter();
        }
        return totalInstantaneousApparentPower;
    }

    public void setTotalInstantaneousApparentPower(StringFilter totalInstantaneousApparentPower) {
        this.totalInstantaneousApparentPower = totalInstantaneousApparentPower;
    }

    public StringFilter getTotalPowerFactor() {
        return totalPowerFactor;
    }

    public StringFilter totalPowerFactor() {
        if (totalPowerFactor == null) {
            totalPowerFactor = new StringFilter();
        }
        return totalPowerFactor;
    }

    public void setTotalPowerFactor(StringFilter totalPowerFactor) {
        this.totalPowerFactor = totalPowerFactor;
    }

    public StringFilter getStatus() {
        return status;
    }

    public StringFilter status() {
        if (status == null) {
            status = new StringFilter();
        }
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public InstantFilter getReadOn() {
        return readOn;
    }

    public InstantFilter readOn() {
        if (readOn == null) {
            readOn = new InstantFilter();
        }
        return readOn;
    }

    public void setReadOn(InstantFilter readOn) {
        this.readOn = readOn;
    }

    public StringFilter getImportActiveEnergy1() {
        return importActiveEnergy1;
    }

    public StringFilter importActiveEnergy1() {
        if (importActiveEnergy1 == null) {
            importActiveEnergy1 = new StringFilter();
        }
        return importActiveEnergy1;
    }

    public void setImportActiveEnergy1(StringFilter importActiveEnergy1) {
        this.importActiveEnergy1 = importActiveEnergy1;
    }

    public StringFilter getExportActiveEnergy1() {
        return exportActiveEnergy1;
    }

    public StringFilter exportActiveEnergy1() {
        if (exportActiveEnergy1 == null) {
            exportActiveEnergy1 = new StringFilter();
        }
        return exportActiveEnergy1;
    }

    public void setExportActiveEnergy1(StringFilter exportActiveEnergy1) {
        this.exportActiveEnergy1 = exportActiveEnergy1;
    }

    public StringFilter getImportReactiveEnergy1() {
        return importReactiveEnergy1;
    }

    public StringFilter importReactiveEnergy1() {
        if (importReactiveEnergy1 == null) {
            importReactiveEnergy1 = new StringFilter();
        }
        return importReactiveEnergy1;
    }

    public void setImportReactiveEnergy1(StringFilter importReactiveEnergy1) {
        this.importReactiveEnergy1 = importReactiveEnergy1;
    }

    public StringFilter getExportReactiveEnergy1() {
        return exportReactiveEnergy1;
    }

    public StringFilter exportReactiveEnergy1() {
        if (exportReactiveEnergy1 == null) {
            exportReactiveEnergy1 = new StringFilter();
        }
        return exportReactiveEnergy1;
    }

    public void setExportReactiveEnergy1(StringFilter exportReactiveEnergy1) {
        this.exportReactiveEnergy1 = exportReactiveEnergy1;
    }

    public StringFilter getImportApparentEnergy1() {
        return importApparentEnergy1;
    }

    public StringFilter importApparentEnergy1() {
        if (importApparentEnergy1 == null) {
            importApparentEnergy1 = new StringFilter();
        }
        return importApparentEnergy1;
    }

    public void setImportApparentEnergy1(StringFilter importApparentEnergy1) {
        this.importApparentEnergy1 = importApparentEnergy1;
    }

    public StringFilter getExportApparentEnergy1() {
        return exportApparentEnergy1;
    }

    public StringFilter exportApparentEnergy1() {
        if (exportApparentEnergy1 == null) {
            exportApparentEnergy1 = new StringFilter();
        }
        return exportApparentEnergy1;
    }

    public void setExportApparentEnergy1(StringFilter exportApparentEnergy1) {
        this.exportApparentEnergy1 = exportApparentEnergy1;
    }

    public StringFilter getImportActiveEnergy2() {
        return importActiveEnergy2;
    }

    public StringFilter importActiveEnergy2() {
        if (importActiveEnergy2 == null) {
            importActiveEnergy2 = new StringFilter();
        }
        return importActiveEnergy2;
    }

    public void setImportActiveEnergy2(StringFilter importActiveEnergy2) {
        this.importActiveEnergy2 = importActiveEnergy2;
    }

    public StringFilter getExportActiveEnergy2() {
        return exportActiveEnergy2;
    }

    public StringFilter exportActiveEnergy2() {
        if (exportActiveEnergy2 == null) {
            exportActiveEnergy2 = new StringFilter();
        }
        return exportActiveEnergy2;
    }

    public void setExportActiveEnergy2(StringFilter exportActiveEnergy2) {
        this.exportActiveEnergy2 = exportActiveEnergy2;
    }

    public StringFilter getImportReactiveEnergy2() {
        return importReactiveEnergy2;
    }

    public StringFilter importReactiveEnergy2() {
        if (importReactiveEnergy2 == null) {
            importReactiveEnergy2 = new StringFilter();
        }
        return importReactiveEnergy2;
    }

    public void setImportReactiveEnergy2(StringFilter importReactiveEnergy2) {
        this.importReactiveEnergy2 = importReactiveEnergy2;
    }

    public StringFilter getExportReactiveEnergy2() {
        return exportReactiveEnergy2;
    }

    public StringFilter exportReactiveEnergy2() {
        if (exportReactiveEnergy2 == null) {
            exportReactiveEnergy2 = new StringFilter();
        }
        return exportReactiveEnergy2;
    }

    public void setExportReactiveEnergy2(StringFilter exportReactiveEnergy2) {
        this.exportReactiveEnergy2 = exportReactiveEnergy2;
    }

    public StringFilter getImportApparentEnergy2() {
        return importApparentEnergy2;
    }

    public StringFilter importApparentEnergy2() {
        if (importApparentEnergy2 == null) {
            importApparentEnergy2 = new StringFilter();
        }
        return importApparentEnergy2;
    }

    public void setImportApparentEnergy2(StringFilter importApparentEnergy2) {
        this.importApparentEnergy2 = importApparentEnergy2;
    }

    public StringFilter getExportApparentEnergy2() {
        return exportApparentEnergy2;
    }

    public StringFilter exportApparentEnergy2() {
        if (exportApparentEnergy2 == null) {
            exportApparentEnergy2 = new StringFilter();
        }
        return exportApparentEnergy2;
    }

    public void setExportApparentEnergy2(StringFilter exportApparentEnergy2) {
        this.exportApparentEnergy2 = exportApparentEnergy2;
    }

    public StringFilter getImportActiveEnergy3() {
        return importActiveEnergy3;
    }

    public StringFilter importActiveEnergy3() {
        if (importActiveEnergy3 == null) {
            importActiveEnergy3 = new StringFilter();
        }
        return importActiveEnergy3;
    }

    public void setImportActiveEnergy3(StringFilter importActiveEnergy3) {
        this.importActiveEnergy3 = importActiveEnergy3;
    }

    public StringFilter getExportActiveEnergy3() {
        return exportActiveEnergy3;
    }

    public StringFilter exportActiveEnergy3() {
        if (exportActiveEnergy3 == null) {
            exportActiveEnergy3 = new StringFilter();
        }
        return exportActiveEnergy3;
    }

    public void setExportActiveEnergy3(StringFilter exportActiveEnergy3) {
        this.exportActiveEnergy3 = exportActiveEnergy3;
    }

    public StringFilter getImportReactiveEnergy3() {
        return importReactiveEnergy3;
    }

    public StringFilter importReactiveEnergy3() {
        if (importReactiveEnergy3 == null) {
            importReactiveEnergy3 = new StringFilter();
        }
        return importReactiveEnergy3;
    }

    public void setImportReactiveEnergy3(StringFilter importReactiveEnergy3) {
        this.importReactiveEnergy3 = importReactiveEnergy3;
    }

    public StringFilter getExportReactiveEnergy3() {
        return exportReactiveEnergy3;
    }

    public StringFilter exportReactiveEnergy3() {
        if (exportReactiveEnergy3 == null) {
            exportReactiveEnergy3 = new StringFilter();
        }
        return exportReactiveEnergy3;
    }

    public void setExportReactiveEnergy3(StringFilter exportReactiveEnergy3) {
        this.exportReactiveEnergy3 = exportReactiveEnergy3;
    }

    public StringFilter getImportApparentEnergy3() {
        return importApparentEnergy3;
    }

    public StringFilter importApparentEnergy3() {
        if (importApparentEnergy3 == null) {
            importApparentEnergy3 = new StringFilter();
        }
        return importApparentEnergy3;
    }

    public void setImportApparentEnergy3(StringFilter importApparentEnergy3) {
        this.importApparentEnergy3 = importApparentEnergy3;
    }

    public StringFilter getExportApparentEnergy3() {
        return exportApparentEnergy3;
    }

    public StringFilter exportApparentEnergy3() {
        if (exportApparentEnergy3 == null) {
            exportApparentEnergy3 = new StringFilter();
        }
        return exportApparentEnergy3;
    }

    public void setExportApparentEnergy3(StringFilter exportApparentEnergy3) {
        this.exportApparentEnergy3 = exportApparentEnergy3;
    }

    public StringFilter getVoltageA() {
        return voltageA;
    }

    public StringFilter voltageA() {
        if (voltageA == null) {
            voltageA = new StringFilter();
        }
        return voltageA;
    }

    public void setVoltageA(StringFilter voltageA) {
        this.voltageA = voltageA;
    }

    public StringFilter getCurrentA() {
        return currentA;
    }

    public StringFilter currentA() {
        if (currentA == null) {
            currentA = new StringFilter();
        }
        return currentA;
    }

    public void setCurrentA(StringFilter currentA) {
        this.currentA = currentA;
    }

    public StringFilter getActivePowerA() {
        return activePowerA;
    }

    public StringFilter activePowerA() {
        if (activePowerA == null) {
            activePowerA = new StringFilter();
        }
        return activePowerA;
    }

    public void setActivePowerA(StringFilter activePowerA) {
        this.activePowerA = activePowerA;
    }

    public StringFilter getReactivePowerA() {
        return reactivePowerA;
    }

    public StringFilter reactivePowerA() {
        if (reactivePowerA == null) {
            reactivePowerA = new StringFilter();
        }
        return reactivePowerA;
    }

    public void setReactivePowerA(StringFilter reactivePowerA) {
        this.reactivePowerA = reactivePowerA;
    }

    public StringFilter getPowerFactorA() {
        return powerFactorA;
    }

    public StringFilter powerFactorA() {
        if (powerFactorA == null) {
            powerFactorA = new StringFilter();
        }
        return powerFactorA;
    }

    public void setPowerFactorA(StringFilter powerFactorA) {
        this.powerFactorA = powerFactorA;
    }

    public StringFilter getVoltageB() {
        return voltageB;
    }

    public StringFilter voltageB() {
        if (voltageB == null) {
            voltageB = new StringFilter();
        }
        return voltageB;
    }

    public void setVoltageB(StringFilter voltageB) {
        this.voltageB = voltageB;
    }

    public StringFilter getCurrentB() {
        return currentB;
    }

    public StringFilter currentB() {
        if (currentB == null) {
            currentB = new StringFilter();
        }
        return currentB;
    }

    public void setCurrentB(StringFilter currentB) {
        this.currentB = currentB;
    }

    public StringFilter getActivePowerB() {
        return activePowerB;
    }

    public StringFilter activePowerB() {
        if (activePowerB == null) {
            activePowerB = new StringFilter();
        }
        return activePowerB;
    }

    public void setActivePowerB(StringFilter activePowerB) {
        this.activePowerB = activePowerB;
    }

    public StringFilter getReactivePowerB() {
        return reactivePowerB;
    }

    public StringFilter reactivePowerB() {
        if (reactivePowerB == null) {
            reactivePowerB = new StringFilter();
        }
        return reactivePowerB;
    }

    public void setReactivePowerB(StringFilter reactivePowerB) {
        this.reactivePowerB = reactivePowerB;
    }

    public StringFilter getPowerFactorB() {
        return powerFactorB;
    }

    public StringFilter powerFactorB() {
        if (powerFactorB == null) {
            powerFactorB = new StringFilter();
        }
        return powerFactorB;
    }

    public void setPowerFactorB(StringFilter powerFactorB) {
        this.powerFactorB = powerFactorB;
    }

    public StringFilter getVoltageC() {
        return voltageC;
    }

    public StringFilter voltageC() {
        if (voltageC == null) {
            voltageC = new StringFilter();
        }
        return voltageC;
    }

    public void setVoltageC(StringFilter voltageC) {
        this.voltageC = voltageC;
    }

    public StringFilter getCurrentC() {
        return currentC;
    }

    public StringFilter currentC() {
        if (currentC == null) {
            currentC = new StringFilter();
        }
        return currentC;
    }

    public void setCurrentC(StringFilter currentC) {
        this.currentC = currentC;
    }

    public StringFilter getActivePowerC() {
        return activePowerC;
    }

    public StringFilter activePowerC() {
        if (activePowerC == null) {
            activePowerC = new StringFilter();
        }
        return activePowerC;
    }

    public void setActivePowerC(StringFilter activePowerC) {
        this.activePowerC = activePowerC;
    }

    public StringFilter getReactivePowerC() {
        return reactivePowerC;
    }

    public StringFilter reactivePowerC() {
        if (reactivePowerC == null) {
            reactivePowerC = new StringFilter();
        }
        return reactivePowerC;
    }

    public void setReactivePowerC(StringFilter reactivePowerC) {
        this.reactivePowerC = reactivePowerC;
    }

    public StringFilter getPowerFactorC() {
        return powerFactorC;
    }

    public StringFilter powerFactorC() {
        if (powerFactorC == null) {
            powerFactorC = new StringFilter();
        }
        return powerFactorC;
    }

    public void setPowerFactorC(StringFilter powerFactorC) {
        this.powerFactorC = powerFactorC;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public StringFilter createdBy() {
        if (createdBy == null) {
            createdBy = new StringFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getCreatedOn() {
        return createdOn;
    }

    public InstantFilter createdOn() {
        if (createdOn == null) {
            createdOn = new InstantFilter();
        }
        return createdOn;
    }

    public void setCreatedOn(InstantFilter createdOn) {
        this.createdOn = createdOn;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public StringFilter lastModifiedBy() {
        if (lastModifiedBy == null) {
            lastModifiedBy = new StringFilter();
        }
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public InstantFilter getLastModifiedOn() {
        return lastModifiedOn;
    }

    public InstantFilter lastModifiedOn() {
        if (lastModifiedOn == null) {
            lastModifiedOn = new InstantFilter();
        }
        return lastModifiedOn;
    }

    public void setLastModifiedOn(InstantFilter lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public LongFilter getMeterId() {
        return meterId;
    }

    public LongFilter meterId() {
        if (meterId == null) {
            meterId = new LongFilter();
        }
        return meterId;
    }

    public void setMeterId(LongFilter meterId) {
        this.meterId = meterId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MeterReadingCriteria that = (MeterReadingCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(meterName, that.meterName) &&
            Objects.equals(meterNumber, that.meterNumber) &&
            Objects.equals(meterAddress, that.meterAddress) &&
            Objects.equals(cycle, that.cycle) &&
            Objects.equals(ctRatio, that.ctRatio) &&
            Objects.equals(vtRatio, that.vtRatio) &&
            Objects.equals(totalImportActiveEnergy, that.totalImportActiveEnergy) &&
            Objects.equals(totalExportActiveEnergy, that.totalExportActiveEnergy) &&
            Objects.equals(totalImportReactiveEnergy, that.totalImportReactiveEnergy) &&
            Objects.equals(totalExportReactiveEnergy, that.totalExportReactiveEnergy) &&
            Objects.equals(totalImportApparentEnergy, that.totalImportApparentEnergy) &&
            Objects.equals(totalExportApparentEnergy, that.totalExportApparentEnergy) &&
            Objects.equals(totalInstantaneousActivePower, that.totalInstantaneousActivePower) &&
            Objects.equals(totalInstantaneousReactivePower, that.totalInstantaneousReactivePower) &&
            Objects.equals(totalInstantaneousApparentPower, that.totalInstantaneousApparentPower) &&
            Objects.equals(totalPowerFactor, that.totalPowerFactor) &&
            Objects.equals(status, that.status) &&
            Objects.equals(readOn, that.readOn) &&
            Objects.equals(importActiveEnergy1, that.importActiveEnergy1) &&
            Objects.equals(exportActiveEnergy1, that.exportActiveEnergy1) &&
            Objects.equals(importReactiveEnergy1, that.importReactiveEnergy1) &&
            Objects.equals(exportReactiveEnergy1, that.exportReactiveEnergy1) &&
            Objects.equals(importApparentEnergy1, that.importApparentEnergy1) &&
            Objects.equals(exportApparentEnergy1, that.exportApparentEnergy1) &&
            Objects.equals(importActiveEnergy2, that.importActiveEnergy2) &&
            Objects.equals(exportActiveEnergy2, that.exportActiveEnergy2) &&
            Objects.equals(importReactiveEnergy2, that.importReactiveEnergy2) &&
            Objects.equals(exportReactiveEnergy2, that.exportReactiveEnergy2) &&
            Objects.equals(importApparentEnergy2, that.importApparentEnergy2) &&
            Objects.equals(exportApparentEnergy2, that.exportApparentEnergy2) &&
            Objects.equals(importActiveEnergy3, that.importActiveEnergy3) &&
            Objects.equals(exportActiveEnergy3, that.exportActiveEnergy3) &&
            Objects.equals(importReactiveEnergy3, that.importReactiveEnergy3) &&
            Objects.equals(exportReactiveEnergy3, that.exportReactiveEnergy3) &&
            Objects.equals(importApparentEnergy3, that.importApparentEnergy3) &&
            Objects.equals(exportApparentEnergy3, that.exportApparentEnergy3) &&
            Objects.equals(voltageA, that.voltageA) &&
            Objects.equals(currentA, that.currentA) &&
            Objects.equals(activePowerA, that.activePowerA) &&
            Objects.equals(reactivePowerA, that.reactivePowerA) &&
            Objects.equals(powerFactorA, that.powerFactorA) &&
            Objects.equals(voltageB, that.voltageB) &&
            Objects.equals(currentB, that.currentB) &&
            Objects.equals(activePowerB, that.activePowerB) &&
            Objects.equals(reactivePowerB, that.reactivePowerB) &&
            Objects.equals(powerFactorB, that.powerFactorB) &&
            Objects.equals(voltageC, that.voltageC) &&
            Objects.equals(currentC, that.currentC) &&
            Objects.equals(activePowerC, that.activePowerC) &&
            Objects.equals(reactivePowerC, that.reactivePowerC) &&
            Objects.equals(powerFactorC, that.powerFactorC) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdOn, that.createdOn) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(lastModifiedOn, that.lastModifiedOn) &&
            Objects.equals(meterId, that.meterId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            meterName,
            meterNumber,
            meterAddress,
            cycle,
            ctRatio,
            vtRatio,
            totalImportActiveEnergy,
            totalExportActiveEnergy,
            totalImportReactiveEnergy,
            totalExportReactiveEnergy,
            totalImportApparentEnergy,
            totalExportApparentEnergy,
            totalInstantaneousActivePower,
            totalInstantaneousReactivePower,
            totalInstantaneousApparentPower,
            totalPowerFactor,
            status,
            readOn,
            importActiveEnergy1,
            exportActiveEnergy1,
            importReactiveEnergy1,
            exportReactiveEnergy1,
            importApparentEnergy1,
            exportApparentEnergy1,
            importActiveEnergy2,
            exportActiveEnergy2,
            importReactiveEnergy2,
            exportReactiveEnergy2,
            importApparentEnergy2,
            exportApparentEnergy2,
            importActiveEnergy3,
            exportActiveEnergy3,
            importReactiveEnergy3,
            exportReactiveEnergy3,
            importApparentEnergy3,
            exportApparentEnergy3,
            voltageA,
            currentA,
            activePowerA,
            reactivePowerA,
            powerFactorA,
            voltageB,
            currentB,
            activePowerB,
            reactivePowerB,
            powerFactorB,
            voltageC,
            currentC,
            activePowerC,
            reactivePowerC,
            powerFactorC,
            createdBy,
            createdOn,
            lastModifiedBy,
            lastModifiedOn,
            meterId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MeterReadingCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (meterName != null ? "meterName=" + meterName + ", " : "") +
            (meterNumber != null ? "meterNumber=" + meterNumber + ", " : "") +
            (meterAddress != null ? "meterAddress=" + meterAddress + ", " : "") +
            (cycle != null ? "cycle=" + cycle + ", " : "") +
            (ctRatio != null ? "ctRatio=" + ctRatio + ", " : "") +
            (vtRatio != null ? "vtRatio=" + vtRatio + ", " : "") +
            (totalImportActiveEnergy != null ? "totalImportActiveEnergy=" + totalImportActiveEnergy + ", " : "") +
            (totalExportActiveEnergy != null ? "totalExportActiveEnergy=" + totalExportActiveEnergy + ", " : "") +
            (totalImportReactiveEnergy != null ? "totalImportReactiveEnergy=" + totalImportReactiveEnergy + ", " : "") +
            (totalExportReactiveEnergy != null ? "totalExportReactiveEnergy=" + totalExportReactiveEnergy + ", " : "") +
            (totalImportApparentEnergy != null ? "totalImportApparentEnergy=" + totalImportApparentEnergy + ", " : "") +
            (totalExportApparentEnergy != null ? "totalExportApparentEnergy=" + totalExportApparentEnergy + ", " : "") +
            (totalInstantaneousActivePower != null ? "totalInstantaneousActivePower=" + totalInstantaneousActivePower + ", " : "") +
            (totalInstantaneousReactivePower != null ? "totalInstantaneousReactivePower=" + totalInstantaneousReactivePower + ", " : "") +
            (totalInstantaneousApparentPower != null ? "totalInstantaneousApparentPower=" + totalInstantaneousApparentPower + ", " : "") +
            (totalPowerFactor != null ? "totalPowerFactor=" + totalPowerFactor + ", " : "") +
            (status != null ? "status=" + status + ", " : "") +
            (readOn != null ? "readOn=" + readOn + ", " : "") +
            (importActiveEnergy1 != null ? "importActiveEnergy1=" + importActiveEnergy1 + ", " : "") +
            (exportActiveEnergy1 != null ? "exportActiveEnergy1=" + exportActiveEnergy1 + ", " : "") +
            (importReactiveEnergy1 != null ? "importReactiveEnergy1=" + importReactiveEnergy1 + ", " : "") +
            (exportReactiveEnergy1 != null ? "exportReactiveEnergy1=" + exportReactiveEnergy1 + ", " : "") +
            (importApparentEnergy1 != null ? "importApparentEnergy1=" + importApparentEnergy1 + ", " : "") +
            (exportApparentEnergy1 != null ? "exportApparentEnergy1=" + exportApparentEnergy1 + ", " : "") +
            (importActiveEnergy2 != null ? "importActiveEnergy2=" + importActiveEnergy2 + ", " : "") +
            (exportActiveEnergy2 != null ? "exportActiveEnergy2=" + exportActiveEnergy2 + ", " : "") +
            (importReactiveEnergy2 != null ? "importReactiveEnergy2=" + importReactiveEnergy2 + ", " : "") +
            (exportReactiveEnergy2 != null ? "exportReactiveEnergy2=" + exportReactiveEnergy2 + ", " : "") +
            (importApparentEnergy2 != null ? "importApparentEnergy2=" + importApparentEnergy2 + ", " : "") +
            (exportApparentEnergy2 != null ? "exportApparentEnergy2=" + exportApparentEnergy2 + ", " : "") +
            (importActiveEnergy3 != null ? "importActiveEnergy3=" + importActiveEnergy3 + ", " : "") +
            (exportActiveEnergy3 != null ? "exportActiveEnergy3=" + exportActiveEnergy3 + ", " : "") +
            (importReactiveEnergy3 != null ? "importReactiveEnergy3=" + importReactiveEnergy3 + ", " : "") +
            (exportReactiveEnergy3 != null ? "exportReactiveEnergy3=" + exportReactiveEnergy3 + ", " : "") +
            (importApparentEnergy3 != null ? "importApparentEnergy3=" + importApparentEnergy3 + ", " : "") +
            (exportApparentEnergy3 != null ? "exportApparentEnergy3=" + exportApparentEnergy3 + ", " : "") +
            (voltageA != null ? "voltageA=" + voltageA + ", " : "") +
            (currentA != null ? "currentA=" + currentA + ", " : "") +
            (activePowerA != null ? "activePowerA=" + activePowerA + ", " : "") +
            (reactivePowerA != null ? "reactivePowerA=" + reactivePowerA + ", " : "") +
            (powerFactorA != null ? "powerFactorA=" + powerFactorA + ", " : "") +
            (voltageB != null ? "voltageB=" + voltageB + ", " : "") +
            (currentB != null ? "currentB=" + currentB + ", " : "") +
            (activePowerB != null ? "activePowerB=" + activePowerB + ", " : "") +
            (reactivePowerB != null ? "reactivePowerB=" + reactivePowerB + ", " : "") +
            (powerFactorB != null ? "powerFactorB=" + powerFactorB + ", " : "") +
            (voltageC != null ? "voltageC=" + voltageC + ", " : "") +
            (currentC != null ? "currentC=" + currentC + ", " : "") +
            (activePowerC != null ? "activePowerC=" + activePowerC + ", " : "") +
            (reactivePowerC != null ? "reactivePowerC=" + reactivePowerC + ", " : "") +
            (powerFactorC != null ? "powerFactorC=" + powerFactorC + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (createdOn != null ? "createdOn=" + createdOn + ", " : "") +
            (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
            (lastModifiedOn != null ? "lastModifiedOn=" + lastModifiedOn + ", " : "") +
            (meterId != null ? "meterId=" + meterId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
