package mn.electrical.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link mn.electrical.domain.CommonValue} entity. This class is used
 * in {@link mn.electrical.web.rest.CommonValueResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /common-values?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CommonValueCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter code;

    private StringFilter description;

    private LongFilter order;

    private StringFilter dataType;

    private StringFilter dataString;

    private StringFilter status;

    private StringFilter dataShort;

    private StringFilter createdBy;

    private InstantFilter createdOn;

    private StringFilter lastModifiedBy;

    private InstantFilter lastModifiedOn;

    private BooleanFilter isDeleted;

    private LongFilter parentId;

    private Boolean distinct;

    public CommonValueCriteria() {}

    public CommonValueCriteria(CommonValueCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.order = other.order == null ? null : other.order.copy();
        this.dataType = other.dataType == null ? null : other.dataType.copy();
        this.dataString = other.dataString == null ? null : other.dataString.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.dataShort = other.dataShort == null ? null : other.dataShort.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdOn = other.createdOn == null ? null : other.createdOn.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.lastModifiedOn = other.lastModifiedOn == null ? null : other.lastModifiedOn.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.parentId = other.parentId == null ? null : other.parentId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public CommonValueCriteria copy() {
        return new CommonValueCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getCode() {
        return code;
    }

    public StringFilter code() {
        if (code == null) {
            code = new StringFilter();
        }
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public StringFilter getDescription() {
        return description;
    }

    public StringFilter description() {
        if (description == null) {
            description = new StringFilter();
        }
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public LongFilter getOrder() {
        return order;
    }

    public LongFilter order() {
        if (order == null) {
            order = new LongFilter();
        }
        return order;
    }

    public void setOrder(LongFilter order) {
        this.order = order;
    }

    public StringFilter getDataType() {
        return dataType;
    }

    public StringFilter dataType() {
        if (dataType == null) {
            dataType = new StringFilter();
        }
        return dataType;
    }

    public void setDataType(StringFilter dataType) {
        this.dataType = dataType;
    }

    public StringFilter getDataString() {
        return dataString;
    }

    public StringFilter dataString() {
        if (dataString == null) {
            dataString = new StringFilter();
        }
        return dataString;
    }

    public void setDataString(StringFilter dataString) {
        this.dataString = dataString;
    }

    public StringFilter getStatus() {
        return status;
    }

    public StringFilter status() {
        if (status == null) {
            status = new StringFilter();
        }
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public StringFilter getDataShort() {
        return dataShort;
    }

    public StringFilter dataShort() {
        if (dataShort == null) {
            dataShort = new StringFilter();
        }
        return dataShort;
    }

    public void setDataShort(StringFilter dataShort) {
        this.dataShort = dataShort;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public StringFilter createdBy() {
        if (createdBy == null) {
            createdBy = new StringFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getCreatedOn() {
        return createdOn;
    }

    public InstantFilter createdOn() {
        if (createdOn == null) {
            createdOn = new InstantFilter();
        }
        return createdOn;
    }

    public void setCreatedOn(InstantFilter createdOn) {
        this.createdOn = createdOn;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public StringFilter lastModifiedBy() {
        if (lastModifiedBy == null) {
            lastModifiedBy = new StringFilter();
        }
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public InstantFilter getLastModifiedOn() {
        return lastModifiedOn;
    }

    public InstantFilter lastModifiedOn() {
        if (lastModifiedOn == null) {
            lastModifiedOn = new InstantFilter();
        }
        return lastModifiedOn;
    }

    public void setLastModifiedOn(InstantFilter lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public BooleanFilter getIsDeleted() {
        return isDeleted;
    }

    public BooleanFilter isDeleted() {
        if (isDeleted == null) {
            isDeleted = new BooleanFilter();
        }
        return isDeleted;
    }

    public void setIsDeleted(BooleanFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public LongFilter getParentId() {
        return parentId;
    }

    public LongFilter parentId() {
        if (parentId == null) {
            parentId = new LongFilter();
        }
        return parentId;
    }

    public void setParentId(LongFilter parentId) {
        this.parentId = parentId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CommonValueCriteria that = (CommonValueCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(code, that.code) &&
            Objects.equals(description, that.description) &&
            Objects.equals(order, that.order) &&
            Objects.equals(dataType, that.dataType) &&
            Objects.equals(dataString, that.dataString) &&
            Objects.equals(status, that.status) &&
            Objects.equals(dataShort, that.dataShort) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdOn, that.createdOn) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(lastModifiedOn, that.lastModifiedOn) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(parentId, that.parentId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            name,
            code,
            description,
            order,
            dataType,
            dataString,
            status,
            dataShort,
            createdBy,
            createdOn,
            lastModifiedBy,
            lastModifiedOn,
            isDeleted,
            parentId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommonValueCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (code != null ? "code=" + code + ", " : "") +
            (description != null ? "description=" + description + ", " : "") +
            (order != null ? "order=" + order + ", " : "") +
            (dataType != null ? "dataType=" + dataType + ", " : "") +
            (dataString != null ? "dataString=" + dataString + ", " : "") +
            (status != null ? "status=" + status + ", " : "") +
            (dataShort != null ? "dataShort=" + dataShort + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (createdOn != null ? "createdOn=" + createdOn + ", " : "") +
            (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
            (lastModifiedOn != null ? "lastModifiedOn=" + lastModifiedOn + ", " : "") +
            (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
            (parentId != null ? "parentId=" + parentId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
