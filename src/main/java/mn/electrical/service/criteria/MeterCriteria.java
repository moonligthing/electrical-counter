package mn.electrical.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link mn.electrical.domain.Meter} entity. This class is used
 * in {@link mn.electrical.web.rest.MeterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /meters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class MeterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter modemName;

    private StringFilter modelName;

    private StringFilter meterTypeName;

    private StringFilter currentName;

    private StringFilter voltageName;

    private InstantFilter installationOn;

    private InstantFilter warrantyOn;

    private StringFilter number;

    private StringFilter name;

    private StringFilter address;

    private StringFilter ctRatio;

    private StringFilter vtRatio;

    private StringFilter deviceAddress;

    private StringFilter status;

    private StringFilter rate1;

    private StringFilter rate2;

    private StringFilter rate3;

    private StringFilter createdBy;

    private InstantFilter createdOn;

    private StringFilter lastModifiedBy;

    private InstantFilter lastModifiedOn;

    private LongFilter modemId;

    private LongFilter meterTypeId;

    private LongFilter currentId;

    private LongFilter voltageId;

    private LongFilter modelId;

    private Boolean distinct;

    public MeterCriteria() {}

    public MeterCriteria(MeterCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.modemName = other.modemName == null ? null : other.modemName.copy();
        this.modelName = other.modelName == null ? null : other.modelName.copy();
        this.meterTypeName = other.meterTypeName == null ? null : other.meterTypeName.copy();
        this.currentName = other.currentName == null ? null : other.currentName.copy();
        this.voltageName = other.voltageName == null ? null : other.voltageName.copy();
        this.installationOn = other.installationOn == null ? null : other.installationOn.copy();
        this.warrantyOn = other.warrantyOn == null ? null : other.warrantyOn.copy();
        this.number = other.number == null ? null : other.number.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.address = other.address == null ? null : other.address.copy();
        this.ctRatio = other.ctRatio == null ? null : other.ctRatio.copy();
        this.vtRatio = other.vtRatio == null ? null : other.vtRatio.copy();
        this.deviceAddress = other.deviceAddress == null ? null : other.deviceAddress.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.rate1 = other.rate1 == null ? null : other.rate1.copy();
        this.rate2 = other.rate2 == null ? null : other.rate2.copy();
        this.rate3 = other.rate3 == null ? null : other.rate3.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdOn = other.createdOn == null ? null : other.createdOn.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.lastModifiedOn = other.lastModifiedOn == null ? null : other.lastModifiedOn.copy();
        this.modemId = other.modemId == null ? null : other.modemId.copy();
        this.meterTypeId = other.meterTypeId == null ? null : other.meterTypeId.copy();
        this.currentId = other.currentId == null ? null : other.currentId.copy();
        this.voltageId = other.voltageId == null ? null : other.voltageId.copy();
        this.modelId = other.modelId == null ? null : other.modelId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public MeterCriteria copy() {
        return new MeterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getModemName() {
        return modemName;
    }

    public StringFilter modemName() {
        if (modemName == null) {
            modemName = new StringFilter();
        }
        return modemName;
    }

    public void setModemName(StringFilter modemName) {
        this.modemName = modemName;
    }

    public StringFilter getModelName() {
        return modelName;
    }

    public StringFilter modelName() {
        if (modelName == null) {
            modelName = new StringFilter();
        }
        return modelName;
    }

    public void setModelName(StringFilter modelName) {
        this.modelName = modelName;
    }

    public StringFilter getMeterTypeName() {
        return meterTypeName;
    }

    public StringFilter meterTypeName() {
        if (meterTypeName == null) {
            meterTypeName = new StringFilter();
        }
        return meterTypeName;
    }

    public void setMeterTypeName(StringFilter meterTypeName) {
        this.meterTypeName = meterTypeName;
    }

    public StringFilter getCurrentName() {
        return currentName;
    }

    public StringFilter currentName() {
        if (currentName == null) {
            currentName = new StringFilter();
        }
        return currentName;
    }

    public void setCurrentName(StringFilter currentName) {
        this.currentName = currentName;
    }

    public StringFilter getVoltageName() {
        return voltageName;
    }

    public StringFilter voltageName() {
        if (voltageName == null) {
            voltageName = new StringFilter();
        }
        return voltageName;
    }

    public void setVoltageName(StringFilter voltageName) {
        this.voltageName = voltageName;
    }

    public InstantFilter getInstallationOn() {
        return installationOn;
    }

    public InstantFilter installationOn() {
        if (installationOn == null) {
            installationOn = new InstantFilter();
        }
        return installationOn;
    }

    public void setInstallationOn(InstantFilter installationOn) {
        this.installationOn = installationOn;
    }

    public InstantFilter getWarrantyOn() {
        return warrantyOn;
    }

    public InstantFilter warrantyOn() {
        if (warrantyOn == null) {
            warrantyOn = new InstantFilter();
        }
        return warrantyOn;
    }

    public void setWarrantyOn(InstantFilter warrantyOn) {
        this.warrantyOn = warrantyOn;
    }

    public StringFilter getNumber() {
        return number;
    }

    public StringFilter number() {
        if (number == null) {
            number = new StringFilter();
        }
        return number;
    }

    public void setNumber(StringFilter number) {
        this.number = number;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getAddress() {
        return address;
    }

    public StringFilter address() {
        if (address == null) {
            address = new StringFilter();
        }
        return address;
    }

    public void setAddress(StringFilter address) {
        this.address = address;
    }

    public StringFilter getCtRatio() {
        return ctRatio;
    }

    public StringFilter ctRatio() {
        if (ctRatio == null) {
            ctRatio = new StringFilter();
        }
        return ctRatio;
    }

    public void setCtRatio(StringFilter ctRatio) {
        this.ctRatio = ctRatio;
    }

    public StringFilter getVtRatio() {
        return vtRatio;
    }

    public StringFilter vtRatio() {
        if (vtRatio == null) {
            vtRatio = new StringFilter();
        }
        return vtRatio;
    }

    public void setVtRatio(StringFilter vtRatio) {
        this.vtRatio = vtRatio;
    }

    public StringFilter getDeviceAddress() {
        return deviceAddress;
    }

    public StringFilter deviceAddress() {
        if (deviceAddress == null) {
            deviceAddress = new StringFilter();
        }
        return deviceAddress;
    }

    public void setDeviceAddress(StringFilter deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    public StringFilter getStatus() {
        return status;
    }

    public StringFilter status() {
        if (status == null) {
            status = new StringFilter();
        }
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public StringFilter getRate1() {
        return rate1;
    }

    public StringFilter rate1() {
        if (rate1 == null) {
            rate1 = new StringFilter();
        }
        return rate1;
    }

    public void setRate1(StringFilter rate1) {
        this.rate1 = rate1;
    }

    public StringFilter getRate2() {
        return rate2;
    }

    public StringFilter rate2() {
        if (rate2 == null) {
            rate2 = new StringFilter();
        }
        return rate2;
    }

    public void setRate2(StringFilter rate2) {
        this.rate2 = rate2;
    }

    public StringFilter getRate3() {
        return rate3;
    }

    public StringFilter rate3() {
        if (rate3 == null) {
            rate3 = new StringFilter();
        }
        return rate3;
    }

    public void setRate3(StringFilter rate3) {
        this.rate3 = rate3;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public StringFilter createdBy() {
        if (createdBy == null) {
            createdBy = new StringFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getCreatedOn() {
        return createdOn;
    }

    public InstantFilter createdOn() {
        if (createdOn == null) {
            createdOn = new InstantFilter();
        }
        return createdOn;
    }

    public void setCreatedOn(InstantFilter createdOn) {
        this.createdOn = createdOn;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public StringFilter lastModifiedBy() {
        if (lastModifiedBy == null) {
            lastModifiedBy = new StringFilter();
        }
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public InstantFilter getLastModifiedOn() {
        return lastModifiedOn;
    }

    public InstantFilter lastModifiedOn() {
        if (lastModifiedOn == null) {
            lastModifiedOn = new InstantFilter();
        }
        return lastModifiedOn;
    }

    public void setLastModifiedOn(InstantFilter lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public LongFilter getModemId() {
        return modemId;
    }

    public LongFilter modemId() {
        if (modemId == null) {
            modemId = new LongFilter();
        }
        return modemId;
    }

    public void setModemId(LongFilter modemId) {
        this.modemId = modemId;
    }

    public LongFilter getMeterTypeId() {
        return meterTypeId;
    }

    public LongFilter meterTypeId() {
        if (meterTypeId == null) {
            meterTypeId = new LongFilter();
        }
        return meterTypeId;
    }

    public void setMeterTypeId(LongFilter meterTypeId) {
        this.meterTypeId = meterTypeId;
    }

    public LongFilter getCurrentId() {
        return currentId;
    }

    public LongFilter currentId() {
        if (currentId == null) {
            currentId = new LongFilter();
        }
        return currentId;
    }

    public void setCurrentId(LongFilter currentId) {
        this.currentId = currentId;
    }

    public LongFilter getVoltageId() {
        return voltageId;
    }

    public LongFilter voltageId() {
        if (voltageId == null) {
            voltageId = new LongFilter();
        }
        return voltageId;
    }

    public void setVoltageId(LongFilter voltageId) {
        this.voltageId = voltageId;
    }

    public LongFilter getModelId() {
        return modelId;
    }

    public LongFilter modelId() {
        if (modelId == null) {
            modelId = new LongFilter();
        }
        return modelId;
    }

    public void setModelId(LongFilter modelId) {
        this.modelId = modelId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MeterCriteria that = (MeterCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(modemName, that.modemName) &&
            Objects.equals(modelName, that.modelName) &&
            Objects.equals(meterTypeName, that.meterTypeName) &&
            Objects.equals(currentName, that.currentName) &&
            Objects.equals(voltageName, that.voltageName) &&
            Objects.equals(installationOn, that.installationOn) &&
            Objects.equals(warrantyOn, that.warrantyOn) &&
            Objects.equals(number, that.number) &&
            Objects.equals(name, that.name) &&
            Objects.equals(address, that.address) &&
            Objects.equals(ctRatio, that.ctRatio) &&
            Objects.equals(vtRatio, that.vtRatio) &&
            Objects.equals(deviceAddress, that.deviceAddress) &&
            Objects.equals(status, that.status) &&
            Objects.equals(rate1, that.rate1) &&
            Objects.equals(rate2, that.rate2) &&
            Objects.equals(rate3, that.rate3) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdOn, that.createdOn) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(lastModifiedOn, that.lastModifiedOn) &&
            Objects.equals(modemId, that.modemId) &&
            Objects.equals(meterTypeId, that.meterTypeId) &&
            Objects.equals(currentId, that.currentId) &&
            Objects.equals(voltageId, that.voltageId) &&
            Objects.equals(modelId, that.modelId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            modemName,
            modelName,
            meterTypeName,
            currentName,
            voltageName,
            installationOn,
            warrantyOn,
            number,
            name,
            address,
            ctRatio,
            vtRatio,
            deviceAddress,
            status,
            rate1,
            rate2,
            rate3,
            createdBy,
            createdOn,
            lastModifiedBy,
            lastModifiedOn,
            modemId,
            meterTypeId,
            currentId,
            voltageId,
            modelId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MeterCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (modemName != null ? "modemName=" + modemName + ", " : "") +
            (modelName != null ? "modelName=" + modelName + ", " : "") +
            (meterTypeName != null ? "meterTypeName=" + meterTypeName + ", " : "") +
            (currentName != null ? "currentName=" + currentName + ", " : "") +
            (voltageName != null ? "voltageName=" + voltageName + ", " : "") +
            (installationOn != null ? "installationOn=" + installationOn + ", " : "") +
            (warrantyOn != null ? "warrantyOn=" + warrantyOn + ", " : "") +
            (number != null ? "number=" + number + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (address != null ? "address=" + address + ", " : "") +
            (ctRatio != null ? "ctRatio=" + ctRatio + ", " : "") +
            (vtRatio != null ? "vtRatio=" + vtRatio + ", " : "") +
            (deviceAddress != null ? "deviceAddress=" + deviceAddress + ", " : "") +
            (status != null ? "status=" + status + ", " : "") +
            (rate1 != null ? "rate1=" + rate1 + ", " : "") +
            (rate2 != null ? "rate2=" + rate2 + ", " : "") +
            (rate3 != null ? "rate3=" + rate3 + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (createdOn != null ? "createdOn=" + createdOn + ", " : "") +
            (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
            (lastModifiedOn != null ? "lastModifiedOn=" + lastModifiedOn + ", " : "") +
            (modemId != null ? "modemId=" + modemId + ", " : "") +
            (meterTypeId != null ? "meterTypeId=" + meterTypeId + ", " : "") +
            (currentId != null ? "currentId=" + currentId + ", " : "") +
            (voltageId != null ? "voltageId=" + voltageId + ", " : "") +
            (modelId != null ? "modelId=" + modelId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
