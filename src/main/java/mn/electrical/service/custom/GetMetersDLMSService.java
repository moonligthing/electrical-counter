package mn.electrical.service.custom;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import mn.electrical.domain.CommonCode;
import mn.electrical.domain.CommonValue;
import mn.electrical.domain.Meter;
import mn.electrical.domain.Modem;
import mn.electrical.repository.CommonCodeRepository;
import mn.electrical.repository.CommonValueRepository;
import mn.electrical.repository.MeterRepository;
import mn.electrical.repository.ModemRepository;
import mn.electrical.service.MeterReadingService;
import mn.electrical.service.MeterService;
import mn.electrical.service.ModemService;
import mn.electrical.service.dto.MeterDTO;
import mn.electrical.service.dto.MeterReadingDTO;
import org.openmuc.jdlms.*;
import org.openmuc.jdlms.datatypes.BitString;
import org.openmuc.jdlms.datatypes.DataObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
//@EnableScheduling
public class GetMetersDLMSService {

    @Autowired
    private ModemRepository modemRepository;

    @Autowired
    private MeterRepository meterRepository;

    @Autowired
    private CommonValueRepository commonValueRepository;

    @Autowired
    private CommonCodeRepository commonCodeRepository;

    @Autowired
    private MeterReadingService meterReadingService;

    @Autowired
    private ModemService modemService;

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    private static final String BUG_FIXED = "Холболтын алдаа засагдсан";
    private static final String TOTAL_IMPORT_ACTIVE_ENERGY = "total_import_active_energy";
    private static final String TOTAL_EXPORT_ACTIVE_ENERGY = "total_export_active_energy";
    private static final String TOTAL_IMPORT_REACTIVE_ENERGY = "total_import_reactive_energy";
    private static final String TOTAL_EXPORT_REACTIVE_ENERGY = "total_export_reactive_energy";
    private static final String TOTAL_IMPORT_APPARENT_ENERGY = "total_import_apparent_energy";
    private static final String TOTAL_EXPORT_APPARENT_ENERGY = "total_export_apparent_energy";
    private static final String INSTANTANEOUS_ACTIVE_ENERGY = "total_instantaneous_active_power";
    private static final String INSTANTANEOUS_REACTIVE_ENERGY = "total_instantaneous_reactive_power";
    private static final String TOTAL_POWER_FACTOR = "total_power_factor";
    private static final String INSTANTANEOUS_APPARENT_POWER = "total_instantaneous_apparent_power";
    private static final String PHASEA_VOLTAGE = "phaseA_voltage";
    private static final String PHASEA_CURRENT = "phaseA_current";
    private static final String PHASEA_ACTIVE_POWER = "phaseA_active_power";
    private static final String PHASEA_REACTIVE_POWER = "phaseA_reactive_power";
    private static final String PHASEA_POWER_FACTOR = "phaseA_power_factor";
    private static final String PHASEB_VOLTAGE = "phaseB_voltage";
    private static final String PHASEB_CURRENT = "phaseB_current";
    private static final String PHASEB_ACTIVE_POWER = "phaseB_active_power";
    private static final String PHASEB_REACTIVE_POWER = "phaseB_reactive_power";
    private static final String PHASEB_POWER_FACTOR = "phaseB_power_factor";
    private static final String PHASEC_VOLTAGE = "phaseC_voltage";
    private static final String PHASEC_CURRENT = "phaseC_current";
    private static final String PHASEC_ACTIVE_POWER = "phaseC_active_power";
    private static final String PHASEC_REACTIVE_POWER = "phaseC_reactive_power";
    private static final String PHASEC_POWER_FACTOR = "phaseC_power_factor";
    private static final String RATE1_IMPORT_ACTIVE_ENERGY = "rate1_import_active_energy";
    private static final String RATE1_EXPORT_ACTIVE_ENERGY = "rate1_export_active_energy";
    private static final String RATE1_IMPORT_REACTIVE_ENERGY = "rate1_import_reactive_energy";
    private static final String RATE1_EXPORT_REACTIVE_ENERGY = "rate1_export_reactive_energy";
    private static final String RATE1_IMPORT_APPARENT_ENERGY = "rate1_import_apparent_energy";
    private static final String RATE1_EXPORT_APPARENT_ENERGY = "rate1_export_apparent_energy";
    private static final String RATE2_IMPORT_ACTIVE_ENERGY = "rate2_import_active_energy";
    private static final String RATE2_EXPORT_ACTIVE_ENERGY = "rate2_export_active_energy";
    private static final String RATE2_IMPORT_REACTIVE_ENERGY = "rate2_import_reactive_energy";
    private static final String RATE2_EXPORT_REACTIVE_ENERGY = "rate2_export_reactive_energy";
    private static final String RATE2_IMPORT_APPARENT_ENERGY = "rate2_import_apparent_energy";
    private static final String RATE2_EXPORT_APPARENT_ENERGY = "rate2_export_apparent_energy";
    private static final String RATE3_IMPORT_ACTIVE_ENERGY = "rate3_import_active_energy";
    private static final String RATE3_EXPORT_ACTIVE_ENERGY = "rate3_export_active_energy";
    private static final String RATE3_IMPORT_REACTIVE_ENERGY = "rate3_import_reactive_energy";
    private static final String RATE3_EXPORT_REACTIVE_ENERGY = "rate3_export_reactive_energy";
    private static final String RATE3_IMPORT_APPARENT_ENERGY = "rate3_import_apparent_energy";
    private static final String RATE3_EXPORT_APPARENT_ENERGY = "rate3_export_apparent_energy";

    private static final String FAILURE_CODE = "103";
    private static final String YES = "yes";
    private static final String NO = "no";
    private static final String OFF = "off";
    private static final String ON = "on";
    private static final String SUCCESS = "success";
    private static final String FAIL = "Тоолуурын системд бүртгэлгүй тоолуурын мэдээлэл байна!";
    private static final String ACTIVE = "active";
    private static final String INACTIVE = "inactive";
    //03->0002+1ea5
    //5A64->FCS
    private static final String command1 = "7E A007034193 5A64 7E";
    private static final String command2 =
        "7EA044 03 4110 B3E1 E6E6006036A1090607608574050801018A0207808B0760857405080201AC0A80083635373630323035BE10040E01000000065F1F0400007E1F04B0 D95D 7E";
    private static final String command3 = "7EA019 03 4132 3ABD E6E600C001C100010100000000FF0200 C3AD 7E";
    private static final String disconnectCommand = "7EA01B 03 4154 7C82 E6E600C301C10046000060030AFF01010F00 68A6 7E";
    private static final String connectCommand = "7EA01B 03 4154 7C82 E6E600C301C10046000060030AFF02010F00 A583 7E";
    private static final String command4 = "7EA007 03 4153 56A2 7E";
    private MeterService meterService;

    //    @Scheduled(cron = "0 0 * * * *")
    //    @Scheduled(cron = "* * * * * *")
    public void getMeterValues() {
        List<Modem> listModem = modemRepository.findAll();
        Instant counterDate = Instant.now();
        CommonCode model = commonCodeRepository.findByCode("get_values_obis_code");
        List<CommonValue> obisCodes = commonValueRepository.findAllByParentIdOrderByOrderAsc(model.getId());

        listModem
            .parallelStream()
            .forEach(modem -> {
                getValueJDLMS(modem, counterDate, obisCodes);
            });
    }

    private void getValueJDLMS(Modem modem, Instant counterDate, List<CommonValue> obisCodes) {
        String ipAddress = modem.getIpAddress();
        int port = Integer.parseInt(modem.getPort());

        InetAddress inetAddress = null;
        try {
            inetAddress = InetAddress.getByName(ipAddress);
        } catch (UnknownHostException e) {
            System.err.println("Unknown host error!");
        }
        List<Meter> listMeter = meterRepository.findAllByModemId(modem.getId());
        for (Meter meter : listMeter) {
            int deviceAddress = Integer.parseInt(meter.getDeviceAddress());
            TcpConnectionBuilder connectionBuilder = new TcpConnectionBuilder(inetAddress)
                .setPort(port)
                .setPhysicalDeviceAddress(deviceAddress)
                .useHdlc();
            // Connection build hiij baina
            try (DlmsConnection dlmsConnection = connectionBuilder.build()) {
                MeterReadingDTO meterReading = new MeterReadingDTO();

                // Obis code-uudiig parallel stream ashiglaad davtalt hiij baina
                obisCodes.parallelStream().forEach(obisCode -> saveValues(dlmsConnection, obisCode, meterReading));

                meterReading.setReadOn(counterDate);
                meterReading.setMeterName(meter.getNumber());
                meterReadingService.save(meterReading);
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void saveValues(DlmsConnection dlmsConnection, CommonValue type, MeterReadingDTO dto) {
        switch (type.getName()) {
            // Total
            case TOTAL_IMPORT_ACTIVE_ENERGY:
                dto.setTotalImportActiveEnergy(getMeterValue(dlmsConnection, type));
                break;
            case TOTAL_EXPORT_ACTIVE_ENERGY:
                dto.setTotalExportActiveEnergy(getMeterValue(dlmsConnection, type));
                break;
            case TOTAL_IMPORT_REACTIVE_ENERGY:
                dto.setTotalImportReactiveEnergy(getMeterValue(dlmsConnection, type));
                break;
            case TOTAL_EXPORT_REACTIVE_ENERGY:
                dto.setTotalExportReactiveEnergy(getMeterValue(dlmsConnection, type));
                break;
            case TOTAL_IMPORT_APPARENT_ENERGY:
                dto.setTotalImportApparentEnergy(getMeterValue(dlmsConnection, type));
                break;
            case TOTAL_EXPORT_APPARENT_ENERGY:
                dto.setTotalExportApparentEnergy(getMeterValue(dlmsConnection, type));
                break;
            case INSTANTANEOUS_ACTIVE_ENERGY:
                dto.setTotalInstantaneousActivePower(getMeterValue(dlmsConnection, type));
                break;
            case INSTANTANEOUS_REACTIVE_ENERGY:
                dto.setTotalInstantaneousReactivePower(getMeterValue(dlmsConnection, type));
                break;
            case TOTAL_POWER_FACTOR:
                dto.setTotalPowerFactor(getMeterValue(dlmsConnection, type));
                break;
            case INSTANTANEOUS_APPARENT_POWER:
                dto.setTotalInstantaneousApparentPower(getMeterValue(dlmsConnection, type));
                break;
            //PHASE a
            case PHASEA_VOLTAGE:
                dto.setVoltageA(getMeterPhaseValue(dlmsConnection, type));
                break;
            case PHASEA_CURRENT:
                dto.setCurrentA(getMeterPhaseValue(dlmsConnection, type));
                break;
            case PHASEA_ACTIVE_POWER:
                dto.setActivePowerA(getMeterPhaseValue(dlmsConnection, type));
                break;
            case PHASEA_REACTIVE_POWER:
                dto.setReactivePowerA(getMeterPhaseValue(dlmsConnection, type));
                break;
            case PHASEA_POWER_FACTOR:
                dto.setPowerFactorA(getMeterPhaseValue(dlmsConnection, type));
                break;
            //PHASE b
            case PHASEB_VOLTAGE:
                dto.setVoltageB(getMeterPhaseValue(dlmsConnection, type));
                break;
            case PHASEB_CURRENT:
                dto.setCurrentB(getMeterPhaseValue(dlmsConnection, type));
                break;
            case PHASEB_ACTIVE_POWER:
                dto.setActivePowerB(getMeterPhaseValue(dlmsConnection, type));
                break;
            case PHASEB_REACTIVE_POWER:
                dto.setReactivePowerB(getMeterPhaseValue(dlmsConnection, type));
                break;
            case PHASEB_POWER_FACTOR:
                dto.setPowerFactorB(getMeterPhaseValue(dlmsConnection, type));
                break;
            //PHASE c
            case PHASEC_VOLTAGE:
                dto.setVoltageC(getMeterPhaseValue(dlmsConnection, type));
                break;
            case PHASEC_CURRENT:
                dto.setCurrentC(getMeterPhaseValue(dlmsConnection, type));
                break;
            case PHASEC_ACTIVE_POWER:
                dto.setActivePowerC(getMeterPhaseValue(dlmsConnection, type));
                break;
            case PHASEC_REACTIVE_POWER:
                dto.setReactivePowerC(getMeterPhaseValue(dlmsConnection, type));
                break;
            case PHASEC_POWER_FACTOR:
                dto.setPowerFactorC(getMeterPhaseValue(dlmsConnection, type));
                break;
            //RATE 1
            case RATE1_IMPORT_ACTIVE_ENERGY:
                dto.setImportActiveEnergy1(getMeterValue(dlmsConnection, type));
                break;
            case RATE1_EXPORT_ACTIVE_ENERGY:
                dto.setExportActiveEnergy1(getMeterValue(dlmsConnection, type));
                break;
            case RATE1_IMPORT_REACTIVE_ENERGY:
                dto.setImportReactiveEnergy1(getMeterValue(dlmsConnection, type));
                break;
            case RATE1_EXPORT_REACTIVE_ENERGY:
                dto.setExportReactiveEnergy1(getMeterValue(dlmsConnection, type));
                break;
            case RATE1_IMPORT_APPARENT_ENERGY:
                dto.setImportApparentEnergy1(getMeterValue(dlmsConnection, type));
                break;
            case RATE1_EXPORT_APPARENT_ENERGY:
                dto.setExportApparentEnergy1(getMeterValue(dlmsConnection, type));
                break;
            //RATE 2
            case RATE2_IMPORT_ACTIVE_ENERGY:
                dto.setImportActiveEnergy2(getMeterValue(dlmsConnection, type));
                break;
            case RATE2_EXPORT_ACTIVE_ENERGY:
                dto.setExportActiveEnergy2(getMeterValue(dlmsConnection, type));
                break;
            case RATE2_IMPORT_REACTIVE_ENERGY:
                dto.setImportReactiveEnergy2(getMeterValue(dlmsConnection, type));
                break;
            case RATE2_EXPORT_REACTIVE_ENERGY:
                dto.setExportReactiveEnergy2(getMeterValue(dlmsConnection, type));
                break;
            case RATE2_IMPORT_APPARENT_ENERGY:
                dto.setImportApparentEnergy2(getMeterValue(dlmsConnection, type));
                break;
            case RATE2_EXPORT_APPARENT_ENERGY:
                dto.setExportApparentEnergy2(getMeterValue(dlmsConnection, type));
                break;
            //RATE 3
            case RATE3_IMPORT_ACTIVE_ENERGY:
                dto.setImportActiveEnergy3(getMeterValue(dlmsConnection, type));
                break;
            case RATE3_EXPORT_ACTIVE_ENERGY:
                dto.setExportActiveEnergy3(getMeterValue(dlmsConnection, type));
                break;
            case RATE3_IMPORT_REACTIVE_ENERGY:
                dto.setImportReactiveEnergy3(getMeterValue(dlmsConnection, type));
                break;
            case RATE3_EXPORT_REACTIVE_ENERGY:
                dto.setExportReactiveEnergy3(getMeterValue(dlmsConnection, type));
                break;
            case RATE3_IMPORT_APPARENT_ENERGY:
                dto.setImportApparentEnergy3(getMeterValue(dlmsConnection, type));
                break;
            case RATE3_EXPORT_APPARENT_ENERGY:
                dto.setExportApparentEnergy3(getMeterValue(dlmsConnection, type));
                break;
            default:
                break;
        }
    }

    private String getMeterPhaseValue(DlmsConnection dlmsConnection, CommonValue type) {
        try {
            GetResult result = dlmsConnection.get(
                new AttributeAddress(
                    Integer.parseInt(type.getDataShort()),
                    type.getCode() + ".255",
                    Integer.parseInt(type.getDescription())
                )
            );
            if (result.getResultCode() == AccessResultCode.SUCCESS) {
                DataObject resultData = result.getResultData();
                return String.valueOf(Double.parseDouble(resultData.getValue().toString()) / 100);
            }
        } catch (IOException e) {
            System.err.println("data fetch error!");
        }
        return "NaN";
    }

    private String getMeterValue(DlmsConnection dlmsConnection, CommonValue type) {
        try {
            GetResult result = dlmsConnection.get(
                new AttributeAddress(
                    Integer.parseInt(type.getDataShort()),
                    type.getCode() + ".255",
                    Integer.parseInt(type.getDescription())
                )
            );
            if (result.getResultCode() == AccessResultCode.SUCCESS) {
                DataObject resultData = result.getResultData();
                return String.valueOf(Double.parseDouble(resultData.getValue().toString()) / 1000);
            }
        } catch (IOException e) {
            System.err.println("data fetch error!");
        }
        return "NaN";
    }

    public String powerOnOff(Long id) {
        Meter meter = meterRepository.getById(id);
        Modem modem = meter.getModem();
        String ipAddress = modem.getIpAddress();
        int port = Integer.parseInt(modem.getPort());

        InetAddress inetAddress = null;
        try {
            inetAddress = InetAddress.getByName(ipAddress);
        } catch (UnknownHostException e) {
            System.err.println("Unknown host error!");
        }
        SecuritySuite securitySuite = SecuritySuite
            .builder()
            .setAuthenticationMechanism(AuthenticationMechanism.LOW)
            .setPassword("".getBytes())
            .build();

        TcpConnectionBuilder connectionBuilder = new TcpConnectionBuilder(inetAddress)
            .setPort(port)
            .setPhysicalDeviceAddress(Integer.parseInt(meter.getDeviceAddress()))
            .useHdlc()
            .setClientId(16)
            .setSecuritySuite(securitySuite);

        try (DlmsConnection dlmsConnection = connectionBuilder.build()) {
            int controlValue = meter.getStatus().equals("inactive") ? 2 : 1;
            DataObject controlData = DataObject.newInteger32Data(controlValue);
            AttributeAddress attributeAddress = new AttributeAddress(70, "0.0.96.3.10.255", 2);
            SetParameter setParameter = new SetParameter(attributeAddress, controlData);
            dlmsConnection.set(setParameter);

            meter.setStatus(controlValue == 2 ? "active" : "inactive");
            meterService.save(meter);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return SUCCESS;
    }
}
