package mn.electrical.service.custom;

import de.re.easymodbus.exceptions.ModbusException;
import de.re.easymodbus.modbusclient.ModbusClient;
import java.io.IOException;
import java.text.DecimalFormat;
import java.time.Instant;
import java.util.List;
import mn.electrical.domain.CommonCode;
import mn.electrical.domain.CommonValue;
import mn.electrical.domain.Meter;
import mn.electrical.domain.Modem;
import mn.electrical.repository.CommonCodeRepository;
import mn.electrical.repository.CommonValueRepository;
import mn.electrical.repository.MeterRepository;
import mn.electrical.repository.ModemRepository;
import mn.electrical.service.MeterReadingService;
import mn.electrical.service.dto.MeterReadingDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class ModBusCustomService {

    private final ModemRepository modemRepository;
    private final MeterRepository meterRepository;
    private final MeterReadingService meterReadingService;
    private final CommonValueRepository commonValueRepository;
    private final CommonCodeRepository commonCodeRepository;

    @Autowired
    public ModBusCustomService(
        ModemRepository modemRepository,
        MeterRepository meterRepository,
        MeterReadingService meterReadingService,
        CommonValueRepository commonValueRepository,
        CommonCodeRepository commonCodeRepository
    ) {
        this.modemRepository = modemRepository;
        this.meterRepository = meterRepository;
        this.meterReadingService = meterReadingService;
        this.commonValueRepository = commonValueRepository;
        this.commonCodeRepository = commonCodeRepository;
    }

    private static final DecimalFormat df = new DecimalFormat("#.###");
    private static final String MICROSTAR_REGADDRESS_TOTAL = "microStar_regAddress_total";
    private static final String MICROSTAR_REGADDRESS_PHASE = "microStar_regAddress_phase";
    private static final String MICROSTAR_REGADDRESS_RATE = "microStar_regAddress_rate";

    @Scheduled(cron = "0 0 */1 * * *")
    public void checkModemConnected() throws IOException, InterruptedException {
        List<Modem> listModem = modemRepository.findAllByProtocolTypeName("MODBUS");
        Instant newDate = Instant.now();
        for (Modem modem : listModem) {
            ModbusClient modbusClient = this.getConnection(modem.getIpAddress(), Integer.parseInt(modem.getPort()));
            if (modbusClient != null) {
                this.saveCounters(modem.getId(), modbusClient, newDate);
                modbusClient.Disconnect();
                Thread.sleep(1000);
            }
        }
    }

    private ModbusClient getConnection(String modemIp, int modemPort) {
        try {
            ModbusClient modbusClient = new ModbusClient(modemIp, modemPort);
            modbusClient.Connect();
            return modbusClient;
        } catch (IOException e) {
            return null;
        }
    }

    private void saveCounters(Long modemId, ModbusClient modbusClient, Instant newDate) {
        if (modbusClient.isConnected()) {
            List<Meter> listMeter = meterRepository.findAllByModemId(modemId);
            for (Meter meter : listMeter) {
                this.getTotalValues(modbusClient, meter, newDate);
            }
        }
    }

    private String getCounter(int deviceAddress, ModbusClient modbusClient, int regAddress) {
        modbusClient.setUnitIdentifier((byte) deviceAddress);
        try {
            int[] value = modbusClient.ReadHoldingRegisters(regAddress, 2);
            float regValue = ModbusClient.ConvertRegistersToFloat(value, ModbusClient.RegisterOrder.HighLow);
            return df.format(regValue);
        } catch (ModbusException | IOException ex) {
            return "";
        }
    }

    private void getTotalValues(ModbusClient modbusClient, Meter meter, Instant newDate) {
        CommonCode model = commonCodeRepository.findByCode(MICROSTAR_REGADDRESS_TOTAL);
        List<CommonValue> modelAddresses = commonValueRepository.findAllByParentIdOrderByOrderAsc(model.getId());
        MeterReadingDTO meterReading = new MeterReadingDTO();
        int devAddress = Integer.parseInt(meter.getDeviceAddress());
        for (CommonValue address : modelAddresses) {
            String meterValue = this.getCounter(devAddress, modbusClient, Integer.parseInt(address.getCode()));
            switch (address.getName()) {
                case "total_import_active_energy":
                    meterReading.setTotalImportActiveEnergy(meterValue);
                    break;
                case "total_export_active_energy":
                    meterReading.setTotalExportActiveEnergy(meterValue);
                    break;
                case "total_import_reactive_energy":
                    meterReading.setTotalImportReactiveEnergy(meterValue);
                    break;
                //                case "total_export_reactive_energy":
                //                    meterReading.setTotalExportReactiveEnergy(meterValue);
                //                    break;
                //                case "total_import_apparent_energy":
                //                    meterReading.setTotalImportApparentEnergy(meterValue);
                //                    break;
                //                case "total_export_apparent_energy":
                //                    meterReading.setTotalExportApparentEnergy(meterValue);
                //                    break;
                case "total_instantaneous_active_power":
                    meterReading.setTotalInstantaneousActivePower(meterValue);
                    break;
                case "total_instantaneous_reactive_power":
                    meterReading.setTotalInstantaneousReactivePower(meterValue);
                    break;
                case "total_power_factor":
                    meterReading.setTotalPowerFactor(meterValue);
                    break;
                //                case "total_instantaneous_apparent_power":
                //                    meterReading.setTotalInstantaneousApparentPower(meterValue);
                //                    break;
                default:
                    break;
            }
        }
        meterReading.setReadOn(newDate);
        meterReading.setMeterName(meter.getName());
        meterReading.setMeterNumber(meter.getNumber());
        meterReading.setMeterAddress(meter.getAddress());
        this.getPhaseValuesToMeter(modbusClient, meter.getDeviceAddress(), meterReading);
        this.getRateValuesToMeter(modbusClient, meter.getDeviceAddress(), meterReading);
        meterReadingService.save(meterReading);
    }

    private void getPhaseValuesToMeter(ModbusClient modbusClient, String deviceAddress, MeterReadingDTO dto) {
        CommonCode modelPhase = commonCodeRepository.findByCode(MICROSTAR_REGADDRESS_PHASE);
        List<CommonValue> modelPhaseAddresses = commonValueRepository.findAllByParentIdOrderByOrderAsc(modelPhase.getId());
        int devAddress = Integer.parseInt(deviceAddress);
        for (CommonValue address : modelPhaseAddresses) {
            String value = this.getCounter(devAddress, modbusClient, Integer.parseInt(address.getCode()));
            switch (address.getName()) {
                case "phaseA_voltage":
                    dto.setVoltageA(value);
                    break;
                case "phaseA_current":
                    dto.setCurrentA(value);
                    break;
                //                case "phaseA_active_power":
                //                    dto.setActivePowerA(value);
                //                    break;
                //                case "phaseA_reactive_power":
                //                    dto.setReactivePowerA(value);
                //                    break;
                //                case "phaseA_power_factor":
                //                    dto.setPowerFactorA(value);
                //                    break;
                case "phaseB_voltage":
                    dto.setVoltageB(value);
                    break;
                case "phaseB_current":
                    dto.setCurrentB(value);
                    break;
                //                case "phaseB_active_power":
                //                    dto.setActivePowerB(value);
                //                    break;
                //                case "phaseB_reactive_power":
                //                    dto.setReactivePowerB(value);
                //                    break;
                //                case "phaseB_power_factor":
                //                    dto.setPowerFactorB(value);
                //                    break;
                case "phaseC_voltage":
                    dto.setVoltageC(value);
                    break;
                case "phaseC_current":
                    dto.setCurrentC(value);
                    break;
                //                case "phaseC_active_power":
                //                    dto.setActivePowerC(value);
                //                    break;
                //                case "phaseC_reactive_power":
                //                    dto.setReactivePowerC(value);
                //                    break;
                //                case "phaseC_power_factor":
                //                    dto.setPowerFactorC(value);
                //                    break;
                default:
                    break;
            }
        }
    }

    private void getRateValuesToMeter(ModbusClient modbusClient, String deviceAddress, MeterReadingDTO dto) {
        CommonCode modelRate = commonCodeRepository.findByCode(MICROSTAR_REGADDRESS_RATE);
        List<CommonValue> modelRateAddresses = commonValueRepository.findAllByParentIdOrderByOrderAsc(modelRate.getId());
        int devAddress = Integer.parseInt(deviceAddress);
        for (CommonValue address : modelRateAddresses) {
            String value = this.getCounter(devAddress, modbusClient, Integer.parseInt(address.getCode()));
            switch (address.getName()) {
                case "rate1_import_active_energy":
                    dto.setImportActiveEnergy1(value);
                    break;
                //                case "rate1_export_active_energy":
                //                    dto.setExportActiveEnergy1(value);
                //                    break;
                //                case "rate1_import_reactive_energy":
                //                    dto.setImportReactiveEnergy1(value);
                //                    break;
                //                case "rate1_export_reactive_energy":
                //                    dto.setExportReactiveEnergy1(value);
                //                    break;
                //                case "rate1_import_apparent_energy":
                //                    dto.setImportApparentEnergy1(value);
                //                    break;
                //                case "rate1_export_apparent_energy":
                //                    dto.setExportApparentEnergy1(value);
                //                    break;

                case "rate2_import_active_energy":
                    dto.setImportActiveEnergy2(value);
                    break;
                //                case "rate2_export_active_energy":
                //                    dto.setExportActiveEnergy2(value);
                //                    break;
                //                case "rate2_import_reactive_energy":
                //                    dto.setImportReactiveEnergy2(value);
                //                    break;
                //                case "rate2_export_reactive_energy":
                //                    dto.setExportReactiveEnergy2(value);
                //                    break;
                //                case "rate2_import_apparent_energy":
                //                    dto.setImportApparentEnergy2(value);
                //                    break;
                //                case "rate2_export_apparent_energy":
                //                    dto.setExportApparentEnergy2(value);
                //                    break;
                case "rate3_import_active_energy":
                    dto.setImportActiveEnergy3(value);
                    break;
                //                case "rate3_export_active_energy":
                //                    dto.setExportActiveEnergy3(value);
                //                    break;
                //                case "rate3_import_reactive_energy":
                //                    dto.setImportReactiveEnergy3(value);
                //                    break;
                //                case "rate3_export_reactive_energy":
                //                    dto.setExportReactiveEnergy3(value);
                //                    break;
                //                case "rate3_import_apparent_energy":
                //                    dto.setImportApparentEnergy3(value);
                //                    break;
                //                case "rate3_export_apparent_energy":
                //                    dto.setExportApparentEnergy3(value);
                //                    break;
                default:
                    break;
            }
        }
    }
}
