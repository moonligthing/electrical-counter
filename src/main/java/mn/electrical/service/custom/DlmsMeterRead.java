//package mn.electrical.service.custom;
//import gurux.dlms.GXDLMSClient;
//import gurux.dlms.enums.Authentication;
//import gurux.dlms.enums.InterfaceType;
//import gurux.dlms.enums.ObjectType;
//import gurux.dlms.objects.GXDLMSObject;
//import gurux.dlms.GXReplyData;
//import gurux.io.BaudRate;
//import gurux.serial.GXSerial;
//
//public class MeterReader {
//
//    public static void main(String[] args) {
//        try {
//            // Create a DLMS client
//            GXDLMSClient client = new GXDLMSClient(true, 16, 1, Authentication.NONE, null, InterfaceType.HDLC); // Authentication.NONE means no authentication
//
//            // Set the logical name reference for the object you want to read (e.g., energy register)
//            String logicalName = "1.0.1.8.0.255";  // Replace with the correct logical name for your meter
//
//            // Define communication settings
//            String serialPort = "COM1"; // Adjust according to your system setup
//            int baudRate = 9600;        // Typical baud rate for communication
//
//            GXSerial serial = new GXSerial();
//            serial.setPortName(serialPort);
//            serial.setBaudRate(BaudRate.forValue(baudRate));
//            serial.open();
//
//            // Initialize the client communication
//            client.(serial);
//
//            // Read the specific DLMS object (e.g., register for active energy)
//            GXDLMSObject object = client.getObjects().findByLN(ObjectType.DATA, logicalName);
//
//            if (object != null) {
//                GXReplyData replyData = new GXReplyData();
//                client.read(object, 2, replyData);  // Read the object at position 2 (data value)
//                System.out.println("Meter value: " + replyData.getValue().toString());
//            }
//
//            // Close the connection
//            client.disconnect(true);
//            serial.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}
