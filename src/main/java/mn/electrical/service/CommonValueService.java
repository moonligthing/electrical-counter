package mn.electrical.service;

import java.util.Optional;
import mn.electrical.domain.CommonValue;
import mn.electrical.repository.CommonValueRepository;
import mn.electrical.service.dto.CommonValueDTO;
import mn.electrical.service.mapper.CommonValueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link CommonValue}.
 */
@Service
@Transactional
public class CommonValueService {

    private final Logger log = LoggerFactory.getLogger(CommonValueService.class);

    private final CommonValueRepository commonValueRepository;

    private final CommonValueMapper commonValueMapper;

    public CommonValueService(CommonValueRepository commonValueRepository, CommonValueMapper commonValueMapper) {
        this.commonValueRepository = commonValueRepository;
        this.commonValueMapper = commonValueMapper;
    }

    /**
     * Save a commonValue.
     *
     * @param commonValueDTO the entity to save.
     * @return the persisted entity.
     */
    public CommonValueDTO save(CommonValueDTO commonValueDTO) {
        log.debug("Request to save CommonValue : {}", commonValueDTO);
        CommonValue commonValue = commonValueMapper.toEntity(commonValueDTO);
        commonValue = commonValueRepository.save(commonValue);
        return commonValueMapper.toDto(commonValue);
    }

    /**
     * Partially update a commonValue.
     *
     * @param commonValueDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<CommonValueDTO> partialUpdate(CommonValueDTO commonValueDTO) {
        log.debug("Request to partially update CommonValue : {}", commonValueDTO);

        return commonValueRepository
            .findById(commonValueDTO.getId())
            .map(existingCommonValue -> {
                commonValueMapper.partialUpdate(existingCommonValue, commonValueDTO);

                return existingCommonValue;
            })
            .map(commonValueRepository::save)
            .map(commonValueMapper::toDto);
    }

    /**
     * Get all the commonValues.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CommonValueDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CommonValues");
        return commonValueRepository.findAll(pageable).map(commonValueMapper::toDto);
    }

    /**
     * Get one commonValue by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CommonValueDTO> findOne(Long id) {
        log.debug("Request to get CommonValue : {}", id);
        return commonValueRepository.findById(id).map(commonValueMapper::toDto);
    }

    /**
     * Delete the commonValue by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CommonValue : {}", id);
        commonValueRepository.deleteById(id);
    }
}
