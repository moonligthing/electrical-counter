package mn.electrical.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import mn.electrical.domain.*; // for static metamodels
import mn.electrical.domain.Modem;
import mn.electrical.repository.ModemRepository;
import mn.electrical.service.criteria.ModemCriteria;
import mn.electrical.service.dto.ModemDTO;
import mn.electrical.service.mapper.ModemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Modem} entities in the database.
 * The main input is a {@link ModemCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ModemDTO} or a {@link Page} of {@link ModemDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ModemQueryService extends QueryService<Modem> {

    private final Logger log = LoggerFactory.getLogger(ModemQueryService.class);

    private final ModemRepository modemRepository;

    private final ModemMapper modemMapper;

    public ModemQueryService(ModemRepository modemRepository, ModemMapper modemMapper) {
        this.modemRepository = modemRepository;
        this.modemMapper = modemMapper;
    }

    /**
     * Return a {@link List} of {@link ModemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ModemDTO> findByCriteria(ModemCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Modem> specification = createSpecification(criteria);
        return modemMapper.toDto(modemRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ModemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ModemDTO> findByCriteria(ModemCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Modem> specification = createSpecification(criteria);
        return modemRepository.findAll(specification, page).map(modemMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ModemCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Modem> specification = createSpecification(criteria);
        return modemRepository.count(specification);
    }

    /**
     * Function to convert {@link ModemCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Modem> createSpecification(ModemCriteria criteria) {
        Specification<Modem> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Modem_.id));
            }
            if (criteria.getModelName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getModelName(), Modem_.modelName));
            }
            if (criteria.getProtocolTypeName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProtocolTypeName(), Modem_.protocolTypeName));
            }
            if (criteria.getIpAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIpAddress(), Modem_.ipAddress));
            }
            if (criteria.getPort() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPort(), Modem_.port));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Modem_.name));
            }
            if (criteria.getAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress(), Modem_.address));
            }
            if (criteria.getLocation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLocation(), Modem_.location));
            }
            if (criteria.getSimInfo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSimInfo(), Modem_.simInfo));
            }
            if (criteria.getCronTab() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCronTab(), Modem_.cronTab));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), Modem_.createdBy));
            }
            if (criteria.getCreatedOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedOn(), Modem_.createdOn));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), Modem_.lastModifiedBy));
            }
            if (criteria.getLastModifiedOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedOn(), Modem_.lastModifiedOn));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildSpecification(criteria.getIsDeleted(), Modem_.isDeleted));
            }
            if (criteria.getFlag() != null) {
                specification = specification.and(buildSpecification(criteria.getFlag(), Modem_.flag));
            }
            if (criteria.getModelId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getModelId(), root -> root.join(Modem_.model, JoinType.LEFT).get(CommonValue_.id))
                    );
            }
            if (criteria.getProtocolTypeId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getProtocolTypeId(),
                            root -> root.join(Modem_.protocolType, JoinType.LEFT).get(CommonValue_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
