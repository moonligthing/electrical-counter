package mn.electrical.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link mn.electrical.domain.Modem} entity.
 */
public class ModemDTO implements Serializable {

    private Long id;

    private String modelName;

    private String protocolTypeName;

    private String ipAddress;

    private String port;

    private String name;

    private String address;

    private String location;

    private String simInfo;

    private String cronTab;

    private String createdBy;

    private Instant createdOn;

    private String lastModifiedBy;

    private Instant lastModifiedOn;

    private Boolean isDeleted;

    private Boolean flag;

    private CommonValueDTO model;

    private CommonValueDTO protocolType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getProtocolTypeName() {
        return protocolTypeName;
    }

    public void setProtocolTypeName(String protocolTypeName) {
        this.protocolTypeName = protocolTypeName;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSimInfo() {
        return simInfo;
    }

    public void setSimInfo(String simInfo) {
        this.simInfo = simInfo;
    }

    public String getCronTab() {
        return cronTab;
    }

    public void setCronTab(String cronTab) {
        this.cronTab = cronTab;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedOn() {
        return lastModifiedOn;
    }

    public void setLastModifiedOn(Instant lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    public CommonValueDTO getModel() {
        return model;
    }

    public void setModel(CommonValueDTO model) {
        this.model = model;
    }

    public CommonValueDTO getProtocolType() {
        return protocolType;
    }

    public void setProtocolType(CommonValueDTO protocolType) {
        this.protocolType = protocolType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ModemDTO)) {
            return false;
        }

        ModemDTO modemDTO = (ModemDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, modemDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ModemDTO{" +
            "id=" + getId() +
            ", modelName='" + getModelName() + "'" +
            ", protocolTypeName='" + getProtocolTypeName() + "'" +
            ", ipAddress='" + getIpAddress() + "'" +
            ", port='" + getPort() + "'" +
            ", name='" + getName() + "'" +
            ", address='" + getAddress() + "'" +
            ", location='" + getLocation() + "'" +
            ", simInfo='" + getSimInfo() + "'" +
            ", cronTab='" + getCronTab() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedOn='" + getLastModifiedOn() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", flag='" + getFlag() + "'" +
            ", model=" + getModel() +
            ", protocolType=" + getProtocolType() +
            "}";
    }
}
