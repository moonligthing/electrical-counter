package mn.electrical.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link mn.electrical.domain.Meter} entity.
 */
public class MeterDTO implements Serializable {

    private Long id;

    private String modemName;

    private String modelName;

    private String meterTypeName;

    private String currentName;

    private String voltageName;

    private Instant installationOn;

    private Instant warrantyOn;

    private String number;

    private String name;

    private String address;

    private String ctRatio;

    private String vtRatio;

    private String deviceAddress;

    private String status;

    private String rate1;

    private String rate2;

    private String rate3;

    private String createdBy;

    private Instant createdOn;

    private String lastModifiedBy;

    private Instant lastModifiedOn;

    private ModemDTO modem;

    private CommonValueDTO meterType;

    private CommonValueDTO current;

    private CommonValueDTO voltage;

    private CommonValueDTO model;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModemName() {
        return modemName;
    }

    public void setModemName(String modemName) {
        this.modemName = modemName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getMeterTypeName() {
        return meterTypeName;
    }

    public void setMeterTypeName(String meterTypeName) {
        this.meterTypeName = meterTypeName;
    }

    public String getCurrentName() {
        return currentName;
    }

    public void setCurrentName(String currentName) {
        this.currentName = currentName;
    }

    public String getVoltageName() {
        return voltageName;
    }

    public void setVoltageName(String voltageName) {
        this.voltageName = voltageName;
    }

    public Instant getInstallationOn() {
        return installationOn;
    }

    public void setInstallationOn(Instant installationOn) {
        this.installationOn = installationOn;
    }

    public Instant getWarrantyOn() {
        return warrantyOn;
    }

    public void setWarrantyOn(Instant warrantyOn) {
        this.warrantyOn = warrantyOn;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCtRatio() {
        return ctRatio;
    }

    public void setCtRatio(String ctRatio) {
        this.ctRatio = ctRatio;
    }

    public String getVtRatio() {
        return vtRatio;
    }

    public void setVtRatio(String vtRatio) {
        this.vtRatio = vtRatio;
    }

    public String getDeviceAddress() {
        return deviceAddress;
    }

    public void setDeviceAddress(String deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRate1() {
        return rate1;
    }

    public void setRate1(String rate1) {
        this.rate1 = rate1;
    }

    public String getRate2() {
        return rate2;
    }

    public void setRate2(String rate2) {
        this.rate2 = rate2;
    }

    public String getRate3() {
        return rate3;
    }

    public void setRate3(String rate3) {
        this.rate3 = rate3;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedOn() {
        return lastModifiedOn;
    }

    public void setLastModifiedOn(Instant lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public ModemDTO getModem() {
        return modem;
    }

    public void setModem(ModemDTO modem) {
        this.modem = modem;
    }

    public CommonValueDTO getMeterType() {
        return meterType;
    }

    public void setMeterType(CommonValueDTO meterType) {
        this.meterType = meterType;
    }

    public CommonValueDTO getCurrent() {
        return current;
    }

    public void setCurrent(CommonValueDTO current) {
        this.current = current;
    }

    public CommonValueDTO getVoltage() {
        return voltage;
    }

    public void setVoltage(CommonValueDTO voltage) {
        this.voltage = voltage;
    }

    public CommonValueDTO getModel() {
        return model;
    }

    public void setModel(CommonValueDTO model) {
        this.model = model;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MeterDTO)) {
            return false;
        }

        MeterDTO meterDTO = (MeterDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, meterDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MeterDTO{" +
            "id=" + getId() +
            ", modemName='" + getModemName() + "'" +
            ", modelName='" + getModelName() + "'" +
            ", meterTypeName='" + getMeterTypeName() + "'" +
            ", currentName='" + getCurrentName() + "'" +
            ", voltageName='" + getVoltageName() + "'" +
            ", installationOn='" + getInstallationOn() + "'" +
            ", warrantyOn='" + getWarrantyOn() + "'" +
            ", number='" + getNumber() + "'" +
            ", name='" + getName() + "'" +
            ", address='" + getAddress() + "'" +
            ", ctRatio='" + getCtRatio() + "'" +
            ", vtRatio='" + getVtRatio() + "'" +
            ", deviceAddress='" + getDeviceAddress() + "'" +
            ", status='" + getStatus() + "'" +
            ", rate1='" + getRate1() + "'" +
            ", rate2='" + getRate2() + "'" +
            ", rate3='" + getRate3() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedOn='" + getLastModifiedOn() + "'" +
            ", modem=" + getModem() +
            ", meterType=" + getMeterType() +
            ", current=" + getCurrent() +
            ", voltage=" + getVoltage() +
            ", model=" + getModel() +
            "}";
    }
}
