package mn.electrical.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link mn.electrical.domain.MeterReading} entity.
 */
public class MeterReadingDTO implements Serializable {

    private Long id;

    private String meterName;

    private String meterNumber;

    private String meterAddress;

    private String cycle;

    private String ctRatio;

    private String vtRatio;

    private String totalImportActiveEnergy;

    private String totalExportActiveEnergy;

    private String totalImportReactiveEnergy;

    private String totalExportReactiveEnergy;

    private String totalImportApparentEnergy;

    private String totalExportApparentEnergy;

    private String totalInstantaneousActivePower;

    private String totalInstantaneousReactivePower;

    private String totalInstantaneousApparentPower;

    private String totalPowerFactor;

    private String status;

    private Instant readOn;

    private String importActiveEnergy1;

    private String exportActiveEnergy1;

    private String importReactiveEnergy1;

    private String exportReactiveEnergy1;

    private String importApparentEnergy1;

    private String exportApparentEnergy1;

    private String importActiveEnergy2;

    private String exportActiveEnergy2;

    private String importReactiveEnergy2;

    private String exportReactiveEnergy2;

    private String importApparentEnergy2;

    private String exportApparentEnergy2;

    private String importActiveEnergy3;

    private String exportActiveEnergy3;

    private String importReactiveEnergy3;

    private String exportReactiveEnergy3;

    private String importApparentEnergy3;

    private String exportApparentEnergy3;

    private String voltageA;

    private String currentA;

    private String activePowerA;

    private String reactivePowerA;

    private String powerFactorA;

    private String voltageB;

    private String currentB;

    private String activePowerB;

    private String reactivePowerB;

    private String powerFactorB;

    private String voltageC;

    private String currentC;

    private String activePowerC;

    private String reactivePowerC;

    private String powerFactorC;

    private String createdBy;

    private Instant createdOn;

    private String lastModifiedBy;

    private Instant lastModifiedOn;

    private MeterDTO meter;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMeterName() {
        return meterName;
    }

    public void setMeterName(String meterName) {
        this.meterName = meterName;
    }

    public String getMeterNumber() {
        return meterNumber;
    }

    public void setMeterNumber(String meterNumber) {
        this.meterNumber = meterNumber;
    }

    public String getMeterAddress() {
        return meterAddress;
    }

    public void setMeterAddress(String meterAddress) {
        this.meterAddress = meterAddress;
    }

    public String getCycle() {
        return cycle;
    }

    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    public String getCtRatio() {
        return ctRatio;
    }

    public void setCtRatio(String ctRatio) {
        this.ctRatio = ctRatio;
    }

    public String getVtRatio() {
        return vtRatio;
    }

    public void setVtRatio(String vtRatio) {
        this.vtRatio = vtRatio;
    }

    public String getTotalImportActiveEnergy() {
        return totalImportActiveEnergy;
    }

    public void setTotalImportActiveEnergy(String totalImportActiveEnergy) {
        this.totalImportActiveEnergy = totalImportActiveEnergy;
    }

    public String getTotalExportActiveEnergy() {
        return totalExportActiveEnergy;
    }

    public void setTotalExportActiveEnergy(String totalExportActiveEnergy) {
        this.totalExportActiveEnergy = totalExportActiveEnergy;
    }

    public String getTotalImportReactiveEnergy() {
        return totalImportReactiveEnergy;
    }

    public void setTotalImportReactiveEnergy(String totalImportReactiveEnergy) {
        this.totalImportReactiveEnergy = totalImportReactiveEnergy;
    }

    public String getTotalExportReactiveEnergy() {
        return totalExportReactiveEnergy;
    }

    public void setTotalExportReactiveEnergy(String totalExportReactiveEnergy) {
        this.totalExportReactiveEnergy = totalExportReactiveEnergy;
    }

    public String getTotalImportApparentEnergy() {
        return totalImportApparentEnergy;
    }

    public void setTotalImportApparentEnergy(String totalImportApparentEnergy) {
        this.totalImportApparentEnergy = totalImportApparentEnergy;
    }

    public String getTotalExportApparentEnergy() {
        return totalExportApparentEnergy;
    }

    public void setTotalExportApparentEnergy(String totalExportApparentEnergy) {
        this.totalExportApparentEnergy = totalExportApparentEnergy;
    }

    public String getTotalInstantaneousActivePower() {
        return totalInstantaneousActivePower;
    }

    public void setTotalInstantaneousActivePower(String totalInstantaneousActivePower) {
        this.totalInstantaneousActivePower = totalInstantaneousActivePower;
    }

    public String getTotalInstantaneousReactivePower() {
        return totalInstantaneousReactivePower;
    }

    public void setTotalInstantaneousReactivePower(String totalInstantaneousReactivePower) {
        this.totalInstantaneousReactivePower = totalInstantaneousReactivePower;
    }

    public String getTotalInstantaneousApparentPower() {
        return totalInstantaneousApparentPower;
    }

    public void setTotalInstantaneousApparentPower(String totalInstantaneousApparentPower) {
        this.totalInstantaneousApparentPower = totalInstantaneousApparentPower;
    }

    public String getTotalPowerFactor() {
        return totalPowerFactor;
    }

    public void setTotalPowerFactor(String totalPowerFactor) {
        this.totalPowerFactor = totalPowerFactor;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Instant getReadOn() {
        return readOn;
    }

    public void setReadOn(Instant readOn) {
        this.readOn = readOn;
    }

    public String getImportActiveEnergy1() {
        return importActiveEnergy1;
    }

    public void setImportActiveEnergy1(String importActiveEnergy1) {
        this.importActiveEnergy1 = importActiveEnergy1;
    }

    public String getExportActiveEnergy1() {
        return exportActiveEnergy1;
    }

    public void setExportActiveEnergy1(String exportActiveEnergy1) {
        this.exportActiveEnergy1 = exportActiveEnergy1;
    }

    public String getImportReactiveEnergy1() {
        return importReactiveEnergy1;
    }

    public void setImportReactiveEnergy1(String importReactiveEnergy1) {
        this.importReactiveEnergy1 = importReactiveEnergy1;
    }

    public String getExportReactiveEnergy1() {
        return exportReactiveEnergy1;
    }

    public void setExportReactiveEnergy1(String exportReactiveEnergy1) {
        this.exportReactiveEnergy1 = exportReactiveEnergy1;
    }

    public String getImportApparentEnergy1() {
        return importApparentEnergy1;
    }

    public void setImportApparentEnergy1(String importApparentEnergy1) {
        this.importApparentEnergy1 = importApparentEnergy1;
    }

    public String getExportApparentEnergy1() {
        return exportApparentEnergy1;
    }

    public void setExportApparentEnergy1(String exportApparentEnergy1) {
        this.exportApparentEnergy1 = exportApparentEnergy1;
    }

    public String getImportActiveEnergy2() {
        return importActiveEnergy2;
    }

    public void setImportActiveEnergy2(String importActiveEnergy2) {
        this.importActiveEnergy2 = importActiveEnergy2;
    }

    public String getExportActiveEnergy2() {
        return exportActiveEnergy2;
    }

    public void setExportActiveEnergy2(String exportActiveEnergy2) {
        this.exportActiveEnergy2 = exportActiveEnergy2;
    }

    public String getImportReactiveEnergy2() {
        return importReactiveEnergy2;
    }

    public void setImportReactiveEnergy2(String importReactiveEnergy2) {
        this.importReactiveEnergy2 = importReactiveEnergy2;
    }

    public String getExportReactiveEnergy2() {
        return exportReactiveEnergy2;
    }

    public void setExportReactiveEnergy2(String exportReactiveEnergy2) {
        this.exportReactiveEnergy2 = exportReactiveEnergy2;
    }

    public String getImportApparentEnergy2() {
        return importApparentEnergy2;
    }

    public void setImportApparentEnergy2(String importApparentEnergy2) {
        this.importApparentEnergy2 = importApparentEnergy2;
    }

    public String getExportApparentEnergy2() {
        return exportApparentEnergy2;
    }

    public void setExportApparentEnergy2(String exportApparentEnergy2) {
        this.exportApparentEnergy2 = exportApparentEnergy2;
    }

    public String getImportActiveEnergy3() {
        return importActiveEnergy3;
    }

    public void setImportActiveEnergy3(String importActiveEnergy3) {
        this.importActiveEnergy3 = importActiveEnergy3;
    }

    public String getExportActiveEnergy3() {
        return exportActiveEnergy3;
    }

    public void setExportActiveEnergy3(String exportActiveEnergy3) {
        this.exportActiveEnergy3 = exportActiveEnergy3;
    }

    public String getImportReactiveEnergy3() {
        return importReactiveEnergy3;
    }

    public void setImportReactiveEnergy3(String importReactiveEnergy3) {
        this.importReactiveEnergy3 = importReactiveEnergy3;
    }

    public String getExportReactiveEnergy3() {
        return exportReactiveEnergy3;
    }

    public void setExportReactiveEnergy3(String exportReactiveEnergy3) {
        this.exportReactiveEnergy3 = exportReactiveEnergy3;
    }

    public String getImportApparentEnergy3() {
        return importApparentEnergy3;
    }

    public void setImportApparentEnergy3(String importApparentEnergy3) {
        this.importApparentEnergy3 = importApparentEnergy3;
    }

    public String getExportApparentEnergy3() {
        return exportApparentEnergy3;
    }

    public void setExportApparentEnergy3(String exportApparentEnergy3) {
        this.exportApparentEnergy3 = exportApparentEnergy3;
    }

    public String getVoltageA() {
        return voltageA;
    }

    public void setVoltageA(String voltageA) {
        this.voltageA = voltageA;
    }

    public String getCurrentA() {
        return currentA;
    }

    public void setCurrentA(String currentA) {
        this.currentA = currentA;
    }

    public String getActivePowerA() {
        return activePowerA;
    }

    public void setActivePowerA(String activePowerA) {
        this.activePowerA = activePowerA;
    }

    public String getReactivePowerA() {
        return reactivePowerA;
    }

    public void setReactivePowerA(String reactivePowerA) {
        this.reactivePowerA = reactivePowerA;
    }

    public String getPowerFactorA() {
        return powerFactorA;
    }

    public void setPowerFactorA(String powerFactorA) {
        this.powerFactorA = powerFactorA;
    }

    public String getVoltageB() {
        return voltageB;
    }

    public void setVoltageB(String voltageB) {
        this.voltageB = voltageB;
    }

    public String getCurrentB() {
        return currentB;
    }

    public void setCurrentB(String currentB) {
        this.currentB = currentB;
    }

    public String getActivePowerB() {
        return activePowerB;
    }

    public void setActivePowerB(String activePowerB) {
        this.activePowerB = activePowerB;
    }

    public String getReactivePowerB() {
        return reactivePowerB;
    }

    public void setReactivePowerB(String reactivePowerB) {
        this.reactivePowerB = reactivePowerB;
    }

    public String getPowerFactorB() {
        return powerFactorB;
    }

    public void setPowerFactorB(String powerFactorB) {
        this.powerFactorB = powerFactorB;
    }

    public String getVoltageC() {
        return voltageC;
    }

    public void setVoltageC(String voltageC) {
        this.voltageC = voltageC;
    }

    public String getCurrentC() {
        return currentC;
    }

    public void setCurrentC(String currentC) {
        this.currentC = currentC;
    }

    public String getActivePowerC() {
        return activePowerC;
    }

    public void setActivePowerC(String activePowerC) {
        this.activePowerC = activePowerC;
    }

    public String getReactivePowerC() {
        return reactivePowerC;
    }

    public void setReactivePowerC(String reactivePowerC) {
        this.reactivePowerC = reactivePowerC;
    }

    public String getPowerFactorC() {
        return powerFactorC;
    }

    public void setPowerFactorC(String powerFactorC) {
        this.powerFactorC = powerFactorC;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedOn() {
        return lastModifiedOn;
    }

    public void setLastModifiedOn(Instant lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public MeterDTO getMeter() {
        return meter;
    }

    public void setMeter(MeterDTO meter) {
        this.meter = meter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MeterReadingDTO)) {
            return false;
        }

        MeterReadingDTO meterReadingDTO = (MeterReadingDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, meterReadingDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MeterReadingDTO{" +
            "id=" + getId() +
            ", meterName='" + getMeterName() + "'" +
            ", meterNumber='" + getMeterNumber() + "'" +
            ", meterAddress='" + getMeterAddress() + "'" +
            ", cycle='" + getCycle() + "'" +
            ", ctRatio='" + getCtRatio() + "'" +
            ", vtRatio='" + getVtRatio() + "'" +
            ", totalImportActiveEnergy='" + getTotalImportActiveEnergy() + "'" +
            ", totalExportActiveEnergy='" + getTotalExportActiveEnergy() + "'" +
            ", totalImportReactiveEnergy='" + getTotalImportReactiveEnergy() + "'" +
            ", totalExportReactiveEnergy='" + getTotalExportReactiveEnergy() + "'" +
            ", totalImportApparentEnergy='" + getTotalImportApparentEnergy() + "'" +
            ", totalExportApparentEnergy='" + getTotalExportApparentEnergy() + "'" +
            ", totalInstantaneousActivePower='" + getTotalInstantaneousActivePower() + "'" +
            ", totalInstantaneousReactivePower='" + getTotalInstantaneousReactivePower() + "'" +
            ", totalInstantaneousApparentPower='" + getTotalInstantaneousApparentPower() + "'" +
            ", totalPowerFactor='" + getTotalPowerFactor() + "'" +
            ", status='" + getStatus() + "'" +
            ", readOn='" + getReadOn() + "'" +
            ", importActiveEnergy1='" + getImportActiveEnergy1() + "'" +
            ", exportActiveEnergy1='" + getExportActiveEnergy1() + "'" +
            ", importReactiveEnergy1='" + getImportReactiveEnergy1() + "'" +
            ", exportReactiveEnergy1='" + getExportReactiveEnergy1() + "'" +
            ", importApparentEnergy1='" + getImportApparentEnergy1() + "'" +
            ", exportApparentEnergy1='" + getExportApparentEnergy1() + "'" +
            ", importActiveEnergy2='" + getImportActiveEnergy2() + "'" +
            ", exportActiveEnergy2='" + getExportActiveEnergy2() + "'" +
            ", importReactiveEnergy2='" + getImportReactiveEnergy2() + "'" +
            ", exportReactiveEnergy2='" + getExportReactiveEnergy2() + "'" +
            ", importApparentEnergy2='" + getImportApparentEnergy2() + "'" +
            ", exportApparentEnergy2='" + getExportApparentEnergy2() + "'" +
            ", importActiveEnergy3='" + getImportActiveEnergy3() + "'" +
            ", exportActiveEnergy3='" + getExportActiveEnergy3() + "'" +
            ", importReactiveEnergy3='" + getImportReactiveEnergy3() + "'" +
            ", exportReactiveEnergy3='" + getExportReactiveEnergy3() + "'" +
            ", importApparentEnergy3='" + getImportApparentEnergy3() + "'" +
            ", exportApparentEnergy3='" + getExportApparentEnergy3() + "'" +
            ", voltageA='" + getVoltageA() + "'" +
            ", currentA='" + getCurrentA() + "'" +
            ", activePowerA='" + getActivePowerA() + "'" +
            ", reactivePowerA='" + getReactivePowerA() + "'" +
            ", powerFactorA='" + getPowerFactorA() + "'" +
            ", voltageB='" + getVoltageB() + "'" +
            ", currentB='" + getCurrentB() + "'" +
            ", activePowerB='" + getActivePowerB() + "'" +
            ", reactivePowerB='" + getReactivePowerB() + "'" +
            ", powerFactorB='" + getPowerFactorB() + "'" +
            ", voltageC='" + getVoltageC() + "'" +
            ", currentC='" + getCurrentC() + "'" +
            ", activePowerC='" + getActivePowerC() + "'" +
            ", reactivePowerC='" + getReactivePowerC() + "'" +
            ", powerFactorC='" + getPowerFactorC() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedOn='" + getLastModifiedOn() + "'" +
            ", meter=" + getMeter() +
            "}";
    }
}
