package mn.electrical.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;

/**
 * A MeterReading.
 */
@Entity
@Table(name = "meter_reading")
@EntityListeners(AuditingEntityListener.class)
public class MeterReading implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "meter_name")
    private String meterName;

    @Column(name = "meter_number")
    private String meterNumber;

    @Column(name = "meter_address")
    private String meterAddress;

    @Column(name = "cycle")
    private String cycle;

    @Column(name = "ct_ratio")
    private String ctRatio;

    @Column(name = "vt_ratio")
    private String vtRatio;

    @Column(name = "total_import_active_energy")
    private String totalImportActiveEnergy;

    @Column(name = "total_export_active_energy")
    private String totalExportActiveEnergy;

    @Column(name = "total_import_reactive_energy")
    private String totalImportReactiveEnergy;

    @Column(name = "total_export_reactive_energy")
    private String totalExportReactiveEnergy;

    @Column(name = "total_import_apparent_energy")
    private String totalImportApparentEnergy;

    @Column(name = "total_export_apparent_energy")
    private String totalExportApparentEnergy;

    @Column(name = "total_instantaneous_active_power")
    private String totalInstantaneousActivePower;

    @Column(name = "total_instantaneous_reactive_power")
    private String totalInstantaneousReactivePower;

    @Column(name = "total_instantaneous_apparent_power")
    private String totalInstantaneousApparentPower;

    @Column(name = "total_power_factor")
    private String totalPowerFactor;

    @Column(name = "status")
    private String status;

    @Column(name = "read_on")
    private Instant readOn;

    @Column(name = "import_active_energy_1")
    private String importActiveEnergy1;

    @Column(name = "export_active_energy_1")
    private String exportActiveEnergy1;

    @Column(name = "import_reactive_energy_1")
    private String importReactiveEnergy1;

    @Column(name = "export_reactive_energy_1")
    private String exportReactiveEnergy1;

    @Column(name = "import_apparent_energy_1")
    private String importApparentEnergy1;

    @Column(name = "export_apparent_energy_1")
    private String exportApparentEnergy1;

    @Column(name = "import_active_energy_2")
    private String importActiveEnergy2;

    @Column(name = "export_active_energy_2")
    private String exportActiveEnergy2;

    @Column(name = "import_reactive_energy_2")
    private String importReactiveEnergy2;

    @Column(name = "export_reactive_energy_2")
    private String exportReactiveEnergy2;

    @Column(name = "import_apparent_energy_2")
    private String importApparentEnergy2;

    @Column(name = "export_apparent_energy_2")
    private String exportApparentEnergy2;

    @Column(name = "import_active_energy_3")
    private String importActiveEnergy3;

    @Column(name = "export_active_energy_3")
    private String exportActiveEnergy3;

    @Column(name = "import_reactive_energy_3")
    private String importReactiveEnergy3;

    @Column(name = "export_reactive_energy_3")
    private String exportReactiveEnergy3;

    @Column(name = "import_apparent_energy_3")
    private String importApparentEnergy3;

    @Column(name = "export_apparent_energy_3")
    private String exportApparentEnergy3;

    @Column(name = "voltage_a")
    private String voltageA;

    @Column(name = "current_a")
    private String currentA;

    @Column(name = "active_power_a")
    private String activePowerA;

    @Column(name = "reactive_power_a")
    private String reactivePowerA;

    @Column(name = "power_factor_a")
    private String powerFactorA;

    @Column(name = "voltage_b")
    private String voltageB;

    @Column(name = "current_b")
    private String currentB;

    @Column(name = "active_power_b")
    private String activePowerB;

    @Column(name = "reactive_power_b")
    private String reactivePowerB;

    @Column(name = "power_factor_b")
    private String powerFactorB;

    @Column(name = "voltage_c")
    private String voltageC;

    @Column(name = "current_c")
    private String currentC;

    @Column(name = "active_power_c")
    private String activePowerC;

    @Column(name = "reactive_power_c")
    private String reactivePowerC;

    @Column(name = "power_factor_c")
    private String powerFactorC;

    @Column(name = "created_by")
    @CreatedBy
    private String createdBy;

    @Column(name = "created_on")
    @CreatedDate
    private Instant createdOn;

    @Column(name = "last_modified_by")
    @LastModifiedBy
    private String lastModifiedBy;

    @Column(name = "last_modified_on")
    @LastModifiedDate
    private Instant lastModifiedOn;

    @ManyToOne
    @JsonIgnoreProperties(value = { "modem", "meterType", "current", "voltage", "model" }, allowSetters = true)
    private Meter meter;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public MeterReading id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMeterName() {
        return this.meterName;
    }

    public MeterReading meterName(String meterName) {
        this.setMeterName(meterName);
        return this;
    }

    public void setMeterName(String meterName) {
        this.meterName = meterName;
    }

    public String getMeterNumber() {
        return this.meterNumber;
    }

    public MeterReading meterNumber(String meterNumber) {
        this.setMeterNumber(meterNumber);
        return this;
    }

    public void setMeterNumber(String meterNumber) {
        this.meterNumber = meterNumber;
    }

    public String getMeterAddress() {
        return this.meterAddress;
    }

    public MeterReading meterAddress(String meterAddress) {
        this.setMeterAddress(meterAddress);
        return this;
    }

    public void setMeterAddress(String meterAddress) {
        this.meterAddress = meterAddress;
    }

    public String getCycle() {
        return this.cycle;
    }

    public MeterReading cycle(String cycle) {
        this.setCycle(cycle);
        return this;
    }

    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    public String getCtRatio() {
        return this.ctRatio;
    }

    public MeterReading ctRatio(String ctRatio) {
        this.setCtRatio(ctRatio);
        return this;
    }

    public void setCtRatio(String ctRatio) {
        this.ctRatio = ctRatio;
    }

    public String getVtRatio() {
        return this.vtRatio;
    }

    public MeterReading vtRatio(String vtRatio) {
        this.setVtRatio(vtRatio);
        return this;
    }

    public void setVtRatio(String vtRatio) {
        this.vtRatio = vtRatio;
    }

    public String getTotalImportActiveEnergy() {
        return this.totalImportActiveEnergy;
    }

    public MeterReading totalImportActiveEnergy(String totalImportActiveEnergy) {
        this.setTotalImportActiveEnergy(totalImportActiveEnergy);
        return this;
    }

    public void setTotalImportActiveEnergy(String totalImportActiveEnergy) {
        this.totalImportActiveEnergy = totalImportActiveEnergy;
    }

    public String getTotalExportActiveEnergy() {
        return this.totalExportActiveEnergy;
    }

    public MeterReading totalExportActiveEnergy(String totalExportActiveEnergy) {
        this.setTotalExportActiveEnergy(totalExportActiveEnergy);
        return this;
    }

    public void setTotalExportActiveEnergy(String totalExportActiveEnergy) {
        this.totalExportActiveEnergy = totalExportActiveEnergy;
    }

    public String getTotalImportReactiveEnergy() {
        return this.totalImportReactiveEnergy;
    }

    public MeterReading totalImportReactiveEnergy(String totalImportReactiveEnergy) {
        this.setTotalImportReactiveEnergy(totalImportReactiveEnergy);
        return this;
    }

    public void setTotalImportReactiveEnergy(String totalImportReactiveEnergy) {
        this.totalImportReactiveEnergy = totalImportReactiveEnergy;
    }

    public String getTotalExportReactiveEnergy() {
        return this.totalExportReactiveEnergy;
    }

    public MeterReading totalExportReactiveEnergy(String totalExportReactiveEnergy) {
        this.setTotalExportReactiveEnergy(totalExportReactiveEnergy);
        return this;
    }

    public void setTotalExportReactiveEnergy(String totalExportReactiveEnergy) {
        this.totalExportReactiveEnergy = totalExportReactiveEnergy;
    }

    public String getTotalImportApparentEnergy() {
        return this.totalImportApparentEnergy;
    }

    public MeterReading totalImportApparentEnergy(String totalImportApparentEnergy) {
        this.setTotalImportApparentEnergy(totalImportApparentEnergy);
        return this;
    }

    public void setTotalImportApparentEnergy(String totalImportApparentEnergy) {
        this.totalImportApparentEnergy = totalImportApparentEnergy;
    }

    public String getTotalExportApparentEnergy() {
        return this.totalExportApparentEnergy;
    }

    public MeterReading totalExportApparentEnergy(String totalExportApparentEnergy) {
        this.setTotalExportApparentEnergy(totalExportApparentEnergy);
        return this;
    }

    public void setTotalExportApparentEnergy(String totalExportApparentEnergy) {
        this.totalExportApparentEnergy = totalExportApparentEnergy;
    }

    public String getTotalInstantaneousActivePower() {
        return this.totalInstantaneousActivePower;
    }

    public MeterReading totalInstantaneousActivePower(String totalInstantaneousActivePower) {
        this.setTotalInstantaneousActivePower(totalInstantaneousActivePower);
        return this;
    }

    public void setTotalInstantaneousActivePower(String totalInstantaneousActivePower) {
        this.totalInstantaneousActivePower = totalInstantaneousActivePower;
    }

    public String getTotalInstantaneousReactivePower() {
        return this.totalInstantaneousReactivePower;
    }

    public MeterReading totalInstantaneousReactivePower(String totalInstantaneousReactivePower) {
        this.setTotalInstantaneousReactivePower(totalInstantaneousReactivePower);
        return this;
    }

    public void setTotalInstantaneousReactivePower(String totalInstantaneousReactivePower) {
        this.totalInstantaneousReactivePower = totalInstantaneousReactivePower;
    }

    public String getTotalInstantaneousApparentPower() {
        return this.totalInstantaneousApparentPower;
    }

    public MeterReading totalInstantaneousApparentPower(String totalInstantaneousApparentPower) {
        this.setTotalInstantaneousApparentPower(totalInstantaneousApparentPower);
        return this;
    }

    public void setTotalInstantaneousApparentPower(String totalInstantaneousApparentPower) {
        this.totalInstantaneousApparentPower = totalInstantaneousApparentPower;
    }

    public String getTotalPowerFactor() {
        return this.totalPowerFactor;
    }

    public MeterReading totalPowerFactor(String totalPowerFactor) {
        this.setTotalPowerFactor(totalPowerFactor);
        return this;
    }

    public void setTotalPowerFactor(String totalPowerFactor) {
        this.totalPowerFactor = totalPowerFactor;
    }

    public String getStatus() {
        return this.status;
    }

    public MeterReading status(String status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Instant getReadOn() {
        return this.readOn;
    }

    public MeterReading readOn(Instant readOn) {
        this.setReadOn(readOn);
        return this;
    }

    public void setReadOn(Instant readOn) {
        this.readOn = readOn;
    }

    public String getImportActiveEnergy1() {
        return this.importActiveEnergy1;
    }

    public MeterReading importActiveEnergy1(String importActiveEnergy1) {
        this.setImportActiveEnergy1(importActiveEnergy1);
        return this;
    }

    public void setImportActiveEnergy1(String importActiveEnergy1) {
        this.importActiveEnergy1 = importActiveEnergy1;
    }

    public String getExportActiveEnergy1() {
        return this.exportActiveEnergy1;
    }

    public MeterReading exportActiveEnergy1(String exportActiveEnergy1) {
        this.setExportActiveEnergy1(exportActiveEnergy1);
        return this;
    }

    public void setExportActiveEnergy1(String exportActiveEnergy1) {
        this.exportActiveEnergy1 = exportActiveEnergy1;
    }

    public String getImportReactiveEnergy1() {
        return this.importReactiveEnergy1;
    }

    public MeterReading importReactiveEnergy1(String importReactiveEnergy1) {
        this.setImportReactiveEnergy1(importReactiveEnergy1);
        return this;
    }

    public void setImportReactiveEnergy1(String importReactiveEnergy1) {
        this.importReactiveEnergy1 = importReactiveEnergy1;
    }

    public String getExportReactiveEnergy1() {
        return this.exportReactiveEnergy1;
    }

    public MeterReading exportReactiveEnergy1(String exportReactiveEnergy1) {
        this.setExportReactiveEnergy1(exportReactiveEnergy1);
        return this;
    }

    public void setExportReactiveEnergy1(String exportReactiveEnergy1) {
        this.exportReactiveEnergy1 = exportReactiveEnergy1;
    }

    public String getImportApparentEnergy1() {
        return this.importApparentEnergy1;
    }

    public MeterReading importApparentEnergy1(String importApparentEnergy1) {
        this.setImportApparentEnergy1(importApparentEnergy1);
        return this;
    }

    public void setImportApparentEnergy1(String importApparentEnergy1) {
        this.importApparentEnergy1 = importApparentEnergy1;
    }

    public String getExportApparentEnergy1() {
        return this.exportApparentEnergy1;
    }

    public MeterReading exportApparentEnergy1(String exportApparentEnergy1) {
        this.setExportApparentEnergy1(exportApparentEnergy1);
        return this;
    }

    public void setExportApparentEnergy1(String exportApparentEnergy1) {
        this.exportApparentEnergy1 = exportApparentEnergy1;
    }

    public String getImportActiveEnergy2() {
        return this.importActiveEnergy2;
    }

    public MeterReading importActiveEnergy2(String importActiveEnergy2) {
        this.setImportActiveEnergy2(importActiveEnergy2);
        return this;
    }

    public void setImportActiveEnergy2(String importActiveEnergy2) {
        this.importActiveEnergy2 = importActiveEnergy2;
    }

    public String getExportActiveEnergy2() {
        return this.exportActiveEnergy2;
    }

    public MeterReading exportActiveEnergy2(String exportActiveEnergy2) {
        this.setExportActiveEnergy2(exportActiveEnergy2);
        return this;
    }

    public void setExportActiveEnergy2(String exportActiveEnergy2) {
        this.exportActiveEnergy2 = exportActiveEnergy2;
    }

    public String getImportReactiveEnergy2() {
        return this.importReactiveEnergy2;
    }

    public MeterReading importReactiveEnergy2(String importReactiveEnergy2) {
        this.setImportReactiveEnergy2(importReactiveEnergy2);
        return this;
    }

    public void setImportReactiveEnergy2(String importReactiveEnergy2) {
        this.importReactiveEnergy2 = importReactiveEnergy2;
    }

    public String getExportReactiveEnergy2() {
        return this.exportReactiveEnergy2;
    }

    public MeterReading exportReactiveEnergy2(String exportReactiveEnergy2) {
        this.setExportReactiveEnergy2(exportReactiveEnergy2);
        return this;
    }

    public void setExportReactiveEnergy2(String exportReactiveEnergy2) {
        this.exportReactiveEnergy2 = exportReactiveEnergy2;
    }

    public String getImportApparentEnergy2() {
        return this.importApparentEnergy2;
    }

    public MeterReading importApparentEnergy2(String importApparentEnergy2) {
        this.setImportApparentEnergy2(importApparentEnergy2);
        return this;
    }

    public void setImportApparentEnergy2(String importApparentEnergy2) {
        this.importApparentEnergy2 = importApparentEnergy2;
    }

    public String getExportApparentEnergy2() {
        return this.exportApparentEnergy2;
    }

    public MeterReading exportApparentEnergy2(String exportApparentEnergy2) {
        this.setExportApparentEnergy2(exportApparentEnergy2);
        return this;
    }

    public void setExportApparentEnergy2(String exportApparentEnergy2) {
        this.exportApparentEnergy2 = exportApparentEnergy2;
    }

    public String getImportActiveEnergy3() {
        return this.importActiveEnergy3;
    }

    public MeterReading importActiveEnergy3(String importActiveEnergy3) {
        this.setImportActiveEnergy3(importActiveEnergy3);
        return this;
    }

    public void setImportActiveEnergy3(String importActiveEnergy3) {
        this.importActiveEnergy3 = importActiveEnergy3;
    }

    public String getExportActiveEnergy3() {
        return this.exportActiveEnergy3;
    }

    public MeterReading exportActiveEnergy3(String exportActiveEnergy3) {
        this.setExportActiveEnergy3(exportActiveEnergy3);
        return this;
    }

    public void setExportActiveEnergy3(String exportActiveEnergy3) {
        this.exportActiveEnergy3 = exportActiveEnergy3;
    }

    public String getImportReactiveEnergy3() {
        return this.importReactiveEnergy3;
    }

    public MeterReading importReactiveEnergy3(String importReactiveEnergy3) {
        this.setImportReactiveEnergy3(importReactiveEnergy3);
        return this;
    }

    public void setImportReactiveEnergy3(String importReactiveEnergy3) {
        this.importReactiveEnergy3 = importReactiveEnergy3;
    }

    public String getExportReactiveEnergy3() {
        return this.exportReactiveEnergy3;
    }

    public MeterReading exportReactiveEnergy3(String exportReactiveEnergy3) {
        this.setExportReactiveEnergy3(exportReactiveEnergy3);
        return this;
    }

    public void setExportReactiveEnergy3(String exportReactiveEnergy3) {
        this.exportReactiveEnergy3 = exportReactiveEnergy3;
    }

    public String getImportApparentEnergy3() {
        return this.importApparentEnergy3;
    }

    public MeterReading importApparentEnergy3(String importApparentEnergy3) {
        this.setImportApparentEnergy3(importApparentEnergy3);
        return this;
    }

    public void setImportApparentEnergy3(String importApparentEnergy3) {
        this.importApparentEnergy3 = importApparentEnergy3;
    }

    public String getExportApparentEnergy3() {
        return this.exportApparentEnergy3;
    }

    public MeterReading exportApparentEnergy3(String exportApparentEnergy3) {
        this.setExportApparentEnergy3(exportApparentEnergy3);
        return this;
    }

    public void setExportApparentEnergy3(String exportApparentEnergy3) {
        this.exportApparentEnergy3 = exportApparentEnergy3;
    }

    public String getVoltageA() {
        return this.voltageA;
    }

    public MeterReading voltageA(String voltageA) {
        this.setVoltageA(voltageA);
        return this;
    }

    public void setVoltageA(String voltageA) {
        this.voltageA = voltageA;
    }

    public String getCurrentA() {
        return this.currentA;
    }

    public MeterReading currentA(String currentA) {
        this.setCurrentA(currentA);
        return this;
    }

    public void setCurrentA(String currentA) {
        this.currentA = currentA;
    }

    public String getActivePowerA() {
        return this.activePowerA;
    }

    public MeterReading activePowerA(String activePowerA) {
        this.setActivePowerA(activePowerA);
        return this;
    }

    public void setActivePowerA(String activePowerA) {
        this.activePowerA = activePowerA;
    }

    public String getReactivePowerA() {
        return this.reactivePowerA;
    }

    public MeterReading reactivePowerA(String reactivePowerA) {
        this.setReactivePowerA(reactivePowerA);
        return this;
    }

    public void setReactivePowerA(String reactivePowerA) {
        this.reactivePowerA = reactivePowerA;
    }

    public String getPowerFactorA() {
        return this.powerFactorA;
    }

    public MeterReading powerFactorA(String powerFactorA) {
        this.setPowerFactorA(powerFactorA);
        return this;
    }

    public void setPowerFactorA(String powerFactorA) {
        this.powerFactorA = powerFactorA;
    }

    public String getVoltageB() {
        return this.voltageB;
    }

    public MeterReading voltageB(String voltageB) {
        this.setVoltageB(voltageB);
        return this;
    }

    public void setVoltageB(String voltageB) {
        this.voltageB = voltageB;
    }

    public String getCurrentB() {
        return this.currentB;
    }

    public MeterReading currentB(String currentB) {
        this.setCurrentB(currentB);
        return this;
    }

    public void setCurrentB(String currentB) {
        this.currentB = currentB;
    }

    public String getActivePowerB() {
        return this.activePowerB;
    }

    public MeterReading activePowerB(String activePowerB) {
        this.setActivePowerB(activePowerB);
        return this;
    }

    public void setActivePowerB(String activePowerB) {
        this.activePowerB = activePowerB;
    }

    public String getReactivePowerB() {
        return this.reactivePowerB;
    }

    public MeterReading reactivePowerB(String reactivePowerB) {
        this.setReactivePowerB(reactivePowerB);
        return this;
    }

    public void setReactivePowerB(String reactivePowerB) {
        this.reactivePowerB = reactivePowerB;
    }

    public String getPowerFactorB() {
        return this.powerFactorB;
    }

    public MeterReading powerFactorB(String powerFactorB) {
        this.setPowerFactorB(powerFactorB);
        return this;
    }

    public void setPowerFactorB(String powerFactorB) {
        this.powerFactorB = powerFactorB;
    }

    public String getVoltageC() {
        return this.voltageC;
    }

    public MeterReading voltageC(String voltageC) {
        this.setVoltageC(voltageC);
        return this;
    }

    public void setVoltageC(String voltageC) {
        this.voltageC = voltageC;
    }

    public String getCurrentC() {
        return this.currentC;
    }

    public MeterReading currentC(String currentC) {
        this.setCurrentC(currentC);
        return this;
    }

    public void setCurrentC(String currentC) {
        this.currentC = currentC;
    }

    public String getActivePowerC() {
        return this.activePowerC;
    }

    public MeterReading activePowerC(String activePowerC) {
        this.setActivePowerC(activePowerC);
        return this;
    }

    public void setActivePowerC(String activePowerC) {
        this.activePowerC = activePowerC;
    }

    public String getReactivePowerC() {
        return this.reactivePowerC;
    }

    public MeterReading reactivePowerC(String reactivePowerC) {
        this.setReactivePowerC(reactivePowerC);
        return this;
    }

    public void setReactivePowerC(String reactivePowerC) {
        this.reactivePowerC = reactivePowerC;
    }

    public String getPowerFactorC() {
        return this.powerFactorC;
    }

    public MeterReading powerFactorC(String powerFactorC) {
        this.setPowerFactorC(powerFactorC);
        return this;
    }

    public void setPowerFactorC(String powerFactorC) {
        this.powerFactorC = powerFactorC;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public MeterReading createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedOn() {
        return this.createdOn;
    }

    public MeterReading createdOn(Instant createdOn) {
        this.setCreatedOn(createdOn);
        return this;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public MeterReading lastModifiedBy(String lastModifiedBy) {
        this.setLastModifiedBy(lastModifiedBy);
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedOn() {
        return this.lastModifiedOn;
    }

    public MeterReading lastModifiedOn(Instant lastModifiedOn) {
        this.setLastModifiedOn(lastModifiedOn);
        return this;
    }

    public void setLastModifiedOn(Instant lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public Meter getMeter() {
        return this.meter;
    }

    public void setMeter(Meter meter) {
        this.meter = meter;
    }

    public MeterReading meter(Meter meter) {
        this.setMeter(meter);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MeterReading)) {
            return false;
        }
        return id != null && id.equals(((MeterReading) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MeterReading{" +
            "id=" + getId() +
            ", meterName='" + getMeterName() + "'" +
            ", meterNumber='" + getMeterNumber() + "'" +
            ", meterAddress='" + getMeterAddress() + "'" +
            ", cycle='" + getCycle() + "'" +
            ", ctRatio='" + getCtRatio() + "'" +
            ", vtRatio='" + getVtRatio() + "'" +
            ", totalImportActiveEnergy='" + getTotalImportActiveEnergy() + "'" +
            ", totalExportActiveEnergy='" + getTotalExportActiveEnergy() + "'" +
            ", totalImportReactiveEnergy='" + getTotalImportReactiveEnergy() + "'" +
            ", totalExportReactiveEnergy='" + getTotalExportReactiveEnergy() + "'" +
            ", totalImportApparentEnergy='" + getTotalImportApparentEnergy() + "'" +
            ", totalExportApparentEnergy='" + getTotalExportApparentEnergy() + "'" +
            ", totalInstantaneousActivePower='" + getTotalInstantaneousActivePower() + "'" +
            ", totalInstantaneousReactivePower='" + getTotalInstantaneousReactivePower() + "'" +
            ", totalInstantaneousApparentPower='" + getTotalInstantaneousApparentPower() + "'" +
            ", totalPowerFactor='" + getTotalPowerFactor() + "'" +
            ", status='" + getStatus() + "'" +
            ", readOn='" + getReadOn() + "'" +
            ", importActiveEnergy1='" + getImportActiveEnergy1() + "'" +
            ", exportActiveEnergy1='" + getExportActiveEnergy1() + "'" +
            ", importReactiveEnergy1='" + getImportReactiveEnergy1() + "'" +
            ", exportReactiveEnergy1='" + getExportReactiveEnergy1() + "'" +
            ", importApparentEnergy1='" + getImportApparentEnergy1() + "'" +
            ", exportApparentEnergy1='" + getExportApparentEnergy1() + "'" +
            ", importActiveEnergy2='" + getImportActiveEnergy2() + "'" +
            ", exportActiveEnergy2='" + getExportActiveEnergy2() + "'" +
            ", importReactiveEnergy2='" + getImportReactiveEnergy2() + "'" +
            ", exportReactiveEnergy2='" + getExportReactiveEnergy2() + "'" +
            ", importApparentEnergy2='" + getImportApparentEnergy2() + "'" +
            ", exportApparentEnergy2='" + getExportApparentEnergy2() + "'" +
            ", importActiveEnergy3='" + getImportActiveEnergy3() + "'" +
            ", exportActiveEnergy3='" + getExportActiveEnergy3() + "'" +
            ", importReactiveEnergy3='" + getImportReactiveEnergy3() + "'" +
            ", exportReactiveEnergy3='" + getExportReactiveEnergy3() + "'" +
            ", importApparentEnergy3='" + getImportApparentEnergy3() + "'" +
            ", exportApparentEnergy3='" + getExportApparentEnergy3() + "'" +
            ", voltageA='" + getVoltageA() + "'" +
            ", currentA='" + getCurrentA() + "'" +
            ", activePowerA='" + getActivePowerA() + "'" +
            ", reactivePowerA='" + getReactivePowerA() + "'" +
            ", powerFactorA='" + getPowerFactorA() + "'" +
            ", voltageB='" + getVoltageB() + "'" +
            ", currentB='" + getCurrentB() + "'" +
            ", activePowerB='" + getActivePowerB() + "'" +
            ", reactivePowerB='" + getReactivePowerB() + "'" +
            ", powerFactorB='" + getPowerFactorB() + "'" +
            ", voltageC='" + getVoltageC() + "'" +
            ", currentC='" + getCurrentC() + "'" +
            ", activePowerC='" + getActivePowerC() + "'" +
            ", reactivePowerC='" + getReactivePowerC() + "'" +
            ", powerFactorC='" + getPowerFactorC() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedOn='" + getLastModifiedOn() + "'" +
            "}";
    }
}
