package mn.electrical.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;

/**
 * A Meter.
 */
@Entity
@Table(name = "meter")
@EntityListeners(AuditingEntityListener.class)
public class Meter implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "modem_name")
    private String modemName;

    @Column(name = "model_name")
    private String modelName;

    @Column(name = "meter_type_name")
    private String meterTypeName;

    @Column(name = "current_name")
    private String currentName;

    @Column(name = "voltage_name")
    private String voltageName;

    @Column(name = "installation_on")
    private Instant installationOn;

    @Column(name = "warranty_on")
    private Instant warrantyOn;

    @Column(name = "number")
    private String number;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "ct_ratio")
    private String ctRatio;

    @Column(name = "vt_ratio")
    private String vtRatio;

    @Column(name = "device_address")
    private String deviceAddress;

    @Column(name = "status")
    private String status;

    @Column(name = "rate_1")
    private String rate1;

    @Column(name = "rate_2")
    private String rate2;

    @Column(name = "rate_3")
    private String rate3;

    @Column(name = "created_by")
    @CreatedBy
    private String createdBy;

    @Column(name = "created_on")
    @CreatedDate
    private Instant createdOn;

    @Column(name = "last_modified_by")
    @LastModifiedBy
    private String lastModifiedBy;

    @Column(name = "last_modified_on")
    @LastModifiedDate
    private Instant lastModifiedOn;

    @ManyToOne
    @JsonIgnoreProperties(value = { "model", "protocolType" }, allowSetters = true)
    private Modem modem;

    @ManyToOne
    @JsonIgnoreProperties(value = { "parent" }, allowSetters = true)
    private CommonValue meterType;

    @ManyToOne
    @JsonIgnoreProperties(value = { "parent" }, allowSetters = true)
    private CommonValue current;

    @ManyToOne
    @JsonIgnoreProperties(value = { "parent" }, allowSetters = true)
    private CommonValue voltage;

    @ManyToOne
    @JsonIgnoreProperties(value = { "parent" }, allowSetters = true)
    private CommonValue model;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Meter id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModemName() {
        return this.modemName;
    }

    public Meter modemName(String modemName) {
        this.setModemName(modemName);
        return this;
    }

    public void setModemName(String modemName) {
        this.modemName = modemName;
    }

    public String getModelName() {
        return this.modelName;
    }

    public Meter modelName(String modelName) {
        this.setModelName(modelName);
        return this;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getMeterTypeName() {
        return this.meterTypeName;
    }

    public Meter meterTypeName(String meterTypeName) {
        this.setMeterTypeName(meterTypeName);
        return this;
    }

    public void setMeterTypeName(String meterTypeName) {
        this.meterTypeName = meterTypeName;
    }

    public String getCurrentName() {
        return this.currentName;
    }

    public Meter currentName(String currentName) {
        this.setCurrentName(currentName);
        return this;
    }

    public void setCurrentName(String currentName) {
        this.currentName = currentName;
    }

    public String getVoltageName() {
        return this.voltageName;
    }

    public Meter voltageName(String voltageName) {
        this.setVoltageName(voltageName);
        return this;
    }

    public void setVoltageName(String voltageName) {
        this.voltageName = voltageName;
    }

    public Instant getInstallationOn() {
        return this.installationOn;
    }

    public Meter installationOn(Instant installationOn) {
        this.setInstallationOn(installationOn);
        return this;
    }

    public void setInstallationOn(Instant installationOn) {
        this.installationOn = installationOn;
    }

    public Instant getWarrantyOn() {
        return this.warrantyOn;
    }

    public Meter warrantyOn(Instant warrantyOn) {
        this.setWarrantyOn(warrantyOn);
        return this;
    }

    public void setWarrantyOn(Instant warrantyOn) {
        this.warrantyOn = warrantyOn;
    }

    public String getNumber() {
        return this.number;
    }

    public Meter number(String number) {
        this.setNumber(number);
        return this;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return this.name;
    }

    public Meter name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return this.address;
    }

    public Meter address(String address) {
        this.setAddress(address);
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCtRatio() {
        return this.ctRatio;
    }

    public Meter ctRatio(String ctRatio) {
        this.setCtRatio(ctRatio);
        return this;
    }

    public void setCtRatio(String ctRatio) {
        this.ctRatio = ctRatio;
    }

    public String getVtRatio() {
        return this.vtRatio;
    }

    public Meter vtRatio(String vtRatio) {
        this.setVtRatio(vtRatio);
        return this;
    }

    public void setVtRatio(String vtRatio) {
        this.vtRatio = vtRatio;
    }

    public String getDeviceAddress() {
        return this.deviceAddress;
    }

    public Meter deviceAddress(String deviceAddress) {
        this.setDeviceAddress(deviceAddress);
        return this;
    }

    public void setDeviceAddress(String deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    public String getStatus() {
        return this.status;
    }

    public Meter status(String status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRate1() {
        return this.rate1;
    }

    public Meter rate1(String rate1) {
        this.setRate1(rate1);
        return this;
    }

    public void setRate1(String rate1) {
        this.rate1 = rate1;
    }

    public String getRate2() {
        return this.rate2;
    }

    public Meter rate2(String rate2) {
        this.setRate2(rate2);
        return this;
    }

    public void setRate2(String rate2) {
        this.rate2 = rate2;
    }

    public String getRate3() {
        return this.rate3;
    }

    public Meter rate3(String rate3) {
        this.setRate3(rate3);
        return this;
    }

    public void setRate3(String rate3) {
        this.rate3 = rate3;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Meter createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedOn() {
        return this.createdOn;
    }

    public Meter createdOn(Instant createdOn) {
        this.setCreatedOn(createdOn);
        return this;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public Meter lastModifiedBy(String lastModifiedBy) {
        this.setLastModifiedBy(lastModifiedBy);
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedOn() {
        return this.lastModifiedOn;
    }

    public Meter lastModifiedOn(Instant lastModifiedOn) {
        this.setLastModifiedOn(lastModifiedOn);
        return this;
    }

    public void setLastModifiedOn(Instant lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public Modem getModem() {
        return this.modem;
    }

    public void setModem(Modem modem) {
        this.modem = modem;
    }

    public Meter modem(Modem modem) {
        this.setModem(modem);
        return this;
    }

    public CommonValue getMeterType() {
        return this.meterType;
    }

    public void setMeterType(CommonValue commonValue) {
        this.meterType = commonValue;
    }

    public Meter meterType(CommonValue commonValue) {
        this.setMeterType(commonValue);
        return this;
    }

    public CommonValue getCurrent() {
        return this.current;
    }

    public void setCurrent(CommonValue commonValue) {
        this.current = commonValue;
    }

    public Meter current(CommonValue commonValue) {
        this.setCurrent(commonValue);
        return this;
    }

    public CommonValue getVoltage() {
        return this.voltage;
    }

    public void setVoltage(CommonValue commonValue) {
        this.voltage = commonValue;
    }

    public Meter voltage(CommonValue commonValue) {
        this.setVoltage(commonValue);
        return this;
    }

    public CommonValue getModel() {
        return this.model;
    }

    public void setModel(CommonValue commonValue) {
        this.model = commonValue;
    }

    public Meter model(CommonValue commonValue) {
        this.setModel(commonValue);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Meter)) {
            return false;
        }
        return id != null && id.equals(((Meter) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Meter{" +
            "id=" + getId() +
            ", modemName='" + getModemName() + "'" +
            ", modelName='" + getModelName() + "'" +
            ", meterTypeName='" + getMeterTypeName() + "'" +
            ", currentName='" + getCurrentName() + "'" +
            ", voltageName='" + getVoltageName() + "'" +
            ", installationOn='" + getInstallationOn() + "'" +
            ", warrantyOn='" + getWarrantyOn() + "'" +
            ", number='" + getNumber() + "'" +
            ", name='" + getName() + "'" +
            ", address='" + getAddress() + "'" +
            ", ctRatio='" + getCtRatio() + "'" +
            ", vtRatio='" + getVtRatio() + "'" +
            ", deviceAddress='" + getDeviceAddress() + "'" +
            ", status='" + getStatus() + "'" +
            ", rate1='" + getRate1() + "'" +
            ", rate2='" + getRate2() + "'" +
            ", rate3='" + getRate3() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedOn='" + getLastModifiedOn() + "'" +
            "}";
    }
}
