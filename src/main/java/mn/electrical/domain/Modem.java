package mn.electrical.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;

/**
 * A Modem.
 */
@Entity
@Table(name = "modem")
@EntityListeners(AuditingEntityListener.class)
public class Modem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "model_name")
    private String modelName;

    @Column(name = "protocol_type_name")
    private String protocolTypeName;

    @Column(name = "ip_address")
    private String ipAddress;

    @Column(name = "port")
    private String port;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "location")
    private String location;

    @Column(name = "sim_info")
    private String simInfo;

    @Column(name = "cron_tab")
    private String cronTab;

    @Column(name = "created_by")
    @CreatedBy
    private String createdBy;

    @Column(name = "created_on")
    @CreatedDate
    private Instant createdOn;

    @Column(name = "last_modified_by")
    @LastModifiedBy
    private String lastModifiedBy;

    @Column(name = "last_modified_on")
    @LastModifiedDate
    private Instant lastModifiedOn;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @Column(name = "flag")
    private Boolean flag;

    @ManyToOne
    @JsonIgnoreProperties(value = { "parent" }, allowSetters = true)
    private CommonValue model;

    @ManyToOne
    @JsonIgnoreProperties(value = { "parent" }, allowSetters = true)
    private CommonValue protocolType;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Modem id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModelName() {
        return this.modelName;
    }

    public Modem modelName(String modelName) {
        this.setModelName(modelName);
        return this;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getProtocolTypeName() {
        return this.protocolTypeName;
    }

    public Modem protocolTypeName(String protocolTypeName) {
        this.setProtocolTypeName(protocolTypeName);
        return this;
    }

    public void setProtocolTypeName(String protocolTypeName) {
        this.protocolTypeName = protocolTypeName;
    }

    public String getIpAddress() {
        return this.ipAddress;
    }

    public Modem ipAddress(String ipAddress) {
        this.setIpAddress(ipAddress);
        return this;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getPort() {
        return this.port;
    }

    public Modem port(String port) {
        this.setPort(port);
        return this;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getName() {
        return this.name;
    }

    public Modem name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return this.address;
    }

    public Modem address(String address) {
        this.setAddress(address);
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocation() {
        return this.location;
    }

    public Modem location(String location) {
        this.setLocation(location);
        return this;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSimInfo() {
        return this.simInfo;
    }

    public Modem simInfo(String simInfo) {
        this.setSimInfo(simInfo);
        return this;
    }

    public void setSimInfo(String simInfo) {
        this.simInfo = simInfo;
    }

    public String getCronTab() {
        return this.cronTab;
    }

    public Modem cronTab(String cronTab) {
        this.setCronTab(cronTab);
        return this;
    }

    public void setCronTab(String cronTab) {
        this.cronTab = cronTab;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Modem createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedOn() {
        return this.createdOn;
    }

    public Modem createdOn(Instant createdOn) {
        this.setCreatedOn(createdOn);
        return this;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public Modem lastModifiedBy(String lastModifiedBy) {
        this.setLastModifiedBy(lastModifiedBy);
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedOn() {
        return this.lastModifiedOn;
    }

    public Modem lastModifiedOn(Instant lastModifiedOn) {
        this.setLastModifiedOn(lastModifiedOn);
        return this;
    }

    public void setLastModifiedOn(Instant lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public Boolean getIsDeleted() {
        return this.isDeleted;
    }

    public Modem isDeleted(Boolean isDeleted) {
        this.setIsDeleted(isDeleted);
        return this;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Boolean getFlag() {
        return this.flag;
    }

    public Modem flag(Boolean flag) {
        this.setFlag(flag);
        return this;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    public CommonValue getModel() {
        return this.model;
    }

    public void setModel(CommonValue commonValue) {
        this.model = commonValue;
    }

    public Modem model(CommonValue commonValue) {
        this.setModel(commonValue);
        return this;
    }

    public CommonValue getProtocolType() {
        return this.protocolType;
    }

    public void setProtocolType(CommonValue commonValue) {
        this.protocolType = commonValue;
    }

    public Modem protocolType(CommonValue commonValue) {
        this.setProtocolType(commonValue);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Modem)) {
            return false;
        }
        return id != null && id.equals(((Modem) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Modem{" +
            "id=" + getId() +
            ", modelName='" + getModelName() + "'" +
            ", protocolTypeName='" + getProtocolTypeName() + "'" +
            ", ipAddress='" + getIpAddress() + "'" +
            ", port='" + getPort() + "'" +
            ", name='" + getName() + "'" +
            ", address='" + getAddress() + "'" +
            ", location='" + getLocation() + "'" +
            ", simInfo='" + getSimInfo() + "'" +
            ", cronTab='" + getCronTab() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedOn='" + getLastModifiedOn() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", flag='" + getFlag() + "'" +
            "}";
    }
}
