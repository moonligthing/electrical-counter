package mn.electrical.domain;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A CommonValue.
 */
@Entity
@Table(name = "common_value")
@EntityListeners(AuditingEntityListener.class)
public class CommonValue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "code", nullable = false, unique = true)
    private String code;

    @Column(name = "description")
    private String description;

    @Column(name = "jhi_order")
    private Long order;

    @NotNull
    @Column(name = "data_type", nullable = false)
    private String dataType;

    @NotNull
    @Column(name = "data_string", nullable = false)
    private String dataString;

    @Column(name = "status")
    private String status;

    @Column(name = "data_short")
    private String dataShort;

    @Column(name = "created_by")
    @CreatedBy
    private String createdBy;

    @Column(name = "created_on")
    @CreatedDate
    private Instant createdOn;

    @Column(name = "last_modified_by")
    @LastModifiedBy
    private String lastModifiedBy;

    @Column(name = "last_modified_on")
    @LastModifiedDate
    private Instant lastModifiedOn;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @ManyToOne
    private CommonCode parent;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public CommonValue id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public CommonValue name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return this.code;
    }

    public CommonValue code(String code) {
        this.setCode(code);
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return this.description;
    }

    public CommonValue description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getOrder() {
        return this.order;
    }

    public CommonValue order(Long order) {
        this.setOrder(order);
        return this;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

    public String getDataType() {
        return this.dataType;
    }

    public CommonValue dataType(String dataType) {
        this.setDataType(dataType);
        return this;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDataString() {
        return this.dataString;
    }

    public CommonValue dataString(String dataString) {
        this.setDataString(dataString);
        return this;
    }

    public void setDataString(String dataString) {
        this.dataString = dataString;
    }

    public String getStatus() {
        return this.status;
    }

    public CommonValue status(String status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDataShort() {
        return this.dataShort;
    }

    public CommonValue dataShort(String dataShort) {
        this.setDataShort(dataShort);
        return this;
    }

    public void setDataShort(String dataShort) {
        this.dataShort = dataShort;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public CommonValue createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedOn() {
        return this.createdOn;
    }

    public CommonValue createdOn(Instant createdOn) {
        this.setCreatedOn(createdOn);
        return this;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public CommonValue lastModifiedBy(String lastModifiedBy) {
        this.setLastModifiedBy(lastModifiedBy);
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedOn() {
        return this.lastModifiedOn;
    }

    public CommonValue lastModifiedOn(Instant lastModifiedOn) {
        this.setLastModifiedOn(lastModifiedOn);
        return this;
    }

    public void setLastModifiedOn(Instant lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public Boolean getIsDeleted() {
        return this.isDeleted;
    }

    public CommonValue isDeleted(Boolean isDeleted) {
        this.setIsDeleted(isDeleted);
        return this;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public CommonCode getParent() {
        return this.parent;
    }

    public void setParent(CommonCode commonCode) {
        this.parent = commonCode;
    }

    public CommonValue parent(CommonCode commonCode) {
        this.setParent(commonCode);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommonValue)) {
            return false;
        }
        return id != null && id.equals(((CommonValue) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommonValue{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            ", description='" + getDescription() + "'" +
            ", order=" + getOrder() +
            ", dataType='" + getDataType() + "'" +
            ", dataString='" + getDataString() + "'" +
            ", status='" + getStatus() + "'" +
            ", dataShort='" + getDataShort() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedOn='" + getLastModifiedOn() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            "}";
    }
}
