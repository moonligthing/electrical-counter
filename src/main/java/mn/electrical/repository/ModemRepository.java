package mn.electrical.repository;

import java.net.InetAddress;
import java.util.List;
import mn.electrical.domain.Modem;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Modem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ModemRepository extends JpaRepository<Modem, Long>, JpaSpecificationExecutor<Modem> {
    List<Modem> findAllByProtocolTypeName(String protocolTypeName);

    List<Modem> findAllByIpAddress(String ipAddress);
}
