package mn.electrical.repository;

import mn.electrical.domain.CommonCode;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the CommonCode entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CommonCodeRepository extends JpaRepository<CommonCode, Long>, JpaSpecificationExecutor<CommonCode> {
    CommonCode findByCode(String code);
}
