package mn.electrical.repository;

import java.util.List;
import mn.electrical.domain.Meter;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Meter entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MeterRepository extends JpaRepository<Meter, Long>, JpaSpecificationExecutor<Meter> {
    List<Meter> findAllByModemIdAndModelIsNotNullOrderByNumberAsc(Long modemId);

    List<Meter> findAllByModemId(Long modemId);
}
