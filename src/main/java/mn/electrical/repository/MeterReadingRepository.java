package mn.electrical.repository;

import mn.electrical.domain.MeterReading;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the MeterReading entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MeterReadingRepository extends JpaRepository<MeterReading, Long>, JpaSpecificationExecutor<MeterReading> {}
