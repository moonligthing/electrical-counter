package mn.electrical.repository;

import mn.electrical.domain.CommonValue;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data SQL repository for the CommonValue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CommonValueRepository extends JpaRepository<CommonValue, Long>, JpaSpecificationExecutor<CommonValue> {
    List<CommonValue> findAllByParentIdOrderByOrderAsc(Long id);
}
