package mn.electrical.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import mn.electrical.repository.CommonCodeRepository;
import mn.electrical.service.CommonCodeQueryService;
import mn.electrical.service.CommonCodeService;
import mn.electrical.service.criteria.CommonCodeCriteria;
import mn.electrical.service.dto.CommonCodeDTO;
import mn.electrical.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link mn.electrical.domain.CommonCode}.
 */
@RestController
@RequestMapping("/api")
public class CommonCodeResource {

    private final Logger log = LoggerFactory.getLogger(CommonCodeResource.class);

    private static final String ENTITY_NAME = "commonCode";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CommonCodeService commonCodeService;

    private final CommonCodeRepository commonCodeRepository;

    private final CommonCodeQueryService commonCodeQueryService;

    public CommonCodeResource(
        CommonCodeService commonCodeService,
        CommonCodeRepository commonCodeRepository,
        CommonCodeQueryService commonCodeQueryService
    ) {
        this.commonCodeService = commonCodeService;
        this.commonCodeRepository = commonCodeRepository;
        this.commonCodeQueryService = commonCodeQueryService;
    }

    /**
     * {@code POST  /common-codes} : Create a new commonCode.
     *
     * @param commonCodeDTO the commonCodeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new commonCodeDTO, or with status {@code 400 (Bad Request)} if the commonCode has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/common-codes")
    public ResponseEntity<CommonCodeDTO> createCommonCode(@Valid @RequestBody CommonCodeDTO commonCodeDTO) throws URISyntaxException {
        log.debug("REST request to save CommonCode : {}", commonCodeDTO);
        if (commonCodeDTO.getId() != null) {
            throw new BadRequestAlertException("A new commonCode cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CommonCodeDTO result = commonCodeService.save(commonCodeDTO);
        return ResponseEntity
            .created(new URI("/api/common-codes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /common-codes/:id} : Updates an existing commonCode.
     *
     * @param id the id of the commonCodeDTO to save.
     * @param commonCodeDTO the commonCodeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated commonCodeDTO,
     * or with status {@code 400 (Bad Request)} if the commonCodeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the commonCodeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/common-codes/{id}")
    public ResponseEntity<CommonCodeDTO> updateCommonCode(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody CommonCodeDTO commonCodeDTO
    ) throws URISyntaxException {
        log.debug("REST request to update CommonCode : {}, {}", id, commonCodeDTO);
        if (commonCodeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, commonCodeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!commonCodeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CommonCodeDTO result = commonCodeService.save(commonCodeDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, commonCodeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /common-codes/:id} : Partial updates given fields of an existing commonCode, field will ignore if it is null
     *
     * @param id the id of the commonCodeDTO to save.
     * @param commonCodeDTO the commonCodeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated commonCodeDTO,
     * or with status {@code 400 (Bad Request)} if the commonCodeDTO is not valid,
     * or with status {@code 404 (Not Found)} if the commonCodeDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the commonCodeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/common-codes/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CommonCodeDTO> partialUpdateCommonCode(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody CommonCodeDTO commonCodeDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update CommonCode partially : {}, {}", id, commonCodeDTO);
        if (commonCodeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, commonCodeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!commonCodeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CommonCodeDTO> result = commonCodeService.partialUpdate(commonCodeDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, commonCodeDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /common-codes} : get all the commonCodes.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of commonCodes in body.
     */
    @GetMapping("/common-codes")
    public ResponseEntity<List<CommonCodeDTO>> getAllCommonCodes(CommonCodeCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CommonCodes by criteria: {}", criteria);
        Page<CommonCodeDTO> page = commonCodeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /common-codes/count} : count all the commonCodes.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/common-codes/count")
    public ResponseEntity<Long> countCommonCodes(CommonCodeCriteria criteria) {
        log.debug("REST request to count CommonCodes by criteria: {}", criteria);
        return ResponseEntity.ok().body(commonCodeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /common-codes/:id} : get the "id" commonCode.
     *
     * @param id the id of the commonCodeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the commonCodeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/common-codes/{id}")
    public ResponseEntity<CommonCodeDTO> getCommonCode(@PathVariable Long id) {
        log.debug("REST request to get CommonCode : {}", id);
        Optional<CommonCodeDTO> commonCodeDTO = commonCodeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(commonCodeDTO);
    }

    /**
     * {@code DELETE  /common-codes/:id} : delete the "id" commonCode.
     *
     * @param id the id of the commonCodeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/common-codes/{id}")
    public ResponseEntity<Void> deleteCommonCode(@PathVariable Long id) {
        log.debug("REST request to delete CommonCode : {}", id);
        commonCodeService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
