/**
 * View Models used by Spring MVC REST controllers.
 */
package mn.electrical.web.rest.vm;
