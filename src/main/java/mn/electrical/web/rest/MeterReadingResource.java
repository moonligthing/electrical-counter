package mn.electrical.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import mn.electrical.repository.MeterReadingRepository;
import mn.electrical.service.MeterReadingQueryService;
import mn.electrical.service.MeterReadingService;
import mn.electrical.service.criteria.MeterReadingCriteria;
import mn.electrical.service.dto.MeterReadingDTO;
import mn.electrical.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link mn.electrical.domain.MeterReading}.
 */
@RestController
@RequestMapping("/api")
public class MeterReadingResource {

    private final Logger log = LoggerFactory.getLogger(MeterReadingResource.class);

    private static final String ENTITY_NAME = "meterReading";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MeterReadingService meterReadingService;

    private final MeterReadingRepository meterReadingRepository;

    private final MeterReadingQueryService meterReadingQueryService;

    public MeterReadingResource(
        MeterReadingService meterReadingService,
        MeterReadingRepository meterReadingRepository,
        MeterReadingQueryService meterReadingQueryService
    ) {
        this.meterReadingService = meterReadingService;
        this.meterReadingRepository = meterReadingRepository;
        this.meterReadingQueryService = meterReadingQueryService;
    }

    /**
     * {@code POST  /meter-readings} : Create a new meterReading.
     *
     * @param meterReadingDTO the meterReadingDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new meterReadingDTO, or with status {@code 400 (Bad Request)} if the meterReading has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/meter-readings")
    public ResponseEntity<MeterReadingDTO> createMeterReading(@RequestBody MeterReadingDTO meterReadingDTO) throws URISyntaxException {
        log.debug("REST request to save MeterReading : {}", meterReadingDTO);
        if (meterReadingDTO.getId() != null) {
            throw new BadRequestAlertException("A new meterReading cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MeterReadingDTO result = meterReadingService.save(meterReadingDTO);
        return ResponseEntity
            .created(new URI("/api/meter-readings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /meter-readings/:id} : Updates an existing meterReading.
     *
     * @param id the id of the meterReadingDTO to save.
     * @param meterReadingDTO the meterReadingDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated meterReadingDTO,
     * or with status {@code 400 (Bad Request)} if the meterReadingDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the meterReadingDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/meter-readings/{id}")
    public ResponseEntity<MeterReadingDTO> updateMeterReading(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody MeterReadingDTO meterReadingDTO
    ) throws URISyntaxException {
        log.debug("REST request to update MeterReading : {}, {}", id, meterReadingDTO);
        if (meterReadingDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, meterReadingDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!meterReadingRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        MeterReadingDTO result = meterReadingService.save(meterReadingDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, meterReadingDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /meter-readings/:id} : Partial updates given fields of an existing meterReading, field will ignore if it is null
     *
     * @param id the id of the meterReadingDTO to save.
     * @param meterReadingDTO the meterReadingDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated meterReadingDTO,
     * or with status {@code 400 (Bad Request)} if the meterReadingDTO is not valid,
     * or with status {@code 404 (Not Found)} if the meterReadingDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the meterReadingDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/meter-readings/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<MeterReadingDTO> partialUpdateMeterReading(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody MeterReadingDTO meterReadingDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update MeterReading partially : {}, {}", id, meterReadingDTO);
        if (meterReadingDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, meterReadingDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!meterReadingRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<MeterReadingDTO> result = meterReadingService.partialUpdate(meterReadingDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, meterReadingDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /meter-readings} : get all the meterReadings.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of meterReadings in body.
     */
    @GetMapping("/meter-readings")
    public ResponseEntity<List<MeterReadingDTO>> getAllMeterReadings(MeterReadingCriteria criteria, Pageable pageable) {
        log.debug("REST request to get MeterReadings by criteria: {}", criteria);
        Page<MeterReadingDTO> page = meterReadingQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /meter-readings/count} : count all the meterReadings.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/meter-readings/count")
    public ResponseEntity<Long> countMeterReadings(MeterReadingCriteria criteria) {
        log.debug("REST request to count MeterReadings by criteria: {}", criteria);
        return ResponseEntity.ok().body(meterReadingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /meter-readings/:id} : get the "id" meterReading.
     *
     * @param id the id of the meterReadingDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the meterReadingDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/meter-readings/{id}")
    public ResponseEntity<MeterReadingDTO> getMeterReading(@PathVariable Long id) {
        log.debug("REST request to get MeterReading : {}", id);
        Optional<MeterReadingDTO> meterReadingDTO = meterReadingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(meterReadingDTO);
    }

    /**
     * {@code DELETE  /meter-readings/:id} : delete the "id" meterReading.
     *
     * @param id the id of the meterReadingDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/meter-readings/{id}")
    public ResponseEntity<Void> deleteMeterReading(@PathVariable Long id) {
        log.debug("REST request to delete MeterReading : {}", id);
        meterReadingService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
