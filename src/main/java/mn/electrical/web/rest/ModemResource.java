package mn.electrical.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import mn.electrical.repository.ModemRepository;
import mn.electrical.service.ModemQueryService;
import mn.electrical.service.ModemService;
import mn.electrical.service.criteria.ModemCriteria;
import mn.electrical.service.dto.ModemDTO;
import mn.electrical.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link mn.electrical.domain.Modem}.
 */
@RestController
@RequestMapping("/api")
public class ModemResource {

    private final Logger log = LoggerFactory.getLogger(ModemResource.class);

    private static final String ENTITY_NAME = "modem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ModemService modemService;

    private final ModemRepository modemRepository;

    private final ModemQueryService modemQueryService;

    public ModemResource(ModemService modemService, ModemRepository modemRepository, ModemQueryService modemQueryService) {
        this.modemService = modemService;
        this.modemRepository = modemRepository;
        this.modemQueryService = modemQueryService;
    }

    /**
     * {@code POST  /modems} : Create a new modem.
     *
     * @param modemDTO the modemDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new modemDTO, or with status {@code 400 (Bad Request)} if the modem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/modems")
    public ResponseEntity<ModemDTO> createModem(@RequestBody ModemDTO modemDTO) throws URISyntaxException {
        log.debug("REST request to save Modem : {}", modemDTO);
        if (modemDTO.getId() != null) {
            throw new BadRequestAlertException("A new modem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ModemDTO result = modemService.save(modemDTO);
        return ResponseEntity
            .created(new URI("/api/modems/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /modems/:id} : Updates an existing modem.
     *
     * @param id the id of the modemDTO to save.
     * @param modemDTO the modemDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated modemDTO,
     * or with status {@code 400 (Bad Request)} if the modemDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the modemDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/modems/{id}")
    public ResponseEntity<ModemDTO> updateModem(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ModemDTO modemDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Modem : {}, {}", id, modemDTO);
        if (modemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, modemDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!modemRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ModemDTO result = modemService.save(modemDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, modemDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /modems/:id} : Partial updates given fields of an existing modem, field will ignore if it is null
     *
     * @param id the id of the modemDTO to save.
     * @param modemDTO the modemDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated modemDTO,
     * or with status {@code 400 (Bad Request)} if the modemDTO is not valid,
     * or with status {@code 404 (Not Found)} if the modemDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the modemDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/modems/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ModemDTO> partialUpdateModem(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ModemDTO modemDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Modem partially : {}, {}", id, modemDTO);
        if (modemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, modemDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!modemRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ModemDTO> result = modemService.partialUpdate(modemDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, modemDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /modems} : get all the modems.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of modems in body.
     */
    @GetMapping("/modems")
    public ResponseEntity<List<ModemDTO>> getAllModems(ModemCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Modems by criteria: {}", criteria);
        Page<ModemDTO> page = modemQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /modems/count} : count all the modems.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/modems/count")
    public ResponseEntity<Long> countModems(ModemCriteria criteria) {
        log.debug("REST request to count Modems by criteria: {}", criteria);
        return ResponseEntity.ok().body(modemQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /modems/:id} : get the "id" modem.
     *
     * @param id the id of the modemDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the modemDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/modems/{id}")
    public ResponseEntity<ModemDTO> getModem(@PathVariable Long id) {
        log.debug("REST request to get Modem : {}", id);
        Optional<ModemDTO> modemDTO = modemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(modemDTO);
    }

    /**
     * {@code DELETE  /modems/:id} : delete the "id" modem.
     *
     * @param id the id of the modemDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/modems/{id}")
    public ResponseEntity<Void> deleteModem(@PathVariable Long id) {
        log.debug("REST request to delete Modem : {}", id);
        modemService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
