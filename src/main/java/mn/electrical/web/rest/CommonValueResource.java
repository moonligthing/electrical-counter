package mn.electrical.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import mn.electrical.repository.CommonValueRepository;
import mn.electrical.service.CommonValueQueryService;
import mn.electrical.service.CommonValueService;
import mn.electrical.service.criteria.CommonValueCriteria;
import mn.electrical.service.dto.CommonValueDTO;
import mn.electrical.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link mn.electrical.domain.CommonValue}.
 */
@RestController
@RequestMapping("/api")
public class CommonValueResource {

    private final Logger log = LoggerFactory.getLogger(CommonValueResource.class);

    private static final String ENTITY_NAME = "commonValue";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CommonValueService commonValueService;

    private final CommonValueRepository commonValueRepository;

    private final CommonValueQueryService commonValueQueryService;

    public CommonValueResource(
        CommonValueService commonValueService,
        CommonValueRepository commonValueRepository,
        CommonValueQueryService commonValueQueryService
    ) {
        this.commonValueService = commonValueService;
        this.commonValueRepository = commonValueRepository;
        this.commonValueQueryService = commonValueQueryService;
    }

    /**
     * {@code POST  /common-values} : Create a new commonValue.
     *
     * @param commonValueDTO the commonValueDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new commonValueDTO, or with status {@code 400 (Bad Request)} if the commonValue has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/common-values")
    public ResponseEntity<CommonValueDTO> createCommonValue(@Valid @RequestBody CommonValueDTO commonValueDTO) throws URISyntaxException {
        log.debug("REST request to save CommonValue : {}", commonValueDTO);
        if (commonValueDTO.getId() != null) {
            throw new BadRequestAlertException("A new commonValue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CommonValueDTO result = commonValueService.save(commonValueDTO);
        return ResponseEntity
            .created(new URI("/api/common-values/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /common-values/:id} : Updates an existing commonValue.
     *
     * @param id the id of the commonValueDTO to save.
     * @param commonValueDTO the commonValueDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated commonValueDTO,
     * or with status {@code 400 (Bad Request)} if the commonValueDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the commonValueDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/common-values/{id}")
    public ResponseEntity<CommonValueDTO> updateCommonValue(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody CommonValueDTO commonValueDTO
    ) throws URISyntaxException {
        log.debug("REST request to update CommonValue : {}, {}", id, commonValueDTO);
        if (commonValueDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, commonValueDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!commonValueRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CommonValueDTO result = commonValueService.save(commonValueDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, commonValueDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /common-values/:id} : Partial updates given fields of an existing commonValue, field will ignore if it is null
     *
     * @param id the id of the commonValueDTO to save.
     * @param commonValueDTO the commonValueDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated commonValueDTO,
     * or with status {@code 400 (Bad Request)} if the commonValueDTO is not valid,
     * or with status {@code 404 (Not Found)} if the commonValueDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the commonValueDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/common-values/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CommonValueDTO> partialUpdateCommonValue(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody CommonValueDTO commonValueDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update CommonValue partially : {}, {}", id, commonValueDTO);
        if (commonValueDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, commonValueDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!commonValueRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CommonValueDTO> result = commonValueService.partialUpdate(commonValueDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, commonValueDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /common-values} : get all the commonValues.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of commonValues in body.
     */
    @GetMapping("/common-values")
    public ResponseEntity<List<CommonValueDTO>> getAllCommonValues(CommonValueCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CommonValues by criteria: {}", criteria);
        Page<CommonValueDTO> page = commonValueQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /common-values/count} : count all the commonValues.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/common-values/count")
    public ResponseEntity<Long> countCommonValues(CommonValueCriteria criteria) {
        log.debug("REST request to count CommonValues by criteria: {}", criteria);
        return ResponseEntity.ok().body(commonValueQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /common-values/:id} : get the "id" commonValue.
     *
     * @param id the id of the commonValueDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the commonValueDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/common-values/{id}")
    public ResponseEntity<CommonValueDTO> getCommonValue(@PathVariable Long id) {
        log.debug("REST request to get CommonValue : {}", id);
        Optional<CommonValueDTO> commonValueDTO = commonValueService.findOne(id);
        return ResponseUtil.wrapOrNotFound(commonValueDTO);
    }

    /**
     * {@code DELETE  /common-values/:id} : delete the "id" commonValue.
     *
     * @param id the id of the commonValueDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/common-values/{id}")
    public ResponseEntity<Void> deleteCommonValue(@PathVariable Long id) {
        log.debug("REST request to delete CommonValue : {}", id);
        commonValueService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
